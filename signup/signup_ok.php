<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?	include("../includes/config.php");

	include("../classes/user.php");
	include("../classes/green_client.php");
	
	include("../classes/client.php");
	include("../classes/master.php");
	
	$user = new user();
	$user->set_id($_COOKIE['green_user_id']);

	$green_client = new green_client();
		?>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<title>Green - Signup Form</title>
		
		<link rel="stylesheet" type="text/css" href="style.css">		
		<link type="text/css" rel="stylesheet" media="all" href="cometchat/css/cometchat.css" /> 
		<link rel="shortcut icon" href="../images/favicon.gif" />


		<script language="javascript" src="jquery-1.3.2.js"></script>
		<script language="javascript" src="jquery-ui-1.7.2.custom.min.js"></script>
		<script language="javascript" src="js.js"></script>
		
		
	</head>

    <body>

	<center>
	<div id="g-div-main">
		<div id="g-div-canvas">		
			<div id="g-div-canvas-content">
			
				<h1>Welcome to Green!</h1>
				<p>You have succesfulyl created your Green account!</p>
				<p>To start using Green go to: <a target="_blank" href="http://green.avanfeel.net/<?=$green_username	?>">http://green.avanfeel.net/<?=$green_username	?></a></p>
			</div>
		</div>
	</div>
	</center>
	
</body>

</html>