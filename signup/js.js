$(document).ready(

	function(){
	
		$("inpuy").keydown(
		
			function(){
				$(this).removeClass("g-highlighted");
			}
		
		);
	
		$("#g-button").click(
			
			function(){
			
				$(this).attr("disabled","disabled");
				
				if(checkInputs()){
					submitForm();
				}else{
					$(this).attr("disabled","");
				}
			}
			
		);
		
		
		$("input[name='address']").keyup(
		
			function(){
				$("#g-span-address").html($(this).val());
			}
		
		);
	
		
	}

)

function checkInputs(){

	return true;

	if($("input[name='name']").val()==''){
		alert("The name filed has no value!");
		highlight("name");
		return(false);
	}else if($("input[name='username']").val()==''){
		alert("The username filed has no value!");
		highlight("username");
		return(false);
	}else if($("input[name='email']").val()==''){
		alert("The email filed has no value!");
		highlight("email");
		return(false);
	}
	
	return(true);
	
}


function submitForm(){
	var s = $("#g-form-signup").serialize();
	var username=$("input[name='username']").val();
	$.ajax({
	
		url: "create.php",
		type: 'POST',
		dataType: "json",
    	data:s,
		success: function(r){
			console.log('RESPONSE WAS:');
			console.log(r);		
			if(r.success){
			
				window.open("../"+username, "_self");
			
				//$("#g-form-signup-ok").submit();
				
				/*$("#g-div-canvas-content").fadeOut(
					
					function(){
					
						$.ajax({
							url: "create_ok.php",
							type: 'POST',
							data:"username="+username,
							success: function(r){
								
								$("#g-div-canvas-content").html(r);
								$("#g-div-canvas-content").fadeIn();
								
							}
							
						});
					
					}
					
				);*/
				
			}else{
				$('#g-button').attr("disabled",false);
				alert(r.error.message);
			}
		},
		error: function(r){
			alert('AN ERROR HAS OCCURED!');
			console.log(r);
			$('#g-button').attr("disabled",false);
		}
	
	});
	
}

function highlight(n){
	$("input[name='"+n+"']").addClass("g-highlighted");
	$("input[name='"+n+"']").focus();
}
