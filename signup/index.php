<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?	

	// include("../includes/config.php");

	include("../sys/green/user.php");
	include("../sys/green/green_client.php");
	
	include("../sys/green/client.php");
	include("../sys/green/master.php");
	
	$user = new user();
	$user->set_id($_COOKIE['green_user_id']);

	$green_client = new green_client();?>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<title>Wizard - Signup Form</title>
		
		<link rel="stylesheet" type="text/css" href="style.css">		
		<link type="text/css" rel="stylesheet" media="all" href="cometchat/css/cometchat.css" /> 
		<link rel="shortcut icon" href="../images/favicon.gif" />


		<script language="javascript" src="jquery-1.3.2.js"></script>
		<script language="javascript" src="jquery-ui-1.7.2.custom.min.js"></script>
		<script language="javascript" src="js.js"></script>
		
		
	</head>

    <body>
	
	<center>
	
	<div id="g-div-main">
	
		<div id="g-div-logo"></div>		
		
		<div id="g-div-canvas">
		
			<div id="g-div-canvas-content">
			
				<h1>Signup Form</h1>

				<form id="g-form-signup" action="create.php" method="post">
				
				<p>Complete the fields bellow in order to create your account.</p>
				
				<table id="g-table-form">
					<tr>
						<th>Nombre</th>
						<td><input type="text" name="first_name"></td>
					</tr>

					<tr>
						<th>Apellido</th>
						<td><input type="text" name="last_name"></td>
					</tr>

					<tr>
						<th>Email</th>
						<td><input type="text" name="email"></td>
					</tr>
					
					
					<tr>
						<th>Password</th>
						<td><input type="password" name="password"></td>
					</tr>

					<tr>
						<th>Empresa</th>
						<td><input type="text" name="company_name"></td>
					</tr>
					
					
					
					<tr>
						<th>Business Type</th>
						<td>
						
							<select name="business_type">
								<option value="0">...</option>
								<option value="bakery">Bakery</option>
								<option value="1">Food & Drink</option>
								<option value="4">Accounting Firm</option>
								<option value="3">Law Firm</option>
								<option value="6">Health</option>
								<option value="5">Notary Firm</option>
							</select>
						
						</td>
					</tr>
					
					
					<tr>
                    <tr></tr>
                    <tr></tr>
						<th>Membership Plan</th>
						<td>
						
							<div id="g-div-memberships">
							
								<div class="g-membership g-membership-selected">
									<div style="visibility:hidden;" class="g-membership-name">TRI</div>
									<div class="g-pricing-price">FREE!</div>
									<div style="visibility:hidden;" class="g-membership-monthly">monthly</div>
									<div class="g-membership-input"><input checked type="radio" name="membership" value="free_trial" /></div>
								</div>
								
								<div class="g-membership">
									<div class="g-membership-name">POP</div>
									<div class="g-pricing-price">$10</div>
									<div class="g-membership-monthly">monthly</div>
									<div class="g-membership-input"><input disabled type="radio" name="membership" value="free_trial" /></div>
								</div>
								
								<div class="g-membership">
									<div class="g-membership-name">ECO</div>
									<div class="g-pricing-price">$25</div>
									<div class="g-membership-monthly">monthly</div>
									<div class="g-membership-input"><input disabled type="radio" name="membership" value="free_trial" /></div>
								</div>
								
								<div class="g-membership">
									<div class="g-membership-name">MAX</div>
									<div class="g-pricing-price">$50</div>
									<div class="g-membership-monthly">monthly</div>
									<div class="g-membership-input"><input disabled type="radio" name="membership" value="free_trial" /></div>
								</div>
								
								<div class="g-membership">
									<div class="g-membership-name">ZEN</div>
									<div class="g-pricing-price">$100</div>
									<div class="g-membership-monthly">monthly</div>
									<div class="g-membership-input"><input disabled type="radio" name="membership" value="free_trial" /></div>
								</div>
								
							</div>
						
						</td>
					</tr>
					
					
					<tr>
                    <tr></tr>
                    <tr></tr>
						<th>Terms & Conditions</th>
						<td>
							<div id="g-div-terms"><?	include("terms.php");	?></div>
							<div id="g-div-terms-message">By clicking on "Create my account" below, you are agreeing to the Terms of Service above and the Privacy Policy.</div>
						</td>
					</tr>
					
					
				</table>
				
				</form>
				
				<div id="g-div-button">
					<button id="g-button">Create account!</button>
				</div>
				
			</div>
		
		</div>
		
	</div>
	</center>
	
	<form id="g-form-signup-ok" action="signup_ok.php"></form>

	
	</body>

</html>