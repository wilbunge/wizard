<?	include_once('../../wizard');

	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	//data_manager::$debug_mode=true;

	$template = new template('templates/list');

	$query = $_POST['query'];
	$wc = "(`name` LIKE '%$query%' OR `code` LIKE '%$query%')";

	$products = wizard::get('product', $wc.' AND NOT `deleted`', 'ORDER BY id DESC');

	$template->replace('QUERY_VALUE', $query);

	if($products){
		
		foreach($products as $module){

			$portion = $template->portion('PRODUCT');

			$portion->replace('NAME', $module->get('name_code'));
			$portion->replace('ID', $module->id);

			$template->append($portion);

		}

	}

	$template->clear('PRODUCT');

	$template->render();

	//wizard::html($template->html());	?>