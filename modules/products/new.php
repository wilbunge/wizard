<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');

	if($_POST!=null){

		wizard::_include('product');
		
		$product = new product;

		$product->set_data($_POST);

		$validation = $product->validate_data();

		if($validation->success){

			$product->save();

			$template = new template('templates/new_success');
			$template->render();

			die;

		}else{

			$template->prepend('ERROR!');


		}

		die;
	}

	$template = new template('templates/new');

	$categories = master::get('category');

	$template->replace('CATEGORY_MENU', html::select('category', $categories));

	$template->render();	?>