<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('product');

	$template = new template('templates/edit');
	$template2 = new template('templates/product_form');

	if($_POST!=null){

		$product = new product($_POST['id']);

		$product->set_data($_POST);
		$template2->replace($product);

		$validation = $product->validate();

		if($validation->success){

			$product->save();

			echo (html::message(wizard::msg('SAVE_CHANGES_OK')));
			
			die;

		}else{



			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($product);

		}
		
	}else{
		$product = new product($_GET['id']);
		$template2->set_form_values($product);
	}

	$template->replace('ID', $product->id);
	$template->replace('NAME', $product);
	$template->replace('PRODUCT_FORM', $template2->html());

	$categories = master::get('category');

	$template->replace('CATEGORY_MENU', html::select('name="category"', $categories, $product->get('category')->id));

	$template->render();	?>