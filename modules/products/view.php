<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('product');
	wizard::_include('category');

	$template = new template('templates/view');

	$product = new product($_GET['id']);

	//data_manager::$debug_mode=true;

	$template->replace('ID', $product->get('id'));
	$template->replace('NAME', $product->get('name'));
	$template->replace('DESCRIPTION', $product->get('description'));
	$template->replace('CODE', $product->get('code'));
	$template->replace('PRICE', $product->get('price'));
	$template->replace('DIMENSIONS', $product->get('dimensions'));
	$template->replace('CATEGORY', $product->get('category')->get('name'));

	$template->render();

	//wizard::html($template->html());	?>