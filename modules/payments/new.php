<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('invoice');
	wizard::_include('payment');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('html');

	$payment = new payment;

	// wizard::generate_db('payment');

	if($_POST!=null){

		$payment->set_data($_POST);
		$payment->save();

		$t_message = new template('templates/message');
		$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
		$t_message->render();

		die;
	}

	$clients = wizard::get('client');

	$template = new template('templates/new');
	$form = new template('templates/payment_form');

	$form->replace('SELECT_CLIENT', html::select("name=client", $clients));
	$form->replace('SELECT_INVOICE', html::select("name=invoice", array()));

	$form->replace($payment);
	// $form->replace('DESCRIPTION', '');

	$template->replace('FORM', $form->html());

	$template->render();

	//wizard::html($template->html());	?>