$(function(){

	var module = wizard.modules.payments;
	
	var $client = $('select[name=client]', module.$);
	var $invoice = $('select[name=invoice]', module.$);
	var $date = $('input[name=date]', module.$);
	

	$("input[name='date_format_friendly']").datepicker({dateFormat: 'dd/mm/yy', altFormat: 'yy-mm-dd', altField: $date, /*showOn: 'button',*/ buttonImage: 'images/calendar.gif', buttonImageOnly: true});	

	$client.change(
		function(){
			var client_id = $(this).val();
			refreshInvoices(client_id);

		}
	);

	refreshInvoices = function(client_id){

		if(client_id == null) client_id=0;

		$.ajax({
			url: 'modules/payments/get_invoices',
			data: {client: client_id},
			dataType: 'json',
			success: function(r){
				
				if(r.success){

					if(r.result!=null){
						renderInvoices(r.result.invoices);
					}else{
						renderInvoices('-');
					}

				}

			},
			error: function(r){
				alert('ERROR');
				console.log(r);
			}
		});

	}


	renderInvoices = function(invoices){

		$invoice.html('');

		for(var i in invoices){

			$option = $('<option>');
			$option.text(invoices[i].number);
			$option.val(invoices[i].id);
			$invoice.append($option);

		}

	}

});