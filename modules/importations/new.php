<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('importation');
	wizard::_include('html');

	// wizard::generate_db('importation_product');

	// wizard::generate_db('inventory_movement');

	if($_POST!=null){

		$importation = new importation;

		$importation->set_data($_POST);
		$importation->save();

		$importation->update_inventory();

		$template = new template('templates/new_success');
		$template->render();

		die;

	}

	$template = new template('templates/new');

	$products = wizard::get('product');

	$template->replace('PRODUCTS', html::select("name=products", $products));

	$template->render();

	//wizard::html($template->html());	?>