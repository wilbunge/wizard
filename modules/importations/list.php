<?	include_once('../../wizard');

	wizard::_include('client');
	wizard::_include('importation');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	$template = new template('templates/list');

	$importations = wizard::get('importation');

	if($importations){
		
		foreach($importations as $importation){

			$portion = $template->portion('IMPORTATION');

			// $portion->replace('CLIENT', $importation->get('client'));
			$portion->replace('ID', $importation->id);
			$portion->replace('DATE', $importation->get('date'));

			$template->append($portion);

		}

	}

	$template->clear('IMPORTATION');

	$template->render();

	//wizard::html($template->html());	?>