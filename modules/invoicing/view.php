<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('order');
	wizard::_include('invoice');
	wizard::_include('category');
	wizard::_include('product');

	$template = new template('templates/view');

	//data_manager::$debug_mode=true;

	// master::generate_db('invoice');

	$id = $_GET['id'];

	$invoice = new invoice($id);

	$payments = $invoice->get_payments();

	$template->replace('NUMBER', $invoice->get('number'));
	$template->replace('ID', $invoice->get('id'));
	$template->replace('COMMENTS', $invoice->get('comments'));
	$template->replace('CLIENT', $invoice->get('client'));
	$template->replace('TYPE', $invoice->get_type_name());
	$template->replace('SUBTOTAL', $invoice->get_subtotal_f(true));
	$template->replace('TAX', $invoice->get_tax_f(true));
	$template->replace('TOTAL', $invoice->get_total_f(true));

	$order = $invoice->get('order');

	$template->replace($client);

	if($order){

		foreach($order->get('products') as $order_product){
			$portion = $template->portion('PRODUCT');
			$portion->replace('PRODUCT_NAME', $order_product->get('product'));
			$portion->replace('PRODUCT_CODE', $order_product->get('product')->get('code'));
			$portion->replace('ORDER_AMOUNT', $order_product->get('order_amount'));
			$template->append($portion);
		}

	}

	if($payments){


		foreach($payments as $payment){
			$portion = $template->portion('PAYMENT');
			$portion->replace('PAYMENT_TOTAL', $payment->get_total_formatted());
			$portion->replace('PAYMENT_ID', $payment->id);
			$template->append($portion);
		}

		$template->clear('PAYMENT');


	}else{
		$template->clear('PAYMENT');
	}

	$template->clear('PRODUCT');

	$template->render();

	//wizard::html($template->html());	?>