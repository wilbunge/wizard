<?	

	// include("../../includes/config.php");
	// include("../../functions/message.php");

	include('../../wizard');

	wizard::_include('invoice');
	wizard::_include('invoice_print_configuration');
	wizard::_include('order');
	wizard::_include('product');

	$print_mode = $_GET['print']=='yes';
	
	if($_POST['action']=='save'){

		
		$invoice_print_configuration = new invoice_print_configuration($_POST['id']);
		$invoice_print_configuration->catch_post_properties();
		
		
		if($invoice_print_configuration->get('id')==0){
		}else{
			$invoice_print_configuration->save();
			echo '{"success": true}';
		}
			?>
<?	}else{	?>


<?	if($print_mode){	?>
<html>
	<head>
<?	}	?>
	
<style>

<?	if($print_mode){	?>

#x-axis{
	margin-top: 30px;
}

#y-axis{
	margin-left: 30px;
}

body{
	margin:0px;
	padding:0px;
	font-family: Tahoma;
	font-size: 9px;
}

.page{
	width: 21cm;
	min-height: 29.7cm;
	height:100%;
	border: 1px #D3D3D3 solid;
	border-radius: 5px;
	background: white;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

 @page {
	size: A4;
	margin: 0;
}

@media print {
	.page {
		margin: 0;
		border: initial;
		border-radius: initial;
		width: initial;
		min-height: initial;
		box-shadow: initial;
		background: initial;
		page-break-after: always;
	}


}

#invoicing-print-settings-grid{
	width:100%;
	height:100%;
	position:relative;
	overflow:hidden;
}

.grid_size_15{
	background-image:url(assets/grid_15_px.png);
}

.grid_size_20{
	background-image:url(assets/grid_20_px.png);
}

.grid_size_40{
	background-image:url(assets/grid_40_px.png);
}

.grid_size_60{
	background-image:url(assets/grid_60_px.png);
}

<?	}else{	?>

.grid_size_15{
	background-image:url(modules/invoicing/assets/grid_15_px.png);
}

.grid_size_20{
	background-image:url(modules/invoicing/assets/grid_20_px.png);
}

.grid_size_40{
	background-image:url(modules/invoicing/assets/grid_40_px.png);
}

.grid_size_60{
	background-image:url(modules/invoicing/assets/grid_60_px.png);
}

#invoicing-print-settings-grid{
	width:100%;
	height:1000px;
	position:relative;
	overflow:hidden;
}




<?	}	?>

.grid_size_15 #x-axis div,
.grid_size_15 #y-axis div{
	width:15px;
	height:15px;
	line-height: 15px;
}


#invoicing-print-message{
	display:inline;
	color:#666;
	font-size:11px;
}

.invoicing-print-settings-grid-item{
	position:absolute;
	background-color:white;
	border:1px solid #666;
	padding:4px;
}

.invoicing-print-settings-selected{
	background-color:#CCC;
}


.invoicing-print-settings-grid-item a{
	text-decoration:none;
	font-size:11px;
	color:#333;
}

#x-axis{
	overflow:hidden;
	width:100%;
	height:20px;
}

#y-axis{
	overflow:hidden;
	height:100%;
	width:20px;
}

#x-axis div{
	font-size:10px;
	text-align: center;
	color:#999;
	padding:0px;
	position:absolute;
}

#y-axis div{
	text-align: center;
	font-size:10px;
	padding:0px;
	color:#999;
	position:absolute;
}



</style>

<?	if($print_mode){	?>
</head>
<body onload="window.print();">
<div class="page">
<?	}	?>

<?	$invoice = new invoice;

	$invoice_print_configuration = master::get_one('invoice_print_configuration');

	$resolution = $_GET['r'];

	if($resolution == '' && $invoice_print_configuration){
		$resolution=$invoice_print_configuration->get('resolution');
	}

	if($resolution == 0 or $resolution == '') $resolution=1;

	$grid_size = 15*$resolution;

	$new = false;

	if($invoice_print_configuration==null){
		$invoice_print_configuration = new invoice_print_configuration;
		$ai_master->store($invoice_print_configuration);
		$new = true;

	}

	if($print_mode){	?>
	
<style>


body{
	margin:0px;
}
</style>
	
<?	}else{	?>
	
	
<!--
<select name="item">

<?	foreach($invoice_print_configuration->properties as $p){	?>
	<option value="<?=$p;	?>"><?=$p;	?></option>
<?	}	?>

</select>
-->

<button value="save">Guardar</button> <button value="l">&larr;</button><button value="u">&uarr;</button><button value="d">&darr;</button><button value="r">&rarr;</button>
  

  <select name="resolution">
  	<option value="1">RESOLUCION</option>
  	<option value="1">x1</option>
  	<option value="2">x2</option>
  	<option value="3">x3</option>
  </select>


  <button value="print">Imprimir prueba</button>

<div id="invoicing-print-message"></div>

<hr />



<form>

	<input type="hidden" name="action" value='save' />
	<input type="hidden" name="id" value='<?=$invoice_print_configuration->get('id');	?>' />
<?	foreach($invoice_print_configuration->properties as $p){
		if($p == 'resolution') continue;	?>
	<input type="hidden" name="<?=$p;	?>" value="<?=$invoice_print_configuration->get($p);	?>" />
<?	}	?>	

	<input type="hidden" name="resolution" value="<?=$resolution;	?>" />
	<input type="hidden" name="grid_size" value="<?=$grid_size;	?>" />

</form>

<?	}	?>

<div id="invoicing-print-settings-grid" class="grid_size_<?=$grid_size;	?>">

	<div id="x-axis" style="">
<?	for($i=1; $i<100; $i++){
		$l = ($i-1)*$grid_size;	?>
		<div style="left:<?=$l;	?>px" style=""><?=$i;	?></div>
<?	}	?>
	</div>
	
	<div id="y-axis" style="">
<?	for($i=2; $i<100; $i++){
		$t = ($i-1)*$grid_size;	?>
		<div style="top:<?=$t;	?>px" style=""><?=$i;	?></div>
<?	}	?>
	</div>

	<div id="invoicing-print-settings-grid-items">

<?	foreach($invoice_print_configuration->properties as $p){

		if(!$new){
			$choords_str = $invoice_print_configuration->get($p);
			$choords = explode('.', $choords_str);
			
			if($choords[1]==null){
				$choords[1]=$grid_size*$n++;
			}else{
				$choords[1]*=$resolution;
			}

			$choords[0]*=$resolution;


		}else{
			$choords = Array(0, $n++);
		}
		
		$l = $choords[0];
		$t = $choords[1];	?>
	<div class="invoicing-print-settings-grid-item" data-name="<?=$p;	?>" style="left:<?=$l;	?>px; top:<?=$t;	?>px;" id="invoicing-print-settings-item-<?=$p;	?>"><a href="#"><?=$invoice_print_configuration->translate_property($p);	?></a></div>
<?	}	?>

	</div>

</div>

<?	}	?>

<?	if($print_mode){	?>
		</div>
	</body>
</html>
<?	}	?>