<?	include_once('../../wizard');

	wizard::_include('client');
	wizard::_include('invoice');
	wizard::_include('template');
	wizard::_include('invoice');
	wizard::_include('javascript');
	wizard::_include('price_list');
	wizard::_include('html');
	wizard::_include('wizard_user');
	
	//$wizard_user = wizard::get_wizard_user();

	$months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	

	$template = new template('templates/list');

	$javascript = new javascript;

	$status = $_POST['status'];

	if($status=='') $status=3;

	$selected_month = $_POST['month'];
	$selected_client = $_POST['client'];
	// $selected_year = 2018;
	$selected_year = date('Y');

	if($selected_month == '') $selected_month = date('m');

	if($status == 1){
		$wc = "`paid`";
	}else if($status == 3){
		$wc = "TRUE";
	}else{
		$wc = "NOT `paid`";
	}

	if($selected_month!='0') $wc .=  " AND MONTH(DATE) = '$selected_month' AND YEAR(DATE) = '$selected_year'";

	// 
	// $wc = "NOT `paid` AND YEAR(DATE) > 2017";
	// 


	$status_array = array('Pagadas', 'Sin pagar', 'Todas');

	$template->replace('SELECT_STATUS', html::select('name="status"', $status_array, $status));
	$template->replace('SELECT_MONTH', html::select('name="month"', $months, $selected_month));

	// echo $wc;

	$invoices = wizard::get('invoice', $wc.' AND NOT deleted');

	$n=0;

	if($invoices){
		
		foreach($invoices as $invoice){

			// if($n++ > 5) continue;

			if(!wizard::get_user()->is_admin()){
				if($invoice->id < 439) continue;
				$portion = $template->portion('INVOICE_NONE_ADMIN');
			}else{
				$portion = $template->portion('INVOICE');

			}
			
			$portion->replace('NUMBER', $invoice->get_id_number());
			$portion->replace('CLIENT', $invoice->get('client'));
			// $portion->replace('TOTAL', $invoice->get_symbol().' '.$invoice->get_total_f(true));
			$portion->replace('DATE', $invoice->get('date'));
			$portion->replace('ID', $invoice->id);

			

			$total_sum += $invoice->get('total');



			$curr = $invoice->get('currency');

			

			if($curr == 2){

				$order = $invoice->get('order');

				$curr_value = $order->get('currency_value');

				if(!$curr_value) $curr_value=34;

				$grand_total_2+=$invoice->get('total');
				
				$portion->replace('TOTAL_2', $invoice->get_total_f(true));

				$total_2 = $invoice->get('total');

				if($curr_value == 1){
					$portion->replace('TOTAL', '-');
				}else{

					$total = ($total_2 / $curr_value);

					$portion->replace('TOTAL', invoice::format_total($total));
					
					$grand_total+=$total;
				}

				

			}else{

				$grand_total+=$invoice->get('total');
				$portion->replace('TOTAL', $invoice->get_total_f(true));
				$portion->replace('TOTAL_2', '-');

			}


			$template->append($portion);

			

		}

	}

	$total_sum_formatted = wizard::format_number($total_sum);

	$template->replace('TOTAL_SUM', invoice::format_total($grand_total));
	$template->replace('TOTAL_SUM_2', invoice::format_total($grand_total_2));

	$template->clear('INVOICE');
	$template->clear('INVOICE_NONE_ADMIN');

	$template->render();

	//wizard::html($template->html());	?>