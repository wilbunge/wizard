$(function() {


	var $selected_item;
	var selected_item;
	var config = Array();
	var module = wizard.modules.invoicing;
	var grid_size = Math.floor(module.$.find('input[name=grid_size]').val());
	var resolution = Math.floor(module.$.find('input[name=resolution]').val());

	module.$.find('button[value=print]').click(
		function(){
			window.open('modules/invoicing/calibration?print=yes', 'green_iframe');
		}
	);


	module.$.find('select[name=resolution]').change(
		function(){
			resolutionChange($(this).val());
		}
	);

	$('button[value=save]', module.$).click(
		function(){
			saveSettings();
		}
	);

	$('div', '#invoicing-print-settings-grid-items').click(
		function(){
		
			var item = $(this).attr('data-name');
			selectItem(item);
			return false;
		}
	);

	$('div', '#invoicing-print-settings-grid-items').draggable({
		grid: [ grid_size,grid_size ],
		containment: "#invoicing-print-settings-grid",
		start: function(){
			var item = $(this).attr('data-name');
			selectItem(item);
		},
		stop: function(){
			updateItemChoords($(this).attr('data-name'));
		}
	});

	$('button[value=r]').click(
		function(){
		
			moveRight();
		}
	);

	$('button[value=l]', module.$).click(
		function(){
			moveLeft();
		}
	);

	$('button[value=u]', module.$).click(
		function(){
			moveUp();
		}
	);

	$('button[value=d]', module.$).click(
		function(){
			moveDown();
		}
	);

	moveRight = function(){
		var l = $selected_item.position().left;
		var t = $selected_item.position().top;
		l+=grid_size;
		
		$selected_item.css('left', l+'px');
		
		config[selected_item].left = l;
		config[selected_item].top = t;
	}

	moveUp = function(){
		var t = $selected_item.position().top;
		var l = $selected_item.position().left;
		
		t-=grid_size;
		$selected_item.css('top', t+'px');
		
		config[selected_item].top = t;
		config[selected_item].left = l;
	}


	moveLeft = function(){
		var l = $selected_item.position().left;
		var t = $selected_item.position().top;
		l-=grid_size;
		$selected_item.css('left', l+'px');
		
		config[selected_item].left = l;
		config[selected_item].top = t;
	}



	moveDown = function(){

		var t = $selected_item.position().top;
		var l = $selected_item.position().left;

		t+=grid_size;

		$selected_item.css('top', t+'px');
		
		config[selected_item].top = t;
		config[selected_item].left = l;
	}



	if(!module.calibration_key_events===true){

		module.calibration_key_events=true;

		$(document).keydown(function(e) {
		    switch(e.which) {
		        case 37: // left
		        moveLeft();
		        break;

		        case 38: // up
		        moveUp();
		        break;

		        case 39: // right
		        moveRight();
		        break;

		        case 40: // down
		        moveDown();
		        break;

		        default: return; // exit this handler for other keys
		    }
		    e.preventDefault(); // prevent the default action (scroll / move caret)
		});

	}

	saveSettings = function(callback){
		
			for(var i in config){
			
				var o = config[i];
				var l = o.left;
				var t = o.top;

				l = Math.floor(l/resolution);
				t = Math.floor(t/resolution);

				var v = l+'.'+t;
				
				$('input[name='+i+']').val(v);
				
			}
		
			//$('form', module.$).submit();
			
			//submitForm($('form', module.$), 'print_config', 'invoicing', 'container_2');
			var d = $('form', module.$).serialize();
			
			$('#invoicing-print-message').html('Guardando...');
			
			$.ajax({
				url: 'modules/invoicing/calibration',
				dataType: 'json',
				type: 'POST',
				data: d,
				success: function(r){
					
					if(r.success){
						
						$('#invoicing-print-message').html('Los cambios se han guardado con éxito!');
						if(callback!=null) callback();
						
					}else{
						alert('SE HA PRODUCIDO UN ERROR');
						console.log(r);
					}
					
				},
				error: function(r){
					alert('SE HA PRODUCIDO UN ERROR');
					console.log(r);
				}
			});
		
		}


	$("select[name=item]").change(
		function(){
			
			var item = $(this).val();
			
			$('.invoicing-print-settings-selected').removeClass('invoicing-print-settings-selected');
			var $item = $('#invoicing-print-settings-item-'+item)
			
			$selected_item = $item;
			selected_item = item;
			
			if(config[selected_item]==null) config[selected_item] = {};
			
			$item.addClass('invoicing-print-settings-selected');
			
			
		}
	);

	selectItem = function(item){
		
		
		$('.invoicing-print-settings-selected').removeClass('invoicing-print-settings-selected');
		var $item = $('#invoicing-print-settings-item-'+item)
		
		$selected_item = $item;
		selected_item = item;
		
		if(config[selected_item]==null) config[selected_item] = {};
		
		$item.addClass('invoicing-print-settings-selected');
		
	}


	updateItemChoords = function(item){

		var $item = $('#invoicing-print-settings-item-'+item)

		var l = $item.position().left;
		var t = $item.position().top;
		
		if(config[item]==null) config[item] = {};

		config[item].left = l;
		config[item].top = t;
		
	}


	resolutionChange = function(r){

		saveSettings(
			function(){
				module.action('calibration?r='+r);
			}
		);
	}

});