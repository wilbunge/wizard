$(function(){
	
	var module = wizard.modules.invoicing;

	module.$.find('button[value=print]').click(
		function(){

			var $b = $(this);

			var invoice_id = $b.attr('w-invoice-id');

			// $a.hide().after('<span>...</span>');

			printInvoice(invoice_id);
		}
	);

	printInvoice = function(invoice_id){

		var $iframe = module.$.find('iframe');

		if($iframe.size()<1){
			$iframe = $('<iframe border="yes">').hide();
			module.$.append($iframe);

		}


		
		$iframe.attr('src', 'modules/invoicing/print.php?invoice='+invoice_id);

	}

	module.printing = function(invoice){
		var $a = module.$.find('a[href=#print][w-invoice-id='+invoice+']');
		$a.parent().find('span').remove();
		$a.show();

	}


})