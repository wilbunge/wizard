$(function() {

	var module = wizard.modules.invoicing;

	module.total = 0;
	module.n = 0;
	module.items = {};

	var $select = $('select[name=products]', module.$);
	var $button = $('button[value=add-product]', module.$);
	var $products = module.$.find('products');
	var $qty = $('input[name=qty]', module.$);
	var $total = $('input[name=total]', module.$);
	var $form = $('form', module.$);

	//$products.text('xx');
	
	//$("input[name='date']").datepicker({dateFormat: 'yy-mm-dd', /*showOn: 'button',*/ buttonImage: 'images/calendar.gif', buttonImageOnly: true});	
		
	$qty.keypress(
		function(e){

			if(e.keyCode!=13) return;

			addProduct();
			$select.focus();
			return false;
		}
	);
	
	$select.change(
		function(){
			$qty.focus();
			return false;
		}
	);
	
	$select.keypress(
		function(){
			$qty.focus();
			return false;
		}
	);

	$button.click(
		function(){
			addProduct();
		}
	);

	addProduct = function(){

		var $option = $('option:selected', $select);
		var product_name = $('option:selected', $select).text();
		var product_price = $option.attr('data-product-price');
		var product_id = $select.val();
		var product_qty = $qty.val();
		var subtotal = product_price*product_qty;
		
		if(product_qty*product_id<1) return;
		
		var $input = $('<input type="hidden" name="products[]" value="'+product_id+'">');
		var $input_qty = $('<input type="hidden" name="order_amount_'+product_id+'" value="'+product_qty+'">');
		
		var $td_inputs = $('<td>');
		
		$td_inputs.append($input);
		$td_inputs.append($input_qty);
		
		$tr = $('<tr data-invoice-item="'+(++module.n)+'">');
		$tr.append($td_inputs);
		$tr.append('<td>'+$qty.val()+ ' x '+product_name+'</td>');
		$tr.append('<td>$'+subtotal+'</td>');
		$tr.append('<td><a data-invoice-item="'+(module.n)+'" href="#delete">eliminar</a></td>');
		$products.append($tr);
		
		$('a[href=#delete]', $tr).click(	
			function(){
				module.removeProduct($(this).attr('data-invoice-item'));
			}
		);
		
		$select.val(0);
		$qty.val(0);
		
		module.items[module.n] = {};
		
		module.items[module.n].subtotal = subtotal;
		module.total += subtotal;
		
		$total.val(module.total);
		
	}

	module.removeProduct = function(n){
		module.total-=module.items[n].subtotal;
		module.refreshTotal();
		$('tr[data-invoice-item='+n+']').fadeOut();
	}	

	module.refreshTotal = function(){
		$total.val(module.total);
	}

})