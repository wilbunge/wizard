<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('invoice');
	wizard::_include('html');
	wizard::_include('price_list');

	if($_POST!=null){

		wizard::_include('category');

		$invoice = new invoice;

		$invoice->set_data($_POST);
		$invoice->save();

		$template = new template('templates/new_success');
		$template->render();

		die;
	}

	$template = new template('templates/new');

	$clients = wizard::get('client');
	$products = wizard::get('product');

	$template->replace('CLIENT', html::select("name=client", $clients));
	$template->replace('PRODUCTS', html::select("name=product", $products));

	$template->render();

	//wizard::html($template->html());	?>