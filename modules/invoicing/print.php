<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('order');
	wizard::_include('product');
	wizard::_include('invoice');
	wizard::_include('category');

	// $client = new client($_GET['id']);

	//data_manager::$debug_mode=true;

	//wizard::html($template->html());	?>

<html>
<head>
	<title></title>

	<script type="text/javascript">

	parent.wizard.modules.invoicing.printing('<?=$_GET['invoice'];	?>');

	</script>

</head>
<body onload="window.print();">

<style>
 
body{
	background:url(assets/i3.jpg) no-repeat;
	background-size: 100%;
	font-family: Tahoma;
	font-size: 10px;
}

div{
	white-space: nowrap;
}

</style>

<?	
	// include("../../includes/config.php");
	// include("../../functions/message.php");

	// include("../../classes/invoice.php");
	// include("../../classes/order.php");
	// include("../../classes/product.php");
	// include("../../classes/client.php");
	// include("../../classes/master.php");
	
	$invoice = new invoice($_GET['invoice']);
	
	$invoice_print_configuration = master::get_one('invoice_print_configuration');	?>
		
<?	$order = $invoice->get('order');


	if($invoice->get('currency') == 2){
		$symbol = "\$UY";
	}else{
		$symbol = "USD";
	}
	
	if($order!=null){
		$products = $order->get_products();
	}else{
		$products = Array();
	}	?>

<!-- <div style="left: 390px; position: absolute; top: 480px; font-size: 20px;"><?=$invoice->get_number2();	?></div> -->
<div style="<?=$invoice_print_configuration->get_css('number');	?>"><?=$invoice->get('number');	?></div>
<div style="<?=$invoice_print_configuration->get_css('code');	?>"><?=$invoice->get('id');	?></div>	
<div style="<?=$invoice_print_configuration->get_css('client');	?>"><?=$invoice->get('client')->get('full_company_name');	?></div>
<div style="<?=$invoice_print_configuration->get_css('client_address');	?>"><?=$invoice->get('client')->get('address');	?></div>
<div style="<?=$invoice_print_configuration->get_css('client_rut');	?>"><?=$invoice->get('client')->get('rut');	?></div>
<div style="<?=$invoice_print_configuration->get_css('date');	?>"><?=$invoice->get_date_formatted();	?></div>
<div style="<?=$invoice_print_configuration->get_css('subtotal');?>"><?=$symbol;?> <?=$invoice->get_subtotal_f(true);	?></div>
<div style="<?=$invoice_print_configuration->get_css('iva');?>"><?=$symbol;?> <?=$invoice->get_tax_f(true);	?></div>
<div style="<?=$invoice_print_configuration->get_css('total');?>"><?=$symbol;?> <?=$invoice->get_total_f(total);	?></div>
<div style="<?=$invoice_print_configuration->get_css('document_type');?>"><?=$invoice->get_type_name();	?></div>


<div style="<?=$invoice_print_configuration->get_css('comments');	?>"><?=$invoice->get('comments');	?></div>

<?	foreach($products as $order_product){
		$n++;	?>
	<div style="<?=$invoice_print_configuration->get_css('item_name', $n);	?>"><?=$order_product->get('product')->get_name_code();	?></div>
	<div style="<?=$invoice_print_configuration->get_css('item_qty', $n);	?>"><?=$order_product->get('order_amount');	?></div>
	<div style="<?=$invoice_print_configuration->get_css('item_subtotal', $n);	?>"><?=$order_product->get('order_subtotal');	?></div>
	<div style="<?=$invoice_print_configuration->get_css('item_price', $n);	?>"><?=$order_product->get('product')->get('price');	?></div>
<?	}	?>


</body>
</html>


