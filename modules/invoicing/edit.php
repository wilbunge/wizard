<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('invoice');

	$template = new template('templates/edit');

	//master::generate_db('invoice');
	
	if($_POST!=null){

		$invoice = new invoice($_POST['id']);

		// $validation = $invoice->validate();

		if(true){

			$invoice->update_property('number', $_POST['number']);
			$invoice->update_property('comments', $_POST['comments']);
			$invoice->update_property('paid', $_POST['paid']=='0');


			$template = new template('templates/message');
			$template->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
			$template->render();

			die;

		}else{

			foreach ($validation->errors as $field => $message) {
				$template->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template->set_form_values($invoice);

		}
		
	}else{
		$invoice = new invoice($_GET['id']);
		$template->set_form_values($invoice);
	}

	// $template2->replace('PRICE_LIST_MENU', html::select('name="price_list"', $price_lists, $invoice->get('price_list')->id));

	$template->replace('ID', $invoice->id);
	// $template->replace('NAME', $invoice);
	// $template->replace('invoice_FORM', $template2->html());

	$template->render();	?>