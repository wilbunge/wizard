<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('category');
	wizard::_include('invoice');
	wizard::_include('product');

	if($_GET['client']==null){
		$response['success'] = false;
		$response['error'] = 'No client ID provided.';
	}else{

		$client = new client($_GET['client']);

		$response = array();

		$invoices = wizard::get('invoice', '`client` = '.$client->id.' AND NOT `deleted` AND NOT `paid`');

		$response['success'] = true;

		if(!$invoices) $invoices=array();

		foreach($invoices as $i){
			$response['result']['invoices'][] = array(
				'id' => $i->id,
				'number' => $i->get_id_number()
			);
		}

	}

	$json = json_encode($response);

	echo $json;	?>