<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('expense');

	$template = new template('templates/edit');
	$template2 = new template('templates/expense_form');

	//master::generate_db('expense');
	
	if($_POST!=null){

		$expense = new expense($_POST['id']);

		$expense->set_data($_POST);
		$template2->replace($expense);

		// $validation = $expense->validate();

		if(true){

			$expense->save();

			$template = new template('templates/message');
			$template->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
			$template->render();

			die;

		}else{

			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($expense);

		}
		
	}else{
		$expense = new expense($_GET['id']);
		$template2->set_form_values($expense);
	}

	// $template2->replace('PRICE_LIST_MENU', html::select('name="price_list"', $price_lists, $expense->get('price_list')->id));

	$template->replace('ID', $expense->id);
	// $template->replace('NAME', $expense);
	$template->replace('EXPENSE_FORM', $template2->html());

	$template->render();	?>