$(function(){

	var module = wizard.modules.tasks;

	var $tasks = module.$.find('ol.tasks');
	var $done = module.$.find('ul.done');

	$tasks.sortable();

	$tasks.find('li').each(
		function(i,o){

			var task_id = $(this).attr('data-task-id');
			var $input = $(this).find('input[type=checkbox]');


			$input.click(
				function(){
					toggleDone(task_id);
				}
			);
		}
	);


	$done.find('li').each(
		function(i,o){

			var task_id = $(this).attr('data-task-id');
			var $input = $(this).find('input[type=checkbox]');


			$input.click(
				function(){
					unsetTaskAsDone(task_id);
				}
			);
		}
	);


	function toggleDone(task_id){

		var $tasks = module.$.find('ol.tasks');
		var $li = $tasks.find('li[data-task-id='+task_id+']');

		if($li.hasClass('done')){
			$li.removeClass('done')
			$li.css({textDecoration: 'none'});
			unsetTaskAsDone(task_id);
		}else{
			$li.addClass('done')
			$li.css({textDecoration: 'line-through'});

			setTaskAsDone(task_id);

		}


		
		// $li.fadeOut();
	}

	function setTaskAsDone(task_id){

		var $tasks = module.$.find('ol.tasks');
		var $done = module.$.find('ul.done');
		
		var $li = $tasks.find('li[data-task-id='+task_id+']');

		setTask(
			task_id,
			true,
			function(r){
				if(r.success){

					$li.slideUp(
						function(){
							var $clone = $li.clone();
							$done.append($clone);
							$clone.slideDown();
						}
					);

				}
			}
		);

		return;

		

	}


	function unsetTaskAsDone(task_id){

		var $tasks = module.$.find('ol.tasks');
		var $done = module.$.find('ul.done');
		
		var $li = $done.find('li[data-task-id='+task_id+']');

		setTask(
			task_id,
			false,
			function(r){
				if(r.success){

					$li.slideUp(
						function(){
							var $clone = $li.clone();
							$tasks.append($clone);
							$clone.slideDown();
						}
					);

				}
			}
		);

		return;

		

	}


	function setTask(task_id, done, callback){


		$.ajax({
			url: 'modules/tasks/set_task',
			data: {task: task_id, done: done?1:0},
			type: 'POST',
			dataType: 'JSON',
			success: function(r){
				callback(r);
			},
			error: function(r){
				callback(r);
			}

		});
	}

});