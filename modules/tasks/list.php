<?	include_once('../../wizard');

	wizard::_include('html');
	wizard::_include('client');
	wizard::_include('invoice');
	wizard::_include('task');
	wizard::_include('category');
	wizard::_include('template');
	wizard::_include('price_list');
	wizard::_include('wizard_user');
	
	//$wizard_user = wizard::get_wizard_user();

	$template = new template('templates/list');

	$months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	$selected_month = $_POST['month'];
	$selected_client = $_POST['client'];

	$wc = 'worker = '.wizard::get_user()->id;

	if($selected_month == '') $selected_month = date('m');

	// if($selected_client!='0') $wc .= " AND client = '$selected_client'";
	// if($selected_month!='0') $wc .=  " AND MONTH(DATE) = '$selected_month'";

	$tasks = wizard::get('task', 'NOT `deleted` AND NOT done');
	$done = wizard::get('task', 'NOT `deleted` AND done');

	if($tasks){
		
		foreach($tasks as $task){

			$portion = $template->portion('TASK');

			$portion->replace('DESCRIPTION', $task->get('description'));
			
			$portion->replace($task);
			
			$portion->replace('TASKID', $task->id);
			
			$template->append($portion);


		}

	}

	if($done){
		
		foreach($done as $task){

			$portion = $template->portion('TASKDONE');

			$portion->replace('DESCRIPTION', $task->get('description'));
			
			$portion->replace($task);
			
			$portion->replace('TASKID', $task->id);
			
			$template->append($portion);


		}

	}

	$template->clear('TASK');
	$template->clear('TASKDONE');

	$template->render();

	//wizard::html($template->html());	?>