<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('invoice');
	wizard::_include('task');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('html');
	wizard::_include('user');
	wizard::_include('wizard_user');

	$task = new task;

	// wizard::generate_db('task');

	if($_POST!=null){

		$task->set_data($_POST);
		$task->set('author', wizard::get_user());
		$task->save();

		$t_message = new template('templates/message');
		$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
		$t_message->render();

		die;
	}

	$users = wizard::get('user');

	$template = new template('templates/new');
	$form = new template('templates/task_form');

	$form->replace('SELECT_USER', html::select("name=worker", $users));

	$form->replace($task);
	// $form->replace('DESCRIPTION', '');

	$template->replace('FORM', $form->html());

	$template->render();

	//wizard::html($template->html());	?>