<?	include('../../wizard');

	wizard::_include('javascript');
	wizard::_include('template');

	$javascript = new javascript;

	$query = $_GET['query'];

	$search_results = Array();

	// Search clients


	$clients = wizard::get('client', '`company_name` LIKE "%'.$query.'%"');

	foreach($clients as $client){
		$search_results['Clientes'][] = $client;
	}

	$template = new template('templates/results');

	foreach ($search_results as $module_search) {

		
		$template_list = new template('templates/results_list');
		$portion = $template_list->portion('RESULTS');

		foreach ($module_search as $result) {

			$portion_2 = $template_list->portion('RESULT');
			$portion_2->replace('TEXT', $result);

			$template_list->append($portion_2);

		}

		$portion->replace('RESULTS', $template_list->html());

		$template->append($portion);

	}

	// $template = new template('templates/results');

	$template->clear('RESULT');


	// Search modules

	$wizard_module = wizard::get_one('wizard_module', '`name_spa` LIKE "'.$query.'"');

	if($wizard_module){

		$javascript = new javascript;

		$javascript->code('wizard.openModule("'.$wizard_module->get('keyword').'");');

	}

	$template->render();

	echo $javascript->output();	?>