<?	
	// include("../../includes/config.php");

	// include("../../classes/master.php");
	
	// include("../../classes/client.php");

	include_once('../../wizard');

	wizard::_include('user');
	wizard::_include('client'); 	?>

<style>

#G_he_tt_button{
	width:85px;
}

#G_ME_timetracker_time_wrapper{
	background:#EEE;
	padding:10px;
}


#G_ME_timetracker_time{
	border:1px solid #999;
	background-color:white;
	color:black;
	font-size:60px;
	padding:4px;
	margin-left:20px;
	margin-right:20px;
	text-align:center;
}

table.time_tracker button{
	width:100%;
}

table.time_tracker textarea{
	font-size:10px;
}

table.time_tracker button.big{
	height:40px;
	width:100%;
}

table.time_tracker td{
	vertical-align:top;
}

table.time_tracker ul.specs{
	width:100%;
	margin:0px;
	padding:0px;
}

table.time_tracker ul.specs li{
	border-bottom:1px dotted #CCC;
	list-style-type:none;
	font-size:10px;
	padding:2px;
}

.G_CN_timetracker_table th{
	text-align:right;
	vertical-align:top;
}

.G_CN_timetracker_big_button{
	font-size:20px;
	
}

#G_ME_timetracker_button{
	width:100px;
}



</style>

<?	$user = new user;
	$master = new master;

	$clients=wizard::get('client','NOT deleted');

	$project=$_GET['project'];	

	if($project=='')$project=0;	?>

<div id="G_ME_timetracker_time_wrapper">
	<div id="G_ME_timetracker_time">00:00:00</div>
</div>

<table class="G_CN_timetracker_table" border="0">
	
	<tr>		
		<th>Cliente</th>
		<td>
		
			<select id="G_ME_timetracker_client">
				<option value="0">...</option>
<?	foreach($clients as $client){	?>
				<option value="<?=$client->get_id();	?>"><?=$client->get_name();	?></option>
<?	}	?>
			</select>
		
		</td>
	</tr>
	
	<!--<tr>		
		<th>Project</th>
		<td><?=$_GET['title'];	?></td>
	</tr>-->
	
	<tr>
		<th>Asunto</th>
		<td><input type="text" id="G_ME_timetracker_subject" /></th></td>
	</tr>
	
	<tr>
		<th>Comentarios</th>
		<td><textarea id="G_ME_timetracker_description" cols="15" rows="2"></textarea></th></td>
	</tr>
	
	<tr>
		<th></th>
		<td>
			<button id="G_ME_timetracker_button" class="G_CN_timetracker_big_button" onclick="tt_click(<?=$project;	?>, '<?=date("Y-m-d");	?>', '<?=date("H:i:s");	?>');" ><span>Start</span></button>
			<button id="G_ME_timetracker_button_reset" onclick="G_JF_timetracker_reset(<?=$project;	?>, '<?=date("Y-m-d");	?>', '<?=date("H:i:s");	?>');">Reset</button>
			<button id="G_ME_timetracker_button_close" onclick="w_close('G_w_time_tracker','time_tracker');">Close</button>
		</td>
	</tr>
	
	
</table>

<br />

<center>
	
</center>