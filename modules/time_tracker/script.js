var G_jv_public_intervals = [];

var i = 50;
var n = 0;

var ms = 0;
var s = 0;
var m = 0;

var t0;
	
var ms0;
var s0;
var m0;

var G_jv_pause_date;
var G_jv_resume_date;

var G_jv_pause_time;

var time_str="00:00:00";
var G_JV_timetracker_track_enabled = true;
var track_interval;
var checkpoint_interval;
var t_project;
var tt_date;
var tt_time;
var tt_elapsed;

var interval_chrono;
var interval_track;

var G_jv_interval_chrono;
var G_jv_interval_track;
var G_jv_switch = true;
var G_jv_p_s = 0;

var G_jv_m0 = 0;
var G_jv_s0 = 0;
var G_jv_ms0 = 0;

var G_jv_m;
var G_jv_s;
var G_jv_ms=0;

G_jv_public_intervals["time_tracker"] = [];

var G_JV_timetracker_elapsed=0;

var G_JV_timetracker_elapsed_s=0;
var G_JV_timetracker_elapsed_m=0;
var G_JV_timetracker_elapsed_h=0;

var G_JV_timetracker_date_0;
var G_JV_timetracker_last_tick_date;
var G_JV_timetracker_tick_date;

var G_JV_timetracker_comment;
var G_JV_timetracker_client;

var G_JV_timetracker_ref_id;
var G_JV_timetracker_ref_type;


function tt_click(G_p_project){

	t_project = G_p_project;
	
	G_JF_timetracker_disable_description();
	G_JF_timetracker_disable_clients();
	
	if(G_JV_timetracker_track_enabled){ // Tracking...
	
		t0 = new Date();
		
		G_JV_timetracker_comment = $("#G_ME_timetracker_description").val();
		G_JV_timetracker_client = $("#G_ME_timetracker_client").val();
		
		G_JV_timetracker_ref_id=G_JV_timetracker_client;
		G_JV_timetracker_ref_type="client";
		
		G_JV_timetracker_t0 = new Date();
		G_JV_timetracker_ti = new Date();
		
		G_jv_resume_date = new Date();
		G_JV_timetracker_date_0 = new Date();
		G_JV_timetracker_last_tick_date=G_JV_timetracker_date_0;
		
		if(G_jv_resume_date!=undefined && G_jv_pause_date!=undefined)G_jv_p_s += (G_jv_resume_date.getSeconds() - G_jv_pause_date.getSeconds());
		
		G_jv_interval_track = setInterval('G_JF_timetracker_tick()', 1000);
		G_jv_interval_chrono = setInterval('checkpoint()', 5000);
		
		G_jv_public_intervals["time_tracker"][0] = G_jv_interval_track;
		G_jv_public_intervals["time_tracker"][1] = G_jv_interval_chrono;
		
		$("#G_ME_timetracker_button").html("Pause");
		
	}else{ // Paused
	
		G_JF_timetracker_blink_button();	
		G_JF_timetracker_clearIntervals();
		
		$("#G_ME_timetracker_button").html("Resume");
		
	}
	
	G_JV_timetracker_track_enabled = !G_JV_timetracker_track_enabled;
	
}


function G_JF_timetracker_clearIntervals(){
	clearInterval(G_jv_public_intervals["time_tracker"][0]);
	clearInterval(G_jv_public_intervals["time_tracker"][1]);
}


function G_JF_timetracker_get_minutes(p_seconds){
	var minutes=Math.floor(p_seconds/60);	
	return(minutes);
}

function G_JF_timetracker_get_hours(p_minutes){
	var minutes=Math.floor(p_minutes/60);	
	return(minutes);
}



function G_JF_timetracker_tick(){

	G_JV_timetracker_elapsed++;	
	G_JV_timetracker_elapsed_s++;
	
	G_JF_timetracker_update_title();
	
	if(G_JV_timetracker_elapsed_s>59){
		G_JV_timetracker_elapsed_s=0;
		G_JV_timetracker_elapsed_m++;
	}
	
	if(G_JV_timetracker_elapsed_m>59){
		G_JV_timetracker_elapsed_m=0;
		G_JV_timetracker_elapsed_h++;
	}
	
	
	var s = G_JV_timetracker_elapsed_s;
	var m = G_JV_timetracker_elapsed_m;
	var h = G_JV_timetracker_elapsed_h;
	
	// Pad the minutes and seconds with leading zeros, if required
	h = (h < 10 ? "0" : "") + h;
	m = (m < 10 ? "0" : "") + m;
	s = (s < 10 ? "0" : "") + s;

	time_str = h+":"+m+":"+s;
	
	$("#G_ME_timetracker_time").html(time_str);
}


function track(){
	G_JV_timetracker_elapsed++;
	
	var t = new Date();
	
	var G_JV_timetracker_tick_date = new Date();
	
	var G_JV_timetracker_tf = new Date();
	
	G_JV_timetracker_elapsed+=(G_JV_timetracker_tf.getTime()-G_JV_timetracker_ti.getTime());
	//G_JV_timetracker_elapsed_s+=(G_JV_timetracker_tf.getSeconds()-G_JV_timetracker_ti.getSeconds());	
	
	G_JV_timetracker_elapsed_s++;
	
	var s = G_JV_timetracker_elapsed_s;
	
	time_str = s;
	
	G_jv_m = G_jv_m0 + t.getMinutes()-t0.getMinutes();
	G_jv_s = G_JV_timetracker_tf.getSeconds()-G_JV_timetracker_ti.getSeconds();
	
	var h = t.getHours()-t0.getHours();
	var m = G_jv_m;
	
	
	G_jv_ms += i;
	
	if(G_jv_ms > 999) G_jv_ms = 0;
	
	var ms = G_jv_ms;
	
	if(G_jv_pause_time != undefined){
		alert(G_jv_pause_time.getSeconds());
	}
	
	/*
	if(G_jv_pause_date!=undefined){
		m -= G_jv_pause_date.getMinutes();
		s -= G_jv_pause_date.getSeconds();
		ms -= G_jv_pause_date.getTime();
	}*/
	
	tt_elapsed = ms;
	
	//if(ms>999) ms = 0;
	
  // Pad the minutes and seconds with leading zeros, if required
	h = (h < 10 ? "0" : "") + h;
	m = (m < 10 ? "0" : "") + m;
	s = (s < 10 ? "0" : "") + s;

	//time_str = h+":"+m+":"+s;
	
	$("#G_ME_timetracker_time").html(time_str);
}


function checkpoint(){

	//alert(G_JV_timetracker_elapsed);
	
	var post_data = "project="+t_project+"&elapsed="+G_JV_timetracker_elapsed+"&tt_time_0="+G_JV_timetracker_date_0.getTime()+"&comment="+G_JV_timetracker_comment+"&ref_id="+G_JV_timetracker_ref_id+"&ref_type="+G_JV_timetracker_ref_type;
	
	$.ajax({
		url:"modules/time_tracker/checkpoint.php",
		data:post_data,
		type:"POST",
		success:function(data){
			if(data!='G_OR_OK'){
				alert("Error tracking!("+data+")");
				G_JF_timetracker_error();
			}
		},
		error:function(){
			alert("error");
		}
		
	
	});
	
}

function G_JF_timetracker_reset(){

	G_JF_timetracker_clearIntervals();
	
	G_JV_timetracker_elapsed=0;
	G_JV_timetracker_elapsed_s=0;
	G_JV_timetracker_elapsed_m=0;
	G_JV_timetracker_elapsed_h=0;
	
	G_JF_timetracker_enable_description()
	G_JF_timetracker_enable_clients()
	G_JF_timetracker_reset_buttons();
	
	G_JV_timetracker_track_enabled = true;
	
	$("#G_ME_timetracker_description").val("");
	$("#G_ME_timetracker_client").val("...");
	$("#G_ME_timetracker_time").html("00:00:00");
	
}


function G_JF_timetracker_error(){
	G_JF_timetracker_clearIntervals();
}


function G_jf_toggle(){
	
	G_jv_switch != G_jv_switch;
	
	if(G_jv_switch){
	
	}else{
	}
	
}

function G_JF_timetracker_enable_clients(){
	$("#G_ME_timetracker_client").attr("disabled","")
}

function G_JF_timetracker_disable_clients(){
	$("#G_ME_timetracker_client").attr("disabled","disabled")
}

function G_JF_timetracker_disable_description(){
	$("#G_ME_timetracker_description").attr("disabled","disabled")
}

function G_JF_timetracker_enable_description(){
	$("#G_ME_timetracker_description").attr("disabled","")
}

function G_JF_timetracker_reset_buttons(){
	$("#G_ME_timetracker_button").html("Start")
}

function G_JF_timetracker_blink_button(){
	//G_JF_timetracker_button_fo();
}


function G_JF_timetracker_update_title(){
	$("div.title", "#G_w_time_tracker").html("Timetracker ("+time_str+")");
}