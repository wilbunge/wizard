
<?	

	include('../time_tracker/track.php');


	die;
	function secondsToWords($seconds){
		/*** return value ***/
		$ret = "";

		/*** get the hours ***/
		$hours = intval(intval($seconds) / 3600);
		
		/*** get the minutes ***/
		$minutes = bcmod((intval($seconds) / 60),60);
		
		if($hours<10) $hours="0$hours";
		if($minutes<10) $minutes="0$minutes";
		
		$ret = "$hours:$minutes";
		
		return $ret;
	}
	
	$sel_client=$_POST["client"];
	$sel_user=$_POST["user"];
	$sel_month=$_POST["month"];
	$sel_year=$_POST["year"];
	
	if($sel_year=="")$sel_year=date('Y');
	if($sel_month=="")$sel_month=date('m');

	$master=new master;
	
	$clients=$master->select("client","NOT deleted");
	$users=$master->select("user","NOT deleted");
	
	$query=" TRUE";
	
	if($sel_client!=""&&$sel_client!="0") $query.=" AND (ref_type='client' AND ref_id=$sel_client)";
	if($sel_user!=""&&$sel_user!="0") $query.=" AND (user=$sel_user)";
	if($sel_month!=""&&$sel_month!="0") $query.=" AND (month(DATE(date))=$sel_month)";
	if($sel_year!=""&&$sel_year!="0") $query.=" AND (year(DATE(date))=$sel_year)";

	$result=$master->mysql_query_select("*", "timetracker", "project=0 AND ($query)", "ORDER BY id DESC");	?>
	
<style>

#G_ME_time_tracker_tt_button{
	font-size:15px;
}

</style>

<div id="G_ME_time_tracker_tt_button"><button onclick="G_jsf_openModule('time_tracker', 'track', null, 'box', 'Time Tracker');">Abrir TimeTracker</button></div>
	
<div class="G_cs_filter">

<form onsubmit="return(submitForm(this, 'home', 'timetracker','container_2'))">

<select name="month">
	<option<?	if($sel_month == '0') echo(' selected');	?> value="0" style="text-align:center;">TODO EL A&Ntilde;O</option>
	<option<?	if($sel_month == '1') echo(' selected');	?> value="1">Enero</option>
	<option<?	if($sel_month == '2') echo(' selected');	?> value="2">Febrero</option>
	<option<?	if($sel_month == '3') echo(' selected');	?> value="3" >Marzo</option>
	<option<?	if($sel_month == '4') echo(' selected');	?> value="4" >Abril</option>
	<option<?	if($sel_month == '5') echo(' selected');	?> value="5" >Mayo</option>
	<option<?	if($sel_month == '6') echo(' selected');	?> value="6" >Junio</option>
	<option<?	if($sel_month == '7') echo(' selected');	?> value="7" >Julio</option>
	<option<?	if($sel_month == '8') echo(' selected');	?> value="8" >Agosto</option>
	<option<?	if($sel_month == '9') echo(' selected');	?> value="9" >Septiembre</option>
	<option<?	if($sel_month == '10') echo(' selected');	?> value="10" >Octubre</option>
	<option<?	if($sel_month == '11') echo(' selected');	?> value="11" >Noviembre</option>
	<option<?	if($sel_month == '12') echo(' selected');	?> value="12" >Diciembre</option>
</select>

<select name="year">

<?	for($i=date('Y')-5; $i<=date('Y')+5; $i++){
		echo '<option value="'.$i.'"'.(($sel_year == $i) ? ' selected' : '').'>'.$i.'</option>';
	}	?>

</select>

<select name="user">
	<option value="0">TODOS LOS USUARIOS</option>
<?	foreach($users as $user){	?>
	<option<?=(($sel_user==$user->get_id()) ? " selected" : "")	?> value="<?=$user->get_id();	?>"><?=$user->get_username();	?></option>
<?	}	?>
</select>

<select name="client">
	<option value="0">TODOS LOS CLIENTES</option>
<?	foreach($clients as $client){	?>
	<option<?=(($sel_client==$client->get_id()) ? " selected" : "")	?> value="<?=$client->get_id();	?>"><?=$client->get_name();	?></option>
<?	}	?>
</select>



<button>Filtrar</button>



<!--
	<a class="G_cs_filter_more_options" href="javascript:G_JF_filter_more_options('G_ME_filter_more_options');">m&aacute;s opciones</a>

	<div class="G_cs_filter_more_options" id="G_ME_filter_more_options">
	
		<select name="user">
			<option value="0">TODOS LOS USUARIOS</option>
<?	foreach($users as $user){	?>
			<option<?=(($sel_user==$user->get_id()) ? " selected" : "")	?> value="<?=$user->get_id();	?>"><?=$user->get_username();	?></option>
<?	}	?>
		</select>
		
	</div>-->

</form>
</div>

<br clear="all" />

<?	if(mysql_num_rows($result)>0){	?>

<!--<div class="G_CS_print_export">
	<a href="javascript:G_JF_print('G_ME_time_tracker_log_print', 'TimeTracker Log');">Imprimir</a>
</div>-->

<div id="G_ME_time_tracker_log_print">
	
	<table class="G_CS_data_grid" cellspacing="0">

		<tr>
			<th class="G_CS_data_grid_left_top_corner">Cliente</th>
			<th>Tiempo</th>
			<th>Usuario</th>
			<th class="G_CS_data_grid_right_top_corner">Fecha</th>
		</tr>

	<?	$seconds_acc=0;

		while($row=mysql_fetch_array($result)){

			$user=new user;
			$client=new client;
			
			$ref_type=$row["ref_type"];
			
			if($ref_type=="client"){
				$client->set_id($row["ref_id"]);	
				$client_name=$client->get_name();
			}else{
				$client_name="-";
			}
			
			$user->set_id($row["user"]);
			
			$timestamp=$row["date"];
			$elapsed=$row["elapsed"];
			
			$seconds = $elapsed;
			$seconds_acc += $seconds;
			
			$time = secondsToWords($seconds);
			
			$date=date_formatter($timestamp,1);

				?>
		<tr>
			<td><?=$client_name;	?></td>
			<td><?=$time;	?></td>
			<td><?=$user->get_username();	?></td>
			<td><?=$date;	?></td>
		</tr>
		
	<?	}

		$time_acc = secondsToWords($seconds_acc);	?>

		<tr class="G_CS_data_grid_outside">
			<td></td>
			<td class="G_CS_data_grid_big"><?=$time_acc;	?></td>
			<td></td>
			<td></td>
		</tr>

	</table>
</div>
<?	}	?>