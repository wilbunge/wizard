<?	
include_once("../../classes/chart.php");
include_once("../../classes/master.php");
include_once("../../includes/functions.php");
include_once("../../includes/path_data.php");

include("../../includes/config.php");

include("../../classes/client.php");
include("../../classes/green_module.php");	?>

<?	$report = $_GET['report'];
	
	if($report == ''){	?>

<ul>
	<li><a href="#" onclick="G_jsf_go('timetracker', 'reports', {report:3}, 'container_2');"><?=translate("timetracker","reports/txt_time_by_user"); ?></a></li>
</ul>

<div id="graph"></div>

<?	}else{

		
		
		if($year == NULL) $year = date('Y');
		$master = $ai_master;

		switch($report){
		
			case "3" :
				
				$title=translate("timetracker","reports/txt_time_by_user")." (horas)";
				$chart=new chart;

				$chart->type="pie";
				$chart->limit=10;
				
				$chart->load_data("user,SUM(elapsed)/60/60 AS total","timetracker","TRUE","GROUP BY user ORDER BY COUNT(*) DESC");			
				
				foreach($chart->data_x as $id){					
					$user=new user($id);
					array_push($chart->data_label_x, $user->get_username());					
				}
				
				break;
		
		}	?>

<h2><?=$title;	?></h2>
<?	$chart->draw();	?>
	
<?	}	?>		