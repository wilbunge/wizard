<?	include('../../wizard');

	wizard::_include('wizard_user');
	wizard::_include('wizard_module');

	$access = $_POST['access'];

	$wizard_user = new wizard_user($_POST['user']);
	$wizard_module = new wizard_module($_POST['module']);

	$response = Array();

	if(!$wizard_user_module = $wizard_user->set_access_to_module($wizard_module, $access)){
		$response['success'] = false;
	}else{
		$response['success'] = true;
		$response['result'] = $wizard_user_module->to_array();
	}

	$json = json_encode($response);

	echo $json;	?>