<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('wizard_user');

	$template = new template('templates/edit');
	$template2 = new template('templates/user_form');

	
	if($_POST!=null){

		$wizard_user = new wizard_user($_POST['id']);

		$wizard_user->set_data($_POST);
		$template2->replace($wizard_user);

		$validation = $wizard_user->validate();

		if($validation->success){

			$wizard_user->save();

			$template = new template('templates/message');
			$template->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
			$template->render();

			die;

		}else{

			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($wizard_user);

		}
		
	}else{
		$wizard_user = new wizard_user($_GET['id']);
		$template2->set_form_values($wizard_user);
	}

	$template->replace('ID', $wizard_user->id);
	$template->replace('USER_FORM', $template2->html());

	$template->render();	?>