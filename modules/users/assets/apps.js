$(function(){

	var module = wizard.modules.users;

	module.$.find('input[type=checkbox]').click(
		function(){
			toggleAccess($(this));
		}
	);

});

toggleAccess = function($checkbox){

	var user_id = $checkbox.attr('w-user-id');
	var module_id = $checkbox.attr('w-module-id');
	var access = $checkbox.prop('checked');

	$checkbox.prop('disabled', true);

	setAccess(user_id, module_id, access,

		function(r){

			if(!r.success){
				alert('error');
				console.log(r);
			}else{

				if(r.result.wizard_user_module.access=='1'){ // Horrible!
					$checkbox.parent().addClass('bg-light-grey');
				}else{
					$checkbox.parent().removeClass('bg-light-grey');
				}

			}

			$checkbox.prop('disabled', false);

			
		}

	);

}

setAccess = function(user, module, access, callback){

	$.ajax({
		url: 'modules/users/set_app_access',
		data: {user: user, module: module, access: access?1:0},
		type: 'POST',
		dataType: 'JSON',
		success: function(r){
			callback(r);
		},
		error: function(r){
			callback(r);
		}

	});

}