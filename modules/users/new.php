<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');

	$template = new template('templates/new');
	$template2 = new template('templates/user_form');

	if($_POST!=null){

		wizard::_include('wizard_user');
		
		$wizard_user = new wizard_user;

		$wizard_user->set_data($_POST);

		$template2->replace('EMAIL', $wizard_user->get('email'));

		$validation = $wizard_user->validate();

		if($validation->success){

			$wizard_user->save();

			$template = new template('templates/message');
			$template->replace('MESSAGE', 'El usuario ha sido ingresado con éxito!');
			$template->render();

			die;

		}else{

			$template->prepend('ERROR!');


		}
		
	}

	$template->replace('USER_FORM', $template2->html());

	$categories = master::get('category');

	$template->replace('CATEGORY_MENU', html::select('category', $categories));

	$template->render();	?>