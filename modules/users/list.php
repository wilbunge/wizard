<?	include_once('../../wizard');

	wizard::_include('user');
	wizard::_include('category');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	//data_manager::$debug_mode=true;

	$template = new template('templates/list');

	$users = wizard::get('user', 'NOT `deleted`', 'ORDER BY id DESC');

	if($users){
		
		foreach($users as $user){

			$portion = $template->portion('USER');

			$portion->replace('EMAIL', $user->get('email'));
			$portion->replace('ID', $user->id);

			$template->append($portion);

		}

	}

	$template->clear('USER');

	$template->render();

	//wizard::html($template->html());	?>