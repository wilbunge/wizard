<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('inventory_movement');
	wizard::_include('inventory_item');
	wizard::_include('html');

	if($_POST!=null){

		$inventory_movement = new inventory_movement;

		$inventory_movement->set_data($_POST);
		$inventory_movement->save();

		$template = new template('templates/new_success');
		$template->render();

		die;
	}

	$template = new template('templates/register_movement');
	
	// $template->replace('FORM', $form->html());

	$inventory_items = master::get('inventory_item');

	$template->replace('INVENTORY_ITEMS_MENU', html::select("name=inventory_item", $inventory_items));

	$template->render();

	//wizard::html($template->html());	?>