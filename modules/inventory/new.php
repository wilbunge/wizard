<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('stock_item');
	wizard::_include('html');

	if($_POST!=null){

		wizard::_include('category');

		$stock_item = new stock_item;

		$stock_item->set_data($_POST);
		$stock_item->save();

		$template = new template('templates/new_success');
		$template->render();

		die;
	}

	$template = new template('templates/new');
	$form = new template('templates/item_form');

	$template->replace('FORM', $form->html());

	$clients = wizard::get('client');
	$products = wizard::get('product');

	$template->replace('CLIENT', html::select("name=client", $clients));
	$template->replace('PRODUCTS', html::select("name=product", $products));

	$template->render();

	//wizard::html($template->html());	?>