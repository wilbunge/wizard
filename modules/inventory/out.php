<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('inventory_movement');
	wizard::_include('inventory_item');
	wizard::_include('html');

	if($_POST!=null){

		$inventory_movement = new inventory_movement;

		$inventory_movement->set_data($_POST);

		$inventory_movement->set('amount', -$_POST['amount']);

		$inventory_movement->save();

		$template = new template('templates/new_success');
		$template->render();

		die;
	}

	$template = new template('templates/out');
	$template_form = new template('templates/movement_form');
	
	// $template->replace('FORM', $form->html());

	$inventory_items = master::get('inventory_item');

	$template_form->replace('INVENTORY_ITEMS', html::select("name=inventory_item", $inventory_items));
	$template->replace('FORM', $template_form->html());

	$template->render();

	//wizard::html($template->html());	?>