<?	include_once('../../wizard');

	wizard::_include('inventory_item');
	wizard::_include('invoice');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	// wizard::generate_db('inventory_movement');

	$template = new template('templates/movements');

	$inventory_movements = master::get('inventory_movement');

	if($inventory_movements){
		
		foreach($inventory_movements as $inventory_movement){

			$portion = $template->portion('INVENTORY_MOVEMENT');

			$origin = $inventory_movement->get_origin();

			$portion->replace('NAME', $inventory_movement->get('inventory_item')->get('product')->get('name_code'));
			$portion->replace('COUNT', $inventory_movement->get('amount'));
			$portion->replace('ORIGIN', $origin);

			$template->append($portion);

		}

	}

	$template->clear('INVENTORY_MOVEMENT');

	$template->render();

	//wizard::html($template->html());	?>