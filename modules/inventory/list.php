<?	include_once('../../wizard');

	wizard::_include('inventory_item');
	wizard::_include('invoice');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	// wizard::generate_db('inventory_movement');

	$template = new template('templates/list');

	inventory_item::check_products();

	$inventory_items = master::get('inventory_item');

	if($inventory_items){
		
		foreach($inventory_items as $inventory_item){

			$portion = $template->portion('INVENTORY_ITEM');

			$portion->replace('ID', $inventory_item->id);
			$portion->replace('NAME', $inventory_item->get('product')->get_name_code());
			$portion->replace('COUNT', $inventory_item->count());

			$template->append($portion);

		}

	}

	$template->clear('INVENTORY_ITEM');

	$template->render();

	//wizard::html($template->html());	?>