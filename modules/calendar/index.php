<link href='modules/calendar/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='modules/calendar/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='modules/calendar/lib/moment.min.js'></script>
<script src='modules/calendar/fullcalendar/fullcalendar.min.js'></script>
<script src='modules/calendar/fullcalendar/gcal.js'></script>

<div id='calendar'></div>