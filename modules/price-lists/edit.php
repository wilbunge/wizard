<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('html');


	if($_POST!=null){

		// master::generate_db('price_list');

		$price_list = new price_list($_POST['id']);

		$price_list->set_data($_POST);
		$price_list->save();

		//$template = new template();
		//$template->render();

		echo wizard::msg('SAVE_CHANGES_OK');

		die;
	}else{
		$price_list = new price_list($_GET['id']);
	}

	$template = new template('templates/edit');
	$form = new template('templates/price_list_form');

	$products = wizard::get('product');

	foreach ($products as $product) {
		$portion = $form->portion('PRODUCT');
		
		$portion->replace('PRODUCT', $product->id);
		$portion->replace('ORIGINAL_PRICE', $product->get('price'));
		$portion->replace('NAME', $product->get_name_code());
		$portion->replace('N', ++$n);

		$price_list_product = $price_list->get_price_list_product($product);

		if($price_list_product){
			$portion->replace('ID', $price_list_product->id);
			$portion->replace('PRICE', $price_list_product->get('price'));
		}else{
			$portion->replace('ID', 0);
			$portion->replace('PRICE', '');
		}

		$form->append($portion);
	}

	$form->clear('PRODUCT');

	$form->set_form_values($price_list);

	$template->replace('FORM', $form);
	$template->replace('ID', $price_list->id);

	$template->render();

	//wizard::html($template->html());	?>