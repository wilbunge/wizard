<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('html');

	if($_POST!=null){

		master::generate_db('price_list');

		wizard::_include('category');

		$price_list = new price_list;

		$price_list->set_data($_POST);
		$price_list->save();

		$t_message = new template('templates/message');
		$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
		$t_message->render();

		die;
	}

	$template = new template('templates/new');
	$form = new template('templates/price_list_form');

	$clients = wizard::get('client');
	$products = wizard::get('product');

	foreach ($products as $product) {
		$portion = $form->portion('PRODUCT');
		$portion->replace('PRODUCT', $product->id);
		$portion->replace('PRICE', $product->get('price'));
		$portion->replace('NAME', $product->get_name_code());
		$portion->replace('N', ++$n);
		$form->append($portion);
	}

	$form->clear('PRODUCT');

	$template->replace('FORM', $form);
	$template->replace('PRODUCTS', html::select("name=product", $products));

	$template->render();

	//wizard::html($template->html());	?>