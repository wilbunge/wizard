<?	include_once('../../wizard');

	wizard::_include('client');
	wizard::_include('invoice');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	$template = new template('templates/list');

	$price_lists = wizard::get('price_list');

	if($price_lists){
		
		foreach($price_lists as $price_list){

			$portion = $template->portion('PRICE_LIST');

			$portion->replace('NAME', $price_list);
			$portion->replace('ID', $price_list->id);
			$portion->replace('DEFAULT', $price_list->get('is_default')?'✔':'');

			$template->append($portion);

		}

	}

	$template->clear('PRICE_LIST');

	$template->render();

	//wizard::html($template->html());	?>