<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('html');

	$price_list = new price_list($_GET['id']);


	$template = new template('templates/view');
	
	$template->replace($price_list);

	$template->replace('NAME', $price_list->get('name'));

	$products = wizard::get('product');

	foreach ($products as $product) {

		$portion = $template->portion('PRODUCT');
		
		$portion->replace('PRODUCT', $product->id);
		$portion->replace('ORIGINAL_PRICE', $product->get('price'));
		$portion->replace('PRODUCT_NAME', $product->get_name_code());
		$portion->replace('N', ++$n);

		$price_list_product = $price_list->get_price_list_product($product);

		if($price_list_product){
			$portion->replace('ID', $price_list_product->id);
			$portion->replace('PRODUCT_PRICE', $price_list_product->get('price'));
		}else{
			$portion->replace('ID', 0);
		}

		$template->append($portion);
	}

	$template->clear('PRODUCT');
	
	$template->replace('ID', $price_list->id);

	$template->render();

	//wizard::html($template->html());	?>