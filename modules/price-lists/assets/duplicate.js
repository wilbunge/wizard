$(function(){

	var module = wizard.modules['price-lists'];
	var $discount = module.$.find('input[name=discount]');

	$inputs = module.$.find('input[data-original-price]');

	console.log($inputs);

	$discount.change(
		function(){
			var discount = $(this).val();
			updatePrices(discount);
		}
	);

	
	updatePrices = function(discount){

		$.each(
			$inputs,
			function(i,o){

				var original_price = Math.floor($(o).attr('data-original-price'));
				var total_discount = original_price-discount*original_price/100;

				console.log(total_discount);

				$(o).val(total_discount);
			}
		);


	}



});