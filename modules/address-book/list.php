<?	include_once('../../wizard');

	wizard::_include('contact');
	wizard::_include('category');
	wizard::_include('template');
	
	//$wizard_contact = wizard::get_wizard_contact();

	//data_manager::$debug_mode=true;

	$template = new template('templates/list');

	$contacts = wizard::get('contact', 'NOT `deleted`', 'ORDER BY id DESC');

	if($contacts){
		
		foreach($contacts as $contact){

			$portion = $template->portion('CONTACT');

			$portion->replace('NAME', $contact);
			$portion->replace('ID', $contact->id);

			$template->append($portion);

		}

	}

	$template->clear('CONTACT');

	$template->render();

	//wizard::html($template->html());	?>