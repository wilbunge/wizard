<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');

	$template = new template('templates/new');
	$template2 = new template('templates/contact_form');

	if($_POST!=null){

		wizard::_include('contact');

		// master::generate_db('contact');
		
		$contact = new contact;

		$contact->set_data($_POST);

		$template2->replace('EMAIL', $contact->get('email'));

		$validation = $contact->validate();

		if($validation->success){

			$contact->save();

			$template = new template('templates/message');
			$template->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
			$template->render();

			die;

		}else{

			$template->prepend('ERROR!');


		}
		
	}

	$template->replace('CONTACT_FORM', $template2->html());

	$template->render();	?>