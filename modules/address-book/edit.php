<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('contact');

	$template = new template('templates/edit');
	$template2 = new template('templates/contact_form');

	
	if($_POST!=null){

		$contact = new contact($_POST['id']);

		$contact->set_data($_POST);
		$template2->replace($contact);

		$validation = $contact->validate();

		if($validation->success){

			$contact->save();

			$template = new template('templates/message');
			$template->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
			$template->render();

			die;

		}else{

			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($contact);

		}
		
	}else{
		$contact = new contact($_GET['id']);
		$template2->set_form_values($contact);
	}

	$template->replace('ID', $contact->id);
	$template->replace('NAME', $contact);
	$template->replace('CONTACT_FORM', $template2->html());

	$categories = master::get('category');

	$template->replace('CATEGORY_MENU', html::select('category', $categories));

	$template->render();	?>