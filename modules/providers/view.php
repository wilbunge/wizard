<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('importation');
	wizard::_include('category');
	wizard::_include('product');

	$template = new template('templates/view');

	$importation = new importation($_GET['id']);

	//data_manager::$debug_mode=true;

	$template->replace('ID', $importation->id);
	$template->replace('DATE', $importation->get('date'));
	$template->replace('TOTAL', $importation->get('total'));
	
	$template->replace($client);

	foreach($importation->get('products') as $importation_product){
		$portion = $template->portion('PRODUCT');
		$portion->replace('PRODUCT_NAME', $importation_product->get('product'));
		$portion->replace('IMPORTATION_AMOUNT', $importation_product->get('amount'));
		$template->append($portion);
	}

	$template->clear('PRODUCT');

	$template->render();

	//wizard::html($template->html());	?>