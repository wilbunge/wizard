<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('expense');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('html');

	$expense = new expense;

	// wizard::generate_db('expense');

	if($_POST!=null){

		$expense->set_data($_POST);
		$expense->save();

		$t_message = new template('templates/message');
		$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
		$t_message->render();

		die;
	}


	$template = new template('templates/new_expense');
	$form = new template('templates/expense_form');

	$form->replace('PRICE_LIST_MENU', html::select('name="price_list"', wizard::get('price_list')));

	$form->replace($expense);
	// $form->replace('DESCRIPTION', '');

	$template->replace('FORM', $form->html());

	$template->render();

	//wizard::html($template->html());	?>