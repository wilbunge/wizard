<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('freelancer');
	wizard::_include('project');
	wizard::_include('salesman');
	wizard::_include('client');

	$template = new template('templates/edit');
	$template2 = new template('templates/form');

	// master::generate_db('client');
	
	if($_POST!=null){

		$client = new project($_POST['id']);

		$client->set_data($_POST);
		$template2->replace($client);

		// $validation = $client->validate();

		if(true){

			$client->save();

			$template = new template('templates/message');
			$template->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
			$template->render();

			die;

		}else{

			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($client);

		}
		
	}else{
		$client = new project($_GET['id']);
		$template2->set_form_values($client);
	}

	$clients = wizard::get('client');
	$freelancers = wizard::get('freelancer');


	$template2->replace('CLIENT', html::select('name="client"', $clients, $client->get('client')->id));
	$template2->replace('PM', html::select('name="pm"', $freelancers, $client->get('pm')->id));
	$template2->replace('FL', html::select('name="fl"', $freelancers, $client->get('fl')->id));

	$template->replace('ID', $client->id);
	$template->replace('NAME', $client);
	$template->replace('CLIENT_FORM', $template2->html());

	$template->render();	?>