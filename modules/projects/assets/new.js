$(function(){

	var module = wizard.modules.projects;
	var $date = module.$.find('input[name=delivery_date]');

	module.$.find("input[name='delivery_date_alt']").datepicker({dateFormat: 'dd/mm/yy', altFormat: 'yy-mm-dd', altField: $date, /*showOn: 'button',*/ buttonImage: 'images/calendar.gif', buttonImageOnly: true});	

});