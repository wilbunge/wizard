<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('freelancer');
	wizard::_include('project');
	wizard::_include('category');
	wizard::_include('client');
	wizard::_include('html');

	// error_reporting(E_ALL);
	// ini_set('display_errors', 'On');




	$project = new project;

	if($_POST!=null){

		$project->set_data($_POST);
		$project->set('status', 2);
		$project->save();

		
		$t_message = new template('templates/message');
		$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
		$t_message->render();

		die;
	}


	$template = new template('templates/new');
	$form = new template('templates/form');

	$clients = wizard::get('client');
	$freelancers = wizard::get('freelancer');

	$form->replace('CLIENT', html::select("name=client", $clients));
	$form->replace('PM', html::select("name=pm", $freelancers));
	$form->replace('FL', html::select("name=fl", $freelancers));

	$form->replace($project);

	$template->replace('FORM', $form->html());

	$template->render();

	//wizard::html($template->html());	?>