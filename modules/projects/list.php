<?	include_once('../../wizard');

	wizard::_include('project');
	wizard::_include('category');
	wizard::_include('template');
	wizard::_include('price_list');
	wizard::_include('html');

	// wizard::generate_db('project');
	
	//$wizard_user = wizard::get_wizard_user();

	$query = $_POST['query'];
	$wc = "`title` LIKE '%$query%'";

	$status = $_POST['status'];
	if(!$status) $status = 2;

	$wc = 'TRUE';
	
	if($status){
		$wc .= " AND `status`='$status'";
	}

	$status_list = array('New', 'Ongoing', 'Done', 'Cancelled');


	$template = new template('templates/list');

	$template->replace('QUERY_VALUE', $query);
	$template->replace('SELECT_STATUS', html::select('name="status"', $status_list, $status));

	$projects = wizard::get('project', $wc.' AND NOT `deleted`','ORDER BY delivery_date');

	if($projects){
		
		foreach($projects as $project){

			// $payout = number_format($project->get('payout'),0,',','.');

			$payout = $project->get('payout');

			$payout_total += $payout;

			$portion = $template->portion('PROJECT');
			$portion->replace('STATUS', $project->get_status_name());
			$portion->replace('TITLE', $project->get('title'));
			$portion->replace('$', $payout);
			$portion->replace('DELIVERY_DAYS', $project->delivery_days());
			$portion->replace('ID', $project->id);
			

			$template->append($portion);

		}


		$template->replace('$$', $payout_total);

	}

	$template->clear('PROJECT');

	$template->render();

	//wizard::html($template->html());	?>