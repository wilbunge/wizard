<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('category');
	wizard::_include('price_list');

	$template = new template('templates/statement');

	$client = new client($_GET['id']);

	//data_manager::$debug_mode=true;

	$template->replace('NAME', $client);

	$template->replace($client);
	
	$template->replace('PRICE_LIST', $client->get('price_list')->get('name'));
	$template->replace('SALESMAN', $client->get('salesman')->get('name'));

	$statement = $client->get_statement(true);
	$statement_balance = $client->get_statement_balance(true);

	foreach($statement as $statement_item){

		$portion = $template->portion('STATEMENT_ITEM');

		$portion->replace('DATE', wizard::format_date(strtotime($statement_item['date']), 1));
		$portion->replace('TOTAL', $statement_item['total']);
		$portion->replace('BALANCE', $statement_item['balance']);
		$portion->replace('ITEM_NAME', $statement_item['name']);

		$template->append($portion);

	}

	$template->clear('STATEMENT_ITEM');
	$template->replace('STATEMENT_BALANCE', $statement_balance);

	$template->render();

	//wizard::html($template->html());	?>