<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('category');
	wizard::_include('project');
	wizard::_include('javascript');

	$template = new template('templates/view');
	$js = new javascript;

	$project = new project($_GET['id']);

	if($project->get('status')!='1') $template->clear('START');

	//data_manager::$debug_mode=true;

	$template->replace('TITLE', $project);
	$template->replace('ID', $project->id);
	$template->replace('STATUS', $project->get_status_name());

	$template->replace($client);
	
	// $template->replace('PRICE_LIST', $client->get('price_list')->get('name'));
	// $template->replace('SALESMAN', $client->get('salesman')->get('name'));


	$template->render();

	$status = $project->get('status');

	$js->code("wizard.modules.projects.$.find('input[value=$status]').attr('checked',true)");
	$js->code("wizard.modules.projects.$.find('input[name=status]').click(function(){ wizard.modules.projects.$.find('form').submit(); });");
	echo $js->output();



	//wizard::html($template->html());	?>