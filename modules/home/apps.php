<?	//error_reporting(E_ALL);

	include('../../wizard');

	wizard::_include('template');
	wizard::_include('wizard_user');

	$template = new template('templates/apps');

	$wizard_modules = wizard::get_account()->get_modules();
	
	$modules = wizard::get_user()->get_my_modules();

	if($modules){

		foreach ($modules as $module) {



			$portion = $template->portion('APP');
			$portion->replace('APP_ID', $module->id);
			$portion->replace('APP_KEYWORD', $module->get('keyword'));
			$portion->replace('APP_NAME', $module);
			$portion->replace('APP_CHECKED', $module->in_quick_menu(wizard::get_user())?'checked':'');


			$template->append($portion);
		}

		$template->clear('APP');

	}

	$template->render();	?>