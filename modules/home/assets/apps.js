$(function() {

	var config = Array();
	var module = wizard.modules.home;

	module.$.find('input[name=app]').each(
		function(i,o){

			var $input = $(o);

			$input.click(
				function(){
					toggleAccess($(this));
				}
			);

		}
	);

	module.$.find('a').click(
		function(){

			var path = $(this).attr('w-app-path');
			wizard.openModule(path);


		}
	);


	toggleAccess = function($checkbox){

		var module_id = $checkbox.attr('w-module-id');
		var mode = $checkbox.prop('checked');

		$checkbox.prop('disabled', true);

		setAccess(module_id, mode,

			function(r){

				if(!r.success){
					alert('error');
					console.log(r);
				}else{

					

				}

				$checkbox.prop('disabled', false);

				wizard.getBottomMenu();

				
			}

		);

	}

	setAccess = function(module, mode, callback){

		$.ajax({
			url: 'modules/home/set_quick_menu',
			data: {module: module, mode: mode?1:0},
			type: 'POST',
			dataType: 'JSON',
			success: function(r){
				callback(r);
			},
			error: function(r){
				callback(r);
			}

		});

	}


})