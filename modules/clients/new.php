<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('salesman');
	wizard::_include('client');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('html');

	$client = new client;

	if($_POST!=null){

		$client->set_data($_POST);
		$client->save();

		$t_message = new template('templates/message');
		$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
		$t_message->render();

		die;
	}


	$template = new template('templates/new');
	$form = new template('templates/client_form');

	$price_lists = wizard::get('price_list');
	$salesforce = wizard::get('salesman');

	$form->replace('PRICE_LIST_MENU', html::select('name="price_list"', wizard::get('price_list')));
	$form->replace('SALESFORCE_MENU', html::select('name="salesman"', $salesforce, $client->get('salesman')->id));

	$form->replace($client);

	$template->replace('FORM', $form->html());

	$template->render();

	//wizard::html($template->html());	?>