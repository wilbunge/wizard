<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('salesman');
	wizard::_include('client');

	$template = new template('templates/edit');
	$template2 = new template('templates/client_form');

	// master::generate_db('client');
	
	if($_POST!=null){

		$client = new client($_POST['id']);

		$client->set_data($_POST);
		$template2->replace($client);

		// $validation = $client->validate();

		if(true){

			$client->save();

			$template = new template('templates/message');
			$template->replace('MESSAGE', wizard::msg('SAVE_CHANGES_OK'));
			$template->render();

			die;

		}else{

			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($client);

		}
		
	}else{
		$client = new client($_GET['id']);
		$template2->set_form_values($client);
	}

	$price_lists = wizard::get('price_list');
	$salesforce = wizard::get('salesman');

	$template2->replace('PRICE_LIST_MENU', html::select('name="price_list"', $price_lists, $client->get('price_list')->id));
	$template2->replace('SALESFORCE_MENU', html::select('name="salesman"', $salesforce, $client->get('salesman')->id));

	$template->replace('ID', $client->id);
	$template->replace('NAME', $client);
	$template->replace('CLIENT_FORM', $template2->html());

	$template->render();	?>