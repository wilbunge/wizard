<?	include_once('../../wizard');

	wizard::_include('client');
	wizard::_include('category');
	wizard::_include('template');
	wizard::_include('price_list');
	
	//$wizard_user = wizard::get_wizard_user();

	$query = $_POST['query'];
	$wc = "`company_name` LIKE '%$query%'";
	

	$template = new template('templates/statements');

	$template->replace('QUERY_VALUE', $query);

	$clients = wizard::get('client', $wc.' AND NOT `deleted`');

	if($clients){
		
		foreach($clients as $client){

			$portion = $template->portion('CLIENT');

			$portion->replace('NAME', $client->get('company_name'));
			$portion->replace('ID', $client->id);
			$portion->replace('BALANCE', $client->get_statement_balance(true));
			

			$template->append($portion);

		}

	}

	$template->clear('CLIENT');

	$template->render();

	//wizard::html($template->html());	?>