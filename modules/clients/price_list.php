<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('category');
	wizard::_include('price_list');
	wizard::_include('product');

	if($_GET['client']==null){
		$response['success'] = false;
		$response['error'] = 'No client ID provided.';
	}else{

		$client = new client($_GET['client']);

		$all_products = wizard::get('product', 'NOT deleted');

		$response = array();
		$price_list_ids = array();

		$price_list = $client->get('price_list');

		$response['success'] = true;

		if($price_list->id==0) $price_list = price_list::get_default_price_list();

		if($price_list->id>0){

			$response['result']['name'] = $price_list->get('name');

			foreach($price_list->get_products() as $p){
				
				$price_list_ids[$p->get('product')->id] = $p->get('price');				

				$response['result']['products'][] = array(
					'product' => $p->get('product')->id,
					'price' => $p->get('price')
				);
			}

		}

		foreach($all_products as $p){
			
			if($price_list_ids[$p->id] == null){ // Not included in pricelist
				$response['result']['products'][] = array(
					'product' => $p->id,
					'price' => $p->get('price')
				);
			}

		}

	}

	$json = json_encode($response);

	echo $json;	?>