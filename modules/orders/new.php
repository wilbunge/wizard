<?	include('../../wizard');

	wizard::_include('html');
	wizard::_include('javascript');
	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('order');
	wizard::_include('invoice');
	wizard::_include('salesman');
	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('price_list');

	$order = new order;

	//wizard::generate_db('order_product');

	if($_POST!=null){

		$order->set_data($_POST);

		$invoice_number = $_POST['invoice_number'];
		$invoice_type = $_POST['invoice_type'];

		$order->save();

		if($invoice_type != '0') $order->generate_invoice($invoice_number, $_POST['invoice_type']);

		// $order->update_inventory();

		$template = new template('templates/message');
		$template->replace('MESSAGE', 'La venta se ha ingresado con éxito!');
		$template->render();

		die;
	}


	$javascript = new javascript;

	$javascript->code('wizard.modules.orders.products = [];');


	$template = new template('templates/new');
	$form = new template('templates/order_form');

	$form->replace('INVOICE_NUMBER', invoice::get_next_number());

	$form->replace($client);

	$clients = wizard::get('client');
	$salesforce = wizard::get('salesman');
	$products = wizard::get('product');

	$form->replace('DATE', '');	
	$form->replace('CLIENT', html::select("name=client", $clients));	
	$form->replace('SALESMAN', html::select("name=salesman", $salesforce));

	$arr = array();

	$select = "<option>...</option>";
	foreach($products as $product){
		$select .= "<option value='$product->id' w-price='".$product->get('price')."'>$product</option>";


		$arr[] = Array(
			"label" => $product->get_name_code(),
			"value" => $product->get_name_code(),
			"id" => $product->id
		);

		
	}

	$javascript->code('wizard.modules.orders.products = '. json_encode($arr));

	$select = "<select name='products'>$select</select>";

	// $form->replace('PRODUCTS', $select);

	$template->replace('FORM', $form->html());

	$template->render();

	echo $javascript->output();

	//wizard::html($template->html());	?>