<?	include_once('../../wizard');

	wizard::_include('client');
	wizard::_include('price_list');
	wizard::_include('category');
	wizard::_include('template');
	wizard::_include('salesman');
	wizard::_include('html');
	
	//$wizard_user = wizard::get_wizard_user();

	$template = new template('templates/list');

	$months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	$selected_month = $_POST['month'];
	$selected_client = $_POST['client'];
	$selected_year = date('Y');

	if($selected_month == '') $selected_month = date('m');

	$wc = 'TRUE';

	if($selected_client!='0' && $selected_client != '') $wc .= " AND client = '$selected_client'";
	if($selected_month!='0') $wc .=  " AND MONTH(DATE) = '$selected_month' AND YEAR(DATE) = '$selected_year'";

	$orders = wizard::get('order', 'NOT `deleted` AND '.$wc);

	$clients = wizard::get('client');

	$template->replace('SELECT_CLIENT', html::select("name=client", $clients, $selected_client));
	$template->replace('SELECT_MONTH', html::select('name="month"', $months, $selected_month));


	if($orders){
		
		foreach($orders as $order){

			$portion = $template->portion('ORDER');

			$curr = $order->get('currency');

			$portion->replace('CLIENT', $order->get('client'));
			$portion->replace('DATE', $order->get('date'));
			
			$portion->replace('ID', $order->id);

			$curr_value = $order->get('currency_value');

			if($curr_value == null || $curr_value == '' || $curr_value == 0 || is_nan($curr_value)) $curr_value = 1;

			if($curr == 2){
				$grand_total_2+=$order->get('total');
				
				$portion->replace('TOTAL_2', $order->get_total_f(true));

				$total_2 = $order->get('total');

				if($curr_value == 1){
					$portion->replace('TOTAL', '-');
				}else{

					$total = ($total_2 / $curr_value);

					$portion->replace('TOTAL', order::format_total($total));
					
					$grand_total+=$total;
				}

				

			}else{

				$grand_total+=$order->get('total');
				$portion->replace('TOTAL', $order->get_total_f(true));
				$portion->replace('TOTAL_2', '-');

			}
			

			$template->append($portion);

		}

	}

	$template->clear('ORDER');

	$template->replace('GRAND_TOTAL', order::format_total($grand_total));
	$template->replace('GRAND_TOTAL_2', order::format_total($grand_total_2));

	$template->render();

	//wizard::html($template->html());	?>