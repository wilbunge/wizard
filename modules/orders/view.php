<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('order');
	wizard::_include('category');
	wizard::_include('product');

	$template = new template('templates/view');

	$order = new order($_GET['id']);

	//data_manager::$debug_mode=true;

	$template->replace('ID', $order->id);
	$template->replace('CLIENT', $order->get('client'));
	$template->replace('TOTAL', $order->get_total_f(true));

	$template->replace($client);

	foreach($order->get('products') as $order_product){
		$portion = $template->portion('PRODUCT');
		$portion->replace('PRODUCT_NAME', $order_product->get('product'));
		$portion->replace('ORDER_AMOUNT', $order_product->get('order_amount'));
		$template->append($portion);
	}

	$template->clear('PRODUCT');

	$template->render();

	//wizard::html($template->html());	?>