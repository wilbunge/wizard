$(function() {

	var module = wizard.modules.orders;
	var counter=0;
	var prices = [];
	var order_products = [];

	module.subtotal = 0;
	module.tax = 0;
	module.total = 0;
	module.n = 0;
	module.items = {};

	var $date = $('input[name=date]', module.$);

	$("input[name='date_format_friendly']").datepicker({dateFormat: 'dd/mm/yy', altFormat: 'yy-mm-dd', altField: $date, /*showOn: 'button',*/ buttonImage: 'images/calendar.gif', buttonImageOnly: true});	

	var $client = $('select[name=client]', module.$);
	var $select = $('select[name=products]', module.$);
	
	var $query_products = $('input[name=product]', module.$);


	var $product_id = $('input[name=product_id]', module.$);
	var $product_name = $('input[name=product_name]', module.$);

	var $select_salesman = $('select[name=salesman]', module.$);
	var $button = $('button[value=add-product]', module.$);
	var $products = module.$.find('products');
	var $qty = $('input[name=qty]', module.$);
	var $subtotal = $('input[name=subtotal]', module.$);
	var $tax = $('input[name=tax]', module.$);
	var $total = $('input[name=total]', module.$);
	var $form = $('form', module.$);


	$query_products.autocomplete({
		source: wizard.modules.orders.products,
		minLength: 0,
		select: function( event, ui ) {
			console.log(ui);
			var id = ui.item.id;
			module.$.find("input[name=product_id]").val(id);
			module.$.find("input[name=product_name]").val(ui.item.label);
			$qty.val('');
			$qty.focus();

		}
	});

	//$products.text('xx');
	
	//$("input[name='date']").datepicker({dateFormat: 'yy-mm-dd', /*showOn: 'button',*/ buttonImage: 'images/calendar.gif', buttonImageOnly: true});	

	$client.change(
		function(){
			var client_id = $(this).val();
			module.refreshPriceList(client_id);
			setSalesman(client_id);

		}
	);
		
	$qty.keypress(
		function(e){

			if(e.keyCode!=13) return;

			addProduct();
			$query_products.focus();
			return false;
		}
	);


	
	// $query_products.change(
	// 	function(){
	// 		$qty.focus();
	// 		$qty.val('');
	// 		return false;
	// 	}
	// );
	
	$query_products.blur(
		function(){
			$qty.focus();
			return false;
		}
	);

	$button.click(
		function(){
			addProduct();
		}
	);

	refreshOrderProducts = function(){

		for(var i in order_products){
			
			var product_id = order_products[i].id;
			var product_qty = order_products[i].qty;
			var price = order_products[i].price;
			var subtotal = price*product_qty;

			$span = $products.find('span[w-order-product-index='+i+']');

			if(subtotal==null){
				$span.html('-');
			}else{

				$span.html('$'+ui.formatNumber(price*product_qty));
			}


		}

	}

	getProductPrice = function(product_id){

		var list_price = prices[product_id];

		if(list_price==null){ // No list price for this product

			$option = $select.find('option[value='+product_id+']')
			var price = $option.attr('w-price');
			if(isNaN(price)) price=0;
			return price;
		}

		if(isNaN(list_price)) list_price=0;
		
		return list_price;

	}

	addProduct = function(){

		++counter;

		var $option = $('option:selected', $select);
		
		var product_name = $product_name.val();
		var product_id = $product_id.val();

		var product_price = getProductPrice(product_id);
		var product_qty = $qty.val();
		var subtotal = product_price*product_qty;
		
		if(product_qty*product_id<1) return;

		order_products[counter] = {id: product_id, qty: product_qty, price: product_price};

		renderProduct(product_id, product_name);
		

		// Reset fields 

		$product_id.val('');
		$product_name.val('');
		$query_products.val('');
		$qty.val(0);
		
		module.items[module.n] = {};
		
		module.items[module.n].subtotal = subtotal;
		
		updateTotal();
		
	}

	renderProduct = function(product_id, product_name){

		var $option = $('option[value='+product_id+']', $select);

		
		var product_qty = $qty.val();
		var product_price = getProductPrice(product_id);
		var subtotal = product_price*product_qty;

		var $input = $('<input type="hidden" name="products['+counter+'][id]" value="'+product_id+'">');
		var $input_qty = $('<input type="hidden" name="products['+counter+'][order_amount]" value="'+product_qty+'">');
		var $input_unit_price = $('<input w-counter='+counter+' type="text" name="products['+counter+'][unit_price]" value="'+product_price+'">');

		$input_unit_price.keypress(
			function(e){

				if(e.keyCode!=13) return;

				var n = $(this).attr('w-counter');
				var v = ui.formatNumberIn($(this).val());
				
				unitPriceChanged(n,v);
				
				return false;
			}
		);

		$input_unit_price.counter = counter;

		// Event handler for unit price change
		$input_unit_price.change(
			function(counter){
				var n = $(this).attr('w-counter');
				var v = ui.formatNumberIn($(this).val());
				
				unitPriceChanged(n,v);

			}
		);
		
		var $td_inputs = $('<td>');
		
		$td_inputs.append($input);
		$td_inputs.append($input_qty);

		$td_inputs.append(product_name);

		$tr = $('<tr data-invoice-item="'+(++module.n)+'">');
		$tr.append($td_inputs);
		$tr.append('<td>'+$qty.val()+ '</td>');
		//$tr.append('<td><input type="text" name="" value="'+product_price+'" /></td>');

		$tr.append($('<td>').append($input_unit_price));

		$tr.append('<td><span w-order-product-index="'+counter+'">$'+ui.formatNumber(subtotal)+'</span></td>');
		$tr.append('<td><a data-invoice-item="'+(module.n)+'" href="#delete-order-product">eliminar</a></td>');
		
		$products.find('table').append($tr);
		
		$('a[href=#delete-order-product]', $tr).click(	
			function(){
				module.removeProduct($(this).attr('data-invoice-item'));
			}
		);

	}


	module.removeProduct = function(n){
		delete order_products[n];
		module.total-=module.items[n].subtotal;
		module.refreshTotal();
		$('tr[data-invoice-item='+n+']').fadeOut();

		updateTotal();

	}	

	module.refreshTotal = function(){

		$total.val(module.total);
	}

	module.refreshPrices = function(price_list){

		prices = [];

		if(price_list == null) return;

		for(var i in price_list.products){

			var product_id = price_list.products[i].product;
			var price = price_list.products[i].price;

			prices[product_id] = price;

		}

		console.log(prices);

	}


	module.refreshPriceList = function(client_id){

		if(client_id == null) client_id=0;

		$.ajax({
			url: 'modules/clients/price_list',
			data: {client: client_id},
			dataType: 'json',
			success: function(r){
				
				if(r.success){

					if(r.result!=null){
						updatePriceListName(r.result.name);
					}else{
						updatePriceListName('-');
					}

					module.refreshPrices(r.result);

					refreshOrderProducts();
					updateTotal();
				}

			},
			error: function(r){
				// alert('ERROR');
				console.log(r);
			}
		});

	}

	updateTotal = function(){

		module.subtotal=0;
	
		for(var i in order_products){
			
			var product_qty = order_products[i].qty;
			var product_id = order_products[i].id;
			var price = order_products[i].price;
			var subtotal = price*product_qty;

			module.subtotal+=subtotal;

		}

		var tax = module.subtotal*.22;
		module.total=module.subtotal+tax;

		$subtotal.val(ui.roundNumber(module.subtotal));
		$tax.val(ui.roundNumber(tax));
		$total.val(ui.roundNumber(module.total));

	}

	updatePriceListName = function(name, id){

		$a = $('<a href=#>');
		$a.text(name);

		$a.click(
			function(){
			//	wizard.moduleAction('price-lists', 'view');
				wizard.openModule('price-lists');
			}
		);

		module.$.find('pricelistname').html($a);

	}

	unitPriceChanged = function(n,v){

		v = parseFloat(v);

		order_products[n].price = v;
		
		refreshOrderProducts();
		updateTotal();
	}

	setSalesman = function(client_id){

		getClientSalesman(
			client_id,
			function(salesman){
				$select_salesman.val(salesman.id);
			}
		);

	}

	getClientSalesman = function(client_id, callback){

		$.ajax({
			url: 'api/client/get',
			data: {args: 'salesman', id: client_id},
			type: 'POST',
			dataType: 'json',
			success: function(r){

				if(r.success){
					callback(r.result);
				}

			},
			error: function(r){
				alert('ERROR');
				console.log(r);
			}
		});

	}


	// Preload defualt pricelist
	module.refreshPriceList();

})