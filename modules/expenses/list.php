<?	include_once('../../wizard');

	wizard::_include('html');
	wizard::_include('expense');
	wizard::_include('category');
	wizard::_include('template');
	wizard::_include('price_list');
	
	//$wizard_user = wizard::get_wizard_user();

	$template = new template('templates/list');

	$months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	$years = array(2013,2014,2015,2016,2017,2018,2019,2020);

	$selected_month = $_POST['month'];
	$selected_year = $_POST['year'];

	$selected_year_n = $_POST['year'];
	$selected_year = $years[$selected_year_n-1];

	if($selected_year_n == ''){
		$selected_year = date('Y');
		$selected_year_n = array_search($selected_year, $years)+1;
		}



	$wc = 'TRUE';

	if($selected_month=='') $selected_month = date('m');
	if($selected_month!='0') $wc .=  " AND MONTH(DATE) = '$selected_month'";
	if($selected_year!='0') $wc .=  " AND YEAR(DATE) = '$selected_year'";

	$expenses = wizard::get('expense',  'NOT `deleted` AND '.$wc);

	$template->replace('SELECT_MONTH', html::select('name="month"', $months, $selected_month));
	$template->replace('SELECT_YEAR', html::select('name="year"', $years, $selected_year_n));

	if($expenses){
		
		foreach($expenses as $expense){

			$portion = $template->portion('EXPENSE');

			$portion->replace('TOTAL', $expense->get_total_formatted());

			$portion->replace($expense);
			
			$portion->replace('ID', $expense->id);
			
			$template->append($portion);

			$total_sum += $expense->get('total');

		}

	}

	$total_sum_formatted = expense::format_total($total_sum);

	$template->replace('TOTAL_SUM', $total_sum_formatted);

	$template->clear('EXPENSE');

	$template->render();

	//wizard::html($template->html());	?>