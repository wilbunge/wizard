$(function(){

	var module = wizard.modules.expenses;
	
	var $date = $('input[name=date]', module.$);

	$("input[name='date_format_friendly']").datepicker({dateFormat: 'dd/mm/yy', altFormat: 'yy-mm-dd', altField: $date, /*showOn: 'button',*/ buttonImage: 'images/calendar.gif', buttonImageOnly: true});	

});