<?	include('../../wizard');

	wizard::_include('product');
	wizard::_include('category');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	$step = $_GET['step'] == '' ? 1 : $_GET['step'];

	$template = new template('templates/index');
	$tutorial = new template('templates/step_'.$step);

	if($step < 5){
		$template->clear('START_BUTTON');
	}else{
		$template->clear('NEXT_BUTTON');
	}

	$template->replace('CONTENT', $tutorial->html());

	$template->render();

	//wizard::html($template->html());	?>