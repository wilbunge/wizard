<?	include_once("includes/config.php");
	include_once("functions/message.php");

	include_once("classes/task.php");
	include_once("classes/master.php");

	include_once("includes/functions.php");
	include_once("includes/path_data.php");

	$task = new task();
	$master = new master();
	
	$task->catch_post_properties();
	
	if(count($_POST['worker'])>0){
	
		foreach($_POST['worker'] as $worker){
			$user = new user();
			$user->set_id($worker);
			
			$task->set_author($ai_master->get_user());
			$task->set_worker($user);
			$master->store($task);
		}
	
	}
	
	
	//	?>
<?	include("menu.php");	?>
<?=message(1, translate("common","messages/txt_data_stored_successfuly"));	?>