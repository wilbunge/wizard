<?	include("../../includes/config.php");
	include("../../functions/message.php");

	include("../../classes/expense.php");
	include("../../classes/master.php");
	include("../../classes/category.php");

	$expense = new expense();
	$master = new master();
	
	$expense->set_id($_POST['expense']);
	
	$expense->catch_post_properties();
	
	$master->update($expense);	?>

<?=message(1, 'The expense was successfully updated.');	?>