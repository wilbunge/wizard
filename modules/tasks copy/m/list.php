<?	include_once("includes/functions.php");
	include_once("includes/path_data.php");
	
	include_once("functions/date_formatter.php");

	include("classes/expense.php");
	include_once("classes/master.php");
	include("classes/category.php");
	
	include("classes/task.php");

	$master = new master;
	$user = $master->get_user();
	
	include("includes/db_connect.php");
	$green_module = new green_module("expenses");
	$green_module->generate_id();
	
	include("includes/db_connect_2.php");
	
	$arr = $master->select("task", "NOT done AND NOT deleted AND worker=".$ai_master->get_user()->get_id());	?>

<?	include("menu.php");	?>

<br clear="all" />


<!--<div class="G_CS_print_export">
	<a href="javascript:G_JF_print('G_ME_expenses_list_print', 'Lista de Egresos');"><img src="images/i_print_2.png" /></a>
</div>-->

<br clear="all">

		<ol id="G_ME_tasks_done_list">
	<?	foreach($arr as $task){	?>		
		<li class="G_CS_tasks_done"><input onclick="G_JF_tasks_set_as_done(this,<?=$task->get_id();	?>)" type="checkbox"><?=$task->get_text();	?><?	if($task->get_author()->get_id()>0){	?><span class="G_CS_tasks_author">(por <?=$task->get_author()->get_username();	?>)</span><?	}	?></li>
	<?	}	?>
	</ol>
		
<script type="text/javascript" src="js/jquery/jquery-1.4.4.min.js"></script>
<script>


function G_JF_tasks_set_as_done(p_cb,p_id){

	var li=$(p_cb).parent();
	//$(li).addClass("G_CS_tasks_done");
	//$("input[type='checkbox']",li).attr("checked","checked");
	
	//var ul=$(li).parent();
	
	$.ajax({
		url:"modules/tasks/update_status.php",
		data:"task="+p_id+"&mode=done",
		method:"POST",
		success:function(r){
			if(r=='G_OR_OK'){			
				$(li).hide();				
			}else{
				$(li).removeClass("G_CS_tasks_done");
			}
		},
		error:function(r){
			alert(r.responseText);
		}
	});
	
}

</script>