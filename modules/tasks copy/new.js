$(function() {
	$("input[name='due_date']").datepicker({
		dateFormat: 'yy-mm-dd',
		showOn: 'both',
		buttonImage: 'images/calendar.gif',
		buttonImageOnly: true
	});
});