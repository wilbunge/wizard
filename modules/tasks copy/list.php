<?	include("../../includes/db_connect_2.php");
	include("../../functions/message.php");

	include("../../classes/client.php");
	include("../../classes/task.php");
	include_once("../../classes/master.php");
	include("../../classes/green_module.php");

	include_once("../../includes/functions.php");
	include_once("../../includes/path_data.php");

	$master = new master();
	$user = new user();
	
	$arr = $master->select("task", "NOT done AND NOT deleted AND worker=".$ai_master->get_user()->get_id());
	$done_tasks = $master->select("task", "done AND NOT deleted AND worker=".$ai_master->get_user()->get_id());
	$assigned_tasks = $master->select("task", "NOT deleted AND `author`=".$ai_master->get_user()->get_id());	?>

	
<style type="text/css">
#sortable { margin: 0; padding: 0; width: 60%; }
	
#sortable li span { position: absolute; margin-left: -1.3em; }

#G_ME_tasks_done{
	float:right;
	width:50%;	
}

#G_ME_tasks{
	float:left;
	width:50%;	
}

#G_ME_tasks_done_list{
	text-decoration:line-through;	
}

.G_CS_tasks_done{
	text-decoration:line-through;
	color:#666;
}

#G_ME_tasks_assigned{
	width:100%;
}

.G_CS_tasks_author{
	padding-left:5px;
	color:#666;
	font-size:11px;
}

.G_CS_tasks_worker{
	padding-left:5px;
	color:#666;
	font-size:11px;
}

#G_ME_tasks_assigned_list li:hover a.G_CS_tasks_delete{
	display:inline;
}

.G_CS_tasks_delete{
	margin-left:4px;
	text-decoration:none!important;
}

</style>

<div id="G_ME_tasks_done">

	<h2><?=translate("tasks","home/txt_done"); ?></h2>

	<ol id="G_ME_tasks_done_list">
	<?	foreach($done_tasks as $task){	?>		
		<li class="G_CS_tasks_done"><input onclick="G_JF_tasks_set_as_not_done(this,<?=$task->get_id();	?>)" checked type="checkbox"><?=$task->get_text();	?><?	if($task->get_author()->get_id()>0){	?><span class="G_CS_tasks_author">(por <?=$task->get_author()->get_username();	?>)</span><?	}	?></li>
	<?	}	?>
	</ol>
</div>

<div id="G_ME_tasks">

<h2><?=translate("tasks","home/txt_todo"); ?></h2>

<div id="G_ME_tasks_message_no_tasks_todo"<?	if(count($arr)>0) echo ' style="display:none;"';	?>><?=message(2,"No hay tareas para mostrar",true);	?></div>
	
	<ol id="G_ME_tasks_list">
	<?	foreach($arr as $task){	?>	
		<li><input onclick="G_JF_tasks_set_as_done(this,<?=$task->get_id();	?>)" type="checkbox"><?=$task->get_text();	?><?	if($task->get_author()->get_id()>0){	?><span class="G_CS_tasks_author">(por <?=$task->get_author()->get_username();	?>)</span><?	}	?></li>	
	<?	}	?>
	</ol>
</div>

<br clear="all" />

<div id="G_ME_tasks_assigned">

<h2><?=translate("tasks","home/txt_asigned"); ?></h2>

<?	if(count($assigned_tasks)<1){	?><?=message(2,"No hay tareas para mostrar",true);	?><?	}	?>
	
	<ol id="G_ME_tasks_assigned_list">
	<?	foreach($assigned_tasks as $task){	?>	
		<li<?	if($task->get_done()) echo " class='G_CS_tasks_done'"	?>><?=$task->get_text();	?><?	if($task->get_worker()->get_id()>0){	?><span class="G_CS_tasks_worker">(a <?=$task->get_worker()->get_username();	?>)</span><?	}	?><span><a class="G_CS_tasks_delete" href="javascript:;" onclick="javascript:G_jsf_delete('tasks', {id:'<?=$task->get_id();	?>'}, this, '');"><img alt="eliminar" src="images/i_delete.png"></a></span></li>	
	<?	}	?>
	</ol>
</div>