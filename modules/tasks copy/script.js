$(function() {
	//$("#G_ME_tasks_list").sortable();
});


$("li","#G_ME_tasks_list").click(
	function(){
		
		
	}
);

function G_JF_tasks_set_as_done(p_cb,p_id){

	var li=$(p_cb).parent();

	$(li).addClass("G_CS_tasks_done");
	$("input[type='checkbox']",li).attr("checked","checked");
	
	var ul=$(li).parent();
	
	$.ajax({
		url:"modules/tasks/update_status.php",
		data:"task="+p_id+"&mode=done",
		method:"POST",
		success:function(r){
			
			if(r=='G_OR_OK'){
			
				$(li).fadeOut(
					function(){
						$("#G_ME_tasks_done_list").append("<li class='G_CS_tasks_done'><input onclick='G_JF_tasks_set_as_not_done(this,"+p_id+")' type='checkbox' checked>"+$(li).text()+"</li>").fadeIn();
					}
				);
			
				
			}else{
				$(li).removeClass("G_CS_tasks_done");
			}
		},
		error:function(r){
			alert(r.responseText);
		}
	});
	
}

function G_JF_tasks_set_as_not_done(p_cb,p_id){

	var li=$(p_cb).parent();

	$("input[type='checkbox']",li).attr("checked","");
	$(li).removeClass("G_CS_tasks_done");
	
	$.ajax({
		url:"modules/tasks/update_status.php",
		data:"task="+p_id+"&mode=not_done",
		method:"POST",
		success:function(r){
			
			if(r=='G_OR_OK'){
			
				$(li).fadeOut(
					function(){
						$("#G_ME_tasks_message_no_tasks_todo").hide();
						$("#G_ME_tasks_list").append("<li><input onclick='G_JF_tasks_set_as_done(this,"+p_id+")' type='checkbox'>"+$(li).text()+"</li>").fadeIn();
					}
				);
			
				
			}else{
				$(li).removeClass("G_CS_tasks_done");
			}
		},
		error:function(r){
			//alert(r.responseText);
		}
	});
	
}