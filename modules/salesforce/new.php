<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('salesman');
	wizard::_include('category');
	wizard::_include('html');

	// wizard::generate_db('salesman');

	// process::execute('modules/salesforce/new');

	if($_POST!=null){

		wizard::_include('category');

		$salesman = new salesman;

		$salesman->set_data($_POST);
		$salesman->save();

		$template = new template('templates/new_success');
		$template->render();

		die;
	}

	$template = new template('templates/new');
	$template2 = new template('templates/salesman_form');

	$clients = wizard::get('client');
	$salesmans = wizard::get('salesman');

	$template2->replace('CLIENT', html::select("name=client", $clients));
	$template2->replace('SALESMAN', html::select("name=salesman", $salesmans));

	$template->replace('FORM', $template2->html());

	$template->render();

	//wizard::html($template->html());	?>