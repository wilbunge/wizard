<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('salesman');
	wizard::_include('category');

	$template = new template('templates/view');

	$salesman = new salesman($_GET['id']);

	//data_manager::$debug_mode=true;

	$commission = $salesman->get('commision');

	$template->replace('ID', $salesman->get('id'));
	$template->replace('NAME', $salesman->get_name());
	$template->replace('COMMISION', $salesman->get('commision'));
	$template->replace('ORDER_COUNT', $salesman->count_orders());


	$orders = $salesman->get_orders();

	if($orders){

		$template_orders = new template('templates/orders');

		$total_commision=0;
		$total_orders=0;

		foreach ($orders as $order) {

			$order_commision = $salesman->calculate_order_commision($order, false);

			$total_commision+=$order_commision;
			$total_orders+=$order->get('total');
			
			$portion = $template_orders->portion('ORDER');

			$portion->replace('CLIENT', $order->get('client'));
			$portion->replace('DATE', $order->get('date'));
			$portion->replace('ORDER_COMMISION', $salesman->calculate_order_commision($order, true));
			$portion->replace('TOTAL', $order->get_total_f(true));
			$portion->replace('ID', $order->id);

			$portion->replace('ID', $order->id);

			$template_orders->replace('TOTAL_COMMISION', $total_commision);

			$template_orders->append($portion);

		}

		$template_orders->clear('ORDER');

		$template->replace('ORDERS', $template_orders->html());

	}else{

		$template->replace('ORDERS', '');

	}

	
	$template->render();

	//wizard::html($template->html());	?>