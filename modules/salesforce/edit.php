<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('salesman');

	$template = new template('templates/edit');
	$template2 = new template('templates/salesman_form');

	if($_POST!=null){

		$salesman = new salesman($_POST['id']);

		$salesman->set_data($_POST);
		$template2->replace($salesman);

		$validation = $salesman->validate();

		if($validation->success){

			$salesman->save();

			echo (html::message(wizard::msg('SAVE_CHANGES_OK')));
			
			die;

		}else{



			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($salesman);

		}
		
	}else{
		$salesman = new salesman($_GET['id']);
		$template2->set_form_values($salesman);
	}

	$template->replace('ID', $salesman->id);
	$template->replace('FORM', $template2->html());

	// $template->replace('CATEGORY_MENU', html::select('name="category"', $categories, $salesman->get('category')->id));

	$template->render();	?>