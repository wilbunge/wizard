<?	include_once('../../wizard');

	wizard::_include('client');
	wizard::_include('salesman');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	$template = new template('templates/list');

	$salesforce = wizard::get('salesman');

	if($salesforce){
		
		foreach($salesforce as $salesman){

			$portion = $template->portion('SALESMAN');

			// $portion->replace('CLIENT', $salesman->get('client'));
			$portion->replace('ID', $salesman->id);
			$portion->replace('NAME', $salesman);

			$template->append($portion);

		}

	}

	$template->clear('SALESMAN');

	$template->render();

	//wizard::html($template->html());	?>