<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('client');
	wizard::_include('freelancer');
	wizard::_include('category');
	wizard::_include('html');

	// wizard::generate_db('freelancer');

	// process::execute('modules/salesforce/new');

	if($_POST!=null){

		wizard::_include('category');

		$freelancer = new freelancer;

		$freelancer->set_data($_POST);
		$freelancer->save();

		$template = new template('templates/new_success');
		$template->render();

		die;
	}

	$template = new template('templates/new');
	$template2 = new template('templates/freelancer_form');

	$clients = wizard::get('client');
	$freelancers = wizard::get('freelancer');

	$template2->replace('CLIENT', html::select("name=client", $clients));
	$template2->replace('freelancer', html::select("name=freelancer", $freelancers));

	$template->replace('FORM', $template2->html());

	$template->render();

	//wizard::html($template->html());	?>