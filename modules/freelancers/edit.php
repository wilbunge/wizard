<?	include('../../wizard');

	wizard::_include('template');
	wizard::_include('html');
	wizard::_include('category');
	wizard::_include('freelancer');

	$template = new template('templates/edit');
	$template2 = new template('templates/freelancer_form');

	if($_POST!=null){

		$freelancer = new freelancer($_POST['id']);

		$freelancer->set_data($_POST);
		$template2->replace($freelancer);

		$validation = $freelancer->validate();

		if($validation->success){

			$freelancer->save();

			echo (html::message(wizard::msg('SAVE_CHANGES_OK')));
			
			die;

		}else{



			foreach ($validation->errors as $field => $message) {
				$template2->form_field_error($field, $message);
			}

			$t_message = new template('templates/message');
			$t_message->replace('MESSAGE', wizard::msg('SAVE_CHANGES_ERROR'));

			$template->prepend($t_message);

			$template2->set_form_values($freelancer);

		}
		
	}else{
		$freelancer = new freelancer($_GET['id']);
		$template2->set_form_values($freelancer);
	}

	$template->replace('ID', $freelancer->id);
	$template->replace('FORM', $template2->html());

	// $template->replace('CATEGORY_MENU', html::select('name="category"', $categories, $freelancer->get('category')->id));

	$template->render();	?>