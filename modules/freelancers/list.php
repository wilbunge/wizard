<?	include_once('../../wizard');

	wizard::_include('client');
	wizard::_include('freelancer');
	wizard::_include('template');
	
	//$wizard_user = wizard::get_wizard_user();

	$template = new template('templates/list');

	$query = $_POST['query'];
	$wc = "(`first_name` LIKE '%$query%' OR `skills` LIKE '%$query%')";

	$freelancers = wizard::get('freelancer', $wc.' AND NOT `deleted`');

	$template->replace('QUERY_VALUE', $query);

	if($freelancers){
		
		foreach($freelancers as $freelancer){

			$portion = $template->portion('freelancer');

			// $portion->replace('CLIENT', $freelancer->get('client'));
			$portion->replace('ID', $freelancer->id);
			$portion->replace('NAME', $freelancer);

			$template->append($portion);

		}

	}

	$template->clear('freelancer');

	$template->render();

	//wizard::html($template->html());	?>