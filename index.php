<?	include('wizard');
	include('sys/includes/mobile_detect.php');

	wizard::_include('wizard_user');
	wizard::_include('template');

	$detect = new Mobile_Detect;
	$is_mobile = $detect->is_mobile();

	if ($detect->isMobile()) {
 		$is_mobile = true;
 	}else{
 		$is_mobile = false;
 	}

	if(isset($_GET['mod'])){
		$mod = $_GET['mod'];
	}else{
		if($user = wizard::get_user()){

			$mod = $is_mobile ? 'desktop_mobile' : 'desktop';
			// $mod = 'desktop_mobile';
		}else{
			$mod = 'signin';
		}
	}


	$inc = "sys/mod/$mod.php";

	include($inc);

	if(wizard::$_render){
		wizard::render();
	}	?>