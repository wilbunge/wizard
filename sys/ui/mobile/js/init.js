$().ready(
	function(){
		init();
	}
);


function init(){

	$('#canvas').hide();

	$pl = $('<preloader><logo></logo><progressbar></progressbar><legend>Cargando...</legend></preloader>');

	$('body').prepend($pl);

	var l = ($(document).width()-$pl.width())/2;
	var t = ($(document).height()-$pl.height())/2;

	$pl.css('left', l);
	$pl.css('top', t);

	$.getScript(
		'sys/ui/js/classes.js',
		function(){

			$.getScript(
				'sys/ui/js/wizard.js',
				function(){

					$.getScript(
						'sys/ui/mobile/js/ui.js',
						function(){

							$.getScript(
								'sys/ui/mobile/js/jquery.mmenu.min.js',
								function(){

									$pl.fadeOut(
										function(){

											$('#canvas').show();

											wizard.initMobile();

										}

									);
								}
							);
						}
					);

				}
			);
		}
	);

}