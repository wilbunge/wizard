var ui = {};

ui.init = function(){

	this.$middle = $('#middle');
	this.$overlay = $('#overlay');
	this.$overlay_container = $('#overlay-container');

	this.$modules = $('<modules>');
	this.$modules_tabs = $('<tabs>');

	this.$middle.append(this.$modules_tabs);
	this.$middle.append(this.$modules);


	$("#menu").show();
	$("#menu").mmenu();
	ui.applyMenuEvents();


	$(document).delegate(
		'form',
		'submit',
		function(){

			ui.delegateModuleForm($(this));
			
			return false;

		}
	);

	this.$modules_tabs.delegate(
		'tab',
		'click',
		function(){
			var id = $(this).attr('w-module-id');
			ui.activateModule(id);

		}
	);

	ui.delegate();


	// Capture search form submit

	$('#top').find('search').find('input').keyup(
		function(e){
			if(e.keyCode==13) ui.search($(this).val()); // Pressed 'enter'

		}
	);

}

ui.applyMenuEvents = function(){
	
	
	$('a', '#menu').click(
		function(){

			var path = $(this).attr('data-module-path');

			wizard.openModule(path);
			$("#menu").trigger("close.mm");


		}
	);
}

ui.renderModule = function(path, r){

	$('module').hide();

	var $module = $('module[w-module-id='+path+']');

	if($module.size()==0){
		$tab = $('<tab>');
		$tab.attr('w-module-id', path);
		$tab.text(wizard.getModule(path).getName());

		$a = $('<a href="#">x<a>');

		$a.click(function(){
			wizard.closeModule(path);
		});

		$tab.append($a);

		$module = $('<module>');
		$module.attr('w-module-id', path);
		this.$modules.append($module);
		// this.$modules_tabs.append($tab);

		wizard.getModule(path).$ = $module;

	}


	this.$modules_tabs.text(wizard.getModule(path).getName());

	if(r!=null) $module.html(r);

	$module.show();

	$module.find('menu').delegate(
		'a',
		'click',
		function(){

			$module.find('menu').find('a').removeClass('selected');
			$(this).addClass('selected');

			var href = $(this).attr('href');

			hash = href.indexOf('#')>=0;

			if(hash) return false;
			
			
			wizard.getModule(path).action(
				href,
				function(r){
					ui.renderModuleContent(path, r);
				}
			);

			return false;
		}
	);

	$module.find('modulecontent').delegate(
		'a',
		'click',
		function(){

			var href = $(this).attr('href');

			if(href=='#delete') return(ui.delete($(this)));

			hash = href.indexOf('#')>=0;

			if(hash) return false;
			
			
			wizard.getModule(path).action(
				href,
				function(r){
					ui.renderModuleContent(path, r);
				}
			);

			return false;
		}
	);

	//this.$middle.html(r);

}

ui.renderModuleContent = function(path, r){

	var $module = $('module[w-module-id='+path+']');

	$module.find('modulecontent').html(r);

}

ui.delegateModuleForm = function($form){

	var action = $form.attr('action');
	var data = $form.serialize();

	var arr = action.split('/');

	var module_id = arr[0];

	var $module = $('module[w-module-id='+module_id+']');
	
	wizard.ajaxPost(
		action,
		data,
		function(r){

			// var $r = $(r);

			// $inputs = $r.find('input');

			// $.each(
			// 	$inputs,
			// 	function(i,o){

			// 		var $input = $(o);

			// 		error = $input.attr('w-forminputerror');
			// 		if(error!=null){
			// 			$message = $('<message class="error mini">');
			// 			$message.html(error);
			// 			$input.after($message);
			// 		}

			// 	}
			// );

			ui.renderModuleContent(module_id, r);


		}
	);

}


ui.activateModule = function(id){

	var $module = $('module[w-module-id='+id+']');

	ui.selectModuleTab(id);
	
	if($module.size()>0){
		$('module').hide();
		$module.show();
		return true;
	}else{
		return false;
	}

}

ui.delegate = function(){

	// this.$modules.delegate(
	// 	'a[href=#delete]',
	// 	'click',
	// 	function(){

	// 		if(!confirm('Confirma que desea eliminar el item?')) return;

	// 		var id = $(this).attr('w-object-id');
	// 		var type = $(this).attr('w-object-type');
	// 		var $tr = $(this).parent().parent();

	// 		wizard.delete(
	// 			type,
	// 			id,
	// 			function(r){
					
	// 				if(r.success){
	// 					$tr.fadeOut();	
	// 				}else{
	// 					alert('ERR');
	// 				}

	// 				console.log(r);
					
	// 			}

	// 		);

	// 	}
	// );
}


ui.delete = function($a){

	if(!confirm('Confirma que desea eliminar el item?')) return;

		var id = $a.attr('w-object-id');
		var type = $a.attr('w-object-type');
		var $tr = $a.parent().parent();

		wizard.delete(
			type,
			id,
			function(r){
				
				if(r.success){
					$tr.fadeOut();	
				}else{
					alert('ERR');
				}

				//console.log(r);
				
			}

		);
}


ui.closeModule = function(id){

	var $module = $('module[w-module-id='+id+']');
	var $tab = $('tab[w-module-id='+id+']');
	
	if($module.size()>0){
		$module.remove();
		$tab.remove();
	}

}

ui.selectModuleTab = function(id){
	var $tab = $('tab[w-module-id='+id+']');
	$('tab').removeClass('selected');
	$tab.addClass('selected');
}

ui.modulePreloader = function(id, s){
	
	if(!s){
		wizard.getModule(id).$.find('modulecontent').removeClass('loading');
	}else{
		wizard.getModule(id).$.find('modulecontent').addClass('loading');
	}

}

ui.activateLastModuleOnClose = function(){
	
	for(var i in wizard.modules){
		
	}
	
	if(wizard.modules[i]!=null) ui.activateModule(wizard.modules[i].id);

}

ui.showModulePreloader = function(id){

	// $pl = $('<preloader><progressbar></progressbar><legend>Loading...</legend></preloader>');
	$pl = $('<preloader><legend>Loading...</legend></preloader>');

	wizard.getModule(id).$.html($pl);

	ui.center($pl);

}

ui.center = function($e){

	var l = ($e.parent().width()-$e.width())/2;
	var t = ($e.parent().height()-$e.height())/2;

	$e.css('left', l);
	$e.css('top', t);

}

ui.search = function(query){

	if(query.indexOf('wizard')>-1) return(eval(query));

	wizard.openModule('finder', {query: query});
}

ui.formatNumber = function(number){

	number = Math.round(parseFloat(number)*100)/100; // 2 decimals


	sign = number/Math.abs(number);
	number=Math.abs(number);

	integer = parseInt(number);
	decimals = Math.round((number-integer)*100)/100;
	decimals_str = decimals.toString().replace('0.', '');

	var a = integer.toString();
	var b = a.split('');
	b.reverse();

	var c = [];

	var n=0;

	for(var i = b.length; i--; i>0){
	    c.push(b[i]);
	    if(i%3==0 && i>0) c.push('.');
	}

	var d = c.join('');

	var e = d;
	e += ','+ decimals_str;

	if(sign<1) e = '-'+e;

	return e;

}


ui.formatNumberIn = function(number){

	number = number.toString();

	number = number.replace(',', '.');

	return number;

}

ui.roundNumber = function(number){

	number = Math.round(parseFloat(number)*100)/100;

	return number;

}


ui.overlayContent = function(url, data, callback){

	$.get(
		url,
		data,
		function(r){

			ui.$overlay_container.html(r);

			callback(r);
		}
	);

}


ui.tutorial = function(){

	ui.$overlay.fadeIn(
		function(){
			ui.tutorialNavigate(1);
		}
	);

}

ui.endTutorial = function(){

	ui.$overlay.fadeOut();

}

ui.tutorialNavigate = function(step){

	ui.overlayContent(
		'modules/tutorial/',
		{'step': step},
		function(r){

			ui.$overlay_container.find('button[value=next]').click(
				function(){
					wizard.tutorial.next();
				}
			);

			ui.$overlay_container.find('button[value=start]').click(
				function(){
					wizard.tutorial.end();
				}
			);

		}
	);

}