function module(id){

	this.id = id;

}

module.prototype.close = function(){
	
}

module.prototype.open = function(data){

	var me = this;

	ui.renderModule(me.id);
	me.select();

	ui.showModulePreloader(me.id);

	var js = 'modules/'+me.id+'/assets/index.js';

	console.log('GETTING:');
	console.log('modules/'+me.id);


	$.get(
		'modules/'+me.id+'/index.php',
		data,
		function(r){

			$.ajax({
				url: js,
				dataType: "script",
				success: function(){
					
				},
				error: function(s){
					
					if(s.status == 404) return;
					console.log(r);
					alert('Se produjo un error al cargar el script');
				}
			});

			console.log('RESPONSE WAS: ');
			console.log(r);

			ui.renderModule(me.id, r);
		}
	).fail(
		function(r){
			alert('ERROR LOADING MODULE: modules/'+me.id);
			console.log(r);
		}
	);

}

module.prototype.select = function(){
	ui.selectModuleTab(this.id);
}

module.prototype.reload = function(){
	this.action(this.current_action);
}

module.prototype.action = function(action, callback){

	this.current_action = action;

	var js = 'modules/'+this.id+'/assets/'+action.split("?")[0]+'.js';

	var id = this.id;

	ui.modulePreloader(this.id, true);

	$.ajax({
		url: 'modules/'+this.id+'/'+action,
		success: function(r){

			if(callback==null){
				ui.renderModuleContent('invoicing', r);
			}else{
				callback(r);
			}

			ui.modulePreloader(id, false);

			$.ajax({
				url: js,
				dataType: "script",
				success: function(){
					
				},
				error: function(s){
					
					if(s.status == 404) return;
					console.log(r);
					alert('Se produjo un error al cargar el script');
				}
			});

		//	ui.renderModuleContent(path, r);
		},
		error: function(){
			alert('Error al cargar la acción!');
			ui.modulePreloader(id, false);

		}
	});

}

module.prototype.getName = function(){

	var name = wizard_module_names[this.id];

	return(name);
}





function tutorial(){

	this.step = 1;

}


tutorial.prototype.next = function(){

	this.step++;

	ui.tutorialNavigate(this.step);

}

tutorial.prototype.start = function(){

	ui.tutorial();

}

tutorial.prototype.end = function(){

	ui.endTutorial();

}