$().ready(
	function(){

		$('form').submit(function(){
			signin();
			return false;
		});

	}
);


function signin(){

	clearError();
	disableButton();

	var data = $('form').serialize();

	$.ajax({
		url: './',
		method: 'post',
		dataType: 'json',
		data: data,
		success: function(r){

			if(r.success){
				$('input[type=submit]').after('<ok>✔</ok>');
				location.replace('./'+r.account);
			}else{
				enableButton();
				signinError(r.error);
			}

		},
		error: function(r){
			signinError('Ha ocurrido un error!<br>'+r.responseText);
			console.log(r);
			enableButton();
		}
	});

}

function signinError(m){

	$('error').text(m).fadeIn();

}

function clearError(){
	$('error').text('').hide();
}

function enableButton(){
	$('input[type=submit]').attr('disabled', false);
}

function disableButton(){
	$('input[type=submit]').attr('disabled', true);
}