var wizard = {};

wizard.modules = [];
wizard.module_names = [];

wizard.getBottomMenu = function(){

	$.get(
		'sys/mod/bottom_menu',
		function(r){
			ui.renderBottomMenu(r);
		}
	);

}

wizard.init = function(){

	ui.init();

	wizard.getBottomMenu();

	wizard.openModule('home');


};


wizard.initMobile = function(){

	ui.init();

	wizard.openModule('home');

	// wizard.getBottomMenu();

	// wizard.openModule('tutorial');


};


wizard.openModule = function(id, data){

	var module = wizard.getModule(id);

	module.open(data);


	ga('send', 'screenview', {
		'appName': module.getName(),
		// 'appId': 'myAppId',
		// 'appVersion': '1.0',
		// 'appInstallerId': 'myInstallerId',
		'screenName': 'Module Home'
	});

	// wizard.activateModule(module);

	// return;
	// if(ui.activateModule(path)) return;
	// wizard.modules[path].open();
	

}

wizard.moduleAction = function(path, action){

	var js = 'modules/'+path+'/assets/'+action+'.js';

	var p = 'modules/'+path+'/'+action;

	$.ajax({	
		url: p,
		success: function(r){

			$.ajax({
				url: js,
				dataType: "script",
				success: function(){

				},
				error: function(r){
					if(r.status == 404) return;
					
					alert('Se produjo un error al cargar el script');
				}
			});

			ui.renderModuleContent(path, r);
		},
		error: function(r){
			alert('ERROR LOADING ACTION: '+p);
			// console.log(r);
		}
	});

}

wizard.ajaxPost = function(action, data, callback){

	$.post(
		'modules/'+action,
		data,
		function(r){
			callback(r);
		}
	);

}

wizard.delete = function(type, id, callback){

	var data = {type: type, id: id};

	$.post(
		'sys/ajax/delete',
		data,
		function(r){
			callback(r);
		},
		'json'
	);

}

wizard.closeModule = function(id){
	wizard.modules[id].close();

	delete wizard.modules[id];

	ui.closeModule(id);
	ui.activateLastModuleOnClose();
}

wizard.getModule = function(id){

	if(wizard.modules[id]==null){
		wizard.modules[id] = new module(id);
	}

	return wizard.modules[id];

}


wizard.startTutorial = function(){
	this.tutorial = new tutorial();
	this.tutorial.start();
}