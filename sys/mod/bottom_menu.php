<?	include('../../wizard');



	wizard::_include('wizard_user');
	wizard::_include('template');
	
	$wizard_user = wizard::get_user();

	$template = new template('../ui/templates/bottom_menu');

	
	
	foreach($wizard_user->get_quick_launch_menu() as $module){

		$portion = $template->portion('MODULE');

		$name = wizard::get_account()->id==3?$module->get('name_eng'):$module->get('name_spa');

		$portion->replace('NAME', $name);
		$portion->replace('PATH', $module->get('keyword'));
		$portion->replace('MODULE_ID', $module->get('keyword'));

		$template->append($portion);

	}

	$template->clear('MODULE');

	$template->render();

	//wizard::html($template->html());

	if($wizard_user->tutorial()){	?>

	<script>
	wizard.startTutorial();
	</script>

<?	}	?>