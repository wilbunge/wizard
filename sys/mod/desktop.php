<?	//error_reporting(E_ALL);

	wizard::_include('javascript');
	wizard::_include('wizard_account');

	$template = new template('sys/ui/templates/desktop');
	$js = new javascript('sys/ui/js/init');

	$user = wizard::get_user();
	
	try {
		$wizard_account = $user->get_wizard_account();	
	} catch (Exception $e) {
		
	}

	if(!$wizard_account){ 
		$user->logout();
		die('NO WIZZARD ACCOUNT!');
	}



	$template->replace('SCRIPT', $js->output());
	$template->replace('ACCOUNT_NAME', $wizard_account->get('name'));
	$template->replace('USER', $user->get('first_name'));
	$template->replace('VER', wizard::get_version());

	wizard::html($template->html());	?>