<?	//error_reporting(E_ALL);

	wizard::_include('javascript');
	wizard::_include('wizard_account');

	$template = new template('sys/ui/mobile/templates/desktop');
	$js = new javascript('sys/ui/mobile/js/init');

	$user = wizard::get_user();
	
	try {
		$wizard_account = $user->get_wizard_account();	
	} catch (Exception $e) {
		
	}

	if(!$wizard_account){ 
		$user->logout();
		die('NO WIZZARD ACCOUNT!');
	}


	$template->replace('SCRIPT', $js->output());
	$template->replace('ACCOUNT_NAME', $wizard_account->get('name'));
	$template->replace('USER', $user->get('first_name'));
	$template->replace('VER', wizard::get_version());

	
	// Menu

	$wizard_user = wizard::get_user();

	$template_menu = new template('sys/ui/mobile/templates/menu');

	foreach($wizard_user->get_quick_launch_menu() as $module){

		$portion = $template_menu->portion('MODULE');

		$name = wizard::get_account()->id==3?$module->get('name_eng'):$module->get('name_spa');

		$portion->replace('MODULE_NAME', $name);
		$portion->replace('MODULE_PATH', $module->get('keyword'));
		$portion->replace('MODULE_ID', $module->get('keyword'));

		$template_menu->append($portion);

	}

	$template_menu->clear('MODULE');

	$template->replace('MENU', $template_menu->html());

	wizard::html($template->html());	?>