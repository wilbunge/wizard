<?php

include_once('db_object.php');
	
class landing extends db_object{
	
	private $_name;
	private $_banner;
	private $_player;
	
	//static $_properties = Array('game:game', 'title', 'description', 'keywords', 'domain','ga:text','banners[]:banner*');
	
	public $properties = array('game:game*', 'title', 'description', 'keywords', 'domain','ga:text','footer_html:text');
	
	public $ndb_properties = array('style:file', 'js:file', 'extra_files:files');
	public $ni_properties = array('pages[]:page*', 'banners[]:banner*');
	public $extra = Array( 'banners' => Array('rotation'));
	
	public $banners = Array();
	
	var $table_name = "landings";
	var $class_name = "landing";
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	public function get_banner(){
		return($this->_banner);
	}
	
	public function set_banner($v){
	
		$this->_banner = $v;
	}
	
	public function get_pages(){
		
		return(Array());
		
	}
	
	
	
	public function has_banner($position='top'){
		return($this->banners[$position]!=NULL);
	
	}
	
	
	public function load_banner($position='top'){
		//data_manager::$debug_mode=true;
		//$result = master::get_raw_data('landing_banners, banners', 'rotation, banner, FLOOR(RAND()*100) AS random', 'ABS(FLOOR(RAND()*100)-rotation) < `rotation` AND landing_banners.landing = '. $this->get('id') .' AND `position` = "'.$position.'" AND (landing_banners.banner = banners.id) AND NOT banners.deleted', '');
		
		$banners = master::get('banner', '`landing` = '. $this->get('id') .' AND `position` = "'.$position.'" AND NOT `deleted`', 'ORDER BY `id` DESC');
		$banner = $banners[0];
		
		return($banner!=null ? $banner : false);
		
		
		$b = master::get('');
		
		$result = master::get_raw_data('landing_banners, banners', 'rotation, banner', 'landing_banners.landing = '. $this->get('id') .' AND `position` = "'.$position.'" AND (landing_banners.banner = banners.id) AND NOT banners.deleted', '');
		
		$arr_temp = Array();
		
		while($row = mysql_fetch_Array($result)){
		
			for($i=0; $i<$row['rotation']/10; $i++){
				array_push($arr_temp, $row['banner']);
			}
			
		}
		
		$rnd_index = (rand(0,count($arr_temp)-1));
		
		$banner = new banner($arr_temp[$rnd_index]);
		
		return($banner!=null ? $banner : false);
		
	}
	
	public function load_banners(){
		
		$this->banners['top'] = $this->load_banner('top');
		$this->banners['left'] = $this->load_banner('left');
		$this->banners['right'] = $this->load_banner('right');
		$this->banners['bottom'] = $this->load_banner('bottom');
		
	}
	
	
	public function embed_banner($position){
	
		if(!$this->has_banner($position)) return(false);

		$src = "/video-slot/banners/".$this->banners[$position]->get('id').".".$this->banners[$position]->get('type');
		
		$link = $this->banners[$position]->get('link');

		$html = "<img src='$src'>";
		
		if($link!='') $html = "<a href='$link'>$html</a>";
		
		return($html);
	
	}
	
	
	
	public function html_input_property__($p){
	
	if($this->property_types[$p] == 'file') return('<input type="file" name="'.$p.'">');
	if($this->property_types[$p] == 'files') return('<input type="file" name="'.$p.'[]" multiple>');

		switch($p){
		
			case 'game' :
			
				$landings = master::get('game');
				
				foreach($landings as $t){
					$html .= "<option".($this->get('game')->equals($t)?' selected':'')." value='".$t->get('id')."'>".$t->get('name')."</option>";
				}
			
				$html = "<select name='game'>$html</select>";
				break;
				
			case 'extra_files':
				$html = '<input name="extra_files[]" type="file" multiple="" />';
				break;
		
			case 'description' :
			case 'keywords' :
			
				$v = $this->get($p);
				
				$html = "<textarea name='$p'>$v</textarea>";
				break;
			
			default :
				$v = $this->get($p);
				$html = "<input name='$p' value='$v' />";
				break;
			
		}
		
		
		
		return($html);
	
	}
	
	
	
	public function load_pages(){
		
		$pages = master::get('page', '`landing`='.$this->get('id').' AND NOT `deleted` AND `language` = "'.master::get_language().'" ORDER BY `order`');
		return($pages);
		
	}
	
	
	
	public function handle_files(){
		
		$theme_dir = '../landings/landing/'.$this->get('id');
		$theme_dir_2 = '../landings/landing/'.$this->get('id').'/files';

		if(!file_exists($theme_dir)) mkdir($theme_dir);
		if(!file_exists($theme_dir_2)) mkdir($theme_dir_2);
		
		if($_FILES['js']["tmp_name"]!='') move_uploaded_file($_FILES['js']["tmp_name"], $theme_dir . "/script.js");
		if($_FILES['style']["tmp_name"]!='') move_uploaded_file($_FILES['style']["tmp_name"], $theme_dir . "/style.css");
		//if($_FILES['sprite']["tmp_name"]!='') move_uploaded_file($_FILES['sprite']["tmp_name"], $theme_dir . "/sprite.png");

		$n=0;
		
		if(count($_FILES['extra_files'])) {
			foreach ($_FILES['extra_files'] as $file) {
				move_uploaded_file($_FILES['extra_files']["tmp_name"][$n], $theme_dir_2 ."/". $_FILES['extra_files']["name"][$n]);
				$n++;
			}
		}

	
	
	}
	
	
	
	public function update_order($arr){
	
		require('page.php');
	
		foreach($arr as $a){
			
			$page = new page($a[0]);
			master::update_property($page, 'order', $a[1]);
			
		};
	
	}
	
	
		
}

?>