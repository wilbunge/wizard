<?php

include_once('db_object.php');
	
class salesman extends db_object{
	
	public $properties = array('first_name', 'last_name', 'email', 'commision:number');
	
	var $table_name = "salesforce";
	var $class_name = "salesman";


	public function get_name(){

		$name[] = $this->get('first_name');
		$name[] = $this->get('last_name');

		return implode(' ', $name);

	}


	public function get_orders(){

		$my_orders = master::get('order', '`salesman` = '.$this->id);

		return $my_orders;

	}


	public function count_orders(){
		if(!$my_orders = $this->get_orders()) return 0;
		return count($my_orders);
	}


	public function calculate_order_commision($order, $format = false){

		$order_commision = $order->get('total')*$this->get('commision')/100;

		if($format) $order_commision = '$'.number_format($order_commision, 2, ',','.');

		return $order_commision;

	}


}	?>