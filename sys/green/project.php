<?php

include_once('db_object.php');
	
class project extends db_object{
	
	//public $properties = array('title', 'description:text', 'team[]:user*', 'status');
	public $properties = array('title', 'description:text', 'status', 'delivery_date:date', 'client:client*', 'fl:freelancer*', 'pm:freelancer*', 'payout');

	var $table_name = "projects";
	var $class_name = "project";


	public function __toString(){
		$t = $this->get('title');
		return($t==''?'-':$t);
	}
	
	public function update_status($s){

		// if($this->get('status')!='2' && $s=='2'){
		// 	master::update_property($this, 'date_done', date('Y-m-d h:i:s'));
		// }
	
		
		$this->update_property('status', $s, true);
	}
	
	public function get_status_name(){
	
		$status = $this->get('status');

		$status_names = Array(
			1 => 'New',
			2 => 'Ongoing',
			3 => 'Done',
			4 => 'Cancelled'
		);
		
		$status_name = $status_names[$status];
		
		if($status_name=='') $status_name = $status_names[0];
		
		return($status_name);
		
	}
	
	public function revise($r){
		
		master::update_property($this, 'revised', $r);
		
	}
	
	
	public function handle_files(){
		
		$theme_dir = 'task-files';
		$theme_dir_2 = 'task-files/'.$this->get('id').'/';

		if(!file_exists($theme_dir)) mkdir($theme_dir);
		if(!file_exists($theme_dir_2)) mkdir($theme_dir_2);
		
		$n=0;
		
		if(count($_FILES['files'])) {
			foreach ($_FILES['files'] as $file) {
				move_uploaded_file($_FILES['files']["tmp_name"][$n], $theme_dir_2 ."/". $_FILES['files']["name"][$n]);
				$n++;
			}
		}

	
	
	}
	
	
	public function get_files(){
		
		$directory = 'task-files/'.$this->get('id').'/';
		
		if(!file_exists($directory)) return(Array());
		
		 $results = array();

	    // create a handler for the directory
    	$handler = opendir($directory);

	    // open directory and walk through the filenames
    	while ($file = readdir($handler)) {

	      // if file isn't this directory or its parent, add it to the results
    	  if ($file != "." && $file != "..") {
        	$results[] = $file;
	      }

    	}

	    // tidy up: close the handler
    	closedir($handler);

	    // done!
    	return $results;

	
	
	}


	public function delivery_days(){

		$delivery_date = strtotime($this->get('delivery_date'));
		$time = $delivery_date-time();
		$days = round($time/60/60/24);

		return "$days días";


	}


	public function start(){
		$this->update_status(1);
		$this->save_to_asana();
		$this->create_slack_channel();
	}



	public function timetracker(){

		master::_include('timetracker');

		// $checkpoint = master::get('timetracker', '`project` = '.$this->id.' AND NOT finished');

	}


	public function create_slack_channel(){
		$t = $this->get('title');
		// https://hooks.zapier.com/hooks/catch/2265413/91qjrn/?project=$t
	}


	public function save_to_asana(){

		require_once('asana/asana.php');

		// See class comments and Asana API for full info
		$asana = new Asana(array('personalAccessToken' => '0/0d9412cad410c4eb2e9934e5bf878ea1')); // Create a personal access token in Asana or use OAuth

		$workspaceId = '343580871560409'; // The workspace where we want to create our task


		// First we create the task
		$result = $asana->createProject(array(
		    'workspace' => $workspaceId, // Workspace ID
    		'name'      => $this->get('title'), // Name of task
    		'team'		=> 345463042872464
		));

		// print_r($result);

		// As Asana API documentation says, when a task is created, 201 response code is sent back so...
		if ($asana->hasError()) {
		    echo '<br />Error while trying to connect to Asana, response code: ' . $asana->responseCode;
		    return;
		}

		// $taskId = $asana->getData()->id; // Here we have the id of the task that have been created

		// Now we do another request to add the task to a project
		// $asana->addProjectToTask($taskId, $projectId);

		if ($asana->hasError()) {
		    echo '<br />Error while assigning project to task: ' . $asana->responseCode;
		} else {
		    echo '<br />Success to add project to Asana.';
		}


	}
	
		
		
}

?>