<?php
include_once('db_object.php');
	
class user extends db_object{

	private $_password;
	private $_username;
	private $_type;
	private $_green_client;
	private $_email;
	
	public $properties = array('username','password','email','type', 'first_name', 'last_name');
	public $ni_properties = array();
	
	public $modules = array();
	
	var $table_name = "users";
	var $class_name = "user";
	
	public $settings=array();
	public $settings_it=array();
	public $settings_attr=array();

	public function __toString(){
		return($this->get('first_name'));
	}


	public function set_data($source) {
	
		
		$ps = array_merge($this->properties, $this->hidden_properties);

		foreach($ps as $property){
		
			$property = str_replace('[]', '', $property);
		
			$post_val = $source[$property];
			
		//	echo($property .": ".(($post_val))."<br />");
			
			/*if(method_exists($this,"get_$property")){

				$code0 = "\$this->get_$property();";
				$code = '$val = ' . $code0;
				eval($code);
				
			}else{

				$val = $this->get_p($property);

			}*/
			
			if($this->property_types2[$property]['is_array']){
			
				$this->values[$property] = Array();
				
				if(is_array($post_val)){

				foreach($post_val as $arr_val){
					//echo $property.': '.$this->values[$property];
					//array_push($this->$property, $arr_val);
					
					//echo $this->property_types[$property];
					
					$post_type = $this->property_types2[$property]['type'];
					
					if($this->property_types2[$property]['is_object']){
						array_push($this->values[$property], new $post_type($arr_val));
					}else{
						array_push($this->values[$property], $arr_val);
					}
					
				}	
				
				}
				
			}else if($this->property_types2[$property]['is_object'] && ($post_val != '')){
				$code0 = "\$this->get($property)->set_id($post_val);";
			}else{
				
				if(method_exists($this,"set_$property")){
					$code0 = "\$this->set_$property('$post_val');";
				}else{
					$this->set_p($property, $post_val);
				}
				
				
			}
			eval ($code0);
		}

		
	}
	
	
	
	public function get_cookie_id(){
		$this->set_id($_COOKIE['green_user_id']);
		return($this->get_id());
	}
	
	
	public function get_email(){
		if ($this->_email== NULL){
			if($this->get_id()>0) {
				$this->_email= $this->load_db_field('email');
			}
		}
		return $this->_email;
	}

	public function set_email($val){
		$this->_email= $val;
	}
	
	
	public function get_type(){
		if ($this->_type== NULL){
			if($this->get_id()>0) {
				$this->_type= $this->load_db_field('type');
			}
		}
		return $this->_type;
	}

	public function set_type($val){
		$this->_type= $val;
	}
	
	
	public function get_green_client(){
		if ($this->_green_client== NULL){
			$this->_green_client = new green_client();
			
			if($this->get_id()>0) {
				$this->_green_client->set_id($this->load_db_field('green_client'));
			}
		}
		return $this->_green_client;
	}

	public function set_green_client($val){
		$this->_green_client= $val;
	}
	
	
	
	public function get_contact(){
		if ($this->_contact== NULL){
			$this->_contact = new contact();
			
			if($this->get_id()>0) {
				$this->_contact->set_id($this->load_db_field('contact'));
			}
		}
		return $this->_contact;
	}

	public function set_contact($val){
		$this->_contact= $val;
	}
	
	
	public function get_username(){
		if ($this->_username== NULL){
		
			if($this->get_id()>0) {
				$this->_username= $this->load_db_field('username');
			}
		}
		return $this->_username;
	}

	public function set_username($val){
		$this->_username= $val;
	}
	
	
	
	public function get_password(){
		if ($this->_password== NULL){
			if($this->get_id()>0) {
				$this->_password= $this->load_db_field('password');
			}
		}
		return $this->_password;
	}

	public function set_password($val){
		$this->_password= $val;
	}
	
	
	
	public function get_phone(){
		if ($this->_phone== NULL){
			if($this->get_id()>0) {
				$this->_phone= $this->load_db_field('phone');
			}
		}
		return $this->_phone;
	}

	public function set_phone($val){
		$this->_phone= $val;
	}
	
	
	public function get_mail(){
		if ($this->_mail== NULL){
			if($this->get_id()>0) {
				$this->_mail= $this->load_db_field('mail');
			}
		}
		return $this->_mail;
	}

	public function set_mail($val){
		$this->_mail= $val;
	}
	
	
	public function get_birth(){
		if ($this->_birth == NULL){
			if($this->get_id()>0) {
				$this->_birth = $this->load_db_field('birth');
			}
		}
		return $this->_birth;
	}

	public function set_birth($val){
		$this->_birth = $val;
	}
	
	
	public function get_sex(){
		if ($this->_sex == NULL){
			if($this->get_id()>0) {
				$this->_sex = $this->load_db_field('sex');
			}
		}
		return $this->_sex;
	}

	public function set_sex($val){
		$this->_sex = $val;
	}
	
	
	public function get_type_name(){
		switch($this->get_type()){
			case 1 : return("Administrador");
				break;
			default : return("Normal");
				break;
		}
	}
	
	
	public function get_name(){
		if ($this->name == NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	
	public function html_input_property($p_property) {
		

		// IMPORTANT !!
		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		// IMPORTANT !!
		
		$html = '';
		switch ($p_property) {
						
			case 'sex':
				
				$html .= '<label><input size="2" type="radio" name="sex" value="male">Male</label> <label><input size="2" type="radio" name="sex" value="female">Female</label>';
				break;
			
			case 'birth':
							
				
				$day_birth = username("d", $this->get_birth());
				$month_birth = username("m", $this->get_birth());
				$year_birth = username("Y", $this->get_birth());
				
				if(!checkusername($day_birth, $month_birth, $year_birth)){
					
					$day_birth = "";
					$month_birth = "";
					$year_birth = "";
					
				}
				
			
				$html .= '<select id="day_birth" name="day_birth">';
				$html .= '	<option value="31"'.(($day_birth == '31') ? ' selected' : '').'>31</option>';
				$html .= '	<option value="30"'.(($day_birth == '30') ? ' selected' : '').'>30</option>';
				$html .= '	<option value="29"'.(($day_birth == '29') ? ' selected' : '').'>29</option>';
				$html .= '	<option value="28"'.(($day_birth == '28') ? ' selected' : '').'>28</option>';
				$html .= '	<option value="27'.(($day_birth == '27') ? ' selected' : '').'>27</option>';
				$html .= '	<option value="26"'.(($day_birth == '26') ? ' selected' : '').'>26</option>';
				$html .= '	<option value="25"'.(($day_birth == '25') ? ' selected' : '').'>25</option>';
				$html .= '	<option value="24"'.(($day_birth == '24') ? ' selected' : '').'>24</option>';
				$html .= '	<option value="23"'.(($day_birth == '23') ? ' selected' : '').'>23</option>';
				$html .= '	<option value="22"'.(($day_birth == '22') ? ' selected' : '').'>22</option>';
				$html .= '	<option value="21"'.(($day_birth == '21') ? ' selected' : '').'>21</option>';
				$html .= '	<option value="20"'.(($day_birth == '20') ? ' selected' : '').'>20</option>';
				$html .= '	<option value="19"'.(($day_birth == '19') ? ' selected' : '').'>19</option>';
				$html .= '	<option value="18"'.(($day_birth == '18') ? ' selected' : '').'>18</option>';
				$html .= '	<option value="17'.(($day_birth == '17') ? ' selected' : '').'>17</option>';
				$html .= '	<option value="16"'.(($day_birth == '16') ? ' selected' : '').'>16</option>';
				$html .= '	<option value="15"'.(($day_birth == '15') ? ' selected' : '').'>15</option>';
				$html .= '	<option value="14"'.(($day_birth == '14') ? ' selected' : '').'>14</option>';
				$html .= '	<option value="13"'.(($day_birth == '13') ? ' selected' : '').'>13</option>';
				$html .= '	<option value="12"'.(($day_birth == '12') ? ' selected' : '').'>12</option>';
				$html .= '	<option value="11"'.(($day_birth == '11') ? ' selected' : '').'>11</option>';
				$html .= '	<option value="10"'.(($day_birth == '10') ? ' selected' : '').'>10</option>';
				$html .= '	<option value="09"'.(($day_birth == '09') ? ' selected' : '').'>09</option>';
				$html .= '	<option value="08"'.(($day_birth == '08') ? ' selected' : '').'>08</option>';
				$html .= '	<option value="07"'.(($day_birth == '07') ? ' selected' : '').'>07</option>';
				$html .= '	<option value="06"'.(($day_birth == '06') ? ' selected' : '').'>06</option>';
				$html .= '	<option value="05"'.(($day_birth == '05') ? ' selected' : '').'>05</option>';
				$html .= '	<option value="04"'.(($day_birth == '04') ? ' selected' : '').'>04</option>';
				$html .= '	<option value="03"'.(($day_birth == '03') ? ' selected' : '').'>03</option>';
				$html .= '	<option value="02"'.(($day_birth == '02') ? ' selected' : '').'>02</option>';
				$html .= '	<option value="01"'.(($day_birth == '01') ? ' selected' : '').'>01</option>';
				$html .= '</select>';
				$html .= " / ";
				$html .= '<select id="month_birth" name="month_birth">';
				$html .= '	<option value="12"'.(($month_birth == '12') ? ' selected' : '').'>12</option>';
				$html .= '	<option value="11"'.(($month_birth == '11') ? ' selected' : '').'>11</option>';
				$html .= '	<option value="10"'.(($month_birth == '10') ? ' selected' : '').'>10</option>';
				$html .= '	<option value="09"'.(($month_birth == '09') ? ' selected' : '').'>09</option>';
				$html .= '	<option value="08"'.(($month_birth == '08') ? ' selected' : '').'>08</option>';
				$html .= '	<option value="07"'.(($month_birth == '07') ? ' selected' : '').'>07</option>';
				$html .= '	<option value="06"'.(($month_birth == '06') ? ' selected' : '').'>06</option>';
				$html .= '	<option value="05"'.(($month_birth == '05') ? ' selected' : '').'>05</option>';
				$html .= '	<option value="04"'.(($month_birth == '04') ? ' selected' : '').'>04</option>';
				$html .= '	<option value="03"'.(($month_birth == '03') ? ' selected' : '').'>03</option>';
				$html .= '	<option value="02"'.(($month_birth == '02') ? ' selected' : '').'>02</option>';
				$html .= '	<option value="01"'.(($month_birth == '01') ? ' selected' : '').'>01</option>';
				$html .= '</select>';
				$html .= " / ";
				$html .= '<select id="month_birth" name="month_birth">';
				$html .= '	<option value="1950"'.(($year_birth == '1950') ? ' selected' : '').'>1950</option>';
				$html .= '	<option value="1951"'.(($year_birth == '1951') ? ' selected' : '').'>1951</option>';
				$html .= '	<option value="1952"'.(($year_birth == '1952') ? ' selected' : '').'>1952</option>';
				$html .= '	<option value="1953"'.(($year_birth == '1953') ? ' selected' : '').'>1953</option>';
				$html .= '	<option value="1954"'.(($year_birth == '1954') ? ' selected' : '').'>1954</option>';
				$html .= '	<option value="1955"'.(($year_birth == '1955') ? ' selected' : '').'>1955</option>';
				$html .= '	<option value="1956"'.(($year_birth == '1956') ? ' selected' : '').'>1956</option>';
				$html .= '	<option value="1957"'.(($year_birth == '1957') ? ' selected' : '').'>1957</option>';
				$html .= '	<option value="1958"'.(($year_birth == '1958') ? ' selected' : '').'>1958</option>';
				$html .= '	<option value="1959"'.(($year_birth == '1959') ? ' selected' : '').'>1959</option>';
				$html .= '	<option value="1960"'.(($year_birth == '1960') ? ' selected' : '').'>1960</option>';
				$html .= '	<option value="1961"'.(($year_birth == '1961') ? ' selected' : '').'>1961</option>';
				$html .= '	<option value="1962"'.(($year_birth == '1962') ? ' selected' : '').'>1962</option>';
				$html .= '	<option value="1963"'.(($year_birth == '1963') ? ' selected' : '').'>1963</option>';
				$html .= '	<option value="1964"'.(($year_birth == '1964') ? ' selected' : '').'>1964</option>';
				$html .= '	<option value="1965"'.(($year_birth == '1965') ? ' selected' : '').'>1965</option>';
				$html .= '	<option value="1966"'.(($year_birth == '1966') ? ' selected' : '').'>1966</option>';
				$html .= '	<option value="1967"'.(($year_birth == '1967') ? ' selected' : '').'>1967</option>';
				$html .= '	<option value="1968"'.(($year_birth == '1968') ? ' selected' : '').'>1968</option>';
				$html .= '	<option value="1969"'.(($year_birth == '1969') ? ' selected' : '').'>1969</option>';
				$html .= '	<option value="1970"'.(($year_birth == '1970') ? ' selected' : '').'>1970</option>';
				$html .= '	<option value="1971"'.(($year_birth == '1971') ? ' selected' : '').'>1971</option>';
				$html .= '	<option value="1972"'.(($year_birth == '1972') ? ' selected' : '').'>1972</option>';
				$html .= '	<option value="1973"'.(($year_birth == '1973') ? ' selected' : '').'>1973</option>';
				$html .= '	<option value="1974"'.(($year_birth == '1974') ? ' selected' : '').'>1974</option>';
				$html .= '	<option value="1975"'.(($year_birth == '1975') ? ' selected' : '').'>1975</option>';
				$html .= '	<option value="1976"'.(($year_birth == '1976') ? ' selected' : '').'>1976</option>';
				$html .= '	<option value="1977"'.(($year_birth == '1977') ? ' selected' : '').'>1977</option>';
				$html .= '	<option value="1978"'.(($year_birth == '1978') ? ' selected' : '').'>1978</option>';
				$html .= '	<option value="1979"'.(($year_birth == '1979') ? ' selected' : '').'>1979</option>';
				$html .= '	<option value="1980"'.(($year_birth == '1980') ? ' selected' : '').'>1980</option>';
				$html .= '	<option value="1981"'.(($year_birth == '1981') ? ' selected' : '').'>1981</option>';
				$html .= '	<option value="1982"'.(($year_birth == '1982') ? ' selected' : '').'>1982</option>';
				$html .= '	<option value="1983"'.(($year_birth == '1983') ? ' selected' : '').'>1983</option>';
				$html .= '	<option value="1984"'.(($year_birth == '1984') ? ' selected' : '').'>1984</option>';
				$html .= '	<option value="1985"'.(($year_birth == '1985') ? ' selected' : '').'>1985</option>';
				$html .= '	<option value="1986"'.(($year_birth == '1986') ? ' selected' : '').'>1986</option>';
				$html .= '	<option value="1987"'.(($year_birth == '1987') ? ' selected' : '').'>1987</option>';
				$html .= '	<option value="1988"'.(($year_birth == '1988') ? ' selected' : '').'>1988</option>';
				$html .= '	<option value="1989"'.(($year_birth == '1989') ? ' selected' : '').'>1989</option>';
				$html .= '	<option value="1990"'.(($year_birth == '1990') ? ' selected' : '').'>1990</option>';
				$html .= '	<option value="1991"'.(($year_birth == '1991') ? ' selected' : '').'>1991</option>';
				$html .= '	<option value="1992"'.(($year_birth == '1992') ? ' selected' : '').'>1992</option>';
				$html .= '	<option value="1993"'.(($year_birth == '1993') ? ' selected' : '').'>1993</option>';
				$html .= '	<option value="1994"'.(($year_birth == '1994') ? ' selected' : '').'>1994</option>';
				$html .= '	<option value="1995"'.(($year_birth == '1995') ? ' selected' : '').'>1995</option>';
				$html .= '	<option value="1996"'.(($year_birth == '1996') ? ' selected' : '').'>1996</option>';
				$html .= '	<option value="1997"'.(($year_birth == '1997') ? ' selected' : '').'>1997</option>';
				$html .= '	<option value="1998"'.(($year_birth == '1998') ? ' selected' : '').'>1998</option>';
				$html .= '	<option value="1999"'.(($year_birth == '1999') ? ' selected' : '').'>1999</option>';
				$html .= '	<option value="2000"'.(($year_birth == '2000') ? ' selected' : '').'>2000</option>';
				$html .= '	<option value="2001"'.(($year_birth == '2001') ? ' selected' : '').'>2001</option>';
				$html .= '	<option value="2002"'.(($year_birth == '2002') ? ' selected' : '').'>2002</option>';
				$html .= '	<option value="2003"'.(($year_birth == '2003') ? ' selected' : '').'>2003</option>';
				$html .= '	<option value="2004"'.(($year_birth == '2004') ? ' selected' : '').'>2004</option>';
				$html .= '	<option value="2005"'.(($year_birth == '2005') ? ' selected' : '').'>2005</option>';
				$html .= '	<option value="2006"'.(($year_birth == '2006') ? ' selected' : '').'>2006</option>';
				$html .= '	<option value="2007"'.(($year_birth == '2007') ? ' selected' : '').'>2007</option>';
				$html .= '	<option value="2008"'.(($year_birth == '2008') ? ' selected' : '').'>2008</option>';
				$html .= '	<option value="2009"'.(($year_birth == '2009') ? ' selected' : '').'>2009</option>';
				$html .= '	<option value="2010"'.(($year_birth == '2010') ? ' selected' : '').'>2010</option>';
				$html .= '</select>';
				break;
						
			case 'type' :

				if($this->get_master()->get_user()->get_type() == 1) {
					$html .= '<label><input'.(($this->get_type() == '1') ? ' checked' : '').' value="1" type="radio" name="type">'.translate("users","add_new/txt_user_type_administrator").'</label><br />';
					$html .= '<label><input'.(($this->get_type() == '3') ? ' checked' : '').' value="3" type="radio" name="type">'.translate("users","add_new/txt_user_type_normal").'</label><br />';
				} else {
					$html .= '<label><input'.(($this->get_type() == '1') ? ' checked' : '').' value="1" type="radio" name="type" disabled>Administrator</label><br />';
					$html .= '<label><input'.(($this->get_type() == '3') ? ' checked' : '').' value="3" type="radio" name="type" disabled>Normal</label><br />';
				}
				break;
			
			
			case 'password' :
			
				$html .= '<input value="'.$val.'" type="password" name="'.$p_property.'">';
				break;
							
			
			case 'username' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
				
			case 'contact':
				
				$result = $this->get_master()->mysql_query("SELECT","id, first_name, last_name","contacts","NOT deleted","ORDER BY last_name, first_name");
				
				$html = "<select name='contact'>";
				$html .= "<option value='0'>...</option>";
				while($row=mysql_fetch_array($result)){
					$html .= "<option value='".$row['id']."'>".$row['last_name'].", ".$row['first_name']."</option>";
				}
				
				$html .= "</select>";
				
				break;
			
			case 'name' :
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
		}
		return $html;
	}
	
	
	
	public function catch_post_properties() {

		foreach($this->properties as $property){
			
			if(!$birth){
				$post_val = $_POST[$property];
				$code0 = "\$this->set_$property('$post_val');";
				//$code = '$val = ' . $code0;
				eval ($code0);
			}else{
				$this->set_birth($_POST['day_birth'],$_POST['month_birth'],$_POST['year_birth']);
			}
		}
	}
	
	
	public function has_access_to($p_module){

		$sql_query = "SELECT id FROM user_modules WHERE module = " . $p_module->get_id() . " AND user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."' AND has_access";
		$result = mysql_query($sql_query);
		//return(mysql_num_rows($result)>0 or $this->get_type()==1);
		return(mysql_num_rows($result));
			
	
	}

	
	public function update_modules($p_arr){
		
		$sql_query = "UPDATE user_modules SET quick_launch = FALSE WHERE user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."'";
		$result = mysql_query($sql_query);
		//echo($sql_query);
		
		foreach($p_arr as $green_module){
			
			$sql_query = "SELECT id FROM user_modules WHERE module = " . $green_module->get_id() . " AND user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."'";
			
			$result = mysql_query($sql_query);
			//echo(mysql_num_rows($result));
			if(mysql_num_rows($result)==0){
				$sql_query_2 = "INSERT INTO user_modules(user, green_client, module, quick_launch) VALUES(".$this->get_id().", ".$this->get_green_client()->get_id().", ".$green_module->get_id().", TRUE)";
			}else{
				$sql_query_2 = "UPDATE user_modules SET quick_launch = TRUE WHERE user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."' AND module = " . $green_module->get_id();
			}
			
			mysql_query($sql_query_2);
			
			//echo($sql_query_2);
		}
		
	}

	// Loads modules into array
	public function load_modules(){
	
		$sql_query = "SELECT module FROM user_modules WHERE user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."'";
		
		$result = mysql_query($sql_query);
		while($row = mysql_fetch_array($result)){
			$green_module = new green_module();
			$green_module->set_id($row['module']);
			
			array_push($this->modules, $green_module);	
		}
		
	}
	
	
	public function load_quick_launch(){
	
		$sql_query = "SELECT module FROM user_modules WHERE quick_launch AND user = '".$this->get_id()."' AND (green_client = '".$this->get_green_client()->get_id()."' OR green_client = 0) AND has_access";
		//echo($sql_query);
		$result = mysql_query($sql_query);
		
		while($row = mysql_fetch_array($result)){
			$green_module = new green_module();
			$green_module->set_id($row['module']);
			
			if($this->get_green_client()->has_access_to($green_module)) array_push($this->modules, $green_module);	
		}
		
	}


	
	
	
	public function query_postits(){
	
		$arr = master::query_generic(new postit(), "`to` = " . $this->get_id());
		
	}
	
	
	public function is_in_quicklaunch($p_module){
	
		$sql_query = "SELECT quick_launch FROM user_modules WHERE module = " . $p_module->get_id() . " AND user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."'";
			//echo($sql_query . "<br />");
		$result = mysql_query($sql_query);
		$row = mysql_fetch_array($result);
		return($row['quick_launch']);
	}
	
	
	public function logoff() {
		$query = "UPDATE cometchat_status SET status = 'offline' WHERE userid = ".$this->get_id();
		mysql_query($query);
		
		setcookie("GCFC", "", time()-1, "/");
		setcookie("GCMB", "", time()-1, "/");
		setcookie("GCCB", "", time()-1, "/");
		setcookie("GCDB", "", time()-1, "/");
		
	}
	
	
	public function get_module_permissions($p_module){
	
		$sql_query_3 = "SELECT permissions FROM user_permissions WHERE user = '".$this->get_id()."' AND green_client = '".$this->get_master()->get_green_client()->get_id()."' AND module = " . $p_module->get_id();
		$result = mysql_query($sql_query_3);
		
		if(mysql_num_rows($result)>0){
			$line = mysql_fetch_array($result);			
			return($line['permissions']);			
		}else{
			return("NEVD");
		}

	}
	
	
	public function preset_ql(){
	
		$arr = array(1,7,10,5,20,44);
		
		foreach($arr as $module){	
	
			$sql_query_3 = "SELECT id FROM user_modules WHERE user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."' AND module = " . $module;
			$result = mysql_query($sql_query_3);
				
			if(mysql_num_rows($result)==0){
				$sql_query_2 = "INSERT INTO user_modules(`quick_launch`, `has_access`, `user`, `green_client`, `module`) VALUES(TRUE, TRUE, ".$this->get_id().", ".$this->get_green_client()->get_id().", " . $module . ")";
			}else{
				$sql_query_2 = "UPDATE user_modules SET `quick_launch`=TRUE, has_access = TRUE WHERE user = '".$this->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."' AND module = " . $module;
			}
			
			mysql_query($sql_query_2);
		
		}
		
	}
	
	
	public function load_settings(){
	
		$green_client = $this->get_master()->get_green_client();
		
		$path3=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $green_client->get_crypt_id()."/";
		$path4=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $green_client->get_crypt_id()."/users/";
		
		$path=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $green_client->get_crypt_id() . "/users/".$this->get_crypt_id()."/";
		$file=$path."settings.xml";
		
		$path2=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."modules/users/";
		$file2=$path2."base.xml";
		
		if(!is_dir($path3)) mkdir($path3);
		if(!is_dir($path4)) mkdir($path4);
		
		if(!is_dir($path)) mkdir($path);
		if(!is_dir($path2)) mkdir($path2);
		
				
		if(!file_exists($file)) copy($file2, $file);

		
//		echo $file."--<br><br>";
		
		$obj = simplexml_load_file($file);
		$obj2 = simplexml_load_file($file2);
		
//		echo count($obj2)."@@@";
		foreach($obj2 as $prop=>$value){
		
			foreach($obj2->$prop->attributes() as $a => $b){
//				echo $b."===<br>";
				if($a=="input_type"){
					$this->settings_it[$prop]=$b;
				}
				$this->settings_attr[$prop][$a]=$b;				
			}
			
			$value=$obj->$prop;
//			echo $value."####<br>";
//			echo count($obj)."/////";
//			echo "*$prop*$value*";
			$this->settings[$prop]=$value;
		}		
	}
	
	
	public function embed_photo(){
	
		$img_path="modules/users/no_photo.png";
		
		$html="<img src='$img_path' />";
		
		return($html);
		
	}
	
	
	function embed_settings_w(){
	
		echo "<table>";
	
		foreach($this->settings as $prop=>$value){
		
			echo "<tr><th>".translate("system-settings","properties/txt_".$prop)."</th><td>";
		
			switch($this->settings_it[$prop]){
				case "textarea" :
					echo "<textarea name='$prop'>". $value . "</textarea>";
				break;
				
				case "checkbox" :
					echo "<input ". ($value=='yes' ? 'checked':'') ." name='$prop' type='checkbox' value='yes'>";
				break;
				
				case "select" :
					$s = $this->settings_attr[$prop]["input_options"];
					$v = $this->settings[$prop];
					$arr = explode("|",$s);

					echo "<select name='$prop'>";
					foreach($arr as $option){
						$value = explode("*",$option);
						echo "<option ". ($v==$value[1] ? ' selected':'') ." value='".$value[1]."'>$value[0]</option>";
					}
										
					echo "</select>";
				
				break;
					
				case "file" :
					echo "<input name='$prop' type='file'>";
				break;
					
					
				default:
					echo "<input name='$prop' type='text' value='$value'>";
				break;
			}
			
			echo "<td></tr>";
			
		}
		echo "</table>";
	}
	
	
	
	public function embed(){
	
		$html = "<table class='G_cs_input' cellspacing='0'>";
		
		foreach($this->properties as $property){
		
			if($property!='password'){
				
			
				$code0 = "\$this->get_$property();";
				$code = '$val = ' . $code0;
				
				if(function_exists(translate)){
					$property_t=translate("class_".$this->class_name, "properties/".$property);
				}else{		
					$property_t=$property;
				}
				
				if($property_t==NULL || $property_t=='')$property_t=$property;
				
			
				eval ($code);
			
				$html .= "<tr>";
				$html .= "	<th>" . $property_t . "</th>";
				
				if(gettype($val) == 'object'){
					$html .= "	<td>" . $val->get_default() . "</td>";
				}elseif($property=='type'){
					$html .= "	<td>" . $this->get_type_name() . "</td>";			
				}else{
					$html .= "	<td>" . $val . "</td>";			
				}
				
				$html .= "</tr>";
			}
		}
		
		$html .= "</table>";
		
		return($html);
	}
	
	
	public function save_settings(){
		
		$green_client = $this->get_master()->get_green_client();
			
		$file=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $green_client->get_crypt_id() . "/users/".$this->get_crypt_id()."/settings.xml";
		$file2=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."modules/users/base.xml";
		
		$obj = simplexml_load_file($file2);
		
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		
		$settings = $doc->createElement("settings");

		foreach($obj as $prop => $value){
			
			$post_val = $_POST[$prop];
			$p = $doc->createElement($prop);
			
			$n = $doc->createTextNode($post_val);
			
			$p->appendChild($n);
			
			foreach($obj->$prop->attributes() as $n => $v){
				$p->setAttribute($n, $v);
			}

			$settings->appendChild($p); 
		}

		$doc->appendChild($settings); 		
		
		$doc->saveXML();
		$doc->save($file);
		
	}
	
	
	
	public function save_settings_2(){
	
		$file=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_green_client()->get_crypt_id() . "/users/".$this->get_crypt_id()."/settings.xml";
		$file2=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."modules/users/base.xml";
		
		$obj = simplexml_load_file($file);
		$obj2 = simplexml_load_file($file2);
		
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		
		$cfg = $doc->createElement("cfg");

		foreach($obj2 as $prop => $value){
		
			$value2=$this->settings[$prop];
		
			$p = $doc->createElement($prop);
			
			if(is_array($value2)){
			
				$val = implode(",", $value2);
			
				$n = $doc->createTextNode($val);
			}else{
			
				$n = $doc->createTextNode($value2);
			}
			
			$p->appendChild($n);
			
			foreach($obj->$prop->attributes() as $n => $v){
				$p->setAttribute($n, $v);
			}
			
			$cfg->appendChild($p); 
			
			
			
		}

		$doc->appendChild($cfg); 		
		
		$doc->saveXML();
		$doc->save($file);
		
	}
	
	
	
	function get_crypt_id(){
	
		
		if($this->get_id()==0){
			exit("--ID must be greater than 0--");
		}
		
		
		$x = $this->get_id()+1000;
		
		$y = $x*$x + 5*($x+25) - $x*10 + 456;
		
		$y += 123;
		$y *= 987;
		
		$arr = Array();
		
		array_push($arr, "U");
		array_push($arr, "V");
		array_push($arr, "W");
		array_push($arr, "X");
		array_push($arr, "Y");
		array_push($arr, "Z");
		array_push($arr, "5");
		array_push($arr, "6");
		array_push($arr, "7");
		array_push($arr, "8");
		array_push($arr, "9");
		
		$crypt_id = $y;
		
		$crypt_id = str_replace("9", "A", $crypt_id);
		$crypt_id = str_replace("8", "B", $crypt_id);
		$crypt_id = str_replace("7", "C", $crypt_id);
		$crypt_id = str_replace("6", "D", $crypt_id);
		$crypt_id = str_replace("5", "E", $crypt_id);
	
		for($i=strlen($y); $i<=12; $i++){
		//	echo($arr[$i]."<br>");
			//$crypt_id = $arr[rand(count($arr),0)] . $crypt_id;
		}

		return($crypt_id);
	
	}
	
	
	public function validate_password($p){
	
		$result = true;
		
		$result = $result && (strlen($p)>=5);
		$result = $result && (strlen($p)<20);
	
		return($result);
	
	}
	
	
	public function change_password($p){
	
		$this->update_property('password', $p);
	
	}
	
	
	
	public function validate_credentials(){
		
		$email = $this->get('email');
		$password = $this->get('password');
		
		if($email == '' or $email == null) return(false);
		if($password == '' or $password == null) return(false);
		
		//data_manager::$debug_mode=true;
		
		$result = master::get('user', "`email` LIKE '$email' AND `password` = '$password'");
		
		if($result==null){
			return(false);
		}else{
			$this->set_id($result[0]->id);
			return(true);
		}
		
	}

	public function login(){
		
		$user_id = $this->get_id();

		$crypt_user_hex=master::encrypt_data2("".$user_id."");

		setcookie("user", $crypt_user_hex, time()+60*60*24*30*12, "/");
		
	}

	public function logout(){
		
		setcookie("user", NULL, time()-1, "/");
		
	}
	
	
}	?>