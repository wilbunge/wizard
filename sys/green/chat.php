<?php

require_once('db_object.php');
	
class chat extends db_object{
	
	private $_name;
	
	public $properties = array();
	public $ni_properties = array();
	
	var $table_name = "";
	var $class_name = "";
	
	public $text;
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	
	public function post_and_get($time=0, $username='', $game=0, $messages=Array()){
	
		$result = Array();
		
	
		if(count($messages)>0){
			foreach($messages as $txt){
			
				$chat_message = new chat_message;
		
				$chat_message->set('text', $txt);
				$chat_message->set('username', $username);
				$chat_message->set('game', $game);
				
				master::store_object($chat_message);
			
			}
		}
		
		$result['post']=Array('success'=>true);
		
		
		
		////////
		
		
		$sql_result = master::get_raw_data('chat_messages', '	`username`, `text`', '(`username`<>"'.$username.'" OR '.($time==0?'true':'false').') AND `id` > '.$time.' AND `game` = '.$game.' ORDER BY `id` DESC LIMIT 10');
		$sql_result_2 = master::get_raw_data('chat_messages', 'MAX(id) as `max_id`', 'TRUE');
		
		$sql_rows_2 = mysql_fetch_array($sql_result_2);
		
		$max_id = $sql_rows_2['max_id'];
		
		$result['messages'] = Array();
		
		$this->set('time', $time);
		
		while($sql_rows = mysql_fetch_array($sql_result)){
			//if($sql_rows['max_id']==null) break;
			array_push(
				$result['messages'],
				
				Array(
					"username" => $sql_rows['username'],
					"text" => $sql_rows['text']
				)
			);
		
		}
		
		$result['messages'] = array_reverse($result['messages']);
		
		if($msg->id==null) $result['time'] = $this->get('time');
		$result['time'] = $max_id;
		
		return($result);
		
	
	}
	
	
	public function post_message($username, $text, $game=0){
	
		$chat_message = new chat_message;
	
		$chat_message->set('text', $text);
		$chat_message->set('username', $username);
		$chat_message->set('game', $game);
		
		master::store_object($chat_message);
		
	}
	
	
	public function get_messages($time=0, $username='', $game=0){
		//data_manager::$debug_mode=true;
		
		$sql_result = master::get_raw_data('chat_messages', '	`username`, `text`', '(`username`<>"'.$username.'" OR '.($time==0?'true':'false').') AND `id` > '.$time.' AND `game` = '.$game.' ORDER BY `id` DESC LIMIT 10');
		$sql_result_2 = master::get_raw_data('chat_messages', 'MAX(id) as `max_id`', 'TRUE');
		
		$sql_rows_2 = mysql_fetch_array($sql_result_2);
		
		$max_id = $sql_rows_2['max_id'];
		
		$result = Array();
		$result['messages'] = Array();
		
		$this->set('time', $time);
		
		while($sql_rows = mysql_fetch_array($sql_result)){
			//if($sql_rows['max_id']==null) break;
			array_push(
				$result['messages'],
				
				Array(
					"username" => $sql_rows['username'],
					"text" => $sql_rows['text']
				)
			);
		
		}
		
		$result['messages'] = array_reverse($result['messages']);
		
		if($msg->id==null) $result['time'] = $this->get('time');
		$result['time'] = $max_id;
		
		return($result);
		
	}
		
}


class chat_message extends db_object{
	
	private $_name;
	
	public $properties = array('username', 'text', 'game');
	public $ni_properties = array();
	
	var $table_name = "chat_messages";
	var $class_name = "";
		
}

?>