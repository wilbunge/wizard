<?php


include_once("aes.php");
//include_once("green_client.php");
//include_once("user.php");

include_once("db.php");
include_once("data_manager.php");

class master {

	private $id;
	private $_green_client;
	private static $_user;
	private static $_account;
	private static $_player;
	private static $_current_player;

	private $users = array();
	private $expenses = array();

	public $api=false;
	public $admin=false;
	
	private static $key = "abcdefghijuklmno0123456789012345"; // 256-bit key
	static $key2 = "abcdefghijuklmno0123456789012345"; // 256-bit key
	
	public function hex2bin($h){
	  if (!is_string($h)) return null;
	  $r='';
	  for ($a=0; $a<strlen($h); $a+=2) { $r.=chr(hexdec($h{$a}.$h{($a+1)})); }
	  return $r;
	}
	
	public function decrypt_data($crypt_data){
		$aes = new AES(self::key);			
		$crypt_data_bin=$this->hex2bin($crypt_data);
		$decrypt_data=$aes->decrypt($crypt_data_bin);
		return($decrypt_data);
	}
	
	public static function decrypt_data2($crypt_data){
		
		$aes = new AES(self::$key2);			
		$crypt_data_bin=self::hex2bin($crypt_data);
		$decrypt_data=$aes->decrypt($crypt_data_bin);

		return($decrypt_data);
	}
	
	static function encrypt_data2($data){
	
		$aes = new AES(self::$key2);
		$crypt_data=$aes->encrypt((string)$data);
		$crypt_data_hex=bin2hex($crypt_data);
		return($crypt_data_hex);
		
	}
	
	
	
	public function login_user($user){
		
		$user_id = $user->get_id();

		$crypt_user_hex=self::encrypt_data2("".$user_id."");

		setcookie("user", $crypt_user_hex, time()+60*60*24*30*12, "/");
		
	}
	
	
	public function logout_user(){
		
		setcookie("user", NULL, time()-1, "/");
		
	}
	
	
	public function get_user(){

		if(self::$_user == NULL){
		
			
			$crypt_user = $_COOKIE['user'];

			if($_COOKIE['user'] == null) return(false);
			
			$user_id=self::decrypt_data2($_COOKIE['user']);

			self::$_user = new user($user_id);
			
		}
		
		return(self::$_user);
		
	}
	
	
	public function set_user($val) {
		self::$_user = $val;
	}
	
	
	
	public function set_green_client($val) {
		$this->_green_client = $val;
	}
	
	public function get_green_client($p_crypt_client='') {
	
		if($this->_green_client == NULL){
			$this->_green_client = new green_client;
			
			if($p_crypt_client!=''){
				$crypt_client=$p_crypt_client;
			}else{
				$crypt_client=$_COOKIE['GCMB'];
			}
			
			$client_id=$this->decrypt_data($crypt_client);
			
			$this->_green_client->set_id($client_id);
			
		}
	
		return($this->_green_client);
	}
	
	
	
	public static function get_account() {

		if($_account === NULL){

			self::$_account = new account;

			$account_id=self::decrypt_data2($_COOKIE['GCMB']);
			
			self::$_account->set_id($account_id);

		}
	
		return(self::$_account);
	}
	
	
	public static function sign_out_current_player() {
		setcookie('BMPID', NULL, time()-1, "/");
	}
	
	public static function sign_in_player_OLD($player) {
		setcookie('BMPID', self::encrypt_data2($player->get('id')), NULL, "/");
	}
	
	
	public static function sign_in_player($email, $password) {
	
		require_once('player.php');
		
		//data_manager::$debug_mode=true;

		$obj = self::get('player', "`email`='$email' AND `password`='$password'");

		$player = $obj[0];
		
		if($player==NULL){
			throw new Exception('Wrong credentials.');
		}
		
		setcookie('BMPID', self::encrypt_data2($player->get('id')), NULL, "/");
		
		return($player);
	}
	
	
	
	public static function __login_player($username, $password){
	
		require_once('player.php');
		
		data_manager::$debug_mode=true;

		$obj = self::get('player', "`username`='$username' AND `password`='$password'");

		$player = $obj[0];
		
		if($player==NULL) return(false);
		
		return($player);
		
		
	}
	
	
	public static function validate_credentials($username, $password){
	
		require_once('player.php');
		
		//data_manager::$debug_mode=true;

		$obj = self::get('player', "`username`='$username' AND `password`='$password'");

		$player = $obj[0];
		
		if($player==NULL) return(false);
		
		return($player);
		
		
	}
	
	
	public static function check_email($email){
		
		$obj = self::get('player', "`email`='$email'");
		return($obj[0]==NULL);
		
	}
	
	public static function check_username($username){
		$obj = self::get('player', "`username`='$username'");
		return($obj[0]==NULL);
	}
	
	
	
	public function login_player($player){
		setcookie('BMPID', self::encrypt_data2($player->get('id')), NULL, "/");
		self::$_current_player = $player;
	}
	
	
	public function check_session () {
		$session = $this->get_green_client();
		if ($session == 0 || $session == "" || $session == NULL) {
			exit();
		}
	}
	
	
	public function master(){
	//	echo("I was born!");
	}
	

	public function load_db_field($field) {
		
		$sql_query = "SELECT ".$field." FROM ". $this->table_name." WHERE id = ".$this->get_id();
		//echo($sql_query);
		$result = mysql_query($sql_query);

		$felo = mysql_fetch_array($result);

		return $felo[$field];
	}
	

	public function mysql_query($p_action, $p_fields, $p_table, $p_where_clause, $p_other, $print_query = false) {
		
		if($p_other != "") $p_other = " " . $p_other;
		
		$sql_query = $p_action . " " . $p_fields." FROM " . $p_table . " WHERE (".$p_where_clause.")". $p_other;
		if ($print_query) {
			echo $sql_query."<br>";
		}
//		echo "".$sql_query . "<br/>";
		
		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("33".mysql_error());
		return($result);
	
	}
	
	
	
	public function mysql_query3($p_action, $p_fields, $p_table, $p_where_clause, $p_other,$p_write_clause = false) {
		$this->check_session();
		if($p_other != "") $p_other = " " . $p_other;
		
		$sql_query = $p_action . " " . $p_fields." FROM " . $p_table . " WHERE (".$p_where_clause.") AND green_client = ".$this->get_green_client()->get_id()."" . $p_other;
		if ($p_write_clause) {
			//echo $sql_query;	
		}
		
		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("4".mysql_error());
		return($result);	
	}


	public function select($p_class_name, $p_where_clause, $p_other='', $print_query = false) {
	
		$obj=new $p_class_name;
		
		if($p_other != "") $p_other = " " . $p_other;
		
		if($p_other == "") $p_other = " ORDER BY id DESC";
		
		$sql_query = "SELECT * FROM `" . $obj->table_name . "` WHERE (".$p_where_clause.") AND green_client = " . $this->get_green_client()->get_id() . $p_other;
		
		if ($print_query) {
			echo $sql_query."<br>";
		}

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("311".mysql_error());
		
		$arr=array();
		
		while($row=mysql_fetch_array($result)){
			$obj=new $p_class_name;
			$obj->set_id($row['id']);
			$obj->set_green_client(new green_client($row['green_client']));
			array_push($arr,$obj);
		}
		
		return($arr);
	
	}
	
	
	public function select_C0($p_class_name, $p_where_clause, $p_other='', $print_query = false) {
	
		$obj=new $p_class_name;
		
		if($p_other != "") $p_other = " " . $p_other;
		
		if($p_other == "") $p_other = " ORDER BY id DESC";
		
		$sql_query = "SELECT * FROM `" . $obj->table_name . "` WHERE (".$p_where_clause.") " . $p_other;
		
		if ($print_query) {
			echo $sql_query."<br>";
		}

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("311".mysql_error());
		
		$arr=array();
		
		while($row=mysql_fetch_array($result)){
			$obj=new $p_class_name;
			$obj->set_id($row['id']);
			$obj->set_green_client(new green_client($row['green_client']));
			array_push($arr,$obj);
		}
		
		return($arr);
	
	}


	public function mysql_query_select($p_fields, $p_table, $p_where_clause, $p_other='', $print_query = false) {
		
		if($p_other != "") $p_other = " " . $p_other;
		
		$sql_query = "SELECT " . $p_fields." FROM " . $p_table . " WHERE (".$p_where_clause.") AND " . ($this->admin?"true":"green_client = " . $this->get_green_client()->get_id()) . $p_other;
		if ($print_query) {
			echo $sql_query."<br>";
		}
		//echo $sql_query . "<br/>";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("399".mysql_error());
		return($result);
	
	}
	
	
	public function mysql_query_select_C0($p_fields, $p_table, $p_where_clause, $p_other='', $print_query = false) {
		
		if($p_other != "") $p_other = " " . $p_other;
		
		$sql_query = "SELECT " . $p_fields." FROM " . $p_table . " WHERE (".$p_where_clause.")". $p_other;
		if ($print_query) {
			echo $sql_query."<br>";
		}
		//echo $sql_query . "<br/>";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("399".mysql_error());
		return($result);
	
	}
	
	
	public function update($p_object){

		foreach($p_object->ni_properties as $p){
//			array_push($p_object->properties,$p);
		}
		
		foreach($p_object->properties as $property){
		
			$code0 = "\$p_object->get($property);";
			$code = '$val = ' . $code0;


			eval ($code);
			

			// The property is an array
			if(is_array($val)){


				//data_manager::$debug_mode=true;
				data_manager::update($p_object->class_name."_".$property, "`deleted` = 1", "`".$p_object->class_name."` = " . $p_object->get_id());
				//data_manager::update($p_object->table_name, "`$property` = '$vale'", "`id` = " . $p_object->get_id());

				foreach($val as $obj){

					// The property contains objects
					if (is_object($obj)) {

						$code1 = "\$p_object->extra['$property'];";
						$code2 = '$arr2 = ' . $code1;

						eval($code2);

						$extra_fields = '';
						$extra_values = '';

						$update_query = '';

						if(is_array($arr2)){
					
							foreach($arr2 as $val2){
						
								//$code3 = "\$obj->get($val2);";
								//$code4 = '$val3 = ' . $code3;
								
								$val3  = $obj->values[$val2];

								//eval($code4);
							
								if($extra_fields != '') $extra_fields .= ',';
								if($extra_values != '') $extra_values .= ',';
							
								$extra_fields .= "`".$val2."`";
								$extra_values .= "'".$val3."'";
							
								if($update_query!='') $update_query .= ",";
								$update_query .= "`".$val2."`='".$val3."'";
							
							}	
						
						}
						
						
						// Iterate through properties of the object in the array
						if(is_array($obj->properties_iaa)){
					
							foreach($obj->properties_iaa as $val2){
						
								$code3 = "\$obj->get_$val2();";
								$code4 = '$val3 = ' . $code3;
					
								eval($code4);
								
								if($extra_fields != '') $extra_fields .= ',';
								if($extra_values != '') $extra_values .= ',';
							
								$extra_fields .= $val2;
								$extra_values .= "'".$val3."'";
							
								if($update_query!='') $update_query .= ",";
								$update_query .= "`".$val2."`='".$val3."'";
								
							}	
						
						}

						$obj->save();

						// if($obj->get_id()>0){
							
						// 	//$result = data_manager::read($p_object->class_name . "_" . $property, "id", "`".$p_object->class_name."` = " . $p_object->get_id() . " AND `" . $obj->class_name . "` = " . $obj->get_id());

						// 	$obj->save();

						// 	//$result = self::mysql_query("SELECT", "id", $p_object->class_name . "_" . $property, "`".$p_object->class_name."` = " . $p_object->get_id() . " AND `" . $obj->class_name . "` = " . $obj->get_id(), "",false);
						// }else{
						

						// echo $obj->id."__";
						// 	$result = data_manager::read($p_object->class_name . "_" . $property, "id",  "`".$p_object->class_name."` = " . $p_object->get_id() . " AND `value` = '" . $obj->get_default() . "'");
						// 	//$result = self::mysql_query("SELECT", "id", $p_object->class_name . "_" . $property, "`".$p_object->class_name."` = " . $p_object->get_id() . " AND `value` = '" . $obj->get_default() . "'", "",false);
						
						
						
						// //echo "<br>-".mysql_num_rows($result)."-<br>";
						
						// 	if(mysql_num_rows($result)>0){
						// 		if($update_query!='') $update_query .= ",";							
						// 		$row = mysql_fetch_array($result);							
						// 		//$this->mysql_query_update($p_object->class_name . "_" . $property, $update_query . "`deleted` = 0", "`id` = " . $row['id']);
						// 		data_manager::update($p_object->class_name . "_" . $property, $update_query . "`deleted` = 0", "`id` = " . $row['id']);
						// 	}else{
								
						// 		if($extra_fields != "") {
						// 			$extra_fields .= ",";
						// 			$extra_values .= ",";	
						// 		}
								
						// 		$extra_fields .= "`".$p_object->class_name . "`,`".$obj->class_name."`";
						// 		$extra_values .= $p_object->get_id() . "," . $obj->get_id();
							
							
						// 		data_manager::create($p_object->class_name . "_" . $property, $extra_fields, $extra_values);
								
						// 	}

//						}

					
					}else{
					
						$result = self::get_raw_data($p_object->class_name . "_" . $property, 'id', "`".$p_object->class_name."` = " . $p_object->get_id() . " AND `value` = '" . $obj."'");
						
						
						//echo "<br>-".mysql_num_rows($result)."-<br>";
						
						if(mysql_num_rows($result)>0){
//							echo $obj."===<br />";
							$this->mysql_query_update($p_object->class_name . "_" . $property, $update_query . " deleted = 0", $p_object->class_name . " = " . $p_object->get_id() . " AND `value` = '" . $obj . "' AND deleted=1",false);
						}else{
						echo ";";
						
							$this->mysql_query_insert($p_object->class_name . "_" . $property, $p_object->class_name . ", `value`", $p_object->get_id(). ", '".$obj."'",false);
						
							//$extra_fields .= "," . $p_object->class_name . "," . $obj->class_name;
							//$extra_values .= "," . $p_object->get_id() . "," . $obj->get_id();
						
							//$this->mysql_query_insert($p_object->class_name . "_" . $property, $extra_fields, $extra_values);
						}
					
						
						//$this->mysql_query_insert($p_object->class_name . "_" . $property, $p_object->class_name . ", value", $p_object->get_id(). ", '".$obj."'");
					}
					//echo(($sql_query_2));
					//mysql_query($sql_query_2) or die(mysql_error()."-->".$sql_query_2);
			
		
				}
			
			
			}else{

				if ($p_object->property_types2[$property]['is_object'] || is_object($val)) {
					$val = $val->get_id();
				}
					
				if($update_clause != '') $update_clause .= ', ';
				$update_clause .= '`' . $property . "` = '" . $val . "'";
		
				if($values != '') $values .= ', ';
				$values .= "'". $val . "'";

				if($fields != '') $fields .= ', ';
				$fields .= '`' . $property . '`';
			}
		}
		



		data_manager::update($p_object->table_name, $update_clause . ", `deleted` = 0", "`id` = " . $p_object->get_id());

		//$result = mysql_query($sql_query) or exit("5".mysql_error());	
		
		
		//self::db_log("UPDATE",$p_object->class_name,$p_object->get_id());
		
	}
	
	
	public function update_property($p_obj, $p_property, $p_value){
	
		if($p_obj->class_name=='account' || $p_obj->class_name=='lead_area'){
			data_manager::$db = 1;
		}else{
			data_manager::$db = 2;
		}
	
		data_manager::update($p_obj->table_name, "`$p_property` = '$p_value'", "`id` = " . $p_obj->get_id());
		
	}
	
	
	public function __store($p_object){
		self::store_generic($p_object);
		$p_object->store_append();
	}
	

	public function store_generic($p_object){
		//$this->check_session();
		$new_array = array();

		$query_2 = "DESCRIBE " . $p_object->table_name;
		$result_2 = mysql_query($query_2);

		//$line_2 = mysql_fetch_array($result_2);
		
		//echo($line_2[1]);
		$n = 0;
		
		$arr = Array();
		
		/*while ($line_2 = mysql_fetch_array($result_2)){
			array_push($new_array,$line_2);
			$arr[$new_array[$n][0]] = $new_array[$n][1];
			$n++;
		}*/
		

		$values = '';
		$fields = '';


		//$all_properties = array_merge($p_object->properties, $p_object->hidden_properties, $p_object->ni_properties, $p_object->oon_properties);
		$all_properties = array_merge($p_object->properties, $p_object->hidden_properties, $p_object->oon_properties);
		
		foreach($all_properties as $property){
		
		
			if(method_exists($p_object,"get_$property")){

				$code0 = "\$p_object->get_$property();";
				$code = '$val = ' . $code0;
				
				eval ($code);
				
			}else{
				
				$val = $p_object->get_p($property);
				
			}
			
			
			if(is_array($val)){
			
				
			
			}else{
				
				if($values != '') $values .= ', ';
		
				if (gettype($val) == "object" ) {
					$values .= $this->green_mysql_value_wrap($val->get_id());
				} else {
					$values .= $this->green_mysql_value_wrap($val);
				}
				
				if($fields != '') $fields .= ', ';
				$fields .=  "`".$property."`";
			
			}

		}
		
		
		
		$fields .=  ", green_client";
		$values .= "," . $this->green_mysql_value_wrap($this->get_green_client()->get_id());
		
		$sql_query = 'INSERT INTO `'.$p_object->table_name.'` ('.$fields.') VALUES ('.$values.')';
		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		
		$sql_query2 = 'SELECT id FROM '. $p_object->table_name.' WHERE NOT deleted ORDER by id desc' ;
		//echo "<br>".$sql_query2."-----===<br>";
		$result = mysql_query($sql_query2);
		
		$felo = mysql_fetch_array($result);
		
		$p_object->set_id($felo['id']);
		
		// **** Double Inheritance
		
		if(isset($p_object->table_name_2)){
		
		if($p_object->table_name_2 != ""){
		
			$fields = $p_object->class_name;
			$values = $p_object->get_id();
			
			foreach($p_object->eon_properties as $p){
				
				if($values != '') $values .= ', ';
		
				if (gettype($val) == "object" ) {
					$values .= "'". $val->get_id() . "'";
				} else {
					$values .= "'". $val . "'";
				}
				
				if($fields != '') $fields .= ', ';
				$fields .=  "`".$p."`";
			}
			
			$fields .=  ", green_client";
			$values .= "," . $this->get_green_client()->get_id();
			
			$sql_query = 'INSERT INTO `'.$p_object->table_name_2.'` ('.$fields.') VALUES ('.$values.')';
			mysql_query($sql_query) or exit("5".mysql_error());

		}
		}
		// **** 
		
		foreach($p_object->properties as $property){
			
			if(method_exists($p_object,"get_$property")){
				$code0 = "\$p_object->get_$property();";
				$code = '$val = ' . $code0;
				eval ($code);
			}else{
		
				$val = $p_object->get_p($property);
			}
		
			
			
			//echo $property . "-> " . is_array($val) . "<br />";
			
			if(is_array($val)){
		
				foreach($val as $obj){
					//echo ".";
					if (gettype($obj) == "object" ) {
					
						$code1 = "\$p_object->extra['$property'];";
						$code2 = '$arr2 = ' . $code1;
						
						eval($code2);
						
						$extra_fields = '';
						$extra_values = '';
						
						$arr3 = $obj->properties_iaa;
						
						// Iterate through properties of the object in the array
						if(is_array($arr3)){
				
							foreach($arr3 as $property2){
								
								$code3 = "\$obj->get($property2);";
								$code4 = '$val3 = ' . $code3;
								
					
								eval($code4);
								
								if($extra_fields != '') $extra_fields .= ',';
								if($extra_values != '') $extra_values .= ',';
							
								$extra_fields .= $property2;
								$extra_values .= "'".$val3."'";
								
							}
		
						}
						
						// Iterate through properties of the object in the array 2
						if(is_array($arr2)){							

							foreach($arr2 as $val2){
							
								$code3 = "\$obj->get($val2);";
								$code4 = '$val3 = ' . $code3;
																
								eval($code4);
								
								if(is_object($val3)) $val3=$val3->get_id();
								
								if($extra_fields != '') $extra_fields .= ',';
								if($extra_values != '') $extra_values .= ',';
								
								$extra_fields .= "`$val2`";
								$extra_values .= "'$val3'";
							}	
							
						}
						
						if($extra_fields != '') $extra_fields .= ',';
						if($extra_values != '') $extra_values .= ',';
						
						$sql_query_2 = "INSERT INTO " . $p_object->class_name . "_" . $property . "(`".$p_object->class_name."`, `".$obj->class_name."`, ".$extra_fields."green_client) VALUES(".$p_object->get_id().", ".$obj->get_id().", ". $extra_values . $this->get_green_client()->get_id() .")";
					}else{
						$sql_query_2 = "INSERT INTO " . $p_object->class_name . "_" . $property . "(`".$p_object->class_name."`, `value`, green_client) VALUES(".$p_object->get_id().", '".$obj."', ". $this->get_green_client()->get_id() .")";
						
					}
					
					//echo(($sql_query_2));

					mysql_query($sql_query_2) or exit($this->GRERROR("A", mysql_error()));
					// mysql_query($sql_query_2) or die(mysql_error()."-->".$sql_query_2);
				}
			
			}
		}
		
		foreach($p_object->tsp_properties as $p => $table_name){
			$code5 = "\$p_object->get_$p();";
			$code6 = '$val4 = ' . $code5;
						
			eval($code6);
		
			$sql_query_3 = "INSERT INTO " . $table_name . "(`".$p_object->class_name."`, `".$p."`, green_client) VALUES(".$p_object->get_id().", '".$val4."', ". $this->get_green_client()->get_id() .")";
			mysql_query($sql_query_3);
		}
		
		$this->db_log("INSERT",$p_object->class_name,$p_object->get_id());
		
	}
	
	
	
	public function delete($p_object){
		
		data_manager::update($p_object->table_name, "`deleted` = 1", "`id` = " . $p_object->get_id());
	
	}
	
	
	public function restore($p_object){
	
		$sql_query = 'UPDATE '.$p_object->table_name.' SET DELETED = 0 WHERE ID = ' . $p_object->get_id();
		//echo $sql_query;
		$result = mysql_query($sql_query) or exit("2".mysql_error());
		$this->db_log("RESTORE",$p_object->class_name,$p_object->get_id());
	
	}
	
	
	public function html_input_property($p_property) {
		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		
		$html = '';
		
		switch ($p_property) {
	
			case 'users' :
			
				$html = '<select name="users[]" multiple size="5">';

				foreach ($this->users as $key => $value) {
					
					if ($this->has($value)) {
						$html .= '	<option selected value="' . $value->get_id() . '">' . $value->get_name() . '</option>';
					} else {
						$html .= '	<option value="' . $value->get_id() . '">' . $value->get_name() . '</option>';
						
					}
					
				}
				
				$html .= "</select>";
				break;
				
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
		}
		
		return $html;
	}
	
	
	public function send_emails() {
		
		// EMAIL FOR SENDER AND RECEIVER
		
		$subject = "You have received a IGNES Gift Card from ".$this->get_sender()->get_first_name()." ".$this->get_sender()->get_last_name();
		$body = "congratulations";
//		$to = $this->get_recipient().", ".$this->get_sender();
		$to = "carreraf@adinet.com.uy, fcarrera@avanfeel.com";
		mail($to,$subject,$body);
		
	}
	
	
	public function catch_post_properties() {

		foreach($this->properties as $property){
			$post_val = $_POST[$property];
			$code0 = "\$this->set_$property('$post_val');";
			//$code = '$val = ' . $code0;
			eval ($code0);
		}
	}	
	
	
	public function authenticate($p_user){
	
		if($p_user->get_username()=='' || $p_user->get_password()=='') return(false);
		
		$sql_query = "SELECT * FROM users WHERE username LIKE '".mysql_real_escape_string($p_user->get_username())."' AND password = '".mysql_real_escape_string($p_user->get_password())."' AND green_client = ".$p_user->get_green_client()->get_id()." AND NOT deleted" ;
	
		//echo $sql_query;
		$result = mysql_query($sql_query);
		
		if($row = mysql_fetch_array($result)){
			$p_user->set_id($row['id']);
			return(true);
		}else{
			return(false);
		}		
		
	}
	
	
	public function check_email_OLD($p_email){
		$sql_query = "SELECT * FROM users WHERE email LIKE '".$p_email."' AND NOT deleted" ;		
		$result = mysql_query($sql_query);		
		return(mysql_num_rows($result)==0);		
	}
	
	
	
	
	
	function encrypt_date($p_date, $p_key){
		//$p_date
		
		date_add($p_date, $p_key);
		//echo($date_crypt);
		
	}
	
	
	function set_module_access($p_user, $p_modules){
	
		$sql_query = "UPDATE user_modules SET has_access = FALSE WHERE user = '".$p_user->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."'";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("ERR");
		//echo($sql_query.".<br />");
		
		foreach($p_modules as $green_module){
			
			$sql_query_3 = "SELECT id FROM user_modules WHERE user = '".$p_user->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."' AND module = " . $green_module->get_id();
			$result = mysql_query($sql_query_3);
			
			if(mysql_num_rows($result)==0){
				$sql_query_2 = "INSERT INTO user_modules(`has_access`, `user`, `green_client`, `module`) VALUES(TRUE, ".$p_user->get_id().", ".$this->get_green_client()->get_id().", " . $green_module->get_id() . ")";
			}else{
				$sql_query_2 = "UPDATE user_modules SET has_access = TRUE WHERE user = '".$p_user->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."' AND module = " . $green_module->get_id();
			}
			//echo $sql_query_2."<br />";

			mysql_query($sql_query_2) or exit($this->GRERROR("A", mysql_error()));
			// mysql_query($sql_query_2) or die("err");
			
		}
	
	}
	
	
	function new_account(){
		
		$sql_query = "UPDATE user_modules SET has_access = FALSE WHERE user = '".$p_user->get_id()."' AND green_client = '".$this->get_green_client()->get_id()."'";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("ERR");
		
	}
	
	function preset_ql(){
		
	}
	
	function check_session_integrity(){
		return(true);
	}
	
	
	public function db_log($action, $ref_type, $ref_id){
	
		$date=date("Y-m-d H:i:s", time());
	
		$sql_query = "INSERT INTO db_log(action,ref_type,ref_id,`user`,`date`,green_client) VALUES('$action', '$ref_type', '$ref_id', ".$this->get_user()->get_id().", '$date',".$this->get_green_client()->get_id().")";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("ERR");
	
	}
	
	
	
	public function query_db_log($ref_type, $ref_id){
	
		$sql_query = "SELECT * FROM db_log WHERE ref_type='$ref_type' AND ref_id='$ref_id' AND green_client = ".$this->get_green_client()->get_id()." ORDER BY id DESC";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("ERR");
		
		$row=mysql_fetch_array($result);
		
		return($row);
	
	}
	
	public function get_db_log_last_activity($limit=10){
	
		$sql_query = "SELECT * FROM db_log WHERE green_client = ".$this->get_green_client()->get_id()." ORDER BY id DESC LIMIT $limit";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("ERR");

		return($result);
	
	}
	
	
	public function calculate_db_size(){
		
		$sql_query = "SHOW TABLE STATUS";

		$result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		// $result = mysql_query($sql_query) or die("ERR".mysql_error());

		$row=mysql_fetch_array($result);
		
		echo $row["X"];
		
		return($row);
		
	}
	
	
	public function sign_in($user){
	
		$token=$user->get_green_client()->get_id()*$user->get_id()+291280;
		
		$crypt_token=security::encrypt_data("".$token."");
		$crypt_user=security::encrypt_data("".$user->get_id()."");
		$crypt_client=security::encrypt_data("".$user->get_green_client()->get_id()."");
		$crypt_name=security::encrypt_data("".$user->get_green_client()->get_domain()."");
		
		setcookie("GCFC", $crypt_user, NULL, "/");
		setcookie("GCMB", $crypt_client, NULL, "/");
		setcookie("GCCB", $crypt_token, NULL, "/");
		setcookie("GCDB", $crypt_name, NULL, "/");
	
	}
	
	
	public function db_connect($db){
		green_db_connect($db);
	}
	
	
	
	public function hit_module(){
	
		$this->db_connect(2);		
		
		$user=$this->get_user()->get_id();		
		$path=$_SERVER["SCRIPT_NAME"];
		$arr=explode("/", $path);
		$kw=$arr[count($arr)-2];
		
		$this->mysql_query_insert("app_hits", "app,user,date", "'$kw',$user,NOW()");
	
	}
	
	
	public function GRERROR($type, $err){
	
		$err=mysql_real_escape_string($err);
		
		$error2=false;
		$code="#GER-$type"."_".date('ymdGis')."-".rand(1,1000);
	
		$this->db_connect(2);
		mysql_query("INSERT INTO
		
			green_bugs(`description`,`code`,`date_reported`,`green_client`,`green_user`)
			VALUES('$err','$code',NOW(),".$this->get_green_client()->get_id().",".$this->get_user()->get_id().")")
			
			or $error2=true;
		
		if($error2){
			$msg="AN ERROR HAS OCCURED!<br /> NO ERROR CODE";
		}else{
			$msg="AN ERROR HAS OCCURED!<br /> ERROR CODE $code";
		}
		
		
		if(function_exists("message")){
			$html=message(3, $msg);
			echo $html;
		}else{
			echo $msg;		
		}
		
	}
	
	
	private function green_mysql_value_wrap($v){		
		// return("'".mysql_real_escape_string($v)."'");		
		return("'".($v)."'");		
	}
	
	public function db_2_array($p_table, $p_wc='TRUE', $p_ob=''){
	
		$arr = Array();
		
		$this->db_connect(1);
		
		//$sql_query = "SELECT * FROM `business_skills` WHERE `business_type` = 3 ORDER BY `skill_name_spa`";
		//ORDER BY `skill_name_spa`
		$sql_query = "SELECT * FROM `$p_table` WHERE $p_wc" . ($p_ob!=''?" ORDER BY $p_ob":"");
		
		$result = mysql_query($sql_query) or die(mysql_error());
		
		while ($line = mysql_fetch_array($result)){
			array_push($arr, $line);
		}
		
		return($arr);
		
	
	}
	
	
	public function mysql_select_static($p_table, $p_wc='TRUE', $p_ob='', $echo=false){
	
		$sql_query = "SELECT * FROM `$p_table` WHERE $p_wc" . ($p_ob!=''?" ORDER BY $p_ob":"");
		if($echo) echo "$sql_query<br>";
		$result = mysql_query($sql_query) or die(mysql_error());
		
		return($result);
		
	
	}
	
	
	public function generate_client($id){
		$gc = new green_client($id);
		$gc->set_master($this);
		$this->set_green_client($gc);
		
		$u = new user(0);
		$u->set_master($this);
		$this->set_user($u);
		
		
		return($gc);
	}
	
	
	
	function get_clients_by_specialty($skill_id){
	
		global $ai_master;
		
		$non_deleted_clients = Array();
		$competent_clients = Array();
		
		
		$ai_master->db_connect(1);
		
		$sql_query = "SELECT `id` FROM `clients` WHERE NOT `deleted`";
		$sql_result = mysql_query($sql_query) or die("ERR: ".mysql_error());
		
		while($sql_row = mysql_fetch_array($sql_result)){
			array_push($non_deleted_clients, $sql_row['id']);
		}
		
		$ai_master->db_connect(2);
		
		$sql_query = "SELECT `green_client` FROM `client_spec_areas` WHERE NOT `deleted` AND `area` = $skill_id GROUP BY `green_client`";
		$sql_result = mysql_query($sql_query) or die("ERR: ".mysql_error());
		
		while($sql_row = mysql_fetch_array($sql_result)){
		
			$ga = new green_client($sql_row['green_client']);
		
			if(in_array($sql_row['green_client'], $non_deleted_clients)) array_push($competent_clients, $ga);

		}
		
		return($competent_clients);
		
	}
	
	
	
	public function load_green_account_by_area($a){
		
		$sql_query = "SELECT `green_client` FROM `client_spec_areas` WHERE `area` = '$a'";
		$result = mysql_query($sql_query);
		
		$arr = Array();
		
		while($row = mysql_fetch_array($result)){
			array_push($arr, new green_client($row['green_client']));
		}
		
		return($arr);
		
		
	}
	
	
	public function load_green_account_by_username($username){
	
		$sql_query = "SELECT `green_client` FROM `users` WHERE `username` LIKE '$username' AND NOT `deleted`";
		//echo $sql_query;
		$sql_result = mysql_query($sql_query) or exit($this->GRERROR("A", mysql_error()));
		
		$sql_row = mysql_fetch_array($sql_result);

		return(new green_client($sql_row['green_client']));
	
	}
	
	public function check_leads_status(){
		
		$sql_result = $this->mysql_query_select_C0('COUNT(*) as `c`, `status`', 'sales_leads', 'NOT deleted', 'GROUP BY `status`');
		
		while($mysql_row = mysql_fetch_array($sql_result)){
			if($json!='') $json .= ", ";
			$json .= '{"status":"' . $mysql_row['status'] . '", "count":' . $mysql_row['c'].'}';
		}
		
		$json = "[$json]";
		
		return($json);
		
	}
	
	
	public function check_lead_rejections($clients, $lead){
	
		$clients2 = Array();
	
		foreach($clients as $c){
		
			if($lead->check_if_rejected($c)){
				$c->set('var', true);
			}

			array_push($clients2, $c);
		
		}
		
		return($clients2);
	
	}
	
	public function get_raw_data($table_name, $fields, $where_clause='TRUE'){
		
		$sql_result = data_manager::read($table_name, "$fields", $where_clause);
		
		return($sql_result);
		
	}
	
	
	public function get_data($obj, $field){
		
		$sql_result = data_manager::read($obj->table_name, "`$field`", '`id`="'.$obj->get_id().'"');
		$sql_row = mysql_fetch_array($sql_result);
		
		return($sql_row[$field]);
		
	}
	
	
	
	public function get_object($class_name, $id){
	
		$obj = self::get($class_name, "`id`='$id'");
		
		return($obj[0]);
	
	}
	
	public function load($class_name, $where_clause='TRUE', $extra_clause=''){
		return(self::get($class_name, $where_clause, $extra_clause));
	}
	
	
	public function get($class_name, $where_clause='NOT deleted', $extra_clause=''){
	
		if($class_name=='account' || $class_name=='lead_area' || $class_name=='specialty'){
			data_manager::$db = 1;
		}else{
			data_manager::$db = 2;
		}
		
		//echo $class_name .' - ' . data_manager::$db ."<br>";

		self::_include($class_name);

		$obj = new $class_name;
		$objects = Array();
		
		$b = debug_backtrace();
		
		$caller = $b[1]['class'];
		
		$sql_result = data_manager::read($obj->table_name, '*', $where_clause, $extra_clause);
		
		if(mysqli_num_rows($sql_result)==0) return(false);

		while($sql_row = mysqli_fetch_array($sql_result)){

			$obj = new $class_name($sql_row['id']);
			$obj2 = new $class_name;
			
			$obj->set('deleted', $sql_row['deleted']);
			
			if(data_manager::$db == 2) $obj->set_green_client($sql_row['green_client']);
			
			foreach($obj->properties as $p){

				if(is_object($obj2->get($p))){

					if($sql_row[$p]!='0'){
						
						// $class_name_2 = $obj2->get($p)->class_name;
						// $obj3 = self::get_object($class_name_2, $sql_row[$p]);
						// $obj->set($p, $obj3);
					}

				}else{
					$obj->set($p, $sql_row[$p]);

				}
			
			
			}
			
			array_push($objects, $obj);
		}
		
		//if(count($objects)==1) $objects = $objects[0];
		
		return($objects);
	
	}
	
	
	public function store($p_object){

		//$this->check_session();
		$new_array = array();

		$class_name = $p_object->class_name;

		$n = 0;
		
		$arr = Array();
		
		/*while ($line_2 = mysql_fetch_array($result_2)){
			array_push($new_array,$line_2);
			$arr[$new_array[$n][0]] = $new_array[$n][1];
			$n++;
		}*/
		

		$values = '';
		$fields = '';


		//$all_properties = array_merge($p_object->properties, $p_object->hidden_properties, $p_object->ni_properties, $p_object->oon_properties);
		
		$all_properties = array_merge($p_object->properties, $p_object->hidden_properties, $p_object->oon_properties, $p_object->ni_properties);
		
		foreach($all_properties as $property){

			if(method_exists($p_object,"get_$property")){

				$code0 = "\$p_object->get_$property();";
				$code = '$val = ' . $code0;
				
				eval ($code);
				
			}else{
				
				$val = $p_object->get_p($property);
				
			}
			
			
			if(is_array($val)){
			
				// echo $class_name.': '.$property.'_<br>';
			
			}else{
				
				if($values != '') $values .= ', ';
		
				if (gettype($val) == "object" ) {
					$values .= self::green_mysql_value_wrap($val->get_id());
				} else {
					$values .= self::green_mysql_value_wrap($val);
				}
				
				if($fields != '') $fields .= ', ';
				$fields .=  "`".$property."`";
				
				
			
			}

		}
		
		$b = debug_backtrace();
		$caller = $b[1]['class'];
		
		if($caller!='admin'){
			//$fields .= ', `user`';
			//$values .= ", '".self::get_user()->get_id()."'";
		}

		$mysqli_insert_id = data_manager::create($p_object->table_name, $fields, $values);

		$p_object->set_id($mysqli_insert_id);

		foreach($all_properties as $property){

			$val = $p_object->get_p($property);

			if(is_array($val)){
			
				foreach ($val as $array_object) {
					if(gettype($array_object)=='object'){
						// Proceed as if element is an object
						$array_object->set($class_name, $p_object->id); // Set its property ('$class_name') to the ID we've just generated
						$array_object->save();
					}
				}
			
			}
		}
		
		$p_object->store_after();
	
	}
	
	
	public function generate_ranking($theme){
	
		$sql_result = self::get_raw_data('results', 'SUM(`prize`) AS `total_prize`, `player`', '`theme` = '.$theme->get('id').' GROUP BY `player` ORDER BY `total_prize` DESC');
		
		$arr = Array();
		
		while($sql_row = mysql_fetch_array($sql_result)){
			
			$player = new player($sql_row['player']);
			
			array_push($arr, Array('total_prize' => $sql_row['total_prize'], 'player' => $player));
		}
		return($arr);
		
	}
	
	public function check_facebook_player($facebook_id){
	
		$player = self::get_facebook_player($facebook_id);

		return($player!=NULL);
	
	}
	
	
	public function get_facebook_player($facebook_id){
		$result = self::get('player', "`facebook_id` = '$facebook_id' AND `facebook_id` <> 0 AND NOT `deleted`");
		return($result[0]);
	
	}
	
	
	public function register_facebook_player($facebook_id, $facebook_username){
	
		$player = new player;
		$player->set('facebook_id', $facebook_id);
		$player->set('username', $facebook_username);
		$player->set('balance', 1000); // Initial credit
		
		self::store_object($player);
		
		return($player);
	
	}
	
	public function register_player($player){
	
		if(!self::check_email($player->get('email'))) throw new Exception('Email already exists.');
		//if(!self::check_username($player->get('username'))) throw new Exception('Username already exists.');
	
		self::store_object($player);
		
		return($player);
	
	}
	
	
	public function set_player($player){
	
		self::$_player = $player;
	
	}
	
	public function get_current_player(){
		if(self::$_current_player == null) self::$_current_player = new player(self::decrypt_data2($_COOKIE['BMPID']));
		return(self::$_current_player->get('id')>0 ? self::$_current_player : false);
	}
	
	
	public function update_rankings(){}
	
	
	public function calculate_payout(){
		
		$payout = 0;
		
		$sql_result = self::get_raw_data('results', 'SUM(`prize`)/SUM(`bet`) as `payout`', 'TRUE');
		
		$sql_rows = mysql_fetch_array($sql_result);
		
		$payout = $sql_rows['payout'];
		
		$payout = round($payout*100);
		
		$payout = "$payout %";
		
		return($payout);
	
	}
	
	
	public function sort_wish_list($o){
		
		require_once('wish.php');

		$arr = explode(',', $o);
		$n=1;
		foreach($arr as $id){
			$n++;
			$wish = new wish($id);
			self::update_property($wish, 'order', $n);
		}
		
	}
	
	
	public function set_language($code){
		setcookie('BMLANG', $code, NULL, "/");
	}
	
	public function get_language(){
	
		$lang = $_COOKIE['BMLANG'];
		
		if($lang=='') $lang = 'pt_BR';
		
		return($lang);
	}
	
	
	function translate_text($k){
	
		$lang = $_COOKIE['BMVSLANG'];
		
		if($lang == null) $lang = 'pt_BR';
		
		$xml_file = "lang/$lang.xml";
		
		if(!file_exists($xml_file)) return("(-1-) $k");
		
		$xml = simpleXML_load_file($xml_file);
		
		$code0 = "\$xml->$k;";
		$code = '$t = ' . $code0;
		
		eval($code);
		
		if($t=='') return("(-2-) $k");

		return($t);
			
	}
	
	
	public function store_facebook_requests($friends){
	
		require_once('player.php');
		require_once('facebook_request.php');
		
		$count=0;
		
		if(!is_array($friends)) return(false);

		foreach($friends as $id){
			$fr = new facebook_request;
			
			$fr->set('player', master::get_current_player());
			$fr->set('to', $id);
			
			self::store_object($fr);
			$count++;
		}
		
		$prize = $count*200;
		
		$current_balance = self::prize_player(master::get_current_player(), $prize);
		
		return((array('invites' => $count, 'balance' => $current_balance)));
		
	}
	
	
	public function prize_player($player, $prize){
		$current_balance = $player->get('balance');
		$total = $current_balance + $prize;
			
		$player->update_property('balance', $total);
		
		return($total); // Return current balance
	}
	
	
	
	public function load_theme_by_domain($domain){

		$domain = str_replace('www.', '', $domain);
		
		$themes = self::get('theme', '`domain` = "'.$domain.'"');

		return($themes[0]==null ? false : $themes[0]);
	}	
	
	
	public function load_landing_by_domain($domain){

		$domain = str_replace('www.', '', $domain);
		$landings = self::get('landing', '`domain` = "'.$domain.'"');

		return($landings[0]==null ? false : $landings[0]);
	}
	
	
	public static function generate_db($cn){
	
		require_once("$cn.php");
	
		$o = new $cn;
		
		array_push($o->properties, 'deleted');

		array_push($o->properties, '__project');
		$o->property_types['__project']=='int';
		
		$all_properties = array_merge($o->properties, $o->hidden_properties, $o->oon_properties, $o->ni_properties);
		
		data_manager::connect();
	
		foreach($all_properties as $p){
		
			if($o->property_types[$p] == 'text'){
				$data_type = "text NOT NULL";
			}elseif($o->property_types[$p] == 'double'){
				$data_type = "double NOT NULL";
			}elseif($o->property_types[$p] == 'int' || $o->property_types_2[$p]['is_object']){
				$data_type = "int NOT NULL";
			}elseif($o->property_types[$p] == 'bool' || $p == 'deleted'){
				$data_type = "tinyint NOT NULL";
			}elseif($o->property_types[$p] == ''){
				$data_type = "VARCHAR(100) NOT NULL";
			}elseif($o->property_types[$p] == 'date'){
				$data_type = "DATE NOT NULL";
			}else{
				$data_type = "int NOT NULL";
			}
		
			
			mysql_query("CREATE TABLE IF NOT EXISTS `".$o->table_name."`(`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY)") or die(mysql_error());
			
			$exists = false;

    		$columns = mysql_query("show columns from `".$o->table_name."`") or die(mysql_error());
   	
   			while($c = mysql_fetch_assoc($columns)){
				if($c['Field'] == $p){
            		$exists = true;
		            break;
    		    }
    		}
    	    
    	    if(!$exists){
				$sql = "ALTER TABLE `".$o->table_name."` ADD `$p`  $data_type";
				$result = mysql_query($sql) or die($sql);
			}    	
	
		}
	
	}

	static function _include($class){

		$path = dirname(__FILE__)."/$class.php";

		if(file_exists($path)){
			include_once($path);
		}

	}

	public function save($obj){
		
		if($obj->id>0){
			if($parent = $obj->get_parent()) master::update($parent);
			master::update($obj);
		}else{
			if($parent = $obj->get_parent()) master::store($parent);
			master::store($obj);
		}
		
	}


	public function get_one($class_name, $where_clause='TRUE', $extra_clause=''){
		
		$results = self::get($class_name, $where_clause, $extra_clause);
		
		return($results[0]==null?false:$results[0]);
		
	}
	
	
		
}

class _error{
	
	public $type;

	public function __construct($type=''){
		$this->type = $type;
	}
	
}

$ai_master=new master();	?>