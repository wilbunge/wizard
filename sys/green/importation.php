<?php

include_once('db_object.php');
	
class importation extends db_object{
	
	public $properties = array('total', 'date:date', 'products');
	
	var $table_name = "importations";
	var $class_name = "importation";

	private $_products = array();

	public function save(){

		$this->set('date', date('Y-m-d'));

		parent::save($this);

	}

	public function set_data($source) {

		parent::set_data($source);

		$this->values['products'] = array();

		foreach($this->properties as $property){
			
			if($property == 'products'){
				$this->_products = array();

				$post_val = $source[$property];

				if(is_array($post_val)){

					foreach($post_val as $arr_val){


						$product = new product($arr_val['id']);
						$importation_product = new importation_product;

						$product->set('importation', $this->id);
						$importation_product->set('amount', $arr_val['amount']);
						$importation_product->set('product', $product);



						$importation_product->set('importation_unit_price', $importation_product->get('product')->get('price'));

						//echo $arr_val.'++++++';

						$importation_product->calculate_subtotal();
						
						$this->values['products'][] = $importation_product;
					}
				}

			}
			
		}
	}


	public function get_products(){
	
		if ($this->values['products']!=null){
			return $this->values['products'];
		}
	
		if ($this->get_id() > 0 and $this->values['products']=='') {

			$products = array();
			
			$result = data_manager::read('importation_products', "`product`,`id`", "NOT deleted AND `importation` = ".$this->get_id());;
			
			$this->values['products']=Array();
			
			while ($line = mysql_fetch_array($result)){

				$importation_product = new importation_product;
				
				$importation_product->set('product', new product($line['product']));
				$importation_product->set_id($line['id']);
				$importation_product->set_container($this);
				
				$this->values['products'][] = $importation_product;
				
			}

		}else{
			$this->values['products']=Array();
		}

		return $this->values['products'];
	}


	public function update_inventory(){

		foreach($this->get_products() as $importation_product){
			$importation_product->get('product')->inventory_movement($importation_product->get('amount'), $this);
		}
		
	}
	
	
}


class importation_product extends db_object{

	public $properties = array('importation','product:product*', 'amount');

	var $table_name = "importation_products";
	var $class_name = "importation_product";


	public function calculate_subtotal(){
		$unit_price = $this->get('product')->get('price');
		$subtotal = $this->get('order_amount')*$unit_price;

		$this->set('order_subtotal', $subtotal);
	}
	
}	?>