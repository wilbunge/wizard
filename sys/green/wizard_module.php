<?php

include_once('db_object.php');

class wizard_module extends db_object{
	
	private $_name;
	private $_description;
	private $_keyword;
	private $_company_area;
	private $_mobile;
	
	public $properties = array('name','keyword');
	
	var $table_name = "wizard_modules";
	var $class_name = "wizard_module";
	
	public $cfg=array();
	public $cfg_it=array();
	public $cfg_attr;
	
	public $data_grid_cfg=array();
	
	public function get_name_spa(){

		return $this->get('name');

	}
	
	public function embed_icon($p_mode){
	
		switch($p_mode){
			case 2 : 
				$img_path = "images/app-icons/".$this->get_keyword()."_2.png";
				break;
			default :
				$img_path = "images/app-icons/".$this->get_keyword()."_1.png";
				break;
		}
		
		if(!file_exists($img_path)){
			switch($p_mode){
				case 2 : 
					$img_path = "images/app-icons/none_icon_2.png";
					break;
				default :
					$img_path = "images/app-icons/none_icon_1.png";
					break;
			}
		}
	
		$tag = "<img green_app_name='".$this->get_name()."' src='$img_path' />";
	
		return $tag;
	
	}
	
	
	function ___construct($p_kw="") {
       $this->set_keyword($p_kw);
	}
	
	
	
	// loads a db field that's specified in the argument
	public function __load_db_field($field) {

	
		$sql_query = "SELECT ".$field." FROM modules WHERE id = ".$this->get_id();
		//echo($sql_query);
		$result = mysql_query($sql_query);

		$felo = mysql_fetch_array($result);

		return $felo[$field];
	}
	
	
	public function get_actions($p_module, $p_id, $p_permissions){
	
		$html = "<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'view', {id:'".$p_id."'});\">";
		$html .= "ver";
		$html .= "</a>";
		
		$html .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'edit', {id:'".$p_id."'});\">";
		$html .= "editar";
		$html .= "</a>";
		
		$html .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"javascript:G_jsf_delete('".$p_module."', {id:'".$p_id."'}, this);\">eliminar</a>";
		
		return($html);
		
	}
	
	
	public function get_actions_2($p_module, $p_id, $p_permissions, $p_container){
	
		$html = "<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'view', {id:'".$p_id."'}, '".$p_container."');\">";
		
		if(function_exists("translate")){
			$html .= translate('common','actions/view');
		}else{
			$html .= "view";
		}
		
		$html .= "</a>";
		
		$html .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'edit', {id:'".$p_id."'}, '".$p_container."');\">";
		
		if(function_exists("translate")){
			$html .= translate('common','actions/edit');
		}else{
			$html .= "edit";
		}
		
		$html .= "</a>";
		
		$html .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"javascript:G_jsf_delete('".$p_module."', {id:'".$p_id."'}, this);\"><img alt='eliminar' src='images/i_delete.png' /></a>";
		
		return($html);
		
	}
	
	
	public function get_actions_3($p_actions,$p_module, $p_id, $p_permissions, $p_container, $p_file = false){

		$p_actions = strtoupper($p_actions);
		$html = "";
	
		if (strpos($p_actions, "V") !== false) {
			if ($p_file != false) {
				$html .= "<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'view$p_file', {id:'".$p_id."'}, '".$p_container."');\">";
				if(function_exists("translate")){
					$html .= translate('common','actions/view');
				}else{
					$html .= "view";
				}
				$html .= "</a>";							
			} else {
				$html .= "<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'view', {id:'".$p_id."'}, '".$p_container."');\">";
				if(function_exists("translate")){
					$html .= translate('common','actions/view');
				}else{
					$html .= "view";
				}
				$html .= "</a>";							
			}
		}
		
		if($html!="") $html.="&nbsp;&nbsp;&nbsp;";
	
		if (strpos($p_actions, "E") !== false) {
			if ($p_file != false) {
				$html .= "<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'edit$p_file', {id:'".$p_id."'}, '".$p_container."');\">";
				if(function_exists("translate")){
					$html .= translate('common','actions/edit');
				}else{
					$html .= "edit";
				}
				
				$html .= "</a>";	
			} else {
				$html .= "<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'edit', {id:'".$p_id."'}, '".$p_container."');\">";
				if(function_exists("translate")){
					$html .= translate('common','actions/edit');
				}else{
					$html .= "edit";
				}
				$html .= "</a>";
			}			
		}
		
		if($html!="") $html.="&nbsp;&nbsp;&nbsp;";
		
		if (strpos($p_actions, "D") !== false) {
			if ($p_file != false) {
				$html .= "<a href='#' onclick=\"javascript:G_jsf_delete('".$p_module."', {id:'".$p_id."'}, this, 'delete$p_file');\"><img alt='eliminar' src='images/i_delete.png' /></a>";
			} else {
				$html .= "<a href='#' onclick=\"javascript:G_jsf_delete('".$p_module."', {id:'".$p_id."'}, this);\"><img alt='eliminar' src='images/i_delete.png' /></a>";
			}			
		}
		
		return($html);
		
	}
	
	
	public function get_actions_4($p_module, $p_id, $p_permissions, $p_container, $p_refresh_container='', $p_refresh_substract=0, $p_aditional_data=''){
	
		$aditional_data=$p_aditional_data;
		//if($aditional_data!='') $aditional_data=',ad:'.$aditional_data;
	
		$html = "<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'view', {id:'".$p_id."'}, '".$p_container."');\">";
		if(function_exists("translate")){
			$html .= translate('common','actions/view');
		}else{
			$html .= "view";
		}
		$html .= "</a>";
		
		if (strpos($p_permissions, "E") !== false) {
			$html .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"javascript:G_jsf_go('".$p_module."', 'edit', {id:'".$p_id."'$aditional_data}, '".$p_container."');\">";
			if(function_exists("translate")){
				$html .= translate('common','actions/edit');
			}else{
				$html .= "edit";
			}			
			$html .= "</a>";
		}else{
			$html .= "&nbsp;&nbsp;&nbsp;editar";
		}
		
		if (strpos($p_permissions, "D") !== false) {
			$html .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"javascript:G_jsf_delete('".$p_module."', {id:'".$p_id."'}, this, '', '$p_refresh_container', '$p_refresh_substract');\"><img alt='eliminar' src='images/i_delete.png' /></a>";
		}else{
			$html .= "&nbsp;&nbsp;&nbsp;eliminar";
		}
		
		return($html);
		
	}
	
	
	public function load_cfg(){
		global $cfg;
		$module = new green_module($module);
		
		$path=$_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."client/".$this->get_master()->get_green_client()->get_crypt_id()."/modules/".$this->get_keyword()."/";
		$path2=$_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."client/".$this->get_master()->get_green_client()->get_crypt_id()."/modules/";
		$path3=$_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."client/".$this->get_master()->get_green_client()->get_crypt_id()."/";

		$file = $path."settings.xml";
		$file2 = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."modules/". $this->get_keyword() . "/settings.xml";
//		echo $file2."///<br>";
		
		if(!is_dir($path3)) mkdir($path3);
		if(!is_dir($path2)) mkdir($path2);
		
		if(!is_dir($path)){
			mkdir($path);
		}
		
		if(!file_exists($file2)) return(false);

		if(!file_exists($file)) copy($file2, $file);
	
		$obj = simplexml_load_file($file);
		$obj2 = simplexml_load_file($file2);
		
		
		foreach($obj2 as $prop=>$value){
		
			foreach($obj2->$prop->attributes() as $a => $b){
			
				if($a=="input_type"){
					$this->cfg_it[$prop]=$b;
				}
				$this->cfg_attr[$prop][$a]=$b;
				
			}
			
			$value=$obj->$prop;
			$this->cfg[$prop]=$value;
		}
		
	}
	
	function embed_cfg_w(){
	
		echo "<table class='G_CS_module_settings'>";
	
		foreach($this->cfg as $prop=>$value){
		
		
			if(function_exists(translate)){
				$prop_t=translate($this->get_keyword(), "settings/txt_".$prop);
			}else{		
				$prop_t=$prop;
			}
			
			if($prop_t=='' || $prop_t==NULL)$prop_t=$prop;
			
			if($this->cfg_it[$prop] != '---') echo "<tr><th>".$prop_t."</th><td>";
		
			switch($this->cfg_it[$prop]){
				case "none" :
					echo "<input name='$prop' type='hidden' value='$value'><label>$value</label>";
					break;

				case "textarea" :
					echo "<textarea name='$prop'>". $value . "</textarea>";
					break;

				case "color_picker" :
					echo ' <input class="color1" type="text" name="show_with_color" value="'.$value.'" />';
					break;
					
				case "checkbox" :
					echo "<input ". ($value=='yes' ? 'checked':'') ." name='$prop' type='checkbox' value='yes'>";
					break;
									
				case "select" :
				
					$s = $this->cfg_attr[$prop]["input_options"];
					$v = $this->cfg[$prop];
					$arr = explode("|",$s);
					
					echo "<select name='$prop'>";
					foreach($arr as $option){
						echo "<option ". ($v==$option ? ' selected':'') .">$option</option>";
					}
					echo "</select>";
				
					
					break;
					
				default:
					echo "<input name='$prop' type='text' value='$value'>";
					break;
			}
			
			if($this->cfg_it[$prop] != '---') echo "<td></tr>";
			
		}
		
		echo "</table>";
		
	}
	
	
	public function save_cfg(){
	
		$path2=$_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."modules/".$this->get_keyword()."/";
		$path=$_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."client/".$this->get_master()->get_green_client()->get_crypt_id()."/modules/".$this->get_keyword()."/";

		$file = $path."settings.xml";
		$file2 = $path2."settings.xml";
	
		$obj = simplexml_load_file($file2);
		
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		
		$cfg = $doc->createElement("cfg");

		foreach($obj as $prop => $value){
			$post_val = $_POST[$prop];
			$p = $doc->createElement($prop);
			
			if(is_array($post_val)){
			
				$val = implode(",", $post_val);
			
				$n = $doc->createTextNode($val);
			}else{
				$n = $doc->createTextNode($post_val);
			}
			
			$p->appendChild($n);
			
			foreach($obj->$prop->attributes() as $n => $v){
				$p->setAttribute($n, $v);
			}
			
			$cfg->appendChild($p); 
			
			
			
		}

		$doc->appendChild($cfg); 		
		
		$doc->saveXML();
		$doc->save($file);
		
	}
	
	public function set_user_permissions($p_user, $p_permissions){
	
		$sql_query_3 = "SELECT id FROM user_permissions WHERE user = '".$p_user->get_id()."' AND green_client = '".$this->get_master()->get_green_client()->get_id()."' AND module = " . $this->get_id();
		$result = mysql_query($sql_query_3);
		
		$sql_query = "UPDATE user_permissions SET `permissions` = '' WHERE user = '".$p_user->get_id()."' AND green_client = '".$this->get_master()->get_green_client()->get_id()."' AND module = " . $this->get_id();
		$result_2 = mysql_query($sql_query) or die("ERR");
			
		if(mysql_num_rows($result)==0){
			$sql_query_2 = "INSERT INTO user_permissions(`user`, `green_client`, `module`, `permissions`) VALUES(".$p_user->get_id().", ".$this->get_master()->get_green_client()->get_id().", " . $this->get_id() . ", '".$p_permissions."')";
		}else{
			$sql_query_2 = "UPDATE user_permissions SET `permissions` = '".$p_permissions."' WHERE user = '".$p_user->get_id()."' AND green_client = '".$this->get_master()->get_green_client()->get_id()."' AND module = " . $this->get_id();
		}
		
		mysql_query($sql_query_2) or die("err:" . mysql_error());
	}
	
	public function generate_id2($kw){
		$sql_query = "SELECT `id` FROM `modules` WHERE `keyword` = '$kw'";
		$result = mysql_query($sql_query);
		$line = mysql_fetch_array($result);
		return($line['id']);
	}
	
	
	public function generate_id(){
		$sql_query = "SELECT id FROM modules WHERE keyword = '".$this->get_keyword() . "'";
		$result = mysql_query($sql_query);		
		$line = mysql_fetch_array($result);
		
		$this->set_id($line['id']);
		
	}
	
	function get_calendar_events_2($color,$month,$year,$day=false) {
	
		$module_name=$this->get_main_class();
	
		$string = "\$obj = new $module_name;";
//		echo $string."//<br>";
		eval($string);
		
		//return $obj->get_calendar_events($obj->class_name,$color,$month,$year,$day);
		return $obj->get_calendar_events_2($color,$month,$year,$day);
	}
	
	
	function get_calendar_events($module_name,$color,$month,$year,$day=false) {
		$string = "\$obj = new $module_name();";
//		echo $string."//<br>";
		eval($string);
		return $obj->get_calendar_events($obj->class_name,$color,$month,$year,$day);
	}
	
	
	public function get_main_class(){
	
		switch($this->get_keyword()){
			case "expenses" :
				return("expense");
				break;
			case "clients" :
				return("client");
				break;
				
			case "payments" :
				return("payment");
				break;
			
			case "reservations" :
				return("reservation");
				break;
			
			case "postit" :
				return("postit");
				break;
			
			case "orders" :
				return("order");
				break;
				
			case "to_pay" :
				return("to_pay");
				break;
				
			case "address_book" :
				return("contact");
				break;
				
			case "products" :
				return("product");
				break;
				
			case "event" :
				return("event");
				break;	
			
		}
	
	}	
	
	public function count_ql_alerts(){
	
		$class=$this->get_main_class();
		$obj=new $class;

		$result=$this->get_master()->mysql_query("SELECT","COUNT(*) AS count",$obj->table_name,"NOT ack AND NOT deleted","");

		$row=mysql_fetch_array($result);
	
		return($row["count"]);
	}
	
	
	public function load_data_grid_cfg(){
		$str=$this->cfg["show_in_data_grid"];
		if($str!=""){
			$this->data_grid_cfg=explode(",", $str);
		}else{
		
			$class=$this->get_main_class();
			
			$code0 = "\$class;";
			$code = '$obj = new ' . $code0;
		
			eval($code);
		
			for($i=0; $i<3; $i++){				
				array_push($this->data_grid_cfg, $obj->properties[$i]);
			}
			
		}
	}


	public function in_quick_menu($wizard_user){

		$quick_menu_item = $wizard_user->get_quick_menu_item($this);

		return $wizard_user->get_quick_menu_item($this)->get('active');

	}
	
	
}
?>