<?php
include_once('db_object.php');
	
class data_row_expense extends db_object{
	
	private $_amount;
	private $_date;
	private $_description;
	private $_curency;
	private $_category;
	private $_provider;
	
	public $properties = array('customer','product','concept','currency','total:double','date:date','description');
	public $ni_properties = array('user:user');
	
	var $table_name = "data_row_expenses";
	var $class_name = "data_row_expense";

	public function set_name($val){
		$this->name = $val;
	}
	
	function embed_w($p_butt=false){
	
		foreach($this->eon_properties as $p){
			array_push($this->properties, $p);
		}
		
		$html = "<table class='G_cs_input' cellspacing='0'>";

		foreach($this->properties as $p){
			
			if($p != 'date') $html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>".($this->translate_property($p))."</th><td>" . $this->html_input_property($p)."</td></tr>";
			
			
		}
		
		foreach($this->ndb_properties as $p){
			$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>".($p)."</th><td>" . $this->html_input_property($p)."</td></tr>";
		}
		
		$html .= "<tr class='G_cs_input_button'><th></th><td><input type='submit'></td></tr>";
		
		$html .= "</table>";
		
		return($html);

	}
	
	
	
	public function html_input_property($p_property) {
		
		// IMPORTANT !!
		$code0 = "\$this->get($p_property);";
		$code = '$val = ' . $code0;
		
		eval ($code);
		// IMPORTANT !!
		
		$html = '';
		switch ($p_property) {
		
			case 'product':
				
				$html = '<select name="product"></select>';
				break;
						
			case 'customer':
				
				$result = master::get_raw_data('data_rows', 'DISTINCT(`customer`)', 'NOT `deleted` ORDER BY `customer`');
								
				$html = '<select name="customer">';
				$html .= "	<option value='0'>...</option>";
				
				while($sql_row = mysql_fetch_array($result)){
					//$html .= "	<option value='".$p->get_id()."'>".$p->get_name()."</option>";
					$html .= "	<option".($this->get('customer')==$sql_row['customer']?' selected':'')." value='".$sql_row['customer']."'>".$sql_row['customer']."</option>";
				}
				
				$html .= "</select>";			
				break;
						
			case 'concept':
			
				$concept_data = Array('Impuesto ejecuciones','Intimación Alguacilatos','Intimacion Mdeo/Interior','Embargo Genérico','Embargo Específico','Gestoria Interior','Información Registral','Diligencias Montevideo','Diligencias Interior','Timbres otras actuaciones','Detención Vehículos Mdeo','Detención Vehículos Interior','Gastos Notariales','Tasas Rescisión Leasing','Levantamiento Embargos','Gestoria Montevideo - Trámites Varios','Traslado Vehículos','Notificación Notarial','Descartes Notariales','Segundas Copias','Testimonios Notariales','Publicación de Edictos','Telegrama colacionado','Demanda Ejecutiva - Timbres','Demanda Hipotecaria - Timbres','Demanda Prendaria - Timbres','Alguacil','Correspondencia Mdeo/Interior','Diferencia timbres','Fotocopias','Certificado Notarial','Gestoria Interior - Diferencia');
				
				$html = '<select name="concept">';
				
				$html .= "	<option value=''>...</option>";
				
				foreach($concept_data as $concept){

					$html .= "	<option value='$concept'>".($concept)."</option>";
				}
				
				$html .= "</select>";			
				break;
						
			case 'currency':
				
				$html .= '<label><input size="2" type="radio" name="currency" checked value="1">$</label><label><input size="2" type="radio" name="currency" value="2">USD</label>';
				break;
			
			case 'credit_card':
				
				$html .= '<label><input type="checkbox" name="credit_card" value="1">Credit Card</label>';
				break;
			
			
			
			
			case '__date':
							
				
				
				if(!checkdate($day_date, $month_birth, $year_birth)){
					
					$day_date = "";
					$month_birth = "";
					$year_birth = "";
					
				}
				
				$date=strtotime($this->get('date'));				
				if($date=='') $date=time();
			
				$html .= '<select id="day_date" name="day_date">';
				
				for($i=1; $i<=31; $i++){
					$html .= '	<option value="'.$i.'"'.((date('d',$date) == $i) ? ' selected' : '').'>'.$i.'</option>';
				}
				
				$html .= "&nbsp;";
				$html .= '</select>';
				
				$html .= '<select id="month_date" name="month_date">';
				
				for($i=1; $i<=12; $i++){
					$html .= '	<option value="'.$i.'"'.((date('m',$date) == $i) ? ' selected' : '').'>'.$i.'</option>';
				}
				
				$html .= '</select>';
				$html .= " ";
				
				$html .= '<select id="year_date" name="year_date">';
				
				for($i=date('Y')-10; $i<=date('Y')+10; $i++){
					$html .= '	<option value="'.$i.'"'.((date('Y',$date) == $i) ? ' selected' : '').'>'.$i.'</option>';
				}
				
				$html .= '</select>';
				break;
						
			case 'phone' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
			
			
			case 'amount' :
			
				$html .= '<input size="5" value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
							
			
			case 'description' :
			
				$html .= '<textarea name="'.$p_property.'">'.$val.'</textarea>';
				break;
			
			case 'name' :
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
		}
		return $html;
	}


	public function  get_date_formatted(){

		$date = strtotime($this->get('date'));

		if($date==null) return '-';

		$date = Date('d/m/Y', $date);

		return $date;

	}
	
	
	
	public function catch_post_properties() {

		foreach($this->properties as $property){
			
			if($property != "date" or true){
				$post_val = ($_POST[$property]);
				$code0 = "\$this->set($property, '$post_val');";
				//$code = '$val = ' . $code0;
				eval ($code0);
			}else{
				$this->set('date', $_POST['year_date']."-".$_POST['month_date']."-".$_POST['day_date']);
			}
		}
	}	
	
	
	public function translate_property($p){
	
		switch($p){
		
			case 'customer':
				$t = '# Customer';
				break;
				
			case 'product':
				$t = 'Producto';
				break;
				
			case 'concept':
				$t = 'Concepto';
				break;
				
			case 'currency':
				$t = 'Moneda';
				break;
				
			case 'total':
				$t = 'Total';
				break;
				
			case 'date':
				$t = 'Fecha';
				break;
				
			case 'description':
				$t = 'Comentario';
				break;
				
			default :
				$t = $p;
				break;
		
		}
	
		return($t);
	}
	
	
}
	
?>