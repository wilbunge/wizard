<?php

include_once('db_object.php');
	
class player extends db_object{
	
	private $_name;
	
	public $properties = array('name', 'password', 'email', 'facebook_id', 'balance');
	public $ni_properties = array();
	
	var $table_name = "players";
	var $class_name = "player";
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	
	public function get_facebook_id(){
		
		if ($this->values['facebook_id'] === NULL){
			if($this->get_id()>0) {
				$this->values['facebook_id'] = $this->load_db_field('facebook_id');
			}else{
				$this->values['facebook_id'] = 0;
			}
		}
		
		return($this->values['facebook_id']);
		
	}
	
	
	
	public function update_ranking($game_id=0, $prize=0){

		//data_manager::$debug_mode=true;
		
		$sql_result_1 = master::get_raw_data('rankings', 'id', '`player` = '.$this->get('id').' AND `game`="'.$game_id.'" AND DATEDIFF(NOW(), `date`)=0');
		
		if(mysql_num_rows($sql_result_1)==0){
		
			$ranking_item = new ranking_item;
			$ranking_item->set('prize', $prize);
			$ranking_item->set('game', $game_id);
			$ranking_item->set('player', $this);
			master::store_object($ranking_item);
		
		}else{
		
			$sql_rows_1= mysql_fetch_array($sql_result_1);
		
			$ranking_item = new ranking_item($sql_rows_1['id']);
			$ranking_item->increase($prize);
		
		}
		
	}
	
	
	public function has_facebook(){
		return($this->get('facebook_id')>0);
	}
		
}

class ranking_item extends db_object{
	
	
	public $properties = array('player', 'game', 'prize');
	
	var $table_name = "rankings";
	var $class_name = "ranking_item";
	
	
	public function get_player(){
		if ($this->values['player'] === NULL){
			$this->values['player'] = new player;
			if($this->get_id()>0) {
				$this->values['player']->set('id',  $this->load_db_field('name'));
			}
		}
		return $this->values['player'];
	}
	
	
	
	public function increase($a){
	
		$current_prize = $this->get('prize');
		$this->update_property('prize', $current_prize+$a);
	
	}
	
	
}

?>