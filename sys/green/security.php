<?php

include_once("aes.php");

class security {

	private static $key_128 = "poiuytrewqasdfgh"; // 128-bit key
	private $key = "abcdefghijuklmno0123456789012345"; // 256-bit key
	
	static function hex2bin($h){
	  if (!is_string($h)) return null;
	  $r='';
	  for ($a=0; $a<strlen($h); $a+=2) { $r.=chr(hexdec($h{$a}.$h{($a+1)})); }
	  return $r;
	}
	
	static function decrypt_data($crypt_data){
		$key = "abcdefghijuklmno0123456789012345"; // 256-bit key
		$aes = new AES($key);			
		$decrypt_data_bin=security::hex2bin($crypt_data);
		$decrypt_data=$aes->decrypt($decrypt_data_bin);		
		return($decrypt_data);
	}
	
	static function encrypt_data($data){
	
		$key = "abcdefghijuklmno0123456789012345"; // 256-bit key
		
		$aes = new AES($key);
		$crypt_data=$aes->encrypt((string)$data);
		$crypt_data_hex=bin2hex($crypt_data);
		return($crypt_data_hex);
		
	}
	
	static function encrypt_data_128($data){
	
		$key = "qwertyui12345678"; // 256-bit key
		
		$aes = new AES($key);
		$crypt_data=$aes->encrypt((string)$data);
		$crypt_data_hex=bin2hex($crypt_data);
		return($crypt_data_hex);
		
	}
	
	static function decrypt_data_128($crypt_data){
		$key = "qwertyui12345678"; // 256-bit key
		$aes = new AES($key);			
		$decrypt_data_bin=security::hex2bin($crypt_data);
		$decrypt_data=$aes->decrypt($decrypt_data_bin);		
		return($decrypt_data);
	}
	
	
	static function letter_scrambler($path){
	
		$str=$path;
		
		$str=str_replace("a","m", $str);
		$str=str_replace("b","n", $str);
		$str=str_replace("c","b", $str);
		$str=str_replace("d","v", $str);
		$str=str_replace("e","c", $str);
		$str=str_replace("f","x", $str);
		$str=str_replace("g","z", $str);
		$str=str_replace("h","l", $str);
		$str=str_replace("i","k", $str);
		$str=str_replace("j","j", $str);
		$str=str_replace("k","h", $str);
		$str=str_replace("l","g", $str);
		$str=str_replace("m","f", $str);
		$str=str_replace("n","d", $str);
		$str=str_replace("o","s", $str);
		$str=str_replace("p","a", $str);
		$str=str_replace("q","q", $str);
		$str=str_replace("r","w", $str);
		$str=str_replace("s","e", $str);
		$str=str_replace("t","r", $str);
		$str=str_replace("u","t", $str);
		$str=str_replace("v","y", $str);
		$str=str_replace("w","u", $str);
		$str=str_replace("x","i", $str);
		$str=str_replace("y","o", $str);
		$str=str_replace("z","p", $str);
	
		return($str);

	}
	
	
	static function number_scrambler($num){
	
		$str=(string)$num;
		
		$str=str_replace("9","A", $str);
		$str=str_replace("8","B", $str);
		$str=str_replace("7","C", $str);
		$str=str_replace("6","D", $str);
		$str=str_replace("5","E", $str);
		$str=str_replace("4","F", $str);
		$str=str_replace("3","G", $str);
		$str=str_replace("2","H", $str);
		$str=str_replace("1","I", $str);
		
		$str=str_replace("A", "1", $str);
		$str=str_replace("B", "2",  $str);
		$str=str_replace("C", "3",  $str);
		$str=str_replace("D", "4",  $str);
		$str=str_replace("E", "5",  $str);
		$str=str_replace("F", "6",  $str);
		$str=str_replace("G", "7",  $str);
		$str=str_replace("H", "8",  $str);
		$str=str_replace("I", "9",  $str);
	
		return((int)$str);

	}
	
	
	static function encrypt_number($num){	
		$num=$num+987654;
		$num=security::number_scrambler($num);
		return($num);
		
	}
	
	static function decrypt_number($num){	
		$num=security::number_scrambler($num);
		$num=$num-987654;
		return($num);
	}
	
	
	static function rect_enc_num($x,$m,$n){	
		$y = $m*$x+$n;
		return($y);
	}
	
	static function rect_dec_num($y,$m,$n){
		$x = ($y-$n)/$m;
		return($x);
	}
	
	
}	?>