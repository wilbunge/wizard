<?php

include_once('db_object.php');
	
class page extends db_object{
	
	private $_name;
	
	public $properties = array('title', 'language', 'landing:landing*', 'content', 'external_link:text', 'order:int', 'seo_link');
	//public $ni_properties = array('');
	//public $ndb_properties = array('');
	
	var $table_name = "landing_pages";
	var $class_name = "page";
	
	public function get_seo_link(){
		
		$link = $this->get('title');
		
		$link = strtolower($link);
		
		$link = str_replace('á', 'a', $link);
		$link = str_replace('é', 'e', $link);
		$link = str_replace('í', 'i', $link);
		$link = str_replace('ó', 'o', $link);
		$link = str_replace('ú', 'u', $link);
		
		$link = str_replace('ê', 'e', $link);
		
		$link = preg_replace('/[\s\W]+/', '-', $link);
		
		return($link);
		
	}
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	
	public function html_input_property($p){
	
		switch($p){
		
			case 'content' :
				$html = "<div id='nicedit-panel'></div><textarea rows='20' cols='100' id='nicedit-content' name='content'>".$this->get('content')."</textarea>";
				break;
			
			case 'landing' :
			
				$landings = master::get('landing', 'NOT `deleted` ORDER BY `title`');
				
				foreach($landings as $t){
					$s = ($t->equals($this->get('landing'))?'selected':'');
					$html .= "<option $s value='".$t->get('id')."'>".$t->get('title')."</option>";
				}
			
				$html = "<select name='landing'>$html</select>";
				break;
				
			case 'language' :
			
				
				$languages = master::get('language', 'NOT `deleted` ORDER BY `name`');
				
				foreach($languages as $l){
					$s = ($this->get('language') == $l->get('code')?'selected':'');
					$html .= "<option $s value='".$l->get('code')."'>".$l->get('code')."</option>";
				}
			
				$html = "<select name='language'>$html</select>";
				break;
				
			default :
				$html = "<input name='$p' value='".$this->get($p)."' />";
				break;
			
		}
		
		return($html);
	
	}
	
	
	public function embed(){
	
		$html = $this->get('content');
		
		return($html);
	
	}
	
		
}

?>