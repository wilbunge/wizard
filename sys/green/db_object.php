<?php
 
class db_object {

	var $class_name = '';
	public $id;
	private $array_id;
	
	private $_container;
	
	private $_green_client;
	private $_master;
	private $_ack;
	private $_var;
	private $_parent;
	
	public $module;
	
	public $hidden_properties = array();
	public $ni_properties = array(); // No Input
	public $oon_properties = array(); // Only On New
	public $eon_properties = array();
	public $ooe_properties = array(); // Only On Edit
	public $ndb_properties = array(); // No DB
	public $tsp_properties = array();
	
	public $property_types = array(); // Property types
	
	public $values = array();
	
	
	public function __toString(){
		return($this->get('name'));
	}
	
	public function get_master(){
		
		if($this->_master == null){
			$this->_master = new master();
		}
		
		return($this->_master);
		
	}
	
	public function set_master($val){
		$this->_master = $val;
	}
		
	
	public function get_green_client() {
		
		if($this->_green_client == null){
			$this->_green_client = new green_client;
			
			if($this->get_id()>0) $this->_green_client->set_id($this->load_db_field('green_client'));
		}
		
		return($this->_green_client);
		
	}
	
	public function set_green_client($val) {
		$this->_green_client=$val;
		
	}

	public function validate(){
		require_once('validation.php');
		return new validation();

	}


	public function has_parent(){
		$parent_class = get_parent_class($this);
		return ($parent_class != 'db_object');
	}

	public function get_parent(){

		$parent_class = get_parent_class($this);

		if($parent_class == 'db_object') return false;

		// if($this->_parent == null){ }$this->_parent = new $parent_class;

		// $p = 

		// print_r($p);

		// die;
		
		// return $this->_parent;

		return $this->get($parent_class);

	}



	public function set_data($source) {

		$ps = $this->properties;

		if($parent = $this->get_parent()){
			$parent->set_data($source);
			echo $parent->_signature.'<br>';
		}

		// print_r($ps);

		foreach($ps as $property){

			$property = str_replace('[]', '', $property);
		
			$post_val = $source[$property];
			
			// echo($property .": ".(($post_val))."<br />");
			
			/*if(method_exists($this,"get_$property")){

				$code0 = "\$this->get_$property();";
				$code = '$val = ' . $code0;
				eval($code);
				
			}else{

				$val = $this->get_p($property);

			}*/
			
			if($this->property_types2[$property]['is_array']){
			
				$this->values[$property] = Array();
				
				if(is_array($post_val)){

				foreach($post_val as $arr_val){
					//echo $property.': '.$this->values[$property];
					//array_push($this->$property, $arr_val);
					
					//echo $this->property_types[$property];
					
					$post_type = $this->property_types2[$property]['type'];
					
					if($this->property_types2[$property]['is_object']){
						array_push($this->values[$property], new $post_type($arr_val));
					}else{
						array_push($this->values[$property], $arr_val);
					}
					
				}	
				
				}
				
			}else if($this->property_types2[$property]['is_object'] && ($post_val != '')){
				
				$post_type = $this->property_types2[$property]['type'];
				$this->set($property, new $post_type($post_val));

				// $code0 = "\$this->get($property)->set_id($post_val);";
			}else{


				
				if(method_exists($this,"set_$property")){
					$code0 = "\$this->set_$property('$post_val');";
				}else{
					$this->set_p($property, $post_val);
				}
				
				
			}
			eval ($code0);
		}

		
	}
	
	
	public function get_account() {
		
		if($this->_account == null){
			$this->_account = new account;
			
			if($this->get_id()>0) $this->_account->set_id($this->load_db_field('green_client'));
		}
		
		return($this->_account);
		
	}
	
	public function set_account($val) {
		$this->_account=$val;
		
	}
	
	public function get_var(){
		return($this->_var);
	}
	
	public function set_var($v){
		$this->_var = $v;
	}
	
	
	
	function __construct($p_id=0) {

		$this->_signature = time();
	
		$this->set_id($p_id);

		$this->id = $p_id;
       
       global $ai_master;
       
		if($ai_master != NULL){
			$this->set_master($ai_master);
		}
		
		$temp = array();
		$temp2 = array();
		$temp3 = array();
		
		foreach($this->ndb_properties as $p){
			$p_arr = explode(":", $p);
			array_push($temp, $p_arr[0]);
			$this->property_types[$p_arr[0]] = $p_arr[1];
		}
		
		foreach($this->properties as $p){
			
			$is_array = strpos($p, '[]');
			$is_object = strpos($p, '*');
			
			
			$p = str_replace('[]', '', $p);
			$p = str_replace('*', '', $p);
			
			$p_arr = explode(":", $p);
			
			array_push($temp2, $p_arr[0]);
			
			$this->property_types[$p_arr[0]] = $p_arr[1];
			
			if(strpos($p_arr[1], '[]')){
				
			}
			
			$subtype = str_replace('[]', '', $p_arr[1]);
			$subtype = str_replace('*', '', $type);
			
			$this->property_types2[$p_arr[0]] = Array(
					"is_array" => (boolean)$is_array,
					"is_object" => (boolean)$is_object,
					"type" => $p_arr[1]

			);
		}
		
		
		
		foreach($this->ni_properties as $p){
			$p_arr = explode(":", $p);
			array_push($temp3, $p_arr[0]);
			
			$this->property_types[$p_arr[0]] = $p_arr[1];
		}
		
		$this->ndb_properties = $temp;
		$this->properties = $temp2;
       	$this->ni_properties = $temp3;
       
	}
	
	public function get_p($p){
	
		if ($this->values[$p]===NULL){
	
			$pt = $this->property_types[$p];
			
			if($this->property_types2[$p]['is_array']){
			
				$this->values[$p] = Array();
				
				if(is_array($this->extra[$p])) $ef = ',' . implode($this->extra[$p], ',');
				//data_manager::$debug_mode=true;
				
				$sql_result = (master::get_raw_data($this->class_name."_".$p, $this->class_name.", id, ". $pt . $ef, $this->class_name .' = "'.$this->id.'" AND NOT `deleted`'));
				//data_manager::$debug_mode=false;
				
				while($row = mysql_fetch_array($sql_result)){

					$o = new $pt($row[$pt]);

					if($this->extra[$p] !=''){
						foreach($this->extra[$p] as $ep){
							$o->values[$ep] = $row[$ep];
						}
					}
					
					array_push($this->values[$p], $o);
					
				}
				
				//data_manager::$debug_mode=false;
			
			}elseif($this->property_types2[$p]['is_object']){

				master::_include($pt);

				$this->values[$p] = new $pt;
				$this->values[$p]->set('id', $this->load_db_field($p));
			}elseif($this->get_id()>0) {
				$this->values[$p] = $this->load_db_field($p);
			}
				

			
		}
	
		return($this->values[$p]);
	}
	
	public function set_p($p,$v){
		$this->values[$p] = $v;
	}
	
	
	public function get($p){
	
		if(method_exists($this,"get_$p")){
			$code0 = "\$this->get_$p();";
			$code = '$val = ' . $code0;	
			eval ($code);
		}else{
			$val = $this->get_p($p);	
		}
		
		
		return($val);
	}
	
	
	public function set($p, $v){

		if(method_exists($this,"set_$p")){
			$code = "\$this->set_$p('$v');";
			eval ($code);
		}else{
			$this->values[$p] = $v;
		}
	
		
	}
	
	public function embed_property($p){
		
		$val=$this->get($p);
		
		if(gettype($val)=='boolean'){
			if($val) $val="&#10004;";
		}
		
		return($val);
	}
	

	public function load_db_field($field, $echo_query=false) {
	
		$sql_result = data_manager::read($this->table_name, "`$field`", '`id`="'.$this->get_id().'"');
		$sql_row = mysqli_fetch_array($sql_result);
		
		if($echo_query) echo $sql_query;

		return $sql_row[$field];
	}
	
	
	public function load_db_field_ext($field, $table, $link, $echo_query=false) {
		
		$sql_query = "SELECT `".$field."` FROM `". $table."` WHERE ".$link." = ".$this->get_id();
		if($echo_query) echo $sql_query;
		$result = mysql_query($sql_query);
		
		$felo = mysql_fetch_array($result);
		return $felo[$field];
	}
	
	
	public function load_db_field_ext_2($field) {
		
		$sql_query = "SELECT `".$field."` FROM `". $this->get_container()."` WHERE id = ".$this->get_array_id();
		echo $sql_query;
		$result = mysql_query($sql_query);
		
		
		$felo = mysql_fetch_array($result);
		return $felo[$field];
	}
	
	
	function embed_for_input(){
		
		foreach(self::$eon_properties as $p){
			array_push(self::$properties, $p);
		}
		
		$html ='';
		
		foreach(self::$hidden_properties as $p){

			$v = self::$get($p);
			
			if(is_object($v)) $v=$v->get_id();
			
			$html .= "<input type='hidden' name='$p' value='$v'>";
		}
		
		$html .= "<table class='input-table' cellspacing='0'>";

		foreach(self::$properties as $p){
			//$html .= "<tr id='G_he_".self::$class_name."_".$p."'><th>".self::$translate_property($prepend_translation_key.$p)."</th><td>" . self::$html_input_property($p)."</td></tr>";
			$html .= "<tr id='G_he_".self::$class_name."_".$p."'><th>$p</th><td>" . self::$html_input_property($p)."</td></tr>";
		}
		
		foreach(self::$ndb_properties as $p){
			//$html .= "<tr id='G_he_".self::$class_name."_".$p."'><th>".self::$translate_property($prepend_translation_key.$p)."</th><td>" . self::$html_input_property($p, false)."</td></tr>";
			$html .= "<tr id='G_he_".self::$class_name."_".$p."'><th>$p</th><td>" . self::$html_input_property($p, false)."</td></tr>";
		}
		
		
		$html .= "<tr class='G_cs_input_button'><th></th><td><input type='submit' value='Submit'></td></tr>";
		
		
		$html .= "</table>";
		
		return($html);

	}
	
	
	function embed_w($p_butt=false, $prepend_translation_key=''){
		// return "..";
		foreach($this->eon_properties as $p){
			array_push($this->properties, $p);
		}
		
		$html ='';
		
		foreach($this->hidden_properties as $p){

			$v = $this->get($p);
			
			if(is_object($v)) $v=$v->get_id();
			
			$html .= "<input type='hidden' name='$p' value='$v'>";
		}
		
		$html .= "<table class='input-table' cellspacing='0'>";

		foreach($this->properties as $p){
			//$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>".$this->translate_property($prepend_translation_key.$p)."</th><td>" . $this->html_input_property($p)."</td></tr>";
			$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>$p</th><td>" . $this->html_input_property($p)."</td></tr>";
		}
		
		foreach($this->ndb_properties as $p){
			//$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>".$this->translate_property($prepend_translation_key.$p)."</th><td>" . $this->html_input_property($p, false)."</td></tr>";
			$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>$p</th><td>" . $this->html_input_property($p, false)."</td></tr>";
		}
		
		
		$html .= "<tr class='G_cs_input_button'><th></th><td><input type='submit' value='Submit'></td></tr>";
		
		
		$html .= "</table>";
		
		return($html);

	}
	
	
	
	public function get_id() {
		if ($this->id === NULL || $this->id == '') {
			$this->id = 0;
		}

   		return floor($this->id);
	}


	public function set_id($id) {
		if(!$id) return;
		$this->id = $id;
	}
	
	
	public function get_array_id() {
		if ($this->array_id == NULL || $this->array_id == '') {
			$this->array_id = 0;
		}

   		return floor($this->array_id);
	}


	public function set_array_id($val) {
		$this->array_id = $val;
	}
	
	
	public function get_ack(){
		if ($this->_ack === NULL){
			if($this->get_id()>0) {
				$this->_ack= $this->load_db_field('ack');
			}
		}
		return $this->_ack;
	}
	
	public function set_ack($val){
		$this->_ack = $val;
	}
	
	
	public function get_container(){
		return($this->_container);
	}
	
	public function set_container($val){
		$this->_container = $val;
	}

	
	public function update(){
		
		foreach($this->properties as $property){
			
			$code0 = "\$this->get_$property();";
			$code = '$val = ' . $code0;
			
			$val = $this->get($property);
		
			//eval ($code);
			
			if (gettype($val) == "object" ) {
		//		$val = str_replace("'", "\'", $val->get_id());
			} else {
		//		$val = str_replace("'", "\'", $val);
			}
						
			if($update_clause != '') $update_clause .= ', ';
			$update_clause .= $property . " = '" . $val . "'";
			
			if($values != '') $values .= ', ';
			$values .= "'". $val . "'";
	
			if($fields != '') $fields .= ', ';
			$fields .=  $property;

		}
		
		$fields .= ', ' . $this->class_name;
		$values .= ', ' . $this->get_id();
		
		$sql_query2 = "SELECT id FROM " . $this->table_name . " WHERE id = " . $this->get_id();
//		echo $sql_query2."--<br>";

		$result2 = mysql_query($sql_query2);
		$count = mysql_num_rows($result2);
		
		if ($count == 0) {
			$sql_query = "INSERT INTO ".$this->class_name." ($fields) VALUES ($values)";
		} else {	
			$sql_query = "UPDATE ".$this->table_name." SET ". $update_clause . " WHERE id = " . $this->get_id();
 		}

		$sql_query = "UPDATE ".$this->table_name." SET ". $update_clause . " WHERE id = " . $this->get_id();

		$result = mysql_query($sql_query) or exit('ERROR: '.mysql_error());
		
	}
	
	
	public function update_property($p_property, $p_value, $p_echo = FALSE){
		$this->set($p_property, $p_value);
		data_manager::update($this->table_name, "`". $p_property . "` = '" . $p_value . "'","id = " . $this->get_id());		
	}
	
	
	public function store($p_object){
		
		if($p_object->class_name == 'bag_stamp'){
			$this->store_bag_stamp($p_object);
		}elseif($p_object->class_name == 'order'){
			$this->store_order($p_object);
		}else{
			$this->store_generic($p_object);
		}
		
	}
	

	
	public function delete($p_object){
	
		if($p_object->class_name == "bag_stamp"){
			$query_delete = "UPDATE bag_stamp_models SET deleted = 1 WHERE bag_stamp = ".$p_object->get_id();
			//echo($query_delete);
			mysql_query($query_delete);
		}
		
		$sql_query = 'UPDATE '.$p_object->table_name.' SET DELETED = 1 WHERE ID = ' . $p_object->get_id();
//		echo $sql_query;
		$result = mysql_query($sql_query) or exit('no se pudo');
	
	}
	
	
	public function equals($p_object){
		if(gettype($p_object) == gettype($this)){
			return($this->get_id() == $p_object->get_id());
		}else{
			return(0);
		}
	}
	
	
	public function html_input_property_append($p_property) {}
	
	public function html_input_property($p_property, $get_value = true) {
		if($get_value){
	
			if(method_exists($this,"get_$p_property")){
				
				$code0 = "\$this->get_$p_property();";
				$code = '$val = ' . $code0;
	
				eval ($code);
				
			}else{
				
				$val = $this->get_p($p_property);
				
			}
		}		
	
				
		$html = '';
		
		if($this->property_types[$p_property] == 'file') return('<input type="file" name="'.$p_property.'">');
		if($this->property_types[$p_property] == 'files') return('<input type="file" name="'.$p_property.'[]" multiple>');
		
		if(gettype($this->get($p_property))=='object'){
		
				$objects = master::get($this->property_types[$p_property]);
				
				$html = "<option value='0'>...</option>";
				
				if(!$objects) $objects=Array();

				foreach($objects as $t){
					$html .= "<option".($this->get($p_property)->equals($t)?' selected':'')." value='".$t->get('id')."'>".$t."</option>";
				}
			
				$html = "<select name='$p_property'>$html</select>";

		
			
		
		}else{
			
			
			switch ($this->property_types[$p_property]) {
			
				case 'text':
					$html = "<textarea name=\"$p_property\">$val</textarea>";
					break;
	
				case 'extra_files':
					$html = '<input name="extra_files[]" type="file" multiple="" />';
					break;
	
				default:
					$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
			}
			
		}
		
		if($this->property_types[$p_property] == 'file') $html = '<input value="'.$val.'" type="file" name="'.$p_property.'">';
		
		return $html;
	}
	
	
	public function html_output_property($p_property) {
	
	
		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		
		$html = '';
		
		switch ($p_property) {		

			default:
				
				if(gettype($val) == 'object'){
					$html = $val->get_default();
				}else{
					$html = $val;
				}					
				
			break;
		}
		
		return $html;
	}
	
	
	
	
	
	public function get_default(){
		return($this->get_name());
	}
	
	
	public function catch_post_properties() {
	
		/*foreach($p_object->eon_properties as $p){
			array_push($p_object->properties, $p);
		}*/
		
		$ps = array_merge($this->properties, $this->hidden_properties);

		foreach($ps as $property){
			$post_val = $_POST[$property];
			
		//	echo($property .": ".(($post_val))."<br />");
			
			/*if(method_exists($this,"get_$property")){

				$code0 = "\$this->get_$property();";
				$code = '$val = ' . $code0;
				eval($code);
				
			}else{

				$val = $this->get_p($property);

			}*/
			
			if($this->property_types2[$property]['is_array']){
			
				$this->values[$property] = Array();
				
				if(is_array($post_val)){

				foreach($post_val as $arr_val){
					//echo $arr_val."***<br>";
					//array_push($this->$property, $arr_val);
					
					//echo $this->property_types[$property];
					
					$post_type = $this->property_types2[$property]['type'];
					
					if($this->property_types2[$property]['is_object']){
						array_push($this->values[$property], new $post_type($arr_val));
					}else{
						array_push($this->values[$property], $arr_val);
					}
					
				}	
				
				}
				
			}else if($this->property_types2[$property]['is_object'] && ($post_val != '')){
				$code0 = "\$this->get($property)->set_id($post_val);";
			}else{
				

				if(method_exists($this,"set_$property")){
					$code0 = "\$this->set_$property('$post_val');";
				}else{
					$this->set_p($property, $post_val);
				}
				
				
			}
			eval ($code0);
		}
		$this->catch_post_properties_append();
	}
	
	
	public function catch_post_properties_append(){}
	
	
	public function authenticate($p_user){
	
		$sql_query = "SELECT * FROM users WHERE username LIKE '".$p_user->get_username()."' AND password = '".$p_user->get_password()."' AND NOT deleted" ;
		$result = mysql_query($sql_query);
		
		
		if($row = mysql_fetch_array($result)){
			$p_user->set_id($row['id']);
			return(true);
		}else{
			return(false);
		}		
		
	}
	
	
	public function translate_property($p_property){
		
		/*if(function_exists(translate)){
			$property_t=translate("class_".$this->class_name, "properties/".$p_property);
		}
		
		if($property_t=='')$property_t = "".str_replace('_', ' ', $p_property);*/
		
		$property_t = master::translate_text($p_property);
		
		return($property_t);
	}
	
	
	public function embed_date_4_input($p){
		
		$html = "<select name='day_".$p."'>";		
		for($i=1; $i<=31; $i++){
			$html .= "<option>".$i."</option>";
		}
		$html .= "</select>";
		
		$html .= " <select name='month_".$p."'>";
		for($i=1; $i<=12; $i++){
			$html .= "<option>".$i."</option>";
		}
		$html .= "</select>";
		
		$html .= " <select name='year_".$p."'>";
		for($i=date('Y')-10; $i<=date('Y')+10; $i++){
			$html .= "<option>".$i."</option>";
		}
		$html .= "</select>";
		
		
		return($html);
		
	}

	
	public function get_date_attribute() {
		switch($this->table_name) {
			case "contacts":
				return "birth";
				break;
				
			case "to_pay":
				return "date";
				break;
				
			case "order":
				return "delivery_date";
				break;
				
				
				
			default: 
				return "date";
				break;
		}
	}

	
	public function get_calendar_events_2($color,$month,$year,$day=false) {
	
		$class_name=$this->class_name;
		$master = $this->get_master();
		
		if($class_name=='event'){
			$author_clause="author = ".$master->get_user()->get_id()." OR public";
		}else{
			$author_clause="TRUE";
		}
		
		if(!$day){
			$obj_arr = $master->select($class_name, $this->get_date_attribute()." > '".$year."-".$month."-1' AND date < '".$year."-".($month+1)."-1' AND ($author_clause) AND NOT Deleted");
		}else{
			$obj_arr = $master->select($class_name, $this->get_date_attribute()." = '".$year."-".$month."-$day' AND ($author_clause) AND NOT Deleted");
		}
		
		if ($obj_arr > 0 ) {
			$events = array();
			foreach($obj_arr as $obj) {
				$event = $obj->convert_to_event($color);
				array_push($events,$event);
			}
			return $events;
		} else {
			return false;
		}
	}
	
	
	public function get_calendar_events($table,$color,$month,$year,$day=false) {
		
		$master = $this->get_master();
		
		if($table=='event'){
			$author_clause="author = ".$master->get_user()->get_id()." OR public";
		}else{
			$author_clause="TRUE";
		}
		
		if(!$day){
			$obj_arr = $master->select($table, $this->get_date_attribute()." > '".$year."-".$month."-1' AND date < '".$year."-".($month+1)."-1' AND ($author_clause) AND NOT Deleted");
		}else{
			$obj_arr = $master->select($table, $this->get_date_attribute()." = '".$year."-".$month."-$day' AND ($author_clause) AND NOT Deleted");
		}
		
		if ($obj_arr > 0 ) {
			$events = array();
			foreach($obj_arr as $obj) {
				$event = $obj->convert_to_event($color);
				array_push($events,$event);
			}
			return $events;
		} else {
			return false;
		}
	}	

	
	public function convert_to_event($color) {
		$event = new event($this->get_id());
		$event->set_color($color);
		$event->set_title($this->get_default());
		$string = "\$event->set_date(new datetime(\$this->get_".$this->get_date_attribute()."() ) );";
		eval($string);
		return $event;
	}
	
	
	public function store_append(){}
	
	public function duplicate(){
	
		$clone = clone $this;
		
		return($clone);
	
	}
	
	public function load_properties(){
		
		foreach($this->properties as $p){
			$this->set($p, $this->load_db_field($p));
		}
		
	}
	
	
	public function store_after(){}


	public function save(){

		master::save($this);

	}


	public function to_array(){

		$array = Array();

		$array[$this->class_name] = Array();

		foreach ($this->properties as $p) {

			$v = $this->get($p);

			if(is_object($v)){
				$array[$this->class_name][$p] = $v->to_array();
			}else{
				$array[$this->class_name][$p] = $v;
			}
		}

		$array[$this->class_name]['id'] = $this->id;

		return $array;

	}
	
}

?>