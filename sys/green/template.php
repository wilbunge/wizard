<?php

	class template {

		var $_html;
	
		public function __construct($template=''){
		
			$this->load($template);
		
		}
	

		public function load($name){
			
			//$path = "/../../supermarket/client/templates/$name.html";
			//$real_path = dirname(__FILE__).($path);

			$real_path = "$name.html";

			//echo $real_path.'___<br>';

			
			$f = fopen($real_path, 'r');
			$this->_html = fread($f, filesize($real_path)) or die('ERROR READING TEMPLATE');
	
		
		}

		public function __toString(){
			return $this->_html;
		}
	
	
		public function render(){
			echo $this->_html;
		}

		public function html($html = null, $position=1){
			if($html!=null) $this->_html = $position<1?$html.$this->_html:$this->_html.$html;
			return $this->_html;
		}


		public function sub($tag, $template){

			$sub = new template($template);

			$this->replace($tag, $sub->html());

		}


		public function set_form_values($obj){

			include('phpQuery/phpQuery.php');

			foreach($obj->properties as $p){

				if(gettype($obj->get($p))=='object') continue;
				
				$token = "name=\"$p\"";

				$v = $obj->get($p);
				
				$replace = $token . ' value="'.$obj->get($p).'"';
			
				$this->_html = str_replace($token, $replace, $this->_html);

				$doc = phpQuery::newDocument($this->_html);

				$textarea = $doc->find("textarea[name=$p]");

				if($textarea->size()>0){
					$textarea->val($v);
					$this->_html = $doc->html();
				}

			}


			

			//$this->_html = str_replace('{ID}', $token->id, $this->_html);

			return;

		}

		public function form_field_error($field, $msg){

			$token = "name=\"$field\"";
				
			$replace = $token . ' w-forminputerror="'.$msg.'"';
		
			$this->_html = str_replace($token, $replace, $this->_html);

			return;

		}

	
		public function replace($token, $value=''){

			if($value == null) $value='';

			if(gettype($token)=='object'){

				foreach($token->properties as $p){
					$q = strtoupper($p);
					$q = "{{$q}}";

					$v = $token->get($p);

					if(gettype($v)=='object') continue;
				
					$this->_html = str_replace($q, $v, $this->_html);
				}

				$this->_html = str_replace('{ID}', $token->id, $this->_html);

				return;
			}
	
			$token = strtoupper($token);
			$token = "{{$token}}";

			$this->_html = str_replace($token, $value, $this->_html);
		
		}
	
		public function portion($tag){

			$tag_name = $tag;
		
			$tag = "<!--$tag-->";
			$c_tag = "<!---->";
		
			$a = strpos($this->_html, $tag);
			$b = strpos($this->_html, $c_tag, $a);

			$c = $a+strlen($tag);
			$d = $b-$c;
			
			$e = $a;
			$f = $b-$e+strlen($c_tag);
			
			$html_1 = substr($this->_html, $c, $d);
			$html_2 = substr($this->_html, $e, $f);
			
			$portion = new template_portion($this, $tag_name);
			$portion->_html = $html_1;
			
			return $portion;

		}


		public function prepend($html){
			
			$this->_html = $html . $this->_html;
		
			
		}


		public function append($portion, $tag=null){
			
			if($tag==null) $tag=$portion->_tag;
			
			$tag = "<!--$tag-->";
			$c_tag = "<!---->";
			
			//$html = $tag . $portion->_html;
			$html = $portion->_html.$tag;


			//$this->_html = str_replace($this->_html, $tag, $html);
			
			//$this->_html = str_replace($tag, $html, $this->_html);
			$this->_html = str_replace($tag, $html, $this->_html);
		
			
		}
		
		
		public function clear($tag){
		
			$tag = "<!--$tag-->";
			$c_tag = "<!---->";
			$curly_tag = "{$tag}";
		
			$a = strpos($this->_html, $tag);
			$b = strpos($this->_html, $c_tag, $a);
			
			$a = strpos($this->_html, $tag);

//			$this->_html = str_replace($curly_tag, '', $this->_html);

			if(!$a) return;

			$c = $a+strlen($tag);
			$d = $b-$c;
			
			$html_1 = substr($this->_html, $c, $d);
			
			$this->_html = str_replace($html_1, '', $this->_html);
		
			
		}

		public function _include($file){
			
			ob_start();
  			include("$file.php");
  			$cache= ob_get_contents();
  			ob_end_clean();
  			return $cache;

		}
		
	
	}
	
	
	class template_portion{
	
		var $_template, $_html, $_tag;
		
		public function __construct($template, $tag=null){
			$this->_template = $template;
			$this->_tag = $tag;
		}
		
		public function replace($token, $value=''){

			if(gettype($token)=='object'){

				foreach($token->properties as $p){
					$q = strtoupper($p);
					$q = "{{$q}}";

					$v = $token->get($p);

					if(gettype($v)=='object') continue;
				
					$this->_html = str_replace($q, $v, $this->_html);
				}

				$this->_html = str_replace('{ID}', $token->id, $this->_html);

				return;
			}

			$token = strtoupper($token);
			$token = "{{$token}}";
		
			$this->_html = str_replace($token, $value, $this->_html);
			
		}
		
		public function append(){
			$this->_template;
		}


		public function html($html){
			$this->_html .= $html;
		}
		
	}	?>