<?php

include_once('db_object.php');
	
class bug extends db_object{
	
	private $_title;
	private $_description;
	private $_fixed;
	private $_green_client;
	private $_green_user;
	private $_green_module;
	private $_date_reported;
	private $_user_agent;
	
	
	public $user_groups = array();
	public $modules = array();
	
	public $properties = array('title','description','game:game');
	public $ni_properties = array('user_agent');
	public $ndb_properties = array('screen_shot');
	
	var $table_name = "bugs";
	var $class_name = "bug";
	
	public function embed_screen_shot($p=''){
		$path = $p.'img/bug-screenshots/'.$this->get('id').'.jpg';
		$html = "<img src='$path' />";
		return($html);
	}
	
	
	public function get_date_reported(){
		if ($this->_date_reported== NULL){
			if($this->get_id()>0) {
				$this->_date_reported= $this->load_db_field('date_reported');
			}
		}
		return $this->_date_reported;
	}

	public function set_date_reported($val){
		$this->_date_reported= $val;
	}
	
	
	public function get_user_agent(){
		if ($this->_user_agent== NULL){
			if($this->get_id()>0) {
				$this->_user_agent= $this->load_db_field('user_agent');
			}
		}
		return $this->_user_agent;
	}

	public function set_user_agent($val){
		$this->_user_agent= $val;
	}
	
	
	public function get_description(){
		if ($this->_description== NULL){
			if($this->get_id()>0) {
				$this->_description= $this->load_db_field('description');
			}
		}
		return $this->_description;
	}

	public function set_description($val){
		$this->_description= $val;
	}
	
	
	public function get_green_module(){
		if ($this->_green_module== NULL){
			if($this->get_id()>0) {
				$this->_green_module= $this->load_db_field('green_module');
			}
		}
		return $this->_green_module;
	}

	public function set_green_module($val){
		$this->_green_module= $val;
	}
	
	
	public function get_green_user(){
		if ($this->_green_user== NULL){
			if($this->get_id()>0) {
				$this->_green_user= $this->load_db_field('green_user');
			}
		}
		return $this->_green_user;
	}

	public function set_green_user($val){
		$this->_green_user= $val;
	}
	
	
	public function get_green_client(){
		if ($this->_green_client== NULL){
			if($this->get_id()>0) {
				$this->_green_client= $this->load_db_field('green_client');
			}
		}
		return $this->_green_client;
	}

	public function set_green_client($val){
		$this->_green_client= $val;
	}
	
	
	public function get_fixed(){
		if ($this->_fixed== NULL){
			if($this->get_id()>0) {
				$this->_fixed= $this->load_db_field('fixed');
			}
		}
		return $this->_fixed;
	}

	public function set_fixed($val){
		$this->_fixed = $val;
	}
	
	
	
	public function get_title(){
		if ($this->_title== NULL){
			if($this->get_id()>0) {
				$this->_title= $this->load_db_field('title');
			}
		}
		return $this->_title;
	}

	public function set_title($val){
		$this->_title= $val;
	}
	
	
	public function get_name(){
		if ($this->name == NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	public function html_input_property($p_property) {
		
		$val = $this->get($p_property);

		$html = '';
		switch ($p_property) {
		
			case 'game' :
			
				$html = "<select name='game'>";
				$html .= "<option value='0'>...</option>";
				foreach(master::get('game', 'NOT `deleted`') as $game){
					$html .= "<option value='".$game->get_id()."'>" . $game->get_name() . "</option>";
				}
				$html .= "</select>";
				break;
						
			case 'fixed':
				
				$html .= '<label><input size="2" type="radio" name="sex" value="male">Male</label> <label><input size="2" type="radio" name="sex" value="female">Female</label>';
				break;
			
			case 'title' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
							
			
			case 'screen_shot' :
			
				$html .= '<input type="file" name="screen_shot" />';
				break;
							
			
			case 'description' :
			
				$html .= '<textarea name="'.$p_property.'">'.$val.'</textarea>';
				break;
			
			case 'name' :
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
		}
		return $html;
	}
	
	
	
	
	public function catch_post_properties() {

		foreach($this->properties as $property){
			
			$post_val = $_POST[$property];
			$this->set($property, $post_val);
		}
	}
	
	
	function get_crypt_id(){
	
		$x = $this->get_id()+1000;
		
		$y = $x*$x + 5*($x+25) - $x*10 + 456;
		
		$y += 123;
		$y *= 987;
		
		$arr = Array();
		
		array_push($arr, "U");
		array_push($arr, "V");
		array_push($arr, "W");
		array_push($arr, "X");
		array_push($arr, "Y");
		array_push($arr, "Z");
		array_push($arr, "5");
		array_push($arr, "6");
		array_push($arr, "7");
		array_push($arr, "8");
		array_push($arr, "9");
		
		$crypt_id = $y;
		
		$crypt_id = str_replace("9", "A", $crypt_id);
		$crypt_id = str_replace("8", "B", $crypt_id);
		$crypt_id = str_replace("7", "C", $crypt_id);
		$crypt_id = str_replace("6", "D", $crypt_id);
		$crypt_id = str_replace("5", "E", $crypt_id);
	
		for($i=strlen($y); $i<=12; $i++){
		//	echo($arr[$i]."<br>");
			//$crypt_id = $arr[rand(count($arr),0)] . $crypt_id;
		}
		
		
		return($crypt_id);
	
	
	}
	
	public function update_status($s){
		master::update_property($this, 'status', $s);
	}
	
	public function set_version($v){
		master::update_property($this, 'version', $v);
	}
	
	
}	?>