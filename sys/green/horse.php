<?php

include_once('db_object.php');
	
class horse extends db_object{
	
	public $properties = array('name', 'birth', 'sex', 'father:horse*', 'mother:horse*');

	var $table_name = "horses";
	var $class_name = "horse";
		
}

?>