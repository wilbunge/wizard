<?php

include_once('db_object.php');
	
class news extends db_object{
	
	public $properties = array('title', 'description', 'url', 'image', 'date:date', 'source');

	var $table_name = "news";
	var $class_name = "news";


	public function __toString(){
		return($this->get('title'));
	}


	public function exists(){
		// data_manager::$debug_mode=true;
		$title = str_replace('"', '\"', $this->get('title'));
		$exists = master::get_one('news', '(`title` LIKE "'.$title.'" OR `url` LIKE "'.$this->get('url').'") AND `id` <> '.$this->id.' AND NOT `deleted`');
		// data_manager::$debug_mode=false;

		return $exists;

	}

	public function generate_source($url = ''){

		if($url == '') $url = $this->get('url');

		$url_arr = explode('/', $url);

		$source = $url_arr[2];

		$source = str_replace('www.', '', $source);

		$this->set('source', $source);

	}

	public function get_friendly_date(){

		$original_date = $this->get('date');

		$delta = 0*60*60;

		$delta = time()-strtotime($original_date)+$delta;

		define('SECOND', 1);
		define('MINUTE', 60 * SECOND);
		define('HOUR', 60 * MINUTE);
		define('DAY', 24 * HOUR);
		define('MONTH', 30 * DAY);

		$minutes = $delta/60;
		$hours = round($minutes/60);

		// echo "$delta - ".(24 * HOUR);

if ($delta < 0){
  $friendly_date = "not yet";
}else if ($delta < 1 * MINUTE){
  $friendly_date = $delta == 1 ? "hace unos segundos" : "hace $delta segundos";
}
else if ($delta < 2 * MINUTE)
{
  $friendly_date = "unos minutos";
}
else if ($delta < 45 * MINUTE)
{
  $friendly_date = "hace ".round($minutes) ." minutos";
}
else if ($delta < 90 * MINUTE)
{
  $friendly_date = "una hora";
}
else if ($delta < 24 * HOUR)
{
  $friendly_date = "hace $hours horas";
}
else if ($delta < 48 * HOUR)
{
  $friendly_date = "ayer";
}
else if($delta < 30 * DAY){
	$friendly_date = date('d F Y', strtotime($original_date));
}
	return $friendly_date;	

	}


		
}

?>