<?php	
include_once('db_object.php');
	
class invoice extends db_object{
	
	private $_total;
	private $_date;
	private $_comments;
	private $_curency;
	private $_client;
	private $_order;
	private $_paid=false;
	
	public $properties = array('number', 'client:client*','date','subtotal','currency','tax','total','comments', 'type', 'order:order*'
		// , 'paid:boolean'
	);
	
	var $table_name = "invoices";
	var $class_name = "invoice";


	public function get_number2(){

		$number = $this->get('number');
		return substr($number, 1);

	}


	public function get_symbol(){
		if($this->get('currency') == 2){
			$symbol = "\$UY";
		}else{
			$symbol = "USD";
		}
		return $symbol;
	}


	public static function get_next_number(){

		// Very expensive way to do this... change it!

		$invoices = master::get('invoice', 'number <> ""');

		if(!$invoices) $next_number = "A1";

		$numbers = array();

		foreach ($invoices as $invoice) {
			$number = preg_replace('/\D/', '', $invoice->get('number'));

			$numbers[] = $number;
		}

		asort($numbers);

		$previous_number = $numbers[0];
		
		$next_number = "A".($previous_number+1);

		return $next_number;

	}

	
	public function get_default(){	
		return($this->get_id());
	}

	public static function format_number($total){

		$total = number_format($total, 2, ',', '.');
		return $total;

	}


	public function get_date_formatted(){
		$ts = strtotime($this->get('date'));
		return wizard::format_date($ts);
	}


	public function get_total_plus_taxes($f=false){

		$total_plus_taxes = $this->get('total')*1.23;

		if($f) $total_plus_taxes = self::format_number($total_plus_taxes);

		return $total_plus_taxes;
	}

	public function get_total_taxes($f=false){
		$total_taxes =  $this->get('total')*.23;
		if($f) $total_taxes = self::format_number($total_taxes);
		return $total_taxes;
	}


	public function get_payments(){

		$payments = master::get('payment', 'invoice='.$this->id);

		return $payments;

	}

	public function get_payments_total(){

		$payments = $this->get_payments();

		$total=0;

		foreach ($payments as $payment) {
			
			$total += $payment->get('total');

		}

		return $total;

	}


	public function check_payments(){

		if($this->get_payments_total()>=$this->get('total')){
			$this->update_property('paid', true);
		}

	}


	
	public function catch_post_properties() {

		foreach($this->properties as $property){
			
			if($property == "paid"){
				$this->set_paid($_POST["paid"]=="yes");
			}elseif($property != "__date"){
				$post_val = $_POST[$property];
				
				$this->set($property, $post_val);


			}else{
				$this->set_date($_POST['year_date']."-".$_POST['month_date']."-".$_POST['day_date']);
			}
		}
	}	
	
	
	public function get_type_name(){
		
		switch($this->get('type')){
			
			case 3 : return 'NOTA DE CREDITO'; break;
			case 2 : return 'CREDITO'; break;
			default : return 'CONTADO'; break;
			
			
		}
		
	}

	public function get_total_f($format){
	
		$t = $this->get('total');

		if($t==null) $t=0;

		if($format) $t = number_format($t, 2, ',', '.');
	
		return $t;
	}

	public function get_subtotal_f($format){
	
		$t = $this->get('subtotal');

		if($t==null) $t=0;

		if($format) $t = number_format($t, 2, ',', '.');
	
		return $t;
	}

	public function get_tax_f($format){
	
		$t = $this->get('tax');

		if($t==null) $t=0;

		if($format) $t = number_format($t, 2, ',', '.');
	
		return $t;
	}


	public function get_id_number(){

		$id = $this->id;
		$number =$this->get('number');

		$id_number = $number==''?"#$id":$number;
		
		return $id_number;

	}


	public function format_total($total){

		$total = number_format($total, 2, ',', '.');

		return $total;


	}	


	
}	?>