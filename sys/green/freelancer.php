<?php

include_once('db_object.php');
	
class freelancer extends db_object{
	
	public $properties = array('first_name', 'last_name', 'email', 'skills:textarea');
	
	var $table_name = "freelancers";
	var $class_name = "freelancer";


	public function get_name(){

		$name[] = $this->get('first_name');
		$name[] = $this->get('last_name');

		return implode(' ', $name);

	}


	public function get_orders(){

		$my_orders = master::get('order', '`freelancer` = '.$this->id);

		return $my_orders;

	}


	



}	?>