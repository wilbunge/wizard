<?php

include_once('db_object.php');
	
class order extends db_object{
	
	private $_description;
	private $_date;
	private $_client;
	private $_total;
	private $_payment_terms;
	private $_delivered;
	
	private $_products = array();
	
	public $properties = array('client:client*','date','total','subtotal','currency', 'currency_value', 'tax','payment_terms','description','delivery_date','products','delivered', 'invoice',

			'salesman:salesman*'

		);
	
	public $ni_properties = array('in_date');
	
	public $extra = array("products" => array("order_unit_price","order_subtotal","order_amount"));
	
	var $table_name = "orders";
	var $class_name = "order";

	public function format_total($total){

		$total = number_format($total, 2, ',', '.');

		return $total;


	}
	
	
	public function get_date_formatted(){
		$date = $this->get_date();
		
		if($date != '') $date = date('d/m/y ',$date);
		
		return($date);
		
	}
	
	
	public function get_in_date(){
		if ($this->_date== NULL){
			if($this->get_id()>0) {
				$this->_date= $this->load_db_field('date');
			}else{
				$this->_date = date('Y-m-d h:m:s');
			}
		}
		return $this->_date;
	}

	public function get_products(){
	
		if ($this->values['products']!=null){
			return $this->values['products'];
		}
	
		if ($this->get_id() > 0 and $this->values['products']=='') {

			$products = array();
			
			//$result = $this->get_master()->query_generic_4_selection("`product`,`gifted`,`id`", "order_prodcuts", "NOT deleted AND `order` = ".$this->get_id());
			$result = data_manager::read('order_products', "`product`,`id`", "NOT deleted AND `order` = ".$this->get_id());;
			
			$this->values['products']=Array();
			
			while ($line = mysqli_fetch_array($result)){

				$order_product = new order_product;
				
				$order_product->set('product', new product($line['product']));
				$order_product->set_id($line['id']);
				$order_product->set_container($this);
				
				$this->values['products'][] = $order_product;
				
			}

		}else{
			$this->values['products']=Array();
		}

		return $this->values['products'];
	}
	
	public function set_products($val){
		$this->_products = $val;
	}
	
	
	public function get_total_f($format){
	
		$t = $this->get('total');

		if($t==null) $t=0;

		if($format) $t = number_format($t, 2, ',', '.');
	
		return $t;
	}
	
	
	public function get_delivered(){
	
		if ($this->_delivered=== NULL){
			if($this->get_id()>0) {
				$this->_delivered= $this->load_db_field('delivered');
			}
		}
	
		return($this->_delivered);
	}
	
	public function set_delivered($val){
		$this->_delivered = $val;
	}
	
	
	public function get_payment_terms(){
	
		if ($this->_payment_terms== NULL){
			if($this->get_id()>0) {
				$this->_payment_terms= $this->load_db_field('payment_terms');
			}
		}
	
		return($this->_payment_terms);
	}
	
	public function set_payment_terms($val){
		$this->_payment_terms = $val;
	}

	
	
	
	public function get_amount(){
		if ($this->_amount== NULL){
			if($this->get_id()>0) {
				$this->_amount= $this->load_db_field('amount');
			}
		}
		return $this->_amount;
	}

	public function set_amount($val){
		$this->_amount= $val;
	}
	
	
	public function get_delivery_date(){
		if ($this->_delivery_date== NULL){
			if($this->get_id()>0) {
				$this->_delivery_date= $this->load_db_field('delivery_date');
			}
		}
		return $this->_delivery_date;
	}

	public function set_delivery_date($val){
		$this->_delivery_date= $val;
	}
	
	
	
	public function get_description(){
		if ($this->_description== NULL){
			if($this->get_id()>0) {
				$this->_description= $this->load_db_field('description');
			}
		}
		return $this->_description;
	}

	public function set_description($val){
		$this->_description= $val;
	}
	
	
	
	
	
	
	public function get_phone(){
		if ($this->_phone== NULL){
			if($this->get_id()>0) {
				$this->_phone= $this->load_db_field('phone');
			}
		}
		return $this->_phone;
	}

	public function set_phone($val){
		$this->_phone= $val;
	}
	
	
	public function get_mail(){
		if ($this->_mail== NULL){
			if($this->get_id()>0) {
				$this->_mail= $this->load_db_field('mail');
			}
		}
		return $this->_mail;
	}

	public function set_mail($val){
		$this->_mail= $val;
	}
	
	
	public function get_default(){
		return($this->get_description());
	}
	
	
	
	public function embed_photo($p_mode){
	
		switch($p_mode){
			case 2 : 
				$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/mp_" . $this->get_id() . "_2.jpg";
				break;
			default :
				$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/mp_" . $this->get_id() . "_1.jpg";
				break;
		}
		
		if(!file_exists($img_path)){
			switch($p_mode){
				case 2 : 
					$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/no_photo.jpg";
					break;
				default :
					$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/no_photo_small.jpg";
					break;
			}
		}
	
		$tag = "<img src='$img_path'>";
	
		return $tag;
	
	}
	
	
	
	public function html_input_property($p_property) {
		

		// IMPORTANT !!
		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		// IMPORTANT !!
		
		$html = '';
		switch ($p_property) {
		
			case 'payment_terms' :
			
				$html="<label><input name='payment_terms' value='contado' type='radio' />".translate("orders","add_new/txt_cash")."</label><label><input name='payment_terms' value='credito' type='radio' />".translate("orders","add_new/txt_credit")."</label>";
			
				break;
					
			case 'products' :

				$result = $this->get_master()->query_generic_4_selection("`id`,`price`, `name`", "products", "NOT deleted","`name`");
			
				$html = "<div style='overflow:scroll; height:100px;'>";
				
				$html .= "<ul id='G_me_ORDERS_product_list' style='margin:0; padding:0;'>";
				
				$this->load_products();
				
				while ($line = mysql_fetch_array($result)){
				
					$product = new product;
					$product->set_id($line['id']);
				
					$s = '<select onchange="javascript:G_jf_ORDERS_refresh_total();"'.(!$this->is_in_products($product)? ' disabled ':'').' unit_price="'.$line['price'].'" name="order_amount_'.$line['id'].'">';
					
					for($i=1; $i<=99; $i++){
					
						$p=$this->get_product($product);
					
						if($p){
							$ss=($p->get_order_amount()==$i);
						}else{
							$ss=false;
						}
					
						$s .= '	<option value="'.$i.'"'.($ss ? ' selected' : '').'>'.$i.'</option>';
					}
					$s .= '</select>';
				
					
					
					if($this->is_in_products($product)){
						$html .= '<li><input onclick="G_jf_orders_toggle_order_amount('.$line['id'].')" name="products[]" checked="checked" type="checkbox" value="'.$line['id'].'">'.$s.'&nbsp;<label>'.$line['name'].'</label></li>';						
					}else{
						$html .= '<li><input onclick="G_jf_orders_toggle_order_amount('.$line['id'].')" name="products[]" '.($this->is_in_products($product)? 'checked':'').' type="checkbox" value="'.$line['id'].'">'.$s.'&nbsp;<label>'.$line['name'].'</label></li>';
					}
				}

				$html .= "</ul></div>";

				break;


			case 'date':
							
				$html .= '<select id="day_'.$p_property.'" name="day_'.$p_property.'">';
				
				for($i=1; $i<=31; $i++){
					$html .= '	<option value="'.$i.'"'.((date('d') == $i) ? ' selected' : '').'>'.$i.'</option>';
				}
				
				$html .= '</select>';
				$html .= " / ";
				
				$html .= '<select id="month_'.$p_property.'" name="month_'.$p_property.'">';
				
				for($i=1; $i<=12; $i++){
					$html .= '	<option value="'.$i.'"'.((date('m') == $i) ? ' selected' : '').'>'.$i.'</option>';
				}
				
				$html .= '</select>';
				$html .= " / ";
				
				$html .= '<select id="year_'.$p_property.'" name="year_'.$p_property.'">';
				
				for($i=date('Y')-10; $i<=date('Y')+10; $i++){
					$html .= '	<option value="'.$i.'"'.((date('Y') == $i) ? ' selected' : '').'>'.$i.'</option>';
				}
				
				$html .= '</select>';
				break;
			
			case 'description' :
			
				$html .= '<textarea name="'.$p_property.'">'.$val.'</textarea>';
				break;
			
			
			case 'client' :
			
				$arr = $this->get_master()->query_generic(new client(),'NOT Deleted ORDER BY company_name, last_name, first_name');
				
				$html .= '<select id="client" name="client">';
				$html .= '	<option value="0">...</option>';
				foreach($arr as $client){
					$html .= '	<option value="'.$client->get_id().'"'.(($client->get_id() == $this->get_client()->get_id()) ? ' selected' : '').'>'.$client->get_name().'</option>';
				}
				$html .= '</select>';
				
				break;
			
			case 'name' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
							
			
			case 'total' :
			
				$html .= '$ <label id="G_ME_orders_label_total_auto">0</label><input size="4" style="display:none;" value="'.$val.'" type="text" name="'.$p_property.'">&nbsp;&nbsp;<label><input id="G_ME_orders_label_total_auto_calc" checked type="checkbox">'.translate("orders","add_new/txt_automatic_calculation").'</label>';
				break;		
			
			case 'date' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
			
			case 'name' :
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
		}

		return $html;

	}
	
	
	public function set_data($source) {

		parent::set_data($source);

		foreach($this->properties as $property){
			
			if($property == 'products'){
				$this->_products = array();

				$post_val = $source[$property];

				if(is_array($post_val)){

					foreach($post_val as $arr_val){

						$product = new product($arr_val['id']);
						$order_product = new order_product;

						$product->set('order', $this->id);
						$order_product->set('order_amount', $arr_val['order_amount']);
						$order_product->set('product', $product);
						$order_product->set('order_unit_price', $arr_val['unit_price']);

						//echo $arr_val.'++++++';

						$order_product->calculate_subtotal();
						
						$this->values['products'][] = $order_product;
					}
				}

			}
			
		}
	}


	public function calculate_subtotal(){

		return;

		$client = $this->get('client');

		$price_list = $client->get('price_list');

		if($price_list->id==0) $price_list = price_list::get_default();

		// echo $price_list;
		
		foreach($this->get_products() as $p){
				
			if($price_list_product = $price_list->get_price_list_product($p)){
				// echo '---';
			}

			//echo $price_list_product->get('price');

		}

	}
	
	
	public function update_stock_items(){
	
		foreach($this->get_products() as $product){
		
			
			$current_product_stock = $product->get_stock_item()->get_qty();
			$difference =  $current_product_stock - $product->get_order_amount();
			
			if ($difference < 0) {
				$product->produce(-$difference, $this->get_id()); // Give production order of N items	
			}
			$stock_item_id = $product->get_stock_item()->get_id();
			$qty = -$product->get_order_amount();
			$this->get_master()->mysql_query_insert('stock_movements', 'stock_item, qty, date, `order`', $stock_item_id . ', ' . $qty . ', ' . time() . ','.$this->get_id());
			
			$product->load_stock_items();
			return;
			
			foreach($product->get_stock_items() as $stock_item){
			
				$amount = $product->get_order_amount()*$stock_item->get_composition_amount();
				
				$result=$this->get_master()->mysql_query_select('unit_cost,id,substractions,qty','stock_movements', 'qty>0 AND qty>substractions AND stock_item=' . $stock_item->get_id(), 'ORDER BY `id`');
				
				if(mysql_num_rows($result)>0){
					
					while($row=mysql_fetch_array($result)){
						$substractions=$row['substractions'];
						$qty=$row['qty'];
						$id=$row['id'];
						$cost=$row['unit_cost'];
						
						$free_space=$qty-$substractions;
						
						$new_substractions=$substractions+$amount;
						
						if($free_space>=$amount){
							$result2=$this->get_master()->mysql_query_update("stock_movements", "`substractions`=(`substractions`+$amount)","id=$id");
							$this->get_master()->mysql_query_insert('stock_movements', 'stock_item, unit_cost, qty, date, `order`', $stock_item->get_id() . ', ' . $cost . ',' . -1*$amount . ', ' . time() . "," . $this->get_id());
							break;
						}else{
							$result2=$this->get_master()->mysql_query_update("stock_movements", "`substractions`=$qty","id=$id");
							$this->get_master()->mysql_query_insert('stock_movements', 'stock_item, unit_cost, qty, date, `order`', $stock_item->get_id() . ', ' . $cost . ',' . -1*$qty . ', ' . time() . "," . $this->get_id());
							$amount-=$free_space;
						}
						
						
					
					}
				
				
				
				}
				
				
				//$this->get_master()->mysql_query_update('', '', '', );
				
			}
			
		}
	
	}


	public function update_inventory(){

		foreach($this->get_products() as $product){
			$product->get('product')->inventory_movement(-$product->get('order_amount'), $this);
		}
		
	}
	
	
	public function load_products(){
		
		$result = $this->get_master()->mysql_query('SELECT', 'product,order_amount', 'order_products', '`order` = '.$this->get_id().' AND NOT deleted', '');
		
		while($row = mysql_fetch_array($result)){
			$p = new product;
			$p->set_id($row['product']);
			$p->set_order_amount($row['order_amount']);
			
			array_push($this->_products,$p);
		}
		
	}
	
	
	public function is_in_products($p_obj) {
		foreach ($this->get_products() as $product) {
			if ($p_obj->equals($product)) {
				return $product;
			}
		}
		return false;
	}
	
	public function get_product($p_obj) {
		foreach ($this->get_products() as $product) {
			if ($p_obj->equals($product)) {
				return $product;
			}
		}
		return false;
	}
	
	
	public function embed(){
	
		$html = "<table class='G_cc_grid' cellspacing='0' border='1'>";
		
		foreach($this->properties as $property){
		
			$code0 = "\$this->get_$property();";
			$code = '$val = ' . $code0;
		
			eval ($code);
		
			$html .= "<tr>";
			$html .= "	<th>" . $this->translate_property($property) . "</th>";
			
			if($property == 'phone'){
			
				$val_s = str_replace(" ", "", $val);
			
				$html .= "	<td><a href='skype:+".$val_s."?call'>" . $val. "</a></td>";
			}elseif($property=="products"){
				$html .= "<td><ul>";
				foreach($val as $obj){
					$args = "'products', 'view', {id:".$obj->get_id()."}, 'Products'";
					$html .= '<li>'.$obj->get_order_amount().' x <a href="javascript:G_jsf_openModule('.$args.')">'.$obj->get_default().'</a></li>';
				}
				$html .= "</ul></td>";
				
				
			}elseif(is_array($val)){
				$html .= "<td><ul>";
				foreach($val as $obj){
					if(gettype($obj) == 'object'){
						$html .= "<li>".$obj->get_default()."</li>";
					}else{
						$html .= "<li>".$obj."</li>";
					}					
				}
				$html .= "</ul></td>";
				
				
			}elseif(gettype($val) == 'object'){
				$html .= "	<td>" . $val->get_default() . "</td>";
			}elseif($property == "total") {
				$html .= "<td><ul>";
				$html .= "$ ".number_formatter($val);
				$html .= "</ul></td>";
				
//				number_formatter
			}else{
				$html .= "	<td>" . $val . "</td>";
			}
			
			$html .= "</tr>";
		}
		
		$html .= "</table>";
		
		return($html);
	}
	
	
	public function get_calendar_events($table,$color,$month,$year,$day=false) {
		$master = $this->get_master();
		
		if(!$day){
			$obj_arr = $master->select($table, "delivery_date > '".$year."-".$month."-1' AND delivery_date < '".$year."-".($month+1)."-1' AND NOT Deleted");
		}else{
			$obj_arr = $master->select($table, $this->get_date_attribute()." = '$year-$month-$day' AND NOT Deleted",'',false);
		}
		
		if (count($obj_arr > 0)) {
			$events = array();
			foreach($obj_arr as $obj) {
				
				$dd = strtotime($obj->get_delivery_date());
				$da = date('d', $dd);
				
				$event = $obj->convert_to_event($color);
				$event->set_title("Pedido #" . $obj->get_id());
				
				$d = strtotime("now", mktime(12,0,0,$month,$da,$year) );
				$d = new DateTime("@{$d}");
				$event->set_date($d);
				
				array_push($events,$event);
			}
			return $events;
		} else {
			return false;
		} 
		
	}
	
	public function generate_invoice($invoice_number='', $invoice_type=''){
	
		$invoice = new invoice;

		$invoice->set('number', $invoice_number);
		$invoice->set('subtotal', $this->get('subtotal'));
		$invoice->set('tax', $this->get('tax'));
		$invoice->set('total', $this->get('total'));
		$invoice->set('currency', $this->get('currency'));
		$invoice->set('date', $this->get('date'));
		$invoice->set('client', $this->get('client'));
		$invoice->set('type', $invoice_type);

		$order = new order($this->get_id());
		$invoice->set('order', $order);

		$invoice->save();

		echo '===='.$this->$id.'-------';
		
		$this->set('invoice', $invoice);

		
		return $invoice;
	
	}
	
	
}


class order_product extends db_object{

	public $properties = array('order','product','order_unit_price','order_subtotal','order_amount');

	var $table_name = "order_products";
	var $class_name = "order_product";


	public function calculate_subtotal(){
		$unit_price = $this->get('product')->get('price');
		$subtotal = $this->get('order_amount')*$unit_price;

		$this->set('order_subtotal', $subtotal);
	}
	
}	?>