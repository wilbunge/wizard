<?php

include_once('db_object.php');
	
class game extends db_object{
	
	private $_name;
	
	public $properties = array('name', 'file_name', 'color', 'fb_app_id', 'fb_secret');
	public $ni_properties = array();
	
	var $table_name = "games";
	var $class_name = "game";
	
	public function __toString(){
		return($this->get('name'));
	}
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
		
}

?>