<?php

include_once('db_object.php');
	
class contact extends db_object{
	
	public $properties = array('first_name', 'last_name', 'email');

	var $table_name = "contacts";
	var $class_name = "contact";

	public function get_name(){

		$name = $this->get('first_name');

		return $name;

	}

	public function validate(){

		wizard::_include('validation');

		$validation = new validation;

		if($this->get('first_name')=='') $validation->error();

		return $validation;


	}
		
}

?>