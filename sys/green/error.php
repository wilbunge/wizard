<?	class error{

		public $number;


		const NONE_ACTIVE_SESSION = 1;
		const NONE_ACTIVE_SESSION_LOAD = 12;
		const NONE_ACTIVE_SESSION_TRANSPORT = 13;

		const ACCOUNT_NOT_ACTIVATED = 2;
		const NONE_ACTIVE_MEMBERSHIP = 21;
		const ACCOUNT_NOT_CONFIRMED = 22;
		const ACCOUNT_DISABLED = 23;
		const EXPIRED_MEMBERSHIP = 24;

		const WRONG_TYPE_PUBLISH_LOAD = 3;
		const WRONG_TYPE_PUBLISH_TRANSPORT = 31;
		const WRONG_TYPE_VIEW_LOAD = 32;
		const WRONG_TYPE_VIEW_TRANSPORT = 33;

		public function __construct($error_number){

			require_once(dirname(__FILE__).'/template.php');


			switch ($error_number) {

				/* --- No session errors --- */

				case error::NONE_ACTIVE_SESSION :
					
					$this->title = 'Es necesario iniciar sesión';
					$this->html = template::load('msg/signin_required', true);

					break;

				case error::NONE_ACTIVE_SESSION_TRANSPORT :
					
					$this->title = 'Es necesario iniciar sesión';
					$this->html = template::load('msg/signup_required_loads', true);

					break;

				case error::NONE_ACTIVE_SESSION_LOAD :
					
					$this->title = 'Es necesario iniciar sesión';
					$this->html = template::load('msg/signup_required_transports', true);

					break;


				/* --- Unauthorized access errors --- */
				
				case error::NONE_ACTIVE_MEMBERSHIP :
					$this->title = 'Subscripción al servicio';
					$this->html = template::load('msg/none_active_membership', true);
					break;

				
				case error::ACCOUNT_NOT_CONFIRMED :
					$this->title = 'Cuenta no confirmada';
					$this->html = template::load('msg/account_not_confirmed', true);
					break;

				case error::ACCOUNT_DISABLED :
					$this->title = 'Cuenta deshabilitada';
					$this->html = template::load('msg/account_disabled', true);
					break;

				case error::EXPIRED_MEMBERSHIP :
					$this->title = 'La subscripción ha vencido';
					$this->html = template::load('msg/expired_membership', true);
					break;



				/* --- Incorrect user type errors --- */

				case error::WRONG_TYPE_PUBLISH_TRANSPORT :
					$this->html = 'Unicamente los usuarios con empresa "Transportadora" pueden publicar transportes.';
					break;

				case error::WRONG_TYPE_PUBLISH_LOAD :
					$this->html = 'Unicamente los usuarios con empresa "Cargadora" pueden publicar cargas.';
					break;

				case error::WRONG_TYPE_VIEW_TRANSPORT :
					$this->html = 'Unicamente los usuarios con empresa "Cargadora" pueden ver transportes.';
					break;

				case error::WRONG_TYPE_VIEW_LOAD :
					$this->html = 'Unicamente los usuarios con empresa "Transportadora" pueden ver cargas.';
					break;



				/* --- Default error message --- */

				default:
					
					$this->html = "Ha ocurrido un error: $error_number";

					break;
			}

			if($this->title=='') $this->title = 'Error';

			$this->text = $this->title;
			$this->number = $error_number;

		}


		static function die_json(){

			
			die;
		}


	}	?>