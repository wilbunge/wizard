<?php

include_once('db_object.php');

class green_client extends db_object{
	
	private $_last_name;
	private $_first_name;
	private $_email;
	private $_tel;
	private $_domain;
	private $_business_type;
	private $_support_hero;
	private $_balance;
	private $_mobile;
	private $_sms;
	private $_affiliate;
	private $_free_leads;
	private $_availability;
	
	public $user_groups = array();
	public $modules = array();
	
	public $properties = array('name','email','mobile','sms','availability');
	public $ni_properties = array('free_leads');
	
	public $settings=array();
	public $settings_it=array();
	public $settings_attr=array();	
	
	var $table_name = "green_clients";
	var $class_name = "green_client";
	
	
	
	public function get_url(){
		return("http://lgs.avanfeel.com.uy/".$this->get_domain());
	}
	
	
	
	public function get_availability(){
		if ($this->_availability===NULL){
			if($this->get_id()>0) {
				$this->_availability= $this->load_db_field('availability');
			}
		}
		return $this->_availability;
	}

	public function set_availability($val){
		$this->_availability= $val;
	}
	
	
	
	
	public function get_first_name(){
		if ($this->_first_name== NULL){
			if($this->get_id()>0) {
				$this->_first_name= $this->load_db_field('first_name');
			}
		}
		return $this->_first_name;
	}

	public function set_first_name($val){
		$this->_first_name= $val;
	}
	
	
	public function get_affiliate(){
		if ($this->_affiliate== NULL){
			if($this->get_id()>0) {
				$this->_affiliate= $this->load_db_field('affiliate');
			}
		}
		return $this->_affiliate;
	}

	public function set_affiliate($val){
		$this->_affiliate= $val;
	}
	
	
	
	public function get_mobile(){
		if ($this->_mobile== NULL){
			if($this->get_id()>0) {
				$this->_mobile= $this->load_db_field('mobile');
			}
		}
		return $this->_mobile;
	}

	public function set_mobile($val){
		$this->_mobile= $val;
	}
	
	
	public function get_sms(){
		if ($this->_sms===NULL){
			if($this->get_id()>0) {
				$this->_sms= $this->load_db_field('sms');
			}
		}
		return $this->_sms;
	}

	public function set_sms($val){
		$this->_sms= $val;
	}
	
	
	public function get_business_type(){
		if ($this->_business_type== NULL){
			if($this->get_id()>0) {
				$this->_business_type= $this->load_db_field('business_type');
			}
		}
		return $this->_business_type;
	}

	public function set_business_type($val){
		$this->_business_type= $val;
	}
	
	
	public function get_domain(){
		if ($this->_domain===NULL){
			if($this->get_id()>0) {
				$this->_domain= $this->load_db_field('domain');
			}
		}
		return $this->_domain;
	}

	public function set_domain($val){
		$this->_domain= $val;
	}
	
	
	public function get_tel(){
		if ($this->_tel== NULL){
			if($this->get_id()>0) {
				$this->_tel= $this->load_db_field('tel');
			}
		}
		return $this->_tel;
	}

	public function set_tel($val){
		$this->_tel= $val;
	}
	
	
	public function get_email(){
		if ($this->_email== NULL){
			if($this->get_id()>0) {
				$this->_email= $this->load_db_field('email');
			}
		}
		return $this->_email;
	}

	public function set_email($val){
		$this->_email = $val;
	}
	
	
	
	public function get_last_name(){
		if ($this->_last_name== NULL){
			if($this->get_id()>0) {
				$this->_last_name= $this->load_db_field('last_name');
			}
		}
		return $this->_last_name;
	}

	public function set_last_name($val){
		$this->_last_name= $val;
	}
	
	
	
	public function get_phone(){
		if ($this->_phone== NULL){
			if($this->get_id()>0) {
				$this->_phone= $this->load_db_field('phone');
			}
		}
		return $this->_phone;
	}

	public function set_phone($val){
		$this->_phone= $val;
	}
	
	
	public function get_mail(){
		if ($this->_mail== NULL){
			if($this->get_id()>0) {
				$this->_mail= $this->load_db_field('mail');
			}
		}
		return $this->_mail;
	}

	public function set_mail($val){
		$this->_mail= $val;
	}
	
	
	public function get_birth(){
		if ($this->_birth == NULL){
			if($this->get_id()>0) {
				$this->_birth = $this->load_db_field('birth');
			}
		}
		return $this->_birth;
	}

	public function set_birth($val){
		$this->_birth = $val;
	}
	
	
	public function get_sex(){
		if ($this->_sex == NULL){
			if($this->get_id()>0) {
				$this->_sex = $this->load_db_field('sex');
			}
		}
		return $this->_sex;
	}

	public function set_sex($val){
		$this->_sex = $val;
	}
	
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db2_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	public function html_input_property($p_property) {
		

		// IMPORTANT !!
		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		// IMPORTANT !!
		
		$html = '';
		switch ($p_property) {
		
			case 'sms' :
				$html = "<input ". ($val ? 'checked':'') ." name='sms' type='checkbox' value='yes'>";
				break;
						
			case 'availability':
				
				$html .= '<label><input'. ($val ? " checked":"") .' type="radio" name="availability" value="1">Disponible</label> <label><input'. (!$val ? " checked":"") .' type="radio" name="availability" value="0">No Disponible</label>';
				break;
			
			case 'phone' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
			
			
			case 'last_name' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
							
			
			case 'first_name' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
			
			case 'name' :
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
		}
		return $html;
	}
	
	
	public function get_cookie_id(){
		//$this->set_id($_COOKIE['green_client']);
		//return($this->get_id());
	}
	
	
	public function get_id_by_domain($p_domain){
	
		$sql_query = "SELECT id FROM green_clients WHERE domain = '".$p_domain."'";
		
		$result = mysql_query($sql_query) or die(mysql_error());
		
		
		
		if(mysql_num_rows($result)<1){
			return(false);
		}
		
		$line = mysql_fetch_array($result) or die("error");

		if($line != null){
			$this->set_id($line['id']);
			return(true);
		}else{
			return(false);
		}
	
	}
	
	
	public function load_user_groups($p_domain){
	
		$sql_query = "SELECT id FROM user_groups WHERE green_client = '".$this->get_id()."' or green_client = 0";
		
		while ($line = mysql_fetch_array($query)){

			$user_group = new user_group();
			$user_group->set_name($line['name']);
			$user_group->set_id($line['id']);
			
			array_push($this->user_groups, $user_group);
		}
	
	}
	
	
	
	public function load_modules_2(){
	
		db::connect(1);
		
		$sql_query = "SELECT modules.id AS module, company_area FROM client_modules, modules WHERE (NOT public) AND (client_modules.module = modules.id) AND (green_client = '".$this->get_id()."') AND deleted = 0 GROUP BY modules.id ORDER BY company_area, name_spa";
		$result = mysql_query($sql_query);
		
		while ($line = mysql_fetch_array($result)){
			$green_module = new green_module();
			$green_module->set_id($line['module']);
			
			$green_module->set_company_area($line['company_area']);
			
			array_push($this->modules, $green_module);
		}
		
		$sql_query_2 = "SELECT * FROM modules WHERE public";
		$result_2 = mysql_query($sql_query_2);
		
		while($row = mysql_fetch_array($result_2)){
			$green_module = new green_module();
			$green_module->set_id($row['id']);		
			$green_module->set_company_area($row['company_area']);			
			
			array_push($this->modules, $green_module);	
		}
		
		return($this->modules);
	
	}
	
	
	
	public function load_modules(){
		
		$sql_query = "SELECT modules.id AS module, company_area FROM client_modules, modules WHERE (NOT public) AND (client_modules.module = modules.id) AND (green_client = '".$this->get_id()."') AND deleted = 0 GROUP BY modules.id ORDER BY company_area, name_spa";
		$result = mysql_query($sql_query);
//		echo mysql_error();
		
		while ($line = mysql_fetch_array($result)){
			$green_module = new green_module();
			$green_module->set_id($line['module']);
			
			$green_module->set_company_area($line['company_area']);
			$green_module->get_name();
			$green_module->get_keyword();
			
			array_push($this->modules, $green_module);
		}
		
		$sql_query_2 = "SELECT * FROM modules WHERE public";
		$result_2 = mysql_query($sql_query_2);
		
		while($row = mysql_fetch_array($result_2)){
			$green_module = new green_module();
			$green_module->set_id($row['id']);		
			$green_module->set_company_area($row['company_area']);			
			
			$green_module->get_name();
			$green_module->get_keyword();
			array_push($this->modules, $green_module);	
		}
		
		return($this->modules);
	
	}
	
	
	public function load_modules_mobile(){
		
		$sql_query = "SELECT modules.id AS module, company_area FROM client_modules, modules WHERE (NOT public) AND mobile AND (client_modules.module = modules.id) AND (green_client = '".$this->get_id()."') AND deleted = 0 GROUP BY modules.id ORDER BY company_area, name_spa";
		$result = mysql_query($sql_query);
//		echo mysql_error();
		
		while ($line = mysql_fetch_array($result)){
			$green_module = new green_module();
			$green_module->set_id($line['module']);
			$green_module->set_mobile($row['mobile']);		
			
			$green_module->set_company_area($line['company_area']);
			$green_module->get_name();
			$green_module->get_keyword();
			
			array_push($this->modules, $green_module);
		}
		
		$sql_query_2 = "SELECT * FROM modules WHERE public AND mobile";
		$result_2 = mysql_query($sql_query_2);
		
		while($row = mysql_fetch_array($result_2)){
			$green_module = new green_module();
			$green_module->set_id($row['id']);		
			$green_module->set_mobile($row['mobile']);		
			$green_module->set_company_area($row['company_area']);			
			
			$green_module->get_name();
			$green_module->get_keyword();
			array_push($this->modules, $green_module);	
		}
		
		return($this->modules);
	
	}
	
	
	
	public function has_access_to($module){
	
		mysql_connect('localhost', 'root','root');
		mysql_select_db('green_1a');
		
		$sql_query = "SELECT modules.id AS module FROM client_modules, modules WHERE (NOT public) AND (client_modules.module = modules.id) AND (green_client = '".$this->get_id()."') AND deleted = 0 AND modules.id = " . $module->get_id() ." GROUP BY modules.id ORDER BY name_spa";
		$result = mysql_query($sql_query);
		
		$sql_query_2 = "SELECT * FROM modules WHERE public AND id=".$module->get_id();
		$result_2 = mysql_query($sql_query_2);
		
		return(mysql_num_rows($result)>0 || mysql_num_rows($result_2)>0);
	
	}
	
	
	
	public function query_modules($p_query){
		
		$sql_query = "SELECT module FROM client_modules, modules WHERE (".$p_query.") AND (client_modules.module = modules.id) AND (green_client = '".$this->get_id()."') AND deleted = 0";
		$sql_query_2 = "SELECT id as module FROM modules WHERE (".$p_query.") AND public";
		
		$result = mysql_query($sql_query);
		$result_2 = mysql_query($sql_query_2);
		
		while ($line = mysql_fetch_array($result)){

			$green_module = new green_module();
			$green_module->set_id($line['module']);
			$green_module->get_name();
			$green_module->get_keyword();
			
			array_push($this->modules, $green_module);
		}
		
		while ($line_2 = mysql_fetch_array($result_2)){

			$green_module = new green_module();
			$green_module->set_id($line_2['module']);
			$green_module->get_name();
			$green_module->get_keyword();
			
			array_push($this->modules, $green_module);
		}
		
		return $this->modules;
	
	}
	
	
	
	public function catch_post_properties() {

		foreach($this->properties as $property){
			$post_val = $_POST[$property];
			if($property=='sms'){
				$this->set_sms($post_val=='yes');
			}else{
				
				$code0 = "\$this->set_$property('$post_val');";
				//$code = '$val = ' . $code0;
				eval ($code0);
			}
		}
	}
	
	
	function get_crypt_id(){
	
		if($this->get_id()==0){
			exit("--ID must be greater than 0--");
		}
	
		$x = $this->get_id()+1000;
		
		$y = $x*$x + 5*($x+25) - $x*10 + 456;
		
		$y += 123;
		$y *= 987;
		
		$arr = Array();
		
		array_push($arr, "U");
		array_push($arr, "V");
		array_push($arr, "W");
		array_push($arr, "X");
		array_push($arr, "Y");
		array_push($arr, "Z");
		array_push($arr, "5");
		array_push($arr, "6");
		array_push($arr, "7");
		array_push($arr, "8");
		array_push($arr, "9");
		
		$crypt_id = $y;
		
		$crypt_id = str_replace("9", "A", $crypt_id);
		$crypt_id = str_replace("8", "B", $crypt_id);
		$crypt_id = str_replace("7", "C", $crypt_id);
		$crypt_id = str_replace("6", "D", $crypt_id);
		$crypt_id = str_replace("5", "E", $crypt_id);
	
		for($i=strlen($y); $i<=12; $i++){
		//	echo($arr[$i]."<br>");
			//$crypt_id = $arr[rand(count($arr),0)] . $crypt_id;
		}
		
		
		return($crypt_id);
	
	
	}
	
	
//	public function has_logo(){
//		return(file_exists("css/custom/" . $this->get_crypt_id() . "/images/logo.png"));
//	}
	
	
	public function has_css(){
		return(file_exists("css/custom/" . $this->get_crypt_id() . "/"));
	}
	
	
	public function check_membership(){	
		$today=date("Y-m-d");
		
		$sql_query = "SELECT * FROM green_clients, green_memberships WHERE (green_memberships.id=clients.membership) AND (green_memberships.date_from<='$today' AND green_memberships.date_to>='$today') AND green_clients.id=".$this->get_id();
		$result = mysql_query($sql_query);
		
		if(mysql_num_rows($result)>0){
			return(true);
		}else{
			return(false);
		}
		
	}
	
	
	public function get_membership_type(){
	
		$sql_query = "SELECT * FROM green_clients, green_memberships WHERE (green_memberships.id=green_clients.membership) AND green_clients.id=".$this->get_id();
		$result = mysql_query($sql_query);
		
		if(mysql_num_rows($result)>0){
			return(false);
		}
		
	}
	
	
	public function get_business_type_name(){
	}
	

	public function load_settings(){

		$path= $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/".$this->get_crypt_id() . "/modules/system-settings/";
		$path2=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."modules/system-settings/";
		$path3=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/";
		$path4=$_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/modules/";

		if(!is_dir($path3)) mkdir($path3);
		if(!is_dir($path4)) mkdir($path4);
		if(!is_dir($path)) mkdir($path);

		$file = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/".$this->get_crypt_id() . "/settings.xml";
		$file2 = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."modules/system-settings/base.xml";

		if(!file_exists($file)) copy($file2, $file);

		$obj = simplexml_load_file($file);
		$obj2 = simplexml_load_file($file2);

		foreach($obj2 as $prop=>$value){

			foreach($obj2->$prop->attributes() as $a => $b){

				if($a=="input_type"){
					$this->settings_it[$prop]=$b;
				}
				$this->settings_attr[$prop][$a]=$b;

			}

			$value=$obj->$prop;
			$this->settings[$prop]=$value;
		}

	}

	function embed_settings_w($h=true){
	
		global $ai_master;
	
		echo "<table>";
	
		foreach($this->settings as $prop=>$value){
		
			echo "<tr>";
			
			if($h) echo "<th>".translate("system-settings","properties/txt_".$prop)."</th>";
			
			echo "<td>";

			switch($this->settings_it[$prop]){
				case "none" :
					echo "";
					break;
				case "textarea" :
					echo "<textarea name='$prop'>". $value . "</textarea>";
				break;
				
				case "checkbox" :
					echo "<input ". ($value=='yes' ? 'checked':'') ." name='$prop' type='checkbox' value='yes'>";
				break;
				
					
				case "select" :
					$s = $this->settings_attr[$prop]["input_options"];
					$v = $this->settings[$prop];
					$arr = explode("|",$s);
					
					echo "<select name='$prop'>";
					foreach($arr as $option){
						echo "<option ". ($v==$option ? ' selected':'') .">$option</option>";
					}
					echo "</select>";
				
				break;

					
				case "file" :
					echo "<input name='$prop' type='file'>";
				break;
				
				case "f" :
				
					if($prop == 'skills'){
					
						$all_skills = $this->load_skills();
						
						//echo $value;
						
						$current_skills = explode(",", $value);
						
						$ai_master->db_connect(2);
						
						foreach($all_skills as $s){
						
							$id = $s['id'];
							$name = $s['name'];
							
							if(!(in_array($id, $current_skills))){
								$s = "style='display:none;'";
							}else{
								$s = "";
							}
							
							$vote = $this->get_vote($id);
							
							$vote_menu = "<select $s name='lead-area-vote-$id'>";
							$vote_menu .= "<option value='0'>...</option>";
							$vote_menu .= "<option".($vote==1?' selected':'')." value='1'>1</option>";
							$vote_menu .= "<option".($vote==2?' selected':'')." value='2'>2</option>";
							$vote_menu .= "<option".($vote==3?' selected':'')." value='3'>3</option>";
							$vote_menu .= "</select>";
						
							echo "<div><label><input".(in_array($id, $current_skills)?" checked":"")." name='$prop"."[]' type='checkbox' value='$id'>".htmlentities($name)."</label>  $vote_menu</div>";
						}
						
						$ai_master->db_connect(1);
					
					}
				
					break;
					
					
				default:
					echo "<input name='$prop' type='text' value='$value'>";
				break;
			}
			
			echo "<td></tr>";
			
		}
		
		echo "</table>";
		
	}
	
	
	public function register_votes(){
	
		global $ai_master;
	
		foreach($_POST['skills'] as $k=>$area){
			
			$vote = $_POST["lead-area-vote-$area"];
			
			$ai_master->mysql_query_update(
				"lead_type_voting",
				"`deleted`=TRUE",
				"`area`='$area'"
			);
			
			$ai_master->mysql_query_insert(
				"lead_type_voting",
				"`area`, `vote`",
				"'$area','$vote'"
			);
			
		}
	
	}
	
	
	public function get_vote($area){
	
		global $ai_master;
		
		$result = $ai_master->mysql_query_select(
			"vote",
			"lead_type_voting",
			"`area` ='$area' AND NOT `deleted`",
			""
		);
		
		$row = mysql_fetch_array($result);
		
		return($row['vote']);
	
	}
	
	
	
	public function create_dir(){
		$folder = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/";
		$folder2 = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/modules/";
		
		if(!is_dir($folder)) mkdir($folder);
		if(!is_dir($folder2)) mkdir($folder2);
	}
	
	
	public function save_settings(){
	
		$file = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/settings.xml";
		$file2=$_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."modules/system-settings/base.xml";
		
		$obj = simplexml_load_file($file2);
		
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		
		$settings = $doc->createElement("settings");

		foreach($obj as $prop => $value){
			
			$post_val = $_POST[$prop];
			$p = $doc->createElement($prop);
			
			if(is_array($post_val)){
			
				$v = implode(",", $post_val);
				$n = $doc->createTextNode($v);
			
			}else{
				$n = $doc->createTextNode($post_val);
			}
			
			$p->appendChild($n);
			
			foreach($obj->$prop->attributes() as $n => $v){
				$p->setAttribute($n, $v);
			}

			$settings->appendChild($p); 
		}

		$doc->appendChild($settings); 		
		
		$doc->saveXML();
		$doc->save($file);
		
	}
	
	
	public function get_logo_image() {
		$target_path = "client/".$this->get_crypt_id()."/logo.jpg";
		if (file_exists($target_path)) {
			$tag = '<img src="'.$target_path.'">';
//			return $target_path;
			return $tag;
		}
		return false;
	}
	
	
	public function get_company_name() {
		$this->load_settings();
		return $this->settings['company_name'];
	}
	
	
	public function get_company_logo() {
//		return "Felo";
		if ($logo = $this->get_logo_image()) {
			return $logo;
		} else {
			return $this->get_name();
		}
		
	}
	
	
	public function check_dir($p_ref_path){
		
		$path= $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/".$this->get_crypt_id() . "/$p_ref_path";
		if(!is_dir($path)) mkdir($path);
		
	}
	
	
	public function calculate_files_size(){
	
		$path= $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/".$this->get_crypt_id() . "/$p_ref_path";		
		
		$total_size=$this->calculate_files_size_browse_dirs($path);		
		return($total_size);
		
		
		
	}
	
	public function calculate_files_size_browse_dirs($path){
	
		$dhandle = opendir($path);
		$files = array();
		
		$temp_size=0;

		if ($dhandle) {		
			while (false !== ($fname = readdir($dhandle))) {
			
				if (($fname != '.') && ($fname != '..')){					
					$file=$path.$fname;
					$path_2=$path.$fname."/";
					if(is_dir($path_2)){
						$temp_size+=$this->calculate_files_size_browse_dirs($path_2);
					}else{
						$temp_size+=fileSize($file);
					}
					
				}
			}
			closedir($dhandle);
		}
		
		return($temp_size);
	
	}
	
	
	public function get_main_user(){
	
		$sql_query = "SELECT id FROM `users` WHERE `green_client`=".$this->get_id()." ORDER BY id";
		
		$result = mysql_query($sql_query);
		
		$row=mysql_fetch_array($result);
		
		$user=new user($row["id"]);
		
		return($user);
	
	}
	
	
	public function get_support_hero(){
	
		$this->_support_hero = new support_hero;
		
		$sql_query = "SELECT support_hero FROM green_clients WHERE id = ".$this->get_id();
		$result=mysql_query($sql_query) or exit(mysql_error());
		
		if($row=mysql_fetch_array($result)){
			$this->_support_hero->set_id($row["support_hero"]);
		}
		
		return($this->_support_hero);
	
	}
	
	
	public function get_api_code(){	
		$a=$this->get_id();
		return(security::encrypt_data($a));	
	}
	
	
	public function load_skills(){
	
		$skills = Array();
	
		$bt = $this->get_business_type();
	
		$sql_query = "SELECT * FROM `business_skills` WHERE `business_type` = $bt ORDER BY `skill_name_spa`";
		$result = mysql_query($sql_query) or die(mysql_error());
		
		while ($line = mysql_fetch_array($result)){

			array_push($skills, 
				
				Array(
					"id" => $line['id'],
					"name" => $line['skill_name_spa']
				)
				
			);
		}
		
		return($skills);
	
	}
	
	
	public function get_balance(){
		if ($this->_balance== NULL){
			if($this->get_id()>0) {
				
				$result = $this->get_master()->mysql_query("SELECT", "SUM(total) AS balance", "green_client_balance_movements", "NOT `DELETED`", "");

				if($row = mysql_fetch_array($result)){
					$this->_balance = $row["balance"];
				}else{
					$this->_balance = 0;
				}
				
			}
		}
		return $this->_balance;

	}
	
	public function add_credit($credit){
	
		if($credit=='') $credit=0;
	
		$this->get_master()->db_connect(2);
	
		$this->get_master()->mysql_query_insert(
			"green_client_balance_movements",
			"`total`",
			"$credit",
			FALSE
		);
		
	}
	
	
	public function assign_lead($lead){
	
		$this->get_master()->db_connect(2);
		
		
		$this->get_master()->mysql_query_update_C0(
			"sales_leads",
			"`green_client` = " . $this->get_id().", `sold` = 1",
			"`id` = " . $lead->get_id(),
			FALSE
		);		
	}
	
	
	
	public function sell_lead($lead){
	
		$this->get_master()->db_connect(2);
		
		$this->get_master()->mysql_query_insert(
			"green_client_balance_movements",
			"`total`",
			"-50"
		);
		
		$this->get_master()->mysql_query_update_C0(
			"sales_leads",
			"`green_client` = " . $this->get_id().", `sold` = 1",
			"`id` = " . $lead->get_id(),
			FALSE
		);		
	}
	
	
	
	public function sell_prospect($green_client, $client, $price){
	
		global $ai_master;
		
		$prospect_resale = new prospect_resale;
		
		$prospect_resale->set('price', $price);
		$prospect_resale->set('buyer', $green_client);
		$prospect_resale->set_client($client);
		
		$ai_master->store($prospect_resale);
		
	}
	
	
	public function buy_prospect($prospect_resale){
	
		global $ai_master;
	
		$price = $prospect_resale->get('price');
		
		if($price>$this->get_balance()) return('G-OPRES-ERR-NO-CREDIT');
		
		$this->add_credit(-1*$price);
				
		$client = $prospect_resale->get_client()->duplicate();
		
		$client->set('first_name', $prospect_resale->get_client()->get('name'));

		$ai_master->store($client);
		
		$prospect_resale->update_property('status', 1);
		
		return('G-OPRES-OK');
		
	
	}
	
	
	public function catch_and_save_areas(){
		
		global $ai_master;
		
		$areas = $_POST['areas'];
		
		$ai_master->mysql_query_update("client_spec_areas", "`deleted` = true", "true");
		
		foreach($areas as $a){
			
			$res = $ai_master->mysql_query_select("*", "client_spec_areas", "`area` = $a");
			
			if(mysql_num_rows($res)>0){
				$ai_master->mysql_query_update("client_spec_areas", "`deleted` = false", "`area` = $a");
			}else{
				$ai_master->mysql_query_insert("client_spec_areas", "`area`", "'$a'");
			}
			
		}
		
	}
	
	
	public function has_area($a){
	
		global $ai_master;
	
		$res = $ai_master->mysql_query_select("*", "client_spec_areas", "`area` = $a AND NOT `deleted`");
		return(mysql_num_rows($res)>0);
	
	}
	
	
	public function get_specialties(){
		
		global $ai_master;
	
		$sql_result = $ai_master->mysql_query_select("*", "client_spec_areas", "NOT `deleted`");
		
		$spec_areas = Array();
		
		while($sql_row = mysql_fetch_array($sql_result)){
			array_push($spec_areas, new client_specialty($sql_row['area']));
		}
		
		return($spec_areas);
	
	}
	
	
	public function buy_lead($lead){
	
		global $ai_master;
	
		$ai_master->db_connect(1);
		$free_leads = $this->get('free_leads');
		
		if($free_leads>0){
		
			$free_leads--;
			$this->update_property('free_leads', $free_leads);
		}else{
		
		
			$this->get_master()->db_connect(2);
		
			$this->get_master()->mysql_query_insert(
				"green_client_balance_movements",
				"`total`",
				-1*$lead->get('price')
			);
		}
		
	}
	
	
}	
?>