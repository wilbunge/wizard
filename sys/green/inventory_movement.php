<?php
include_once('db_object.php');
	
class inventory_movement extends db_object{
	
	public $properties = array('inventory_item:inventory_item*', 'amount', 'date:date', 'ref_type', 'ref_id:number');

	var $table_name = "inventory_movements";
	var $class_name = "inventory_movement";


	public function get_origin(){

		$ref_id = $this->get('ref_id');
		$ref_type = $this->get('ref_type');

		switch($ref_type){

			case 'importation':
				$origin = "Importación #$ref_id";
				break;

			case 'order':
				$origin = "Venta #$ref_id";
				break;

			default:
				$origin = $ref_type;
				break;

		}

		return $origin;

	}

}	?>