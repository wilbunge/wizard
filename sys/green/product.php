<?php

include_once('db_object.php');
	
class product extends db_object{
	
	private $_name;
	private $_description;
	private $_price;
	private $_category;
	private $_dimensions;
	private $_materials;
	private $_code;
	private $_extra;
	private $_photo_qty;
	private $_inventory_item;
	private $_preparation_time;
	 
	private $_order_amount;
	private $_order_unit_price;
	private $_order_subtotal;
	private $_show_in_catalog;
	private $_prominent;
	
	private $_recommended1;
	private $_recommended2;
	private $_recommended3;
	
	public $properties = array('name','description','code','price','category:category*','dimensions','extra');
	
	public $extra = array("inventory_items" => array("composition_amount"));
	
	public $inventory_items = array();
	
	var $table_name = "products";
	var $class_name = "product";
		
	
	public function get_order_amount(){
		if ($this->_order_amount === NULL)$this->_order_amount=0;
		return $this->_order_amount;
	}
	
	public function set_order_amount($val){		
		$this->_order_amount = $val;		
	}
	
	
	public function get_preparation_time(){
		if ($this->_preparation_time === NULL)$this->_preparation_time=0;
		return $this->_preparation_time;
	}
	
	public function set_preparation_time($val){		
		$this->_preparation_time = $val;		
	}
	
	
	public function get_code(){
		if ($this->_code === NULL && $this->get_id()>0) $this->_code = $this->load_db_field('code');
		return $this->_code;
	}
	
	public function set_code($val){		
		$this->_code = $val;		
	}
	
	
	public function get_currency(){	
		if ($this->_currency === NULL && $this->get_id()>0) $this->_currency = $this->load_db_field('currency');
		
		return $this->_currency;
	}
	
	public function set_currency($val){
		$this->_currency = $val;
	}
	
	
	public function get_currency_symbol(){
		switch($this->get_currency()){
			
			case "2" :
				return("USD");
				break;
			
			default :
				return("$");
				break;
			
		}
	}
	
	
	public function get_order_unit_price(){
		if ($this->_order_unit_price === NULL)$this->_order_unit_price=0;
		return $this->_order_unit_price;
	}
	
	public function set_order_unit_price($val){
		$this->_order_unit_price = $val;
	}
	
	
	public function get_order_subtotal(){
		if ($this->_order_subtotal === NULL)$this->_order_subtotal=0;
		return $this->_order_subtotal;
	}
	
	public function set_order_subtotal($val){
		$this->_order_subtotal = $val;
	}
	
	
	public function get_image(){}
	public function get_image_2(){}
	public function get_image_3(){}
	public function get_image_4(){}
	
	public function get_inventory_items(){
		return($this->inventory_items);
	}

	public function get_inventory_item(){

		$inventory_item = master::get_one('inventory_item', '`product` = '.$this->id);

		return $inventory_item;

	}


	public function has_inventory_item(){

		$inventory_item = master::get_one('inventory_item', '`product` = '.$this->id);

		return $inventory_item;

	}

	public function generate_inventory_item(){

		if($this->has_inventory_item()) return false;

		$inventory_item = new inventory_item;

		$inventory_item->set('product', $this);
		$inventory_item->set('name', $this->get('name'));

		$inventory_item->save();

		return $inventory_item;

	}


	public function inventory_movement($amount, $ref = 'manual'){

		require_once('inventory_movement.php');

		$inventory_movement = new inventory_movement;

		if(is_object($ref)){
			$ref_type = $ref->class_name;
			$ref_id = $ref->id;
		}else{
			$ref_type = $ref;
			$ref_id = 0;
		}

		$inventory_movement->set('inventory_item', $this->get_inventory_item());
		$inventory_movement->set('amount', $amount);

		$inventory_movement->set('ref_type', $ref_type);
		$inventory_movement->set('ref_id', $ref_id);

		$inventory_movement->save();

		return $inventory_movement;

	}


	public function html_input_property($p_property, $p_append_to_name='') {

		// IMPORTANT !!
		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		// IMPORTANT !!
//		echo $val->get_default()."--";
		$html = '';
		switch ($p_property) {

			case 'category':
//				$result = $this->get_master()->mysql_query('SELECT', 'name,id', 'categorizer', 'NOT deleted AND family = "products"', '');
								
				$html = '<select name="category">';
				$html .= "	<option value='0'>...</option>";
				$html .=	get_category_childs(0,"products",$this);
				$html .= "</select>";			
			break;


			case 'recommended1':
				$result = $this->get_master()->mysql_query('SELECT', 'name,id', 'products', 'NOT deleted AND ID <> '.$this->get_id(), '');
				$html = '<select name="recommended1">';
				$html .= "	<option value='0'>...</option>";
				// echo $this->get_recommended1()->get_id().".......";
				while ($row = mysql_fetch_array($result)) {
					if ($row['id'] == $this->get_recommended1()->get_id()) {
						$html .=	"<option value='".$row['id']."' selected>".$row['name']."</option>";
					} else {
						$html .=	"<option value='".$row['id']."'>".$row['name']."</option>";
					}

				}
				$html .= "</select>";
			break;


			case 'recommended2':
				$result = $this->get_master()->mysql_query('SELECT', 'name,id', 'products', 'NOT deleted AND ID <> '.$this->get_id(), '');
				$html = '<select name="recommended2">';
				$html .= "	<option value='0'>...</option>";
				// echo $this->get_recommended2()->get_id().".......";
				while ($row = mysql_fetch_array($result)) {
					if ($row['id'] == $this->get_recommended2()->get_id()) {
						$html .=	"<option value='".$row['id']."' selected>".$row['name']."</option>";
					} else {
						$html .=	"<option value='".$row['id']."'>".$row['name']."</option>";
					}

				}
				$html .= "</select>";
			break;


			case 'recommended3':
				$result = $this->get_master()->mysql_query('SELECT', 'name,id', 'products', 'NOT deleted AND ID <> '.$this->get_id(), '');
				$html = '<select name="recommended3">';
				$html .= "	<option value='0'>...</option>";
				// echo $this->get_recommended3()->get_id().".......";
				while ($row = mysql_fetch_array($result)) {
					if ($row['id'] == $this->get_recommended3()->get_id()) {
						$html .=	"<option value='".$row['id']."' selected>".$row['name']."</option>";
					} else {
						$html .=	"<option value='".$row['id']."'>".$row['name']."</option>";
					}
				}
				$html .= "</select>";
			break;

		

			case 'inventory_items':
			
				$html .= '<a class="button" href="#set_composition">configurar composici&oacute;n</a>';
				//$html .= '<input id="G_ME_clients_input_contacts" value="'.$val.'" type="text" name="contacts">';
				
				if($this->get_id()>0){
				//	$this->load_team();
				}
				
				$c="";
				
				/*foreach($this->get_team() as $contact){
					$c.="<input type='hidden' name='contacts[]' value='".$contact->get_id()."'>";
					$c.="<li>".$contact->get_username()."</li>";
				}*/
				
				$html .= "<div id='G_ME_products_div_composition'>$c</div>";
				
				break;
			
			case '-inventory_items':
			
				$result = $this->get_master()->mysql_query('SELECT', 'id', 'inventory_items', 'type<>2 AND NOT deleted', '');
				
				//$html = '<label><input name="inventory_type" type="checkbox" checked="checked" value="simple">inventory simple</label>';
				
				$html .= '<table id="G_DE_products_inventory" style="border:0;">';
				
				$this->load_inventory_items();
				
				while($row = mysql_fetch_array($result)){
				
					$n++;
					$inventory_item = new inventory_item;
					$inventory_item->set_id($row['id']);
					
					$checked = "";
					$amount = '';
					
					foreach($this->get_inventory_items() as $si){
						if($inventory_item->equals($si)){
							$checked = " checked";
							$amount = $si->get_composition_amount();
							break;
						}
					}
					
					$html .= "<tr>";
					$html .= "	<td style='border:0;'><label><input".$checked." value='" . $inventory_item->get_id() . "' name='inventory_items[]' type='checkbox'>" . $inventory_item->get_name() . "</td>";
					
					$html .= "	<td style='border:0;'>x <input size='2' value='".$amount."' name='composition_amount_".$inventory_item->get_id()."'></td>";
					
					//$html .= "	<td><select name='composition_amount_".$inventory_item->get_id()."'>";
					//for($i=1; $i<100; $i++){
					//$html .= "<option>" . $i . "</option>";
					//}
					//$html .= "</select></td>";
					
					$html .= "</tr>";

				}
			
				$html .= "</table>";
			
			break;
		

			case 'description' :
//				echo $val."---";
				$html .= '<textarea name="'.$p_property.'">'.$val.'</textarea>';
			break;
			

			case 'show_in_catalog' :
				$html .= '<input '.(($this->get_show_in_catalog()) ? " checked" : "").' type="checkbox" name="show_in_catalog" value="yes" />';
			break;

			case 'prominent' :
				$html .= '<input '.(($this->get_prominent()) ? " checked" : "").' type="checkbox" name="prominent" value="yes" />';
			break;

			case 'name' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
			break;


			case 'materials' :
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
			break;
			

			case 'dimensions' :
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
			break;
			
							
			case 'image' :
				$html .= '<input type="file" name="'.$p_property."_".$p_append_to_name.'">';
				if($this->get_id()>0) {
					$html .= $this->embed_photo($p_append_to_name, "c",true);
				}				
			break;		
					
			case 'price':
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
				
			case 'preparation_time':
					$html .= '<input value="'.$val.'" size="5" type="text" name="'.$p_property.'"> (en minutos)';
					break;

			case 'currency':
				$html .= '<label><input name="currency"' . ($this->get_currency()=='1' ? ' checked' : '') . ' type="radio" value="1">$U</label><label><input' . ($this->get_currency()=='2' ? ' checked' : '') . ' name="currency" type="radio" value="2">USD</label>';
				break;
				
			default :			
				$html=$this->html_input_property_append($p_property);			
				if($html=='') $html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
			break;
			
		}
		return $html;
	}
	
	
	
	public function catch_post_properties() {

		foreach($this->properties as $property){
		
			if($property == 'inventory_items'){
				if(is_array($_POST['inventory_items'])){
					foreach($_POST['inventory_items'] as $id){		
						$inventory_item = new inventory_item;
						$inventory_item->set_id($id);		
					
						$inventory_item->set_composition_amount($_POST["composition_amount_".$id]);
					
						array_push($this->inventory_items, $inventory_item);		
					}
				}
			
			} elseif($property=="prominent") {
				
				if (gettype($_POST["prominent"])=='NULL') {
					$this->set_prominent(0);
				} elseif ($_POST["prominent"]=="yes") {
					$this->set_prominent(1);
				} else {
					$this->set_prominent(0);
				}
			
			} elseif($property=="show_in_catalog") {
				
				if (gettype($_POST["show_in_catalog"])=='NULL') {
					$this->set_show_in_catalog(0);
				} elseif ($_POST["show_in_catalog"]=="yes") {
					$this->set_show_in_catalog(1);
				} else {
					$this->set_show_in_catalog(0);
				}
			
			}else{
			
				$post_val = $_POST[$property];
				echo ($property.' - '. $post_val.'<br>');
				$this->set($property, $post_val);
			
			}
		}
		
		$this->catch_post_properties_append();
	}

		
		
	public function update_inventory(){
	
		
	
		$result = $this->get_master()->mysql_query('SELECT', 'id', 'inventory_items', 'NOT deleted', '');
	}


	public function load_inventory_items(){
		
		$result = $this->get_master()->mysql_query('SELECT', 'inventory_item,composition_amount', 'product_inventory_items', 'product = '.$this->get_id().' AND NOT deleted', '');
		$arr=Array();
		while($row = mysql_fetch_array($result)){
			$si = new inventory_item;
			
			$si->set_id($row['inventory_item']);
			$si->set_composition_amount($row['composition_amount']);
			
			array_push($arr,$si);
		}
		
		$this->inventory_items=$arr;
		
		return($arr);
		
	}
	
	
	public function embed(){

		$html = "<table class='G_cc_grid G_CS_data_grid' cellspacing='0' border='1'>";
		
		foreach($this->properties as $property){	
		
			$code0 = "\$this->get_$property();";
			$code = '$val = ' . $code0;
		
			eval ($code);
		
			$html .= "<tr>";
			
			if($property != 'currency') $html .= "	<th>" . $property . "</th>";
			
			if($property == 'currency'){
			}elseif($property == 'price'){
//				echo is_numeric($this->get_price())."--<br>";
				if ($this->get_currency() == 2) {
					$curr = new currency();
					
					$pesos = $curr->convert_currency("USD","UYU",$this->get_price());
					
					$html .= "	<td>USD ". number_formatter($this->get_price()) . " &#8776; $ ".number_formatter($pesos)."</td>";
				} else {
					$html .= "	<td>$". number_formatter($this->get_price());	
				}
				
				
			}elseif($property == 'inventory_items'){
			
				$html .= "	<td><ol style='list-style-type:none; margin:0; padding:0;'>";
			
				$this->load_inventory_items();
			
				foreach($this->get_inventory_items() as $si){					
					$html .= "<li>".$si->get_composition_amount()." ".$si->get_unit()." x ".$si->get_name()."</li>";
				}
				
				$html .= "	</ol></td>";
			
				
			}elseif(gettype($val) == 'object'){
				$html .= "	<td>" . $val->get_default() . "</td>";
			}else{
				$html .= "	<td>" . $val . "</td>";
			}
			
			$html .= "</tr>";
		}
		
		$html .= "<tr><th>Cost</th><td>$ ".$this->calculate_inventory_cost()."</td></tr>";
		
		$green_module = new green_module("products");
		$green_module->load_cfg();
		
		for($i = 1; $i <= $green_module->cfg['photo_count']; $i++) {
			$html .= "<tr><th>Image ".$i."</th><td>".$this->embed_photo($i,"c")."</td></tr>";
		}
		
		$html .= "</table>";
		
		return($html);
	}
	
	
	public function calculate_inventory_cost(){
		
		$cost=0;
			
		foreach($this->get_inventory_items() as $si){					
			$cost += $si->get_composition_amount()*$si->get_cost();			
		}
		
		return($cost);
		
	}
	
	
	function embed_w(){
	
		foreach($this->eon_properties as $p){
			array_push($this->properties, $p);
		}
		
		$green_module = new green_module("products");
		$green_module->load_cfg();
		
		$html = "<table class='G_cs_input' cellspacing='0'>";
		
		$arr = explode(",",$green_module->cfg["exclude"]);
		

		foreach($this->properties as $p){
		
			if(!in_array($p	, $arr)){
				$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>".$this->translate_property($p)."</th><td>" . $this->html_input_property($p)."</td></tr>";
			}
			
		}
		
		foreach($this->ndb_properties as $p){
			$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>".$this->translate_property($p)."</th><td>" . $this->html_input_property($p)."</td></tr>";
		}
		
		if($green_module->cfg["photo_count"]!=''){
			$photo_count=$green_module->cfg["photo_count"];
		}else{
			$photo_count=0;
		}
		
		for($i=1; $i<=$photo_count; $i++){
			$html .= "<tr id='G_he_".$this->class_name."_".$p."'><th>".$this->translate_property("image")." ".$i."</th><td>" . $this->html_input_property("image",$i)."</td></tr>";
		}
		
		$html .= "<tr class='G_cs_input_button'><th></th><td><input type='submit' value='Enviar'></td></tr>";
		
		$html .= "</table>";
		
		return($html);

	}
	
	public function produce($p_qty=1, $p_order_id=0){
	
		$inventory_item_id=$this->get_inventory_item()->get_id();
		$qty=$p_qty;

		$this->load_inventory_items();
		
		foreach($this->get_inventory_items() as $si){
			
			if($si->get_id() != $inventory_item_id){
				//echo ($si->get_id() ." -- ". $si->get_type() ." -- ". $inventory_item_id)."<br>";
				
				if($si->get_type()==2){
				
					$ca = $si->get_composition_amount();
					$qty2 = $ca*$qty;
					
					//echo $si->get_product()->get_name()."(".$si->get_id().")";
					
					// PROPAGATION !!!!
					//$si->get_product()->produce($qty2, $p_order_id);
				
				}
				
				$item_qty = $si->get_composition_amount();
				$this->get_master()->mysql_query_insert('inventory_movements', 'inventory_item, qty, date, `order`', $si->get_id() . ', ' . -$qty*$item_qty . ', ' . time() . ",$p_order_id", false);
				
			}
		}
		
		if($inventory_item_id>0){
			$this->get_master()->mysql_query_insert('inventory_movements', 'inventory_item, qty, date, `order`', $inventory_item_id . ', ' . $qty . ', ' . time() . ",$p_order_id");
		}
	
	}
	
	
	public function count_photos() {
		if ($this->_photo_qty != null) {
			return $this->_photo_qty;
		}
		$green_module = new green_module("products");
		$green_module->load_cfg();
		
		$html = "<table class='G_cs_input' cellspacing='0'>";
		
		$arr = explode(",",$green_module->cfg["exclude"]);
		$photo_count = 0;
		for($i=0; $i<=$green_module->cfg["photo_count"]; $i++) {
			if(file_exists($_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."client/".$this->get_master()->get_green_client()->get_crypt_id()."/modules/products/images/g_p_".$this->get_id()."_".$i."_c.jpg")) {
				$photo_count++;
			}
		}
		return $this->_photo_qty=$photo_count;
	}


	public function get_name_code(){

		$name = $this->get('name');
		$code = $this->get('code');

		if($code != '') $name = "$code - $name";

		return $name;


	}


	public function validate_data(){

			wizard::_include('validation');

			$validation = new validation;

			// $validation->error('Email already exists');

			return $validation;


	}

	public function validate(){

		wizard::_include('validation');

		$validation = new validation;

		// if($this->get('password')=='') $validation->error('password', 'La contaseña no puede estar vacía');

		return $validation;


	}
	
	
	
	}	?>