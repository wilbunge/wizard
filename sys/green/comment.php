<?php

include_once('db_object.php');
	
class comment extends db_object{
	
	public $properties = array('text', 'user:user*', 'task:task*','parent:comment*', 'read:bool');

	var $table_name = "task_comments";
	var $class_name = "comment";


	public function __toString(){
		return($this->get('title'));
	}
		
}

?>