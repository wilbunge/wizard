<?	abstract class data_manager{

		private static $_conn;
		private static $_db;
		
		public static $mysqli_insert_id = 2;
		public static $db = 2;
		public static $debug_mode = false;
		public static $debug = false;
		public static $ignore_wizard_account = false;

		const PROJECT_WIZARD = 5;

		// Project ID??
		public static function get_project_id(){

			return 5;
			
			$ru = $_SERVER['REQUEST_URI'];
			$hh = $_SERVER['HTTP_HOST'];
			
			$ru_arr = explode('/', $ru);

			$cron = ($_SERVER['SHELL']=='/bin/sh');

			if($cron){
				$sn = $_SERVER['SCRIPT_NAME'];
				$sn_arr = explode('/', $sn);
				$project_path = $sn_arr[6];
			}else{
				$project_path = $ru_arr[3];
			}
			
			$sql_query = "SELECT id FROM `__projects` WHERE (`path` = '$project_path')";
			// $sql_query = "SELECT id FROM `__projects` WHERE (`path` = '$hh')";
			
			//echo $sql_query.'<br>';
			
			$sql_result = mysql_query($sql_query) or die(mysql_error()." @ $sql_query");

			if(mysql_num_rows($sql_result)<1) return (new Exception('NO PROJECT ID'));
			
			$sql_row = mysql_fetch_array($sql_result);
			
			$project_id = $sql_row['id'];
			
			return($project_id);
			
		}


		public static function get_wizard_account_id(){

			return 1;

			require_once('wizard_account.php');
			
			if(!$wizard_account = wizard::get_account()) throw new Exception("No wizard account in session", 1);
			if($wizard_account->id==0) throw new Exception("Unrecognizad wizard account in session", 1);

			return($wizard_account->id);
			
		}
		

		// CREATE
		public function create($table_name, $fields, $values){
		
			$link = self::connect();

			// $fields .= ', __project';
			// $values .= ', '.self::get_project_id();

			if(self::get_project_id()==self::PROJECT_WIZARD && !self::$ignore_wizard_account){
				$fields .= ', __wacc';
				$values .= ', '.self::get_wizard_account_id();
			}

			$sql_query = "INSERT INTO `$table_name`($fields) VALUES($values)";
			if(self::$debug_mode) echo $sql_query."<br>";
			$result = self::x($sql_query);


			return self::$mysqli_insert_id;
			
		}
		
		
		// READ
		public static function read($table_name, $fields, $where_clause='TRUE', $extra_clauses=''){

			$link = self::connect();

			$project_clause = "TRUE";

			if(self::get_project_id()==self::PROJECT_WIZARD && $table_name!='wizard_accounts' && !self::$ignore_wizard_account){
				$project_clause .= ' AND __wacc = '.self::get_wizard_account_id();	
			}
			

			$sql_query = "SELECT $fields FROM $table_name WHERE ($where_clause) $extra_clauses";

			$sql_query_2 = "SELECT id FROM $table_name WHERE $project_clause";
			$sql_query_3 = "SELECT $fields FROM $table_name WHERE ($where_clause) AND id IN ($sql_query_2) $extra_clauses";

			if(!strpos(strtoupper($sql_query_3), 'ORDER BY')) $sql_query_3 = "$sql_query_3 ORDER BY id DESC";

			// die($sql_query_3);

			if($table_name == 'wizard_modules'){
				$sql_query_3 = $sql_query;
			}



			if(self::$debug_mode) echo $sql_query."<br>";
			$sql_result = mysqli_query($link, $sql_query_3) or die(mysqli_error($link)." @ $sql_query_3");
			return($sql_result);
			
		}
		
		// UPDATE
		
		public function update($table_name, $update_clause, $where_clause){
		
			$link = self::connect();
			
			$sql_query = "UPDATE $table_name SET $update_clause WHERE $where_clause";
			if(self::$debug_mode) echo $sql_query."<br>";
			$sql_result = mysqli_query($link, $sql_query) or die(mysqli_error($link)." @ $sql_query_3");
		
		}


		public function fetch($field, $echo_query=false) {
		
			 if($this instanceof db_object){ // I'm an object
			 
			 	$sql_query = "SELECT `".$field."` FROM `". $this->table_name."` WHERE id = ".$this->get_id();
			 
			 }
			 
		
			$sql_query = "SELECT `".$field."` FROM `". $this->table_name."` WHERE id = ".$this->get_id();
			if($echo_query) echo $sql_query;
			$result = mysql_query($sql_query) or die("ERROR:".mysql_error());
			$sql_row = mysql_fetch_array($result);
			return $felo[$field];
		}


		public function check_table($table_name){
			
			if(!mysql_query("select 1 from `$table_name`")){
				

				master::generate_db();
			}

		}


		public function x($sql_query){

			if(self::$debug) echo $sql_query."<br>";

			$link = self::connect();

			$result = mysqli_query($link, $sql_query) or die(mysqli_error($link)."@$sql_query");

			self::$mysqli_insert_id = mysqli_insert_id($link);

			return $result;

		}
		
		
		public static function connect(){

			return mysqli_connect(db::$host, db::$username, db::$password, db::$db);
			///

			if(!self::$_conn){
				
				self::$_conn = mysqli_connect(db::$host, db::$username, db::$password, db::$db);

            }else{  
				return false;  
				
			}
			
			return(self::$_conn);

	}
		

}	?>