

<?	class invoice_print_configuration extends db_object{
	
		public $properties = array('client', 'client_rut', 'client_address', 'date','total','comments', 'code', 'item_name', 'item_qty', 'item_price', 'item_subtotal',
				'end_consumer', 'document_type', 'iva', 'subtotal', 'number',
				'resolution');
		
		var $table_name = "invoice_print_configurations";
		var $class_name = "invoice_print_configuration";
		
		
		public function get_css($p, $n=0){
			
			$v = $this->get($p);
			
			$a = explode('.', $v);
			
			$a[0];
			$a[1]+=$n*20;
			
			$l = $a[0].'px';
			$t = $a[1].'px';
			
			return "position:absolute; left: $l; top: $t";
			
		}
		
		
		public function translate_property($p){
			
			
			switch(strtolower($p)){
				
				case 'item_price' : return 'P.UNITARIO';
					break;
				
				case 'client_rut' : return 'RUT';
					break;
				
				case 'number' : return 'NUMERO';
					break;
				
				case 'item_qty' : return 'UNIDADES';
					break;
				
				case 'item_subtotal' : return 'SUBTOTAL ITEM';
					break;
				
				case 'document_type' : return 'TIPO DOCUMENTO';
					break;
				
				case 'client_address' : return 'DIRECCION';
					break;
				
				case 'item_name' : return 'DETALLE';
					break;
				
				case 'comments' : return 'OBSERVACIONES';
					break;
				
				case 'end_consumer' : return 'C.FINAL';
					break;
				
				case 'date' : return 'FECHA';
					break;
				
				case 'client' : return 'CLIENTE';
					break;
				
				default : return strtoupper($p);
					break;
				
			}
			
		}
	
	}	?>
