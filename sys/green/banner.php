<?php

include_once('db_object.php');
	
class banner extends db_object{
	
	private $_name;
	
	public $properties = array('name', 'link', 'position', 'language', 'w', 'h', 'landing:landing*');
	
	public $ni_properties = array('type');
	public $ndb_properties = array('file');
	
	var $table_name = "banners";
	var $class_name = "banner";
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	
	public function html_input_property($p){
	
		switch($p){
		
			case 'landings' :
				
				$landings = master::get('landing', 'NOT `deleted`');
				$my_landings = $this->get('landings');

				foreach($landings as $l){
				
					$match=false;
				
					foreach($my_landings as $l2){

						if($l->equals($l2)){
							$match=true;
							break;
						}
					}
				
					$html .= "<li><label><input name='landings[]' type='checkbox' ".($match?' checked':'')." value='".$l->get('id')."'>".$l->get('title')."</label></li>";
				}
			
				$html = "<ol>$html</ol>";
				break;
				
			
		
			case 'w' :
				$html = "<input size='2' type='text' name='w' value='".$this->get('w')."'>";
				break;
			case 'h' :
				$html = "<input size='2' type='text' name='h' value='".$this->get('h')."'>";
				break;
			
			case 'position' :
			
				$v = $this->get('position');
			
				$html = "
					<label><input".($v=='top'?' checked':'')." type='radio' name='position' value='top'>top</label>
					<label><input".($v=='bottom'?' checked':'')." type='radio' name='position' value='bottom'>bottom</label>
					<label><input".($v=='popup'?' checked':'')." type='radio' name='position' value='popup'>popup</label>
					";
				break;
				
			case 'type' :
			
				$html = "<label><input type='radio' name='type' value='gif'>GIF</label>";
				break;
				
			case 'file' :
			
				$html = "<input type='file' name='file' />";
				break;
				
			case 'landing' :
			
				$themes = master::get('landing', 'NOT `deleted`');
				
				foreach($themes as $t){
					if($t->equals($this->get('landing'))){
						$selected = " selected";
					}else{
						$selected = "";
					}
					$html .= "<option $selected value='".$t->get('id')."'>".$t->get('title')."</option>";
				}
			
				$html = "<select name='landing'>$html</select>";
				break;
				
			case 'language' :
			
				$languages = master::get('language', 'NOT `deleted`');
				
				foreach($languages as $l){
					$html .= "<option value='".$l->get('id')."'>".$l->get('code')."</option>";
				}
			
				$html = "<select name='theme'>$html</select>";
				break;
				
			default :
				$html = "<input name='$p' value='".$this->get($p)."' />";
				break;
			
		}
		
		return($html);
	
	}
	
	
	public function embed($p=''){
	
		$src = $p."banners/".$this->get('id').".".$this->get('type');
		
		$link = $this->get('link');		

		$html = "<img border='0' src='$src'>";
		
		if($this->get('type')=='swf'){
		
		
			$html = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" 

codebase="http://download.macromedia.com/pub/shockwave/
cabs/flash/swflash.cab#version=6,0,40,0" 
 
width="'.$this->get('w').'" height="'.$this->get('h').'" 
 id="mymoviename"> 

<param name="movie" value="'.$src.'" /> 
<param name="quality" value="high" />
<param name="align" value="middle" />
<param name="wmode" value="transparent" /> 


<embed src="'.$src.'" quality="high"

width="'.$this->get('w').'" height="'.$this->get('h').'" 

name="mymoviename" align="center" type="application/x-shockwave-flash" 

pluginspage="http://www.macromedia.com/go/getflashplayer"> 


</embed> 

</object>';
		
		
		}else{
		
			if($link!=''){
				if(!strpos($link, 'javascript:')){
					$html = "<a href=\"$p"."banners/click.php?banner=".$this->id."&landing=".$this->get('landing')->id."\">$html</a>";
				}else{
					$html = "<a href='#' onclick=\"$link\">$html</a>";
				}
			}
			
		}
		
		$html = "<div class='banner'>$html</div>";
		
		$banner_impression = new banner_impression;
		
		$banner_impression->set('banner', $this);
		$banner_impression->set('landing', $this->get('landing'));
		
		master::store_object($banner_impression);
		
		return($html);
	
	}
	
	public function update_status($s){
		master::update_property($this, 'disabled', $s);
	}
	
	
	
	public function click($landing){
	
		$banner_click = new banner_click;
		
		$banner_click->set('landing', $landing);
		$banner_click->set('banner', $this);
		
		master::store_object($banner_click);
	
	}
	
		
}

class banner_click extends db_object{
	
	private $_name;
	
	public $properties = array('banner', 'landing:landing');

	var $table_name = "banner_clicks";
	var $class_name = "banner_click";
}

class banner_impression extends db_object{
	
	private $_name;
	
	public $properties = array('banner', 'landing:landing');

	var $table_name = "banner_impressions";
	var $class_name = "banner_impression";
}

?>