<?php

include_once('db_object.php');
	
class payment extends db_object{
	
	
	public $properties = array('date','total','currency', 'currency_value','description','client:client*','number','invoice:invoice*');
	
	var $table_name = "payments";
	var $class_name = "payment";
	
	
	public function format_total($total){

		$total = number_format($total, 2, ',', '.');

		return $total;


	}	


	public function get_number_id(){

		$number_id = $this->get('number');

		if($number_id=='') $number_id = '#'.$this->id;

		return $number_id;

	}


	public function get_total_formatted(){

		return $this->get_symbol().' '.self::format_total($this->get('total'));

	}


	public function save(){

		parent::save();

		$this->get('invoice')->check_payments();


	}

	public function get_symbol(){
		if($this->get('currency') == 2){
			$symbol = "\$UY";
		}else{
			$symbol = "USD";
		}
		return $symbol;
	}
	
	
	
}
?>