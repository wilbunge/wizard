<?php
include_once('db_object.php');
	
class category extends db_object{
	
	private $_name;
	private $_father;
	private $_family;
	
	public $properties = array('name','father');
	public $ni_properties = array('family');
	
	var $table_name = "categories";
	var $class_name = "category";
	
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	
	public function get_family(){
		if ($this->_family === NULL){
			if($this->get_id()>0) {
				$this->_family = $this->load_db_field('family');
			}
		}
		return $this->_family;
	}


	public function set_family($val){
		$this->_family = $val;
	}
	
	
	public function get_father(){
		if ($this->_father === NULL){
			$this->_father = new category;
			if($this->get_id()>0) {
				$this->_father->set_id($this->load_db_field('father'));
//				echo $this->_father;
			}
		}
		
		return $this->_father;
	}

	public function set_father($val){
		$this->_father = $val;
	}
	
	
	function category_builder($p_cat) {
//		echo "-".$p_cat."-";
		global $master, $all_categories;
		$cats_array = array();
		$num_subcategories_query_result = $master->mysql_query("SELECT","id","categorizer","NOT DELETED AND father = '$p_cat'","");
		// echo mysql_error();
		while ($row = mysql_fetch_array($num_subcategories_query_result)) {
			array_push($all_categories, $row[0]);
//			echo "<<".count($all_categories).">><br>";
			$this->category_builder($row[0]);
		}
//		echo "<br>..".count($all_categories)."..<br>";
		return $all_categories;
	}

	
	function category_builder_2($p_cat, $p_arr="") {
//		echo "-".$p_cat."-";
		if($p_arr == "") $p_arr = array();
		
		global $ai_master;
		$cats_array = array();
		$num_subcategories_query_result = $ai_master->mysql_query("SELECT","id","categorizer","NOT DELETED AND father = '$p_cat'","");
//		 echo mysql_error();
		array_push($p_arr, $p_cat);
		while ($row = mysql_fetch_array($num_subcategories_query_result)) {
			
			array_push($p_arr, $row[0]);
//			echo "<<".count($all_categories).">><br>";
			$this->category_builder_2($row[0], $p_arr);
		}
//		echo "<br>..".count($all_categories)."..<br>";
		return $p_arr;
	}
	
	
	
	
	
	function html_input_property($p_property) {

		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		
		$html = '';
		
		switch ($p_property) {		

			case 'father':
				
				$result = $this->get_master()->mysql_query('SELECT', 'name,id', 'categorizer', 'NOT deleted AND family = "products" AND id <> '.$this->get_id().'', '');
				$father_id= $val->get_id();
				
				
									 
				$html .= '<select name="father">';
				$html .= "	<option value='0'>...</option>";
				while($row = mysql_fetch_array($result)) {
					if($row['id'] == "$father_id") {
						$html .= "<option selected value='".$row['id']."'>".$row['name']."</option>";	
					} else{
						$html .= "<option value='".$row['id']."'>".$row['name']."</option>";
					}
				}
				
				$html .= "</select>";
				
			break;

			case 'family':
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
			break;
			
			
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
			break;

		}
		
		return $html;
		
	}
	
	
	public static function embed_menu(){
		
		$childs = master::get('category');
		
		$html = "<select name='category'></select>";
		
		return($html);
		
	}
	
		
}

?>