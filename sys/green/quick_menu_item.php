<?php
include_once('db_object.php');
	
class quick_menu_item extends db_object{
	
	public $properties = array('wizard_user:user*', 'wizard_module:wizard_module*', 'active:boolean');
	
	var $table_name = "quick_menu_item";
	var $class_name = "quick_menu_item";

	

	public function update_status($mode){

		$this->update_property('active', $mode?1:0);

	}
	
	
}
	
?>