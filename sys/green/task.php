<?php

include_once('db_object.php');
	
class task extends db_object{
	
	public $properties = array('title', 'description:text', 'worker:user*', 'author:user*', 'status', 'priority', 'order:int', 'read:bool', 'project:project*');
	
	var $table_name = "tasks";
	var $class_name = "task";


	public function __toString(){
		return($this->get('title'));
	}
	
	public function update_status($s){

		if($this->get('status')!='2' && $s=='2'){
			master::update_property($this, 'date_done', date('Y-m-d h:i:s'));
		}
	
		master::update_property($this, 'status', $s);
	}
	
	public function get_status_name(){
	
		$status = $this->get('status');
		
		$status_names = Array(
			0 => 'New',
			1 => 'In Progress',
			2 => 'Done'
		);
		
		$status_name = $status_names[$status];
		
		if($status_name=='') $status_name = $status_names[0];
		
		return($status_name);
		
	}
	
	public function revise($r){
		
		master::update_property($this, 'revised', $r);
		
	}
	
	
	public function handle_files(){
		
		$theme_dir = 'task-files';
		$theme_dir_2 = 'task-files/'.$this->get('id').'/';

		if(!file_exists($theme_dir)) mkdir($theme_dir);
		if(!file_exists($theme_dir_2)) mkdir($theme_dir_2);
		
		$n=0;
		
		if(count($_FILES['files'])) {
			foreach ($_FILES['files'] as $file) {
				move_uploaded_file($_FILES['files']["tmp_name"][$n], $theme_dir_2 ."/". $_FILES['files']["name"][$n]);
				$n++;
			}
		}

	
	
	}
	
	
	public function get_files(){
		
		$directory = 'task-files/'.$this->get('id').'/';
		
		if(!file_exists($directory)) return(Array());
		
		 $results = array();

	    // create a handler for the directory
    	$handler = opendir($directory);

	    // open directory and walk through the filenames
    	while ($file = readdir($handler)) {

	      // if file isn't this directory or its parent, add it to the results
    	  if ($file != "." && $file != "..") {
        	$results[] = $file;
	      }

    	}

	    // tidy up: close the handler
    	closedir($handler);

	    // done!
    	return $results;

	
	
	}
	
	
	public function comment(){
	
		require('comment.php');
		require('user.php');
		
		$args = func_get_args();
		
		$comment = new comment;
		
		$comment->set('text', $args[0]['text']);
		$comment->set('user', new user($args[0]['user']));
		$comment->set('parent', new user($args[0]['parent']));
		$comment->set('task', $this);
		
		master::store_object($comment);
		
	}
	
	
	public function count_comments(){
	
		$comments = master::get('comment', 'NOT `deleted` AND `task` = '.$this->id." ORDER BY `id` DESC");
	
		return(count($comments));
	
	}
	
		
		
}

?>