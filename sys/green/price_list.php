<?php

include_once('db_object.php');
	
class price_list extends db_object{
	
	public $properties = array('total', 'date:date', 'products', 'name', 'is_default:boolean');
	
	var $table_name = "price_lists";
	var $class_name = "price_list";


	public static function get_default_price_list(){

		$price_list = master::get_one('price_list', '`is_default` AND NOT `deleted`');

		return $price_list;

	}


	public function get_price_list_product($product){

		$price_list_product = master::get_one('price_list_product', '`price_list` = '.$this->id.' AND `product` = '.$product->id);

		return $price_list_product;

	}


	public function set_data($source) {

		parent::set_data($source);

		foreach($this->properties as $property){
			
			$post_val = $source[$property];


			if($property == 'is_default'){

				if($post_val!==NULL){
					$this->set_as_default();
				}

			}else if($property == 'products'){

				$this->values['products'] = array();
				
				if(is_array($post_val)){

					foreach($post_val as $arr_val){

						$price_list_product = new price_list_product($arr_val['id']);
						$product = new product($arr_val['product']);

						$price_list_product->set('price_list', $this->id);
						$price_list_product->set('price', $arr_val['price']);
						$price_list_product->set('product', $product);

						// print_r($arr_val);
						// echo $arr_val['price'];
						// echo '<br><br><br>';

						$this->values['products'][] = $price_list_product;

					}
				}

			}
			
		}
	}

	public function get_products(){
	
		if ($this->values['products']!=null){
			return $this->values['products'];
		}
	
		if ($this->get_id() > 0 and $this->values['products']=='') {

			$products = array();
			
			//$result = $this->get_master()->query_generic_4_selection("`product`,`gifted`,`id`", "price_list_prodcuts", "NOT deleted AND `price_list` = ".$this->get_id());
			$result = data_manager::read('price_list_products', "`product`,`id`", "NOT deleted AND `price_list` = ".$this->get_id());;
			
			$this->values['products']=Array();
			
			while ($line = mysql_fetch_array($result)){

				$price_list_product = new price_list_product;
				
				$price_list_product->set('product', new product($line['product']));
				$price_list_product->set_id($line['id']);
				$price_list_product->set_container($this);
				
				$this->values['products'][] = $price_list_product;
				
			}

		}else{
			$this->values['products']=Array();
		}

		return $this->values['products'];
	}


	public function set_as_default(){

		if($default_price_list = price_list::get_default_price_list()){
			$default_price_list->update_property('is_default', false);
		}

		$this->update_property('is_default', true);

	}
	
}


class price_list_product extends db_object{

	public $properties = array('price_list','product', 'price');

	var $table_name = "price_list_products";
	var $class_name = "price_list_product";
	
}	?>