<?php

include_once('db_object.php');
	
class facebook_request extends db_object{
	
	private $_name;
	
	public $properties = array('player', 'to');
	
	var $table_name = "fb_requests";
	var $class_name = "facebook_request";
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
		
}

?>