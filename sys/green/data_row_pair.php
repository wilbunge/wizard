<?	include_once('db_object.php');
	//require_once('data_row.php');

	class data_row_pair extends db_object{
	
		public $properties = array('data_row:*data_row', 'name', 'value');
	
		public $table_name = "data_row_pairs";
		public $class_name = "data_row_pair";
		
		public function is_editable(){
			return(data_row::get_pair_color($this->get('name')) == 'GREEN' || data_row::get_pair_color($this->get('name')) == 'YELLOW');
		}
		
		public function is_editable_by_name($name){
			return(data_row::get_pair_color($name) == 'GREEN' || data_row::get_pair_color($name) == 'YELLOW');
		}
		
		public function update_value($val){
			return(master::update_property($this, 'value', $val));
		}
		
		public function embed_for_input($name, $value = ''){
		
			switch($name){
			
				case 'TIPO_DE_JUICIO' : 
				
					$values = Array(
						1 => 'Prendario',
						2 => 'Hipotecarios',
						3 => 'Ejecutivo',
						4 => 'Entrega de la Cosa (Leasing)',
						5 => 'Entrega de la Cosa (Hipotecarios)',
						6 => 'Convenios BKB',
						7 => 'Extrajudicial'
						
					);
					
					$html = "
						<select name='$name'>
							<option value=''>...</option>";
							
					foreach($values as $k=>$v){
						$l=$k+0;
						$html .= "<option".($value==$l ? " selected" : "")." value='$l'>$v</option>";
					}
							
					$html .= "</select>";
					break;
				
				case 'CODETAPA' : 
				
					$codetapas = Array(
						1 => Array('Intimado','Demandado','Secuestro del vehículo','Estudio de Títulos','Decreto de remate','Rematado','Aprobación del remate','Escriturado judicialmente','Liquidación del crédito','Detenido','Acuerdo de Pago','Concurso (concordarto, quiebra)','Incobrables'),
						2 => Array('Intimado','Demandado','Estudio de Títulos','Decreto de remate','Rematado','Aprobación del remate','Escriturado judicialmente','Liquidación del crédito','Detenido','Acuerdo de Pago','Concurso (concordarto, quiebra)','Incobrables'),
						3 => Array('Intimado','Demandado','Mejora de embargo','Reconstrucción de títulos','Estudio de Títulos','Decreto de remate','Rematado','Aprobación del precio del remate','Escrituración judicial','Liquidación del crédito','Detenido','Acuerdo de Pago','Concurso (concordato, quiebra)','Incobrables'),
						4 => Array('Intimado','Demandado','Desapoderamiento','Rescisión del contrato','Detenido','Acuerdo de pago','Finalizado'),
						5 => Array('Intimado','Lanzamiento','Detenido','Finalizado'),
						7 => Array('Sin Acciones Judiciales','Acuerdo de Pago'),
					);
					
					$tipo_de_juicio = $this->get('data_row')->load_pair('TIPO_DE_JUICIO')->get('value');

					$html = "
						<select name='$name'>
							<option value=''>...</option>";
					
					$values = $codetapas[$tipo_de_juicio];
							
					foreach($values as $k=>$v){
						$l=$k+1;
						$html .= "<option".($value==$l ? " selected" : "")." value='$l'>$v</option>";
					}
							
					$html .= "</select>";
					break;
				
				
				default :
					$html = "";
					break;
			
			}
			
			return $html;
		
		}
	
	}	?>