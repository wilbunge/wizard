<?php

	include_once("master.php");

	class wizard extends master {

		static $_render = false;
		static $_template;
		static $_wizard_user;
		static $_wizard_account;
		static $_html;

		static function body($body, $script=''){

			self::$_render = true;

			// self::$_template = new template('sys/templates/index');

			// self::$_template->replace('BODY', $body);
			// self::$_template->replace('SCRIPT', $script);
			
		}

		static function html($html, $script=''){
			self::$_render = true;
			self::$_html = $html;
		}

		static function render(){

			//self::$_template->render();

			echo self::$_html;

		}

		public function get_user(){


			if(self::$_wizard_user == NULL){
			
				
				$crypt_user = $_COOKIE['user'];

				if($_COOKIE['user'] == null) return(false);
				
				// $user_id=self::decrypt_data2($_COOKIE['user']);
				$user_id=($_COOKIE['user']);

				self::$_wizard_user = new wizard_user($user_id);
				
			}
			
			return(self::$_wizard_user);
			
		}

		static function get_account(){

			self::_include('wizard_account');
			return new wizard_account(1);

			if(self::$_wizard_account == NULL){
				
				if($_COOKIE['account'] == null) return(false);
				
				$account_id=self::decrypt_data2($_COOKIE['account']);

				self::$_wizard_account = new wizard_account($account_id);
				
			}
			
			return(self::$_wizard_account);
			
		}


		static function msg($id){

			switch ($id) {
				case 'SAVE_CHANGES_OK':
					
					$msg = "Los cambios se guardaron con éxito!";

					break;
				
				case 'SAVE_CHANGES_ERROR':
					
					$msg = "Error al guardar los cambios!";

					break;
				
				default:
					$msg = $id;
					break;
			}

			return $msg;

		}


		static function generate_db($cn){

			parent::generate_db($cn);

			self::_include("$cn.php");
			$o = new $cn;

			$sql = "ALTER TABLE `".$o->table_name."` ADD `__wacc` INT";
			$result = mysql_query($sql) or die($sql);

		}

		static function format_number($number){

			$number_formatted = number_format($number, 2, ',', '.');

			return $number_formatted;

		}


		static function format_date($ts, $mode=0){

			switch($mode){

				case 1:
					$date_f = date('d-M', $ts);
					break;

				default:
					$date_f = date('d/m/Y', $ts);
					break;

			}

			return $date_f;


		}



		static public function get_version()
		{
			$f = fopen('version.txt', 'r');
			$version = fread($f, filesize('version.txt')) or die('ERROR READING VERSION');
			return $version;
		}


		static public function error($msg){

			$error = new error($msg);


			return $error;


		}

		static function response(){

			return new response;

		}


		

	}

		?>