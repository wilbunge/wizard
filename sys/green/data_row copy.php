<?php

include_once('db_object.php');

class data_row extends db_object{
	
	public $properties = array('date:date');
	
	public $ni_properties = array('type');
	public $ndb_properties = array('file');
	public static $key_names = Array('CUSTOMER','CONTRATO','PRODUCTO');

	public $pairs = Array();
	
	public $names, $values;
	
	var $table_name = "data_rows";
	var $class_name = "data_row";
	
	var $pairs_loaded = false;
	
	
	//public function catch_data_after
	
	static function load_unique_names(){
		//data_manager::$debug_mode=true;
		
		$order = "`name` LIKE 'NRO_REF' DESC";
		$order .= ", `name` LIKE 'Nombre1' DESC";
		$order .= ", `name` LIKE 'Nombre' DESC";
		$order .= ", `name` LIKE 'Cedula' DESC";
		$order .= ", `name` LIKE 'Customer' DESC";
		$order .= ", `name` LIKE 'Fecha_Cast' DESC";
		$order .= ", `name` LIKE 'CONTRATO' DESC";
		$order .= ", `name` LIKE 'PRODUCTO' DESC";
		$order .= ", `name` LIKE 'SEC/UNSEC' DESC";
		$order .= ", `name` LIKE 'MONEDA' DESC";
		$order .= ", `name` LIKE 'SDO_CAP' DESC";
		$order .= ", `name` LIKE 'TIPO_DE_JUICIO' DESC";
		$order .= ", `name` LIKE 'FECHA_RECIBIDO' DESC";
		$order .= ", `name` LIKE 'CODETAPA' DESC";
		$order .= ", `name` LIKE 'FECHA_INICIO_ETAPA' DESC";
		$order .= ", `name` LIKE 'EMBARGO' DESC";
		
		
		
		$result = master::get_raw_data('data_row_pairs', 'DISTINCT(`name`) AS `name`', 'NOT `deleted` ORDER BY '.$order);
		
		$names = Array();
		
		while($row = mysql_fetch_array($result)){
			array_push($names, $row['name']);
		}
		
		return($names);
		
	}
	
	
	public function get_date(){
		return(time());
	}
	
	public function get_pair($name){
	
		$name = strtolower($name);
	
		if($this->pairs[$name] != null){
			return($this->pairs[$name]);
		}else{
			return(new data_row_pair);
		}
	
		
	}
	
	public function has_pair($name){
		return($this->get_pair($name)->id > 0);
	}
	
	public function load_pairs(){
		$pairs = master::get('data_row_pair', 'data_row='.$this->id);
		
		foreach($pairs as $pair){
			$this->pairs[strtolower($pair->get('name'))] = $pair;
		}
		
		$pairs_loaded = true;
		
		
		return($pairs);
	}
	
	public function load_pair($name){
		$objects = master::get('data_row_pair', 'data_row='.$this->id.' AND `name` = "'.$name.'"');
		if(!$objects) return(false);
		return($objects[0]);
	}
	
	
	public function catch_data_after(){
		
		echo "catch_data_after()!";
		
		$data = $_FILES['data'];
		
		print_r($data);
		
	}
	
	
	public function add_pair($pair){
		$pair->set('data_row', $this);
		//array_push($this->pairs, $pair);
		$this->pairs[$pair->get('name')] = $pair;
		
	}
	
	
	public function store_after(){
		//data_manager::$debug_mode = true;
		foreach($this->pairs as $pair){
			master::store($pair);
		}
	
	}
	
	
	public function update_pair_value($name, $value){
	
		if(!$pair = $this->load_pair($name)) return;
		
		$pair->set('value', $value);
		master::update($pair);
	}
	
	
	static function get_pair_color($name){
		
		switch($name){
			case 'PRODUCTO' :
			case 'CONTRATO' :
			case 'CUSTOMER' :
				$color = "RED";
				break;
			case 'TIPO_DE_JUICIO' :
			case 'FECHA_RECIBIDO' :
			case 'CODETAPA' :
			case 'JUZGADO' :
			case 'AUTOS' :
			case 'FICHA' :
			case 'COMENTARIOS' :
			case 'ASIGNADO' :
			case 'CEDULA' :
				$color = "GREEN";
				break;
			case 'SEC_UNSEC' :
			case 'MONEDA' :
			case 'SALDO' :
			case 'COMENTARIOS' :
			case 'ASIGNADO' :
			case 'FECHACAST' :
				$color = "BLACK"; // HIDE
				break;
			default:
				$color = "YELLOW";
				break;
		}
		
		return($color);
		
	}
	
	
	public function get_key(){
		
		$a = is_object($this->pairs['CUSTOMER']) ? $this->pairs['CUSTOMER']->get('value') : '';
		$b = is_object($this->pairs['PRODUCTO']) ? $this->pairs['PRODUCTO']->get('value') : '';
		$c = is_object($this->pairs['CONTRATO']) ? $this->pairs['CONTRATO']->get('value') : '';
		
		$key = Array();
		
		$key['CUSTOMER'] = $a;
		$key['PRODUCTO'] = $b;
		$key['CONTRATO'] = $c;
		
		return($key);
		
	}
	
	
	public function check_key($names){
	
		
		
		foreach(self::$key_names as $x){
			if(!in_array($x, $names)) return(false);
		}
		
		return(true);
		
	}
	
	
	public function exists(){
	
		$key = $this->get_key();
	
		$a = $key['CUSTOMER'];
		$b = $key['CONTRATO'];
		$c = $key['PRODUCTO'];
	
		if($a == '' or $b == '' or $c == '') return(false);
		
		//echo "$a - $b - $c <br>";
		
		// Is there a registry with this CUSTOMER? If not so, key does not exist
		$result_1 = master::get_raw_data('data_row_pairs', 'data_row', "`name` = 'CUSTOMER' AND `value` = '$a' AND NOT `deleted` GROUP BY `data_row`");
		
		if(mysql_num_rows($result_1) < 1) return(false); // No. Key does not exists
		
		while($sql_row = mysql_fetch_array($result_1)){
			$array_1[] = $sql_row['data_row']; // All data_rows with given CUSTOMER
		}
		
		
		//echo "1#";
		
		//print_r($array_1);
		
		// Is there a registry with this CONTRATO? If not so, key does not exist
		$result_2 = master::get_raw_data('data_row_pairs', 'data_row', "`name` = 'CONTRATO' AND `value` = '$b' AND NOT `deleted` GROUP BY `data_row`");
		
		if(mysql_num_rows($result_2) < 1) return(false); // No. Key does not exists
		
		while($sql_row = mysql_fetch_array($result_2)){
			$array_2[] = $sql_row['data_row'];  // All data_rows with given PRODUCTO
		}
		
		
		//echo "2#";
		
		// Is there a registry with this PRODUCTO? If not so, key does not exist
		$result_3 = master::get_raw_data('data_row_pairs', 'data_row', "`name` = 'PRODUCTO' AND `value` = '$c' AND NOT `deleted` GROUP BY `data_row`");
		
		if(mysql_num_rows($result_3) < 1) return(false); // No. Key does not exists
		
		while($sql_row = mysql_fetch_array($result_3)){
			$array_3[] = $sql_row['data_row']; // All data_rows with given CONTRATO
		}
		
		//echo "3#";
		
		$i = array_intersect($array_1, $array_2, $array_3);
		$k = Array();
		
		
		if(count($i)<1) return(false);
		
		foreach($i as $j){
			array_push($k, $j);
		}
		
		$this->set_id($k[0]);
		
		return(true);
	
	}
	
	
	public function update(){
		
		foreach($this->pairs as $pair){
		
			if(data_row::get_pair_color($pair) == 'YELLOW'){ // UPDATE
				$this->update_pair_value($pair->get('name'), $pair->get('value'));
			}
			
		}
		
		
	}
	
		
		
	
}	?>