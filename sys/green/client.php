<?php
include_once('db_object.php');
	
class client extends db_object{
	
	private $_company_name;
	private $_full_company_name;
	private $_last_name;
	private $_first_name;
	private $_email;
	private $_tel;
	private $_cel;
	private $_address;
	private $_address_2;
	private $_city;
	private $_state;
	private $_postal_code;
	private $_country;
	private $_RUT;
	private $_type;
	private $_category;
	private $_comments;
	
	public $contacts = array();

	public $properties = array('company_name','full_company_name','rut', 'first_name','last_name','email','tel','cel','address','address_2','city','state','postal_code','country','comments',
		'price_list:price_list*', 'salesman:salesman*'
	);
	
	var $table_name = "clients";
	var $class_name = "client";

	public function get_contacts(){
		if($this->get_id()>0) $this->load_contacts();
		return($this->contacts);
	}
	
	public function set_contacts($val){
		$this->contacts = $val;
	}
	
	
	public function load_contacts(){
		
		$result=$this->get_master()->mysql_query("SELECT", "contact", "client_contacts","NOT `deleted` AND `client`=".$this->get_id(),"");
		
		while($row=mysql_fetch_array($result)){
			$contact=new contact($row["contact"]);
			array_push($this->contacts, $contact);
		}
		
		
	}
	
	
	public function get_category(){
	
		if ($this->_category== NULL){
			$this->_category=new category;
			if($this->get_id()>0) {
				$this->_category->set_id($this->load_db_field('category'));
			}
		}
	
		return($this->_category);
	}
	
	public function set_category($val){
		$this->_category = $val;
	}
	
	public function get_RUT(){
	
		if ($this->_RUT== NULL){
			if($this->get_id()>0) {
				$this->_RUT = $this->load_db_field('RUT');
			}
		}
	
		return($this->_RUT);
	}
	
	public function set_RUT($val){
		$this->_RUT = $val;
	}


	public function get_type(){
		return($this->_type);
	}
	
	public function set_type($val){
		$this->_type = $val;
	}
	
	
	
	public function get_address(){
	
		if ($this->_address== NULL){
			if($this->get_id()>0) {
				$this->_address = $this->load_db_field('address');
			}
		}
	
		return($this->_address);
	}
	
	public function set_address($val){
		$this->_address = $val;
	}
	
	
	public function get_comments(){
	
		if ($this->_comments== NULL){
			if($this->get_id()>0) {
				$this->_comments = $this->load_db_field('comments');
			}
		}
	
		return($this->_comments);
	}
	
	public function set_comments($val){
		$this->_comments = $val;
	}
	
	
	public function get_address_2(){
	
		if ($this->_address_2== NULL){
			if($this->get_id()>0) {
				$this->_address_2 = $this->load_db_field('address_2');
			}
		}
	
		return($this->_address_2);
	}
	
	public function set_address_2($val){
		$this->_address_2 = $val;
	}
	
	
	public function get_city(){
	
		if ($this->_city== NULL){
			if($this->get_id()>0) {
				$this->_city= $this->load_db_field('city');
			}
		}
	
		return($this->_city);
	}
	
	public function set_city($val){
		$this->_city = $val;
	}
	
	
	public function get_state(){
	
		if ($this->_state== NULL){
			if($this->get_id()>0) {
				$this->_state= $this->load_db_field('state');
			}
		}
	
		return($this->_state);
	}
	
	public function set_state($val){
		$this->_state = $val;
	}
	
	
	public function get_postal_code(){
		
		if ($this->_postal_code== NULL){
			if($this->get_id()>0) {
				$this->_postal_code= $this->load_db_field('postal_code');
			}
		}
		
		return($this->_postal_code);
	}
	
	public function set_postal_code($val){
		$this->_postal_code = $val;
	}
	
	
	public function get_country(){
		if ($this->_country== NULL){
			if($this->get_id()>0) {
				$this->_country= $this->load_db_field('country');
			}
		}
		return($this->_country);
	}
	
	public function set_country($val){
		$this->_country = $val;
	}
	
	public function get_company_name(){
		if ($this->_company_name== NULL){
			if($this->get_id()>0) {
				$this->_company_name= $this->load_db_field('company_name');
			}
		}
		return $this->_company_name;
	}

	public function set_company_name($val){
		$this->_company_name= $val;
	}
	
	
	public function get_full_company_name(){
		if ($this->_full_company_name== NULL){
			if($this->get_id()>0) {
				$this->_full_company_name= $this->load_db_field('full_company_name');
			}
		}
		return $this->_full_company_name;
	}

	public function set_full_company_name($val){
		$this->_full_company_name= $val;
	}


	public function get_first_name(){
		if ($this->_first_name===NULL){
			if($this->get_id()>0) {
				$this->_first_name= $this->load_db_field('first_name');
			}
		}
		return $this->_first_name;
	}

	public function set_first_name($val){
		$this->_first_name= $val;
	}
	
	
	public function get_tel(){
		if ($this->_tel== NULL){
			if($this->get_id()>0) {
				$this->_tel= $this->load_db_field('tel');
			}
		}
		return $this->_tel;
	}

	public function set_tel($val){
		$this->_tel= $val;
	}
	
	
	public function get_cel(){
		if ($this->_cel== NULL){
			if($this->get_id()>0) {
				$this->_cel= $this->load_db_field('cel');
			}
		}
		return $this->_cel;
	}

	public function set_cel($val){
		$this->_cel= $val;
	}
	
	
	public function get_email(){
		if ($this->_email== NULL){
			if($this->get_id()>0) {
				$this->_email= $this->load_db_field('email');
			}
		}
		return $this->_email;
	}

	public function set_email($val){
		$this->_email = $val;
	}
	
	
	
	public function get_last_name(){
		if ($this->_last_name== NULL){
			if($this->get_id()>0) {
				$this->_last_name= $this->load_db_field('last_name');
			}
		}
		return $this->_last_name;
	}

	public function set_last_name($val){
		$this->_last_name= $val;
	}
	
	
	
	public function get_phone(){
		if ($this->_phone== NULL){
			if($this->get_id()>0) {
				$this->_phone= $this->load_db_field('phone');
			}
		}
		return $this->_phone;
	}

	public function set_phone($val){
		$this->_phone= $val;
	}
	
	
	public function get_mail(){
		if ($this->_mail== NULL){
			if($this->get_id()>0) {
				$this->_mail= $this->load_db_field('mail');
			}
		}
		return $this->_mail;
	}

	public function set_mail($val){
		$this->_mail= $val;
	}
	
	
	public function get_birth(){
		if ($this->_birth == NULL){
			if($this->get_id()>0) {
				$this->_birth = $this->load_db_field('birth');
			}
		}
		return $this->_birth;
	}

	public function set_birth($val){
		$this->_birth = $val;
	}
	
	
	public function get_sex(){
		if ($this->_sex == NULL){
			if($this->get_id()>0) {
				$this->_sex = $this->load_db_field('sex');
			}
		}
		return $this->_sex;
	}

	public function set_sex($val){
		$this->_sex = $val;
	}
	
	
	public function get_handicap(){
		if ($this->_handicap == NULL){
			if($this->get_id()>0) {
				$this->_handicap = $this->load_db_field('handicap');
			}
		}
		return $this->_handicap;
	}

	public function set_handicap($val){
		$this->_handicap = $val;
	}
	
	public function embed_photo($p_mode){
	
		switch($p_mode){
			case 2 : 
				$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/mp_" . $this->get_id() . "_2.jpg";
				break;
			default :
				$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/mp_" . $this->get_id() . "_1.jpg";
				break;
		}
		
		if(!file_exists($img_path)){
			switch($p_mode){
				case 2 : 
					$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/no_photo.jpg";
					break;
				default :
					$img_path = "/websites/laercilia.com/ipub/admin/_polo_player/photos/no_photo_small.jpg";
					break;
			}
		}
	
		$tag = "<img src='$img_path'>";
	
		return $tag;
	
	}
	
	
	
	public function _html_input_property($p_property) {
		

		// IMPORTANT !!
		$code0 = "\$this->get_$p_property();";
		$code = '$val = ' . $code0;
		
		eval ($code);
		// IMPORTANT !!
		
		$html = '';
		switch ($p_property) {
		
			case 'category':
//				$result = $this->get_master()->mysql_query('SELECT', 'name,id', 'categorizer', 'NOT deleted AND family = "products"', '');

				
								
				$html = '<select name="category">';
				$html .= "	<option value='0'>...</option>";
				$html .=	get_category_childs(0,"clients",$this);
				$html .= "</select>";			
			break;
		
						
			case 'contacts' :

				$html .= '<a href="#" onclick="G_JS_clients_select_contacts();">Seleccionar contactos...</a>';
				//$html .= '<input id="G_ME_clients_input_contacts" value="'.$val.'" type="text" name="contacts">';
				
				if($this->get_id()>0){
					$this->load_contacts();
				}
				
				$c="";
				
				foreach($this->get_contacts() as $contact){
					$c.="<input type='hidden' name='contacts[]' value='".$contact->get_id()."'>";
					$c.="<li>".$contact->get_name()."</li>";
				}
				
				$html .= "<div id='G_ME_clients_div_contacts'>$c</div>";
				
				break;
			
						
			case 'type':
				
				$html .= '<label><input type="radio" name="type" value="1">Company</label> <label><input type="radio" name="type" value="2">Person</label>';
				break;
			
			case 'birth':
							
				
				$day_birth = first_name("d", $this->get_birth());
				$month_birth = first_name("m", $this->get_birth());
				$year_birth = first_name("Y", $this->get_birth());
				
				if(!checkfirst_name($day_birth, $month_birth, $year_birth)){
					
					$day_birth = "";
					$month_birth = "";
					$year_birth = "";
					
				}
				
			
				$html .= '<select id="day_birth" name="day_birth">';
				$html .= '	<option value="31"'.(($day_birth == '31') ? ' selected' : '').'>31</option>';
				$html .= '	<option value="30"'.(($day_birth == '30') ? ' selected' : '').'>30</option>';
				$html .= '	<option value="29"'.(($day_birth == '29') ? ' selected' : '').'>29</option>';
				$html .= '	<option value="28"'.(($day_birth == '28') ? ' selected' : '').'>28</option>';
				$html .= '	<option value="27'.(($day_birth == '27') ? ' selected' : '').'>27</option>';
				$html .= '	<option value="26"'.(($day_birth == '26') ? ' selected' : '').'>26</option>';
				$html .= '	<option value="25"'.(($day_birth == '25') ? ' selected' : '').'>25</option>';
				$html .= '	<option value="24"'.(($day_birth == '24') ? ' selected' : '').'>24</option>';
				$html .= '	<option value="23"'.(($day_birth == '23') ? ' selected' : '').'>23</option>';
				$html .= '	<option value="22"'.(($day_birth == '22') ? ' selected' : '').'>22</option>';
				$html .= '	<option value="21"'.(($day_birth == '21') ? ' selected' : '').'>21</option>';
				$html .= '	<option value="20"'.(($day_birth == '20') ? ' selected' : '').'>20</option>';
				$html .= '	<option value="19"'.(($day_birth == '19') ? ' selected' : '').'>19</option>';
				$html .= '	<option value="18"'.(($day_birth == '18') ? ' selected' : '').'>18</option>';
				$html .= '	<option value="17'.(($day_birth == '17') ? ' selected' : '').'>17</option>';
				$html .= '	<option value="16"'.(($day_birth == '16') ? ' selected' : '').'>16</option>';
				$html .= '	<option value="15"'.(($day_birth == '15') ? ' selected' : '').'>15</option>';
				$html .= '	<option value="14"'.(($day_birth == '14') ? ' selected' : '').'>14</option>';
				$html .= '	<option value="13"'.(($day_birth == '13') ? ' selected' : '').'>13</option>';
				$html .= '	<option value="12"'.(($day_birth == '12') ? ' selected' : '').'>12</option>';
				$html .= '	<option value="11"'.(($day_birth == '11') ? ' selected' : '').'>11</option>';
				$html .= '	<option value="10"'.(($day_birth == '10') ? ' selected' : '').'>10</option>';
				$html .= '	<option value="09"'.(($day_birth == '09') ? ' selected' : '').'>09</option>';
				$html .= '	<option value="08"'.(($day_birth == '08') ? ' selected' : '').'>08</option>';
				$html .= '	<option value="07"'.(($day_birth == '07') ? ' selected' : '').'>07</option>';
				$html .= '	<option value="06"'.(($day_birth == '06') ? ' selected' : '').'>06</option>';
				$html .= '	<option value="05"'.(($day_birth == '05') ? ' selected' : '').'>05</option>';
				$html .= '	<option value="04"'.(($day_birth == '04') ? ' selected' : '').'>04</option>';
				$html .= '	<option value="03"'.(($day_birth == '03') ? ' selected' : '').'>03</option>';
				$html .= '	<option value="02"'.(($day_birth == '02') ? ' selected' : '').'>02</option>';
				$html .= '	<option value="01"'.(($day_birth == '01') ? ' selected' : '').'>01</option>';
				$html .= '</select>';
				$html .= " / ";
				$html .= '<select id="month_birth" name="month_birth">';
				$html .= '	<option value="12"'.(($month_birth == '12') ? ' selected' : '').'>12</option>';
				$html .= '	<option value="11"'.(($month_birth == '11') ? ' selected' : '').'>11</option>';
				$html .= '	<option value="10"'.(($month_birth == '10') ? ' selected' : '').'>10</option>';
				$html .= '	<option value="09"'.(($month_birth == '09') ? ' selected' : '').'>09</option>';
				$html .= '	<option value="08"'.(($month_birth == '08') ? ' selected' : '').'>08</option>';
				$html .= '	<option value="07"'.(($month_birth == '07') ? ' selected' : '').'>07</option>';
				$html .= '	<option value="06"'.(($month_birth == '06') ? ' selected' : '').'>06</option>';
				$html .= '	<option value="05"'.(($month_birth == '05') ? ' selected' : '').'>05</option>';
				$html .= '	<option value="04"'.(($month_birth == '04') ? ' selected' : '').'>04</option>';
				$html .= '	<option value="03"'.(($month_birth == '03') ? ' selected' : '').'>03</option>';
				$html .= '	<option value="02"'.(($month_birth == '02') ? ' selected' : '').'>02</option>';
				$html .= '	<option value="01"'.(($month_birth == '01') ? ' selected' : '').'>01</option>';
				$html .= '</select>';
				$html .= " / ";
				$html .= '<select id="month_birth" name="month_birth">';
				$html .= '	<option value="1950"'.(($year_birth == '1950') ? ' selected' : '').'>1950</option>';
				$html .= '	<option value="1951"'.(($year_birth == '1951') ? ' selected' : '').'>1951</option>';
				$html .= '	<option value="1952"'.(($year_birth == '1952') ? ' selected' : '').'>1952</option>';
				$html .= '	<option value="1953"'.(($year_birth == '1953') ? ' selected' : '').'>1953</option>';
				$html .= '	<option value="1954"'.(($year_birth == '1954') ? ' selected' : '').'>1954</option>';
				$html .= '	<option value="1955"'.(($year_birth == '1955') ? ' selected' : '').'>1955</option>';
				$html .= '	<option value="1956"'.(($year_birth == '1956') ? ' selected' : '').'>1956</option>';
				$html .= '	<option value="1957"'.(($year_birth == '1957') ? ' selected' : '').'>1957</option>';
				$html .= '	<option value="1958"'.(($year_birth == '1958') ? ' selected' : '').'>1958</option>';
				$html .= '	<option value="1959"'.(($year_birth == '1959') ? ' selected' : '').'>1959</option>';
				$html .= '	<option value="1960"'.(($year_birth == '1960') ? ' selected' : '').'>1960</option>';
				$html .= '	<option value="1961"'.(($year_birth == '1961') ? ' selected' : '').'>1961</option>';
				$html .= '	<option value="1962"'.(($year_birth == '1962') ? ' selected' : '').'>1962</option>';
				$html .= '	<option value="1963"'.(($year_birth == '1963') ? ' selected' : '').'>1963</option>';
				$html .= '	<option value="1964"'.(($year_birth == '1964') ? ' selected' : '').'>1964</option>';
				$html .= '	<option value="1965"'.(($year_birth == '1965') ? ' selected' : '').'>1965</option>';
				$html .= '	<option value="1966"'.(($year_birth == '1966') ? ' selected' : '').'>1966</option>';
				$html .= '	<option value="1967"'.(($year_birth == '1967') ? ' selected' : '').'>1967</option>';
				$html .= '	<option value="1968"'.(($year_birth == '1968') ? ' selected' : '').'>1968</option>';
				$html .= '	<option value="1969"'.(($year_birth == '1969') ? ' selected' : '').'>1969</option>';
				$html .= '	<option value="1970"'.(($year_birth == '1970') ? ' selected' : '').'>1970</option>';
				$html .= '	<option value="1971"'.(($year_birth == '1971') ? ' selected' : '').'>1971</option>';
				$html .= '	<option value="1972"'.(($year_birth == '1972') ? ' selected' : '').'>1972</option>';
				$html .= '	<option value="1973"'.(($year_birth == '1973') ? ' selected' : '').'>1973</option>';
				$html .= '	<option value="1974"'.(($year_birth == '1974') ? ' selected' : '').'>1974</option>';
				$html .= '	<option value="1975"'.(($year_birth == '1975') ? ' selected' : '').'>1975</option>';
				$html .= '	<option value="1976"'.(($year_birth == '1976') ? ' selected' : '').'>1976</option>';
				$html .= '	<option value="1977"'.(($year_birth == '1977') ? ' selected' : '').'>1977</option>';
				$html .= '	<option value="1978"'.(($year_birth == '1978') ? ' selected' : '').'>1978</option>';
				$html .= '	<option value="1979"'.(($year_birth == '1979') ? ' selected' : '').'>1979</option>';
				$html .= '	<option value="1980"'.(($year_birth == '1980') ? ' selected' : '').'>1980</option>';
				$html .= '	<option value="1981"'.(($year_birth == '1981') ? ' selected' : '').'>1981</option>';
				$html .= '	<option value="1982"'.(($year_birth == '1982') ? ' selected' : '').'>1982</option>';
				$html .= '	<option value="1983"'.(($year_birth == '1983') ? ' selected' : '').'>1983</option>';
				$html .= '	<option value="1984"'.(($year_birth == '1984') ? ' selected' : '').'>1984</option>';
				$html .= '	<option value="1985"'.(($year_birth == '1985') ? ' selected' : '').'>1985</option>';
				$html .= '	<option value="1986"'.(($year_birth == '1986') ? ' selected' : '').'>1986</option>';
				$html .= '	<option value="1987"'.(($year_birth == '1987') ? ' selected' : '').'>1987</option>';
				$html .= '	<option value="1988"'.(($year_birth == '1988') ? ' selected' : '').'>1988</option>';
				$html .= '	<option value="1989"'.(($year_birth == '1989') ? ' selected' : '').'>1989</option>';
				$html .= '	<option value="1990"'.(($year_birth == '1990') ? ' selected' : '').'>1990</option>';
				$html .= '	<option value="1991"'.(($year_birth == '1991') ? ' selected' : '').'>1991</option>';
				$html .= '	<option value="1992"'.(($year_birth == '1992') ? ' selected' : '').'>1992</option>';
				$html .= '	<option value="1993"'.(($year_birth == '1993') ? ' selected' : '').'>1993</option>';
				$html .= '	<option value="1994"'.(($year_birth == '1994') ? ' selected' : '').'>1994</option>';
				$html .= '	<option value="1995"'.(($year_birth == '1995') ? ' selected' : '').'>1995</option>';
				$html .= '	<option value="1996"'.(($year_birth == '1996') ? ' selected' : '').'>1996</option>';
				$html .= '	<option value="1997"'.(($year_birth == '1997') ? ' selected' : '').'>1997</option>';
				$html .= '	<option value="1998"'.(($year_birth == '1998') ? ' selected' : '').'>1998</option>';
				$html .= '	<option value="1999"'.(($year_birth == '1999') ? ' selected' : '').'>1999</option>';
				$html .= '	<option value="2000"'.(($year_birth == '2000') ? ' selected' : '').'>2000</option>';
				$html .= '	<option value="2001"'.(($year_birth == '2001') ? ' selected' : '').'>2001</option>';
				$html .= '	<option value="2002"'.(($year_birth == '2002') ? ' selected' : '').'>2002</option>';
				$html .= '	<option value="2003"'.(($year_birth == '2003') ? ' selected' : '').'>2003</option>';
				$html .= '	<option value="2004"'.(($year_birth == '2004') ? ' selected' : '').'>2004</option>';
				$html .= '	<option value="2005"'.(($year_birth == '2005') ? ' selected' : '').'>2005</option>';
				$html .= '	<option value="2006"'.(($year_birth == '2006') ? ' selected' : '').'>2006</option>';
				$html .= '	<option value="2007"'.(($year_birth == '2007') ? ' selected' : '').'>2007</option>';
				$html .= '	<option value="2008"'.(($year_birth == '2008') ? ' selected' : '').'>2008</option>';
				$html .= '	<option value="2009"'.(($year_birth == '2009') ? ' selected' : '').'>2009</option>';
				$html .= '	<option value="2010"'.(($year_birth == '2010') ? ' selected' : '').'>2010</option>';
				$html .= '</select>';
				break;
						
			case 'handicap' :
			
				$html .= '<select id="handicap" name="handicap">';
				$html .= '	<option value="10"'.(($this->get_handicap() == '10') ? ' selected' : '').'>10</option>';
				$html .= '	<option value="9"'.(($this->get_handicap() == '9') ? ' selected' : '').'>9</option>';
				$html .= '	<option value="8"'.(($this->get_handicap() == '8') ? ' selected' : '').'>8</option>';
				$html .= '	<option value="7"'.(($this->get_handicap() == '7') ? ' selected' : '').'>7</option>';
				$html .= '	<option value="6"'.(($this->get_handicap() == '6') ? ' selected' : '').'>6</option>';
				$html .= '	<option value="5"'.(($this->get_handicap() == '5') ? ' selected' : '').'>5</option>';
				$html .= '	<option value="4"'.(($this->get_handicap() == '4') ? ' selected' : '').'>4</option>';
				$html .= '	<option value="3"'.(($this->get_handicap() == '3') ? ' selected' : '').'>3</option>';
				$html .= '	<option value="2"'.(($this->get_handicap() == '2') ? ' selected' : '').'>2</option>';
				$html .= '	<option value="1"'.(($this->get_handicap() == '1') ? ' selected' : '').'>1</option>';
				$html .= '	<option value="0"'.(($this->get_handicap() == '0') ? ' selected' : '').'>0</option>';
				$html .= '	<option value="-1"'.(($this->get_handicap() == '-1') ? ' selected' : '').'>-1</option>';
				$html .= '	<option value="-2"'.(($this->get_handicap() == '-2') ? ' selected' : '').'>-2</option>';
				$html .= '</select>';
				break;
			
			
			case 'address' :
			case 'address_2' :
			case 'comments' :
			
				$html .= '<textarea name="'.$p_property.'">'.$val.'</textarea>';
				break;
			
			
			case 'phone' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
			
			
			case 'last_name' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
							
			
			case 'first_name' :
			
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
			
			case 'name' :
			default:
				$html .= '<input value="'.$val.'" type="text" name="'.$p_property.'">';
				break;
		}
		return $html;
	}
	
	
	
	public function catch_post_properties() {

		foreach($this->properties as $property){
		
			$post_val = $_POST[$property];
			
			if($property == 'team'){
				foreach($post_val as $arr_val){
					$contact = new contact();
					$contact->set_id($arr_val);
					array_push($this->team, $contact);
				}
			}else if($property == 'contacts'){
			
				if(is_array($_POST['contacts'])){
					foreach($_POST['contacts'] as $id){		
						$contact = new contact;
						$contact->set_id($id);
						array_push($this->contacts, $contact);		
					}
				}
			
			}else if(gettype($val) == 'object' && ($post_val != '')){
				$code0 = "\$this->get_$property()->set_id($post_val);";
			}else if(!$birth){
				$post_val = $_POST[$property];
				$code0 = "\$this->set_$property('$post_val');";
				//$code = '$val = ' . $code0;
				eval ($code0);
			}else{
				$this->set_birth($_POST['day_birth'],$_POST['month_birth'],$_POST['year_birth']);
			}
		}
	}


	public function get_name(){
	
		if($this->get_company_name() != ""){
			return($this->get_company_name());
		}else if($this->get_last_name() != '' && $this->get_first_name() != ''){
			return($this->get_full_name());
		}else if($this->get_last_name() != ''){
			return($this->get_last_name());
		}else if($this->get_first_name() != ''){
			return($this->get_first_name());
		}else{
			return("-");
		}
	
	}
	
	public function get_full_name(){
	
		$full_name = ($this->get_last_name() . ', ' . $this->get_first_name());
		return($full_name);
	}
	
	
	public function embed(){
	
		$html = "<table class='G_cs_input' cellspacing='0'>";
		
		foreach($this->properties as $property){
		
			$code0 = "\$this->get_$property();";
			$code = '$val = ' . $code0;
			
			if(function_exists(translate)){
				$property_t=translate("class_".$this->class_name, "properties/".$property);
			}else{		
				$property_t=$property;
			}
			
			if($property_t==NULL || $property_t=='')$property_t=$property;
			
		
			eval ($code);
		
			$html .= "<tr>";
			$html .= "	<th>" . $property_t . "</th>";
			
			if($property == 'phone'){
			
				$val_s = str_replace(" ", "", $val);
			
				$html .= "	<td><a href='skype:+".$val_s."?call'>" . $val . "</a></td>";
			}elseif($property == 'contacts'){
				$html .= "<td><ul>";
				foreach($val as $obj){
					$oc = 'javascript:G_jsf_openModule("address_book", "view", "id='.$obj->get_id().'", "container_2");';
					$html .= "<li><a href='$oc'>".$obj->get_name()."</a></li>";					
				}
				$html .= "</ul></td>";
				
				
			}elseif(gettype($val) == 'object'){
				$html .= "	<td>" . $val->get_default() . "</td>";
			}else{
				$html .= "	<td>" . $val . "</td>";
			
			}
			
			$html .= "</tr>";
		}
		
		$html .= "</table>";
		
		return($html);
	}

	public function get_statement_balance($format = false){

		$statement = $this->get_statement();
		$statement_balance = 0;

		foreach($statement as $statement_item){

			$statement_balance += $statement_item['total'];

		}

		if($format){
			$symbol = '$';
			$statement_balance = number_format(($statement_balance), 2, ',', '.');
			$statement_balance = abs($statement_balance)==$statement_balance?$symbol.' '.$statement_balance:'-'.$symbol.' '.abs($statement_balance);
		}

		// $statement_balance = abs($statement_balance)==$statement_balance?$symbol.$statement_balance:'-'.$symbol.' '.abs($statement_balance);

		// $statement_balance = "$statement_balance";

		return $statement_balance;
	}

	public static function statement_date_compare($a, $b){
	    $t1 = strtotime($a['date']);
	    $t2 = strtotime($b['date']);
	    return $t1 - $t2;
	}


	public function get_statement($format=false){

		
		// data_manager::$debug_mode=true;

		$statement = array();

		$invoices = master::get('invoice', '`client` = '.$this->id.' AND NOT deleted');
		$payments = master::get('payment', '`client` = '.$this->id.' AND NOT deleted');

		if($invoices){

			foreach ($invoices as $invoice) {

				$total = $invoice->get('total');
				
				$statement_item['name'] = 'Factura '.$invoice->get_id_number();
				$statement_item['date'] = $invoice->get('date');
				$statement_item['total'] = $total;
				
				array_push($statement, $statement_item);

			}

		}

		if($payments){
			foreach ($payments as $payment) {

			$total = -$payment->get('total');

			$statement_item['name'] = 'Recibo '.$payment->get_number_id();
			
			$statement_item['date'] = $payment->get('date');
			$statement_item['total'] = $total;
			
			array_push($statement, $statement_item);

			}
		}

		usort($statement, array('client', 'statement_date_compare'));

		foreach ($statement as $k => $statement_item){

			$total = $statement_item['total'];

			$balance += $total;

			if($format) $total = number_format($total, 2, ',', '.');
			if($format) $balance = number_format($balance, 2, ',', '.');

			$statement[$k]['total'] = $total;
			$statement[$k]['balance'] = $balance;

			
		}


		return $statement;


	}
	
	
}
?>