<?	include_once('user.php');
	
	class wizard_user extends user{

		public function email_exists(){
			// data_manager::$debug_mode=true;
			return master::get('wizard_user', '`email` LIKE "'.$this->get('email').'" AND `id` <> '.$this->id);
		}


		public function validate(){

			wizard::_include('validation');

			$validation = new validation;

			if($this->email_exists()){
				$validation->error('email', 'El email ingresado ya está en uso por otro usuario.');
			}

			// if($this->get('password')=='') $validation->error('password', 'La contaseña no puede estar vacía');

			return $validation;


		}

		public function get_quick_launch_menu(){

			$quick_menu = wizard::get('quick_menu_item', "`wizard_user` = ".$this->id.' AND `active`');

			$account_modules = wizard::get_account()->get_modules();
			$user_modules  = Array();

			if(!$quick_menu) $quick_menu = $this->generate_quick_menu();

			foreach ($quick_menu as $quick_menu_item) {
				$wizard_module = $quick_menu_item->get('wizard_module');
				if($this->is_admin() || $this->has_access_to_module($wizard_module)) array_push($user_modules, $wizard_module);
			}


			return $user_modules;


			$sql_query = "SELECT module FROM user_modules WHERE quick_launch AND user = '".$this->get_id()."' AND (green_client = '".$this->get_green_client()->get_id()."' OR green_client = 0) AND has_access";
			// $sql_query = "SELECT module FROM user_modules WHERE quick_launch AND user = '".$this->get_id()."' AND (green_client = '".$this->get_green_client()->get_id()."' OR green_client = 0) AND has_access";
			//echo($sql_query);
			$result = mysql_query($sql_query);
		
			while($row = mysql_fetch_array($result)){
				$green_module = new green_module();
				$green_module->set_id($row['module']);
				
				if($this->get_green_client()->has_access_to($green_module)) array_push($this->modules, $green_module);	
			}
		
		}

		public function has_access_to_module($wizard_module){

			$wizard_user_module = wizard::get_one('wizard_user_module', '`wizard_user` = '.$this->id.' AND `wizard_module` = "'.$wizard_module->id.'" AND NOT deleted');

			if(!$wizard_user_module) return false;

			return $wizard_user_module->get('access');

		}

		public function get_wizard_user_module($wizard_module){

			$wizard_user_module = wizard::get_one('wizard_user_module', 'wizard_user = '.$this->id.' AND `wizard_module` = "'.$wizard_module->id.'"  AND NOT deleted');

			if(!$wizard_user_module) return false;

			return $wizard_user_module;

		}


		public function set_access_to_module($wizard_module, $access){

			if(!$wizard_user_module = $this->get_wizard_user_module($wizard_module)){
				$wizard_user_module = new wizard_user_module;
				$wizard_user_module->set('wizard_user', $this);
				$wizard_user_module->set('wizard_module', $wizard_module);
			}

			$wizard_user_module->set('access', $access);
			$wizard_user_module->save();

			return $wizard_user_module;
			
		}

		public function get_quick_menu_item($wizard_module){

			$quick_menu_item = wizard::get_one('quick_menu_item', "`wizard_user` = ".$this->id.' AND `wizard_module` = '.$wizard_module->id);

			if(!$quick_menu_item){

				$quick_menu_item = new quick_menu_item;

				$quick_menu_item->set('wizard_user', $this);
				$quick_menu_item->set('wizard_module', $wizard_module);

				$quick_menu_item->save();

			}


			return $quick_menu_item;

		}






		public function set_quick_menu($wizard_module, $mode){

			$quick_menu_item = $this->get_quick_menu_item($wizard_module);

			$quick_menu_item->update_status($mode);

			return $quick_menu_item;

			return;

			if(!$wizard_user_module = $this->get_wizard_user_module($wizard_module)){
				$wizard_user_module = new wizard_user_module;
				$wizard_user_module->set('wizard_user', $this);
				$wizard_user_module->set('wizard_module', $wizard_module);
			}

			$wizard_user_module->set('access', $access);
			$wizard_user_module->save();

			return $wizard_user_module;
			
		}

		public function validate_credentials(){
		
			$email = $this->get('email');
			$password = $this->get('password');
			
			if($email == '' or $email == null) return(false);
			if($password == '' or $password == null) return(false);
			
			//data_manager::$debug_mode=true;
			
			$result = master::get('user', "`email` LIKE '$email' AND `password` = '$password'");
			
			if($result==null){
				return(false);
			}else{
				$this->set_id($result[0]->id);
				return(true);
			}
			
		}

		public function login(){
		
			$user_id = $this->get_id();
			$account_id = $this->get_wizard_account()->id;

			// $crypt_user_hex=master::encrypt_data2("".$user_id."");
			// $crypt_account_hex=master::encrypt_data2("".$account_id."");

			$crypt_user_hex=("".$user_id."");
			$crypt_account_hex=("".$account_id."");

			
			setcookie("user", $crypt_user_hex, time()+60*60*24*30*12, "/");
			setcookie("account", $crypt_account_hex, time()+60*60*24*30*12, "/");
		
		}


		public function get_wizard_account(){
 
			require_once('wizard_account.php');

			$wizard_account = new wizard_account($this->get('__wacc'));

			return $wizard_account;

		}

		public function logout(){
		
			setcookie("user", NULL, time()-1, "/");
			setcookie("account", NULL, time()-1, "/");
		
		}


		public function is_admin(){
			return $this->get('type')=='admin';
		}


		public function tutorial(){

			$tutorial = $this->get('tutorial');

			if(!$tutorial){
				$this->update_property('tutorial', true);
				return true;
			}

			if($tutorial) return false;

		}



		public function get_my_modules(){

			$account_modules = wizard::get_account()->get_modules();
			$user_modules  = Array();

			foreach ($account_modules as $module) {
				if($this->is_admin() || $this->has_access_to_module($module)) array_push($user_modules, $module);
			}

			return $user_modules;

		}



		public function generate_quick_menu(){

			$quick_menu = Array();

			$account_modules = wizard::get_account()->get_modules();

			foreach ($account_modules as $wizard_module) {
				array_push($quick_menu, $this->set_quick_menu($wizard_module, true));
			}

			
			return $quick_menu;


		}





	}

	class wizard_user_module extends db_object{

		public $properties = array('wizard_user:wizard_user*', 'wizard_module:wizard_module*', 'access:boolean');
		
		var $table_name = "wizard_user_modules";
		var $class_name = "wizard_user_module";

	}

		?>