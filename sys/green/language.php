<?php

include_once('db_object.php');
	
class language extends db_object{
	
	private $_name;
	
	public $properties = array('name', 'code');
	public $ni_properties = array();
	
	var $table_name = "languages";
	var $class_name = "language";
	
	public function get_name(){
		if ($this->name === NULL){
			if($this->get_id()>0) {
				$this->name = $this->load_db_field('name');
			}
		}
		return $this->name;
	}

	public function set_name($val){
		$this->name = $val;
	}
	
	
	
	public function load_base_texts(){
	
		$base_texts = Array();
		
		$xml_file = "../__common/f/lang/base.xml";
		
		$xml = simpleXML_load_file($xml_file);
		
		foreach($xml as $k => $v){
			array_push($base_texts, $v);
		}
		
		return($base_texts);
		
	}
	
	
	public function update_status($s){
		master::update_property($this, 'disabled', $s);
	}
		
}

?>