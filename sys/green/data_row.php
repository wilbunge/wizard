<?php

include_once('db_object.php');

class data_row extends db_object{

	public static $s_properties = array(
		'nombre','customer','contrato','producto','cedula','codetapa','tipo_de_juicio','comentarios'
		
	);
	
	public $properties = array(
		'nombre', 'customer','contrato','producto','cedula','codetapa','tipo_de_juicio','comentarios', 'fecha_castigo', 'secured', 'moneda', 'saldo_capital', 'fecha_recibido', 'fecha_juicio_etapa', 'fecha_embargo',
		'juzgado', 'autos', 'ficha', 'asignado', 'ultimo_informe_de'
	);

	public static $properties_sorted = array(
		'nombre', 'cedula', 'customer', 'fecha_castigo', 'contrato', 'producto', 'secured',
		'moneda', 'saldo_capital', 'tipo_de_juicio', 'fecha_recibido', 'codetapa', 'fecha_juicio_etapa', 'fecha_embargo', 'juzgado', 'autos', 'ficha',
'comentarios', 'asignado', 'ultimo_informe_de'

	);

	public $ni_properties = array('type');
	public $ndb_properties = array('file');
	public static $key_names = Array('CUSTOMER','CONTRATO','PRODUCTO');

	public $pairs = Array();
	
	public $names, $values;
	
	var $table_name = "data_rows";
	var $class_name = "data_row";
	
	var $pairs_loaded = false;
	
	
	//public function catch_data_after


	public static function get_all_properties_sorted(){

		$o = new data_row;
		$am = array_merge(self::$properties_sorted, $o->properties);
		$au = array_unique($am);

		return $au;

	}
	
	
	public function fill_properties($data){

		$modifications=false;

		foreach($this->properties as $p){

			$cur_value = $this->get($p);

			$this->set($p, $data[$p]);

			if ($data[$p] != $cur_value && $p!='date'){
				$modifications=true;

				//echo $p.') '.$modifications.'..'.($data[$p] .' != '. $cur_value.'<br>');
			}
		}

		return $modifications;
			
	
	}
	
	
	static function load_unique_names(){
		//data_manager::$debug_mode=true;
		
		$order = "`name` LIKE 'NRO_REF' DESC";
		$order .= ", `name` LIKE 'Nombre1' DESC";
		$order .= ", `name` LIKE 'Nombre' DESC";
		$order .= ", `name` LIKE 'Cedula' DESC";
		$order .= ", `name` LIKE 'Customer' DESC";
		$order .= ", `name` LIKE 'Fecha_Cast' DESC";
		$order .= ", `name` LIKE 'CONTRATO' DESC";
		$order .= ", `name` LIKE 'PRODUCTO' DESC";
		$order .= ", `name` LIKE 'SEC/UNSEC' DESC";
		$order .= ", `name` LIKE 'MONEDA' DESC";
		$order .= ", `name` LIKE 'SDO_CAP' DESC";
		$order .= ", `name` LIKE 'TIPO_DE_JUICIO' DESC";
		$order .= ", `name` LIKE 'FECHA_RECIBIDO' DESC";
		$order .= ", `name` LIKE 'CODETAPA' DESC";
		$order .= ", `name` LIKE 'FECHA_INICIO_ETAPA' DESC";
		$order .= ", `name` LIKE 'EMBARGO' DESC";
		
		
		
		$result = master::get_raw_data('data_row_pairs', 'DISTINCT(`name`) AS `name`', 'NOT `deleted` ORDER BY '.$order);
		
		$names = Array();
		
		while($row = mysql_fetch_array($result)){
			array_push($names, $row['name']);
		}
		
		return($names);
		
	}
	
	
	public function get_date(){
		return(time());
	}
	
	public function get_pair($name){
	
		$name = strtolower($name);
	
		if($this->pairs[$name] != null){
			return($this->pairs[$name]);
		}else{
			return(new data_row_pair);
		}
	
		
	}
	
	public function has_pair($name){
		return($this->get_pair($name)->id > 0);
	}
	
	public function load_pairs(){
		$pairs = master::get('data_row_pair', 'data_row='.$this->id);
		
		foreach($pairs as $pair){
			$this->pairs[strtolower($pair->get('name'))] = $pair;
		}
		
		$pairs_loaded = true;
		
		
		return($pairs);
	}
	
	public function load_pair($name){
		$objects = master::get('data_row_pair', 'data_row='.$this->id.' AND `name` = "'.$name.'"');
		if(!$objects) return(false);
		return($objects[0]);
	}
	
	
	public function catch_data_after(){
		
		echo "catch_data_after()!";
		
		$data = $_FILES['data'];
		
		print_r($data);
		
	}
	
	
	public function add_pair($pair){
		$pair->set('data_row', $this);
		//array_push($this->pairs, $pair);
		$this->pairs[$pair->get('name')] = $pair;
		
	}
	
	
	public function store_after(){
		//data_manager::$debug_mode = true;
		foreach($this->pairs as $pair){
			master::store($pair);
		}
	
	}
	
	
	public function update_pair_value($name, $value){
	
		if(!$pair = $this->load_pair($name)) return;
		
		$pair->set('value', $value);
		master::update($pair);
	}
	
	
	static function get_pair_color($name){
	
		$name = strtoupper($name);
		
		switch($name){
			case 'PRODUCTO' :
			case 'CONTRATO' :
			case 'CUSTOMER' :
				$color = "RED";
				break;
			case 'TIPO_DE_JUICIO' :
			case 'FECHA_RECIBIDO' :
			case 'CODETAPA' :
			case 'COMENTARIOS' :
			case 'CEDULA' :
				$color = "GREEN";
				break;

			case 'SEC_UNSEC' :
			case 'MONEDA' :
			case 'SALDO' :
//			case 'COMENTARIOS' :
			case 'ASIGNADO' :
			
			case 'ULTIMO_INFORME_DE' :
			case 'ASIGNADO' :
			case 'FICHA' :
			case 'AUTOS' :
			case 'JUZGADO' :
			case 'FECHA_EMBARGO' :
			case 'FECHA_CASTIGO' :
			case 'SECURED' :
			case 'SALDO_CAPITAL' :
			case 'FECHA_RECIBIDO' :
			case 'FECHA_JUICIO_ETAPA' :
			case 'FECHA_EMBARGO' :
			
			case 'FECHACAST' :
				$color = "BLACK"; // HIDE
				break;
			default:
				$color = "YELLOW";
				break;
		}
		
		return($color);
		
	}


	public function get_key_str(){

		$key = $this->get_key();

		$key_str = implode('-', $key);

		return $key_str;

	}

	public function set_key($data){

		$this->set('customer', $data['customer']);
		$this->set('producto', $data['producto']);
		$this->set('contrato', $data['contrato']);
		
	}
	
	
	public function get_key(){

		$a = $this->get('customer');
		$b = $this->get('producto');
		$c = $this->get('contrato');
		
		$key = Array();
		
		$key['CUSTOMER'] = $a;
		$key['PRODUCTO'] = $b;
		$key['CONTRATO'] = $c;
		
		return($key);
		
	}
	
	
	public function check_key($names){
	
		
		
		foreach(self::$key_names as $x){
			if(!in_array(strtoupper($x), $names)) return(false);
		}
		
		return(true);
		
	}
	
	
	
	
	
	public function exists(){
	
		$key = $this->get_key();
	
		$a = $key['CUSTOMER'];
		$b = $key['CONTRATO'];
		$c = $key['PRODUCTO'];
	
		if($a == '' or $b == '' or $c == '') return(false);
		
		
		$result = master::get_raw_data('data_rows', 'id', "`customer` = '$a' AND `contrato` = '$b' AND `producto` = '$c' AND NOT `deleted`");
		
		if(mysql_num_rows($result) < 1) return(false); // No. Key does not exists
		
		$sql_row = mysql_fetch_array($result);
		
		$this->set_id($sql_row['id']);
		
		return(true);
		
		
		// ---------------------------------------------------------------------------------
		// ---------------------------------------------------------------------------------
		
		return;
		
		//echo "$a - $b - $c <br>";
		
		// Is there a registry with this CUSTOMER? If not so, key does not exist
		$result_1 = master::get_raw_data('data_row_pairs', 'data_row', "`name` = 'CUSTOMER' AND `value` = '$a' AND NOT `deleted` GROUP BY `data_row`");
		
		if(mysql_num_rows($result_1) < 1) return(false); // No. Key does not exists
		
		while($sql_row = mysql_fetch_array($result_1)){
			$array_1[] = $sql_row['data_row']; // All data_rows with given CUSTOMER
		}
		
		
		//echo "1#";
		
		//print_r($array_1);
		
		// Is there a registry with this CONTRATO? If not so, key does not exist
		$result_2 = master::get_raw_data('data_row_pairs', 'data_row', "`name` = 'CONTRATO' AND `value` = '$b' AND NOT `deleted` GROUP BY `data_row`");
		
		if(mysql_num_rows($result_2) < 1) return(false); // No. Key does not exists
		
		while($sql_row = mysql_fetch_array($result_2)){
			$array_2[] = $sql_row['data_row'];  // All data_rows with given PRODUCTO
		}
		
		
		//echo "2#";
		
		// Is there a registry with this PRODUCTO? If not so, key does not exist
		$result_3 = master::get_raw_data('data_row_pairs', 'data_row', "`name` = 'PRODUCTO' AND `value` = '$c' AND NOT `deleted` GROUP BY `data_row`");
		
		if(mysql_num_rows($result_3) < 1) return(false); // No. Key does not exists
		
		while($sql_row = mysql_fetch_array($result_3)){
			$array_3[] = $sql_row['data_row']; // All data_rows with given CONTRATO
		}
		
		//echo "3#";
		
		$i = array_intersect($array_1, $array_2, $array_3);
		$k = Array();
		
		
		if(count($i)<1) return(false);
		
		foreach($i as $j){
			array_push($k, $j);
		}
		echo $k[0]."=====";
		$this->set_id($k[0]);
		
		return(true);
	
	}
	
	
	public function update(){
		
		foreach($this->pairs as $pair){
		
			if(data_row::get_pair_color($pair) == 'YELLOW'){ // UPDATE
				$this->update_pair_value($pair->get('name'), $pair->get('value'));
			}
			
		}
		
		
	}
	
	public static function is_editable_by_name($name){
		return(data_row::get_pair_color($name) == 'GREEN' || data_row::get_pair_color($name) == 'YELLOW');
	}
	
	public static function is_editable($name){
		return(data_row::get_pair_color($name) == 'GREEN' || data_row::get_pair_color($name) == 'YELLOW');
	}
		
		
	public function embed_for_input($name, $value = ''){
		
		switch(strtoupper($name)){
			
				case 'TIPO_DE_JUICIO' : 
				
					$values = Array(
						1 => 'Prendario',
						2 => 'Hipotecarios',
						3 => 'Ejecutivo',
						4 => 'Entrega de la Cosa (Leasing)',
						5 => 'Entrega de la Cosa (Hipotecarios)',
						6 => 'Convenios BKB',
						7 => 'Extrajudicial'
						
					);
					
					$html = "
						<select name='$name'>
							<option value=''>...</option>";
							
					foreach($values as $k=>$v){
						$l=$k+0;
						$html .= "<option".($value==$l ? " selected" : "")." value='$l'>$v</option>";
					}
							
					$html .= "</select>";
					break;
				
				case 'CODETAPA' : 
				
					$codetapas = Array(
						1 => Array('Intimado','Demandado','Secuestro del vehículo','Estudio de Títulos','Decreto de remate','Rematado','Aprobación del remate','Escriturado judicialmente','Liquidación del crédito','Detenido','Acuerdo de Pago','Concurso (concordarto, quiebra)','Incobrables'),
						2 => Array('Intimado','Demandado','Estudio de Títulos','Decreto de remate','Rematado','Aprobación del remate','Escriturado judicialmente','Liquidación del crédito','Detenido','Acuerdo de Pago','Concurso (concordarto, quiebra)','Incobrables'),
						3 => Array('Intimado','Demandado','Mejora de embargo','Reconstrucción de títulos','Estudio de Títulos','Decreto de remate','Rematado','Aprobación del precio del remate','Escrituración judicial','Liquidación del crédito','Detenido','Acuerdo de Pago','Concurso (concordato, quiebra)','Incobrables'),
						4 => Array('Intimado','Demandado','Desapoderamiento','Rescisión del contrato','Detenido','Acuerdo de pago','Finalizado'),
						5 => Array('Intimado','Lanzamiento','Detenido','Finalizado'),
						7 => Array('Sin Acciones Judiciales','Acuerdo de Pago'),
					);
					
					$tipo_de_juicio = $this->get('tipo_de_juicio');

					$html = "
						<select name='$name'>
							<option value=''>...</option>";
					
					$values = $codetapas[$tipo_de_juicio];
					
					if($values==null) $values=Array();
							
					foreach($values as $k=>$v){
						$l=$k+1;
						$html .= "<option".($value==$l ? " selected" : "")." value='$l'>$v</option>";
					}
							
					$html .= "</select>";
					break;
				
				
				default :
					$html = "";
					break;
			
			}
			
			return $html;
		
		}


		public function check_modifications($data){

		}

		static function translate_field_bi($field){

			$field_upp = strtoupper($field);

			$headers = Array(
				"FECHA_CAST" => "FECHA_CASTIGO",
				"EMBARGO" => "FECHA_EMBARGO",
				"DATE" => "FECHA",
				"SEC/UNSEC" => "SECURED",
				"SDO_CAP" => "SALDO_CAPITAL",
				"FECHA_INICIO_ETAPA" => "FECHA_JUICIO_ETAPA"
			);

			foreach ($headers as $key => $value) {
				
				//if($key == $field_upp) return str_replace('_', ' ', strtolower($value));
				if($key == $field_upp) return $value;
				if($value == $field_upp) return $key;

			}

			return $field_upp;

		}


		static function translate_field($name){

			$name = str_replace(' ', '_', strtoupper($name));

			switch($name){

				case 'FECHA_CAST' : $name = 'FECHA_CASTIGO';
					break;
				case 'EMBARGO' : $name = 'FECHA_EMBARGO';
					break;
				case 'SEC/UNSEC' : $name = 'SECURED';
					break;
				case 'SDO_CAP' : $name = 'SALDO_CAPITAL';
					break;
				case 'FECHA_INICIO_ETAPA' : $name = 'FECHA_JUICIO_ETAPA';
					break;
				default:
					break;

			}

			return $name;

		}
		
		
	
}	?>