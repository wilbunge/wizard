<?php
include_once('db_object.php');

class provider extends db_object{
	
	public $properties = array('date:date','total:int','currency','description','category:expense_category','credit_card:boolean');
	
	var $table_name = "expenses";
	var $class_name = "expense";

	
	public function save(){

		// $this->set('date', date('Y-m-d'));

		parent::save($this);

	}
	
	
	public function get_total_formatted(){

		return self::format_total($this->get('total'));

	}

	public function format_total($total){

		$total = number_format($total, 2, ',', '.');

		$total = "U\$S $total";

		return $total;


	}
	
	
}


include_once('expense.php');
class provider_expense extends expense{
	
	public $properties = array('expense:expense*', 'provider:provider*');
	
	var $table_name = "provider_expenses";
	var $class_name = "provider_expense";

	
	public function save(){

		// $this->set('date', date('Y-m-d'));

		parent::save($this);

	}
	
	
	public function get_total_formatted(){

		return self::format_total($this->get('total'));

	}

	public function format_total($total){

		$total = number_format($total, 2, ',', '.');

		$total = "U\$S $total";

		return $total;


	}
	
	
}
	
?>