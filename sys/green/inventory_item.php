<?php
include_once('db_object.php');
	
class inventory_item extends db_object{
	
	public $properties = array('name','unit','cost','provider', 'type', 'product:product*');

	var $table_name = "inventory_items";
	var $class_name = "inventory_item";


	public static function check_products(){

		$products = master::get('product');

		foreach($products as $product){
			if(!$product->has_inventory_item()) $product->generate_inventory_item();
		}


	}


	public function count(){

		$inventory_movements = master::get('inventory_movement', '`inventory_item` = '.$this->id);

		if(!$inventory_movements) return 0;

		$count = 0;

		foreach ($inventory_movements as $inventory_movement) {
			$count += $inventory_movement->get('amount');
		}

		return $count;

	}

	public function move_in($amount, $ref = null){

		$inventory_movement = new inventory_movement;
		$inventory_movement->set('inventory_item', $inventory_movement);
		$inventory_movement->set('amount', $amount);

		$inventory_movement->set('ref_type', $ref);

	}

}	?>