<?php

include_once('db_object.php');

class wizard_account extends db_object{
	
	public $properties = array('name');
	
	public $settings=array();
	public $settings_it=array();
	public $settings_attr=array();	
	
	var $table_name = "wizard_accounts";
	var $class_name = "wizard_account";
	
	
	
	
	function get_crypt_id(){
	
		if($this->get_id()==0){
			exit("--ID must be greater than 0--");
		}
	
		$x = $this->get_id()+1000;
		
		$y = $x*$x + 5*($x+25) - $x*10 + 456;
		
		$y += 123;
		$y *= 987;
		
		$arr = Array();
		
		array_push($arr, "U");
		array_push($arr, "V");
		array_push($arr, "W");
		array_push($arr, "X");
		array_push($arr, "Y");
		array_push($arr, "Z");
		array_push($arr, "5");
		array_push($arr, "6");
		array_push($arr, "7");
		array_push($arr, "8");
		array_push($arr, "9");
		
		$crypt_id = $y;
		
		$crypt_id = str_replace("9", "A", $crypt_id);
		$crypt_id = str_replace("8", "B", $crypt_id);
		$crypt_id = str_replace("7", "C", $crypt_id);
		$crypt_id = str_replace("6", "D", $crypt_id);
		$crypt_id = str_replace("5", "E", $crypt_id);
	
		for($i=strlen($y); $i<=12; $i++){
		//	echo($arr[$i]."<br>");
			//$crypt_id = $arr[rand(count($arr),0)] . $crypt_id;
		}
		
		
		return($crypt_id);
	
	
	}
	

	
	public function create_dir(){
		$folder = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/";
		$folder2 = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/modules/";
		
		if(!is_dir($folder)) mkdir($folder);
		if(!is_dir($folder2)) mkdir($folder2);
	}
	
	
	public function save_settings(){
	
		$file = $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/" . $this->get_crypt_id() . "/settings.xml";
		$file2=$_SERVER['DOCUMENT_ROOT'].G_PC_BASE_PATH."modules/system-settings/base.xml";
		
		$obj = simplexml_load_file($file2);
		
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		
		$settings = $doc->createElement("settings");

		foreach($obj as $prop => $value){
			
			$post_val = $_POST[$prop];
			$p = $doc->createElement($prop);
			
			if(is_array($post_val)){
			
				$v = implode(",", $post_val);
				$n = $doc->createTextNode($v);
			
			}else{
				$n = $doc->createTextNode($post_val);
			}
			
			$p->appendChild($n);
			
			foreach($obj->$prop->attributes() as $n => $v){
				$p->setAttribute($n, $v);
			}

			$settings->appendChild($p); 
		}

		$doc->appendChild($settings); 		
		
		$doc->saveXML();
		$doc->save($file);
		
	}
	

	
	
	public function check_dir($p_ref_path){
		
		$path= $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/".$this->get_crypt_id() . "/$p_ref_path";
		if(!is_dir($path)) mkdir($path);
		
	}
	
	
	public function calculate_files_size(){
	
		$path= $_SERVER["DOCUMENT_ROOT"].G_PC_BASE_PATH."client/".$this->get_crypt_id() . "/$p_ref_path";		
		
		$total_size=$this->calculate_files_size_browse_dirs($path);		
		return($total_size);
		
		
		
	}

	public function get_modules(){

		wizard::_include('wizard_account');

		$sql_result = wizard::get_raw_data('wizard_account_modules', 'wizard_module');

		$public_modules = wizard::get('wizard_module', 'public AND `keyword` <> "finder"', 'ORDER BY name');
		$private_modules = array();

		
		while($sql_row = mysqli_fetch_array($sql_result)){

			$wizard_module = new wizard_module($sql_row['wizard_module']);
			$private_modules[] = $wizard_module;

		}

		$modules = array_merge($private_modules, $public_modules);

		return $modules;


	}
	
	
}	
?>